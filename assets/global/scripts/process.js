/*
 *  Admin CSR
*/
function csr_process(stat, ele, e){
    e.preventDefault();
    mes = null;
    swal({
        title: "Are you sure?",
        text: mes,
        html: true,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes',
        closeOnConfirm: false
    },
    function(){
        var sus = "Successfully Process data!";
        var err = "Error Process data!";
        var href = $(ele).attr('href');
        $.post(href, function(data, textStatus, xhr) {
            if(data.status == 1){
                gridT.getDataTable().ajax.reload();
                swal("Success", sus, "success");
            }else{
                swal("Error", err, "error");
            }
        }, 'json');
    });
};