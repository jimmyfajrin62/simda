var FormValidation = function() {

    return {
        handleValidation : function( form_target, rule, message, title, text ) {
            $('.tipe_program').select2();
            // for more info visit the official plugin documentation: 
            // http://docs.jquery.com/Plugins/Validation
            var form_input   = $(form_target);
            var form_action  = form_input.attr('action');
            var form_confirm = form_input.attr('data-confirm');
            var error        = $('.alert-danger', form_input);
            var warning      = $('.alert-warning', form_input);
            var data         = '';

            form_input.validate({
                errorElement: 'span', //default input error message container
                errorClass: 'help-block help-block-error', // default input error message class
                focusInvalid: false, // do not focus the last invalid input
                ignore: "", // validate all fields including form hidden input
                messages: message,
                rules: rule,

                invalidHandler: function(event, validator) { //display error alert on form submit              
                    error.show();
                    App.scrollTo(error, -200);
                },

                errorPlacement: function(error, element) {
                    if (element.is(':checkbox')) {
                        error.insertAfter(element.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline"));
                    } else if (element.is(':radio')) {
                        error.insertAfter(element.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline"));
                    } else {
                        error.insertAfter(element); // for other inputs, just perform default behavior
                    }
                },

                // errorPlacement: function (error, element) { // render error placement for each input type
                //     if (element.parent(".input-group").size() > 0) {
                //         error.insertAfter(element.parent(".input-group"));
                //     } else if (element.attr("data-error-container")) { 
                //         error.appendTo(element.attr("data-error-container"));
                //     } else if (element.parents('.radio-list').size() > 0) { 
                //         error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                //     } else if (element.parents('.radio-inline').size() > 0) { 
                //         error.appendTo(element.parents('.radio-inline').attr("data-error-container"));
                //     } else if (element.parents('.checkbox-list').size() > 0) {
                //         error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                //     } else if (element.parents('.checkbox-inline').size() > 0) { 
                //         error.appendTo(element.parents('.checkbox-inline').attr("data-error-container"));
                //     } else {
                //         error.insertAfter(element); // for other inputs, just perform default behavior
                //     }
                // },

                highlight: function(element) { // hightlight error inputs
                    $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
                },

                unhighlight: function(element) { // revert the change done by hightlight
                    $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
                },

                success: function(label) {
                    label.closest('.form-group').removeClass('has-error'); // set success class to the control group
                },

                submitHandler: function (form) {
                    if(form_confirm == 1){
                        swal({
                            title: title,
                            text: text,
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes',
                            closeOnConfirm: true,
                        }).then(function(isConfirm){
                            if(isConfirm){
                                App.blockUI({
                                    target: form_target,
                                    animate: true
                                });

                                var options = { 
                                    dataType:      'json',
                                    success:       callback_form,
                                    error:         callback_error
                                }; 

                                $(form).ajaxSubmit(options);
                            }
                        });
                    }else{
                        App.blockUI({
                            target: form_target,
                            animate: true
                        });

                        var options = { 
                            dataType:      'json',
                            success:       callback_form,
                            error:         callback_error
                        }; 

                        $(form).ajaxSubmit(options);

                        // untuk nutup modal content ketika submit
                        $('#showProses').modal('hide');
                        $('body').removeClass('modal-open');
                        $('.modal-backdrop').remove();
                    }
                }
            });

            function callback_form(res, statusText, xhr, $form){
                if(res.status == 1){
                    warning.hide();

                    toastr.options = call_toastr('4000');
                    var $toast = toastr['success'](res.message, "Success");

                    if($('.reload').length)
                    {
                        $('.reload').trigger('click');
                    }

                }else if(res.status == 0 || res.status == 2){
                    warning.hide();

                    toastr.options = call_toastr('4000');
                    var $toast = toastr['error'](res.message, "Error");
                }else{
                    warning.find('span').html(res.message);
                    warning.show();
                    App.scrollTo(warning, -200);
                }

                App.unblockUI(form_target);
            }

            function callback_error(){
                toastr.options = call_toastr('4000');
                var $toast = toastr['error']('Something wrong!', "Error");

                App.unblockUI(form_target);
            }

        }
    }

}();