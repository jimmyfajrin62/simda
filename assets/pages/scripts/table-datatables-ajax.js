var TableDatatablesAjax = function () {

    return {

        handleRecords: function ( url, header, order, sort, ele) {

            var grid = new Datatable();

            grid.init({
                src: (ele != null ? $(ele): $("#datatable_ajax") ),
                onSuccess: function (grid, response) {
                    // grid:        grid object
                    // response:    json object of server side ajax response
                    // execute some code after table records loaded
                },
                onError: function (grid) {
                    // execute some code on network or other general error
                },
                onDataLoad: function(grid) {
                    // execute some code on ajax data load
                    $('.tooltips').tooltip();
                },
                loadingMessage: 'Loading...',
                dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options 

                    // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                    // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js). 
                    // So when dropdowns used the scrollable div should be removed. 
                    //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",
                    
                    "aoColumns": header,

                    "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

                    "lengthMenu": [
                        [10, 20, 50, 100, 150, -1],
                        [10, 20, 50, 100, 150, "All"] // change per page values here
                    ],
                    "pageLength": 10, // default record count per page
                    "ajax": {
                        "url": url, // ajax source
                    },
                    "aoColumnDefs": [
                      { "bSortable": false, "aTargets": sort }
                    ],
                    "order": order // set first column as a default sort by asc
                }
            });

            grid.getTableWrapper().on('keyup', '.form-filter', function (e) {
                if(e.keyCode == 13){
                    $('.filter-submit').trigger('click');
                }
            });

            grid.getTableWrapper().on('change', '.select-filter', function (e) {
                $('.filter-submit').trigger('click');
            });

            // handle group actionsubmit button click
            grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
                e.preventDefault();
                var action = $(".table-group-action-input", grid.getTableWrapper());
                if (action.selectpicker('val') != "" && grid.getSelectedRowsCount() > 0) {
                    grid.setAjaxParam("customActionType", "group_action");
                    grid.setAjaxParam("customActionName", action.selectpicker('val'));
                    grid.setAjaxParam("id", grid.getSelectedRows());
                    grid.getDataTable().ajax.reload();
                    grid.clearAjaxParams();
                } else if (action.selectpicker('val') == "") {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                } else if (grid.getSelectedRowsCount() === 0) {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            });

           $('.kangmus').on('click', function(e){
                e.preventDefault();

                var c_id = grid.getSelectedRowsCount(),
                    ids  = grid.getSelectedRows();

                $.ajax({
                    url : base_url + "user/print_pdf",
                    data: {count: c_id, ids: ids},
                    type: 'post',
                    success: function(data) {
                        console.log(data);
                    }
                });
            });
            
            gridT = grid;
            // grid.setAjaxParam("customActionType", "group_action");
            // grid.getDataTable().ajax.reload();
            // grid.clearAjaxParams();
        }
    }
}();