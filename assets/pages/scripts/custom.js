/**
Custom module for you to write your own javascript functions
**/
var Custom = function () {

    // private functions & variables

    var myFunc = function( ele, url ) {
        
    }

    // public functions
    return {

        //main function
        init: function () {
            //initialize here something.            
        },

        //some helper function
        doSomeStuff: function () {
            myFunc();
        }

    };

}();

var Helper = function () {

    return {

        select2 : function( ele, url, tag, placeholder, clear ) {
            var Dtag = (typeof tag == "undefined" || tag == false  ? false : true); 
            var Dpcd = (typeof placeholder == "undefined" ? '' : placeholder); 
            var Dclr = (typeof clear == "undefined" ? false : true); 
            $( ele ).select2({
                allowClear: Dclr,
                placeholder: Dpcd,
                ajax: {
                    url: base_url + url,
                    dataType: 'json',
                    data: function (params) {
                      return {
                        q: params.term, // search term
                        page: params.page
                      };
                    },
                    processResults: function (data, params) {
                        return {
                            results: data.item
                        }
                    },
                    cache: true
                },
                tags: Dtag,
                tokeSparator: [','],
                escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                templateResult: formatResult, // omitted for brevity, see the source of this page
                templateSelection: formatResult // omitted for brevity, see the source of this page
            });
        },

        daterange : function ( ele ) {
            $(ele).daterangepicker({
                opens: (App.isRTL() ? 'left' : 'right'),
                format: 'MM/DD/YYYY',
                separator: ' to ',
                startDate: moment().subtract('days', 29),
                endDate: moment(),
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                    'Last 7 Days': [moment().subtract('days', 6), moment()],
                    'Last 30 Days': [moment().subtract('days', 29), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
                },
                minDate: '01/01/2012',
                maxDate: '12/31/2018',
            },
            function (start, end) {
                $(ele + ' input').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            });  
        },
        ckeditor : function( ele ) {
            var editor = CKEDITOR.replace( ele, {
                allowedContent : true,
                extraAllowedContent: 'table[class]'
            });
            editor.on( 'change', function( evt ) {
                // getData() returns CKEditor's HTML content.
                var dumy = evt.editor.getData();
                $('#'+ele+'_value').val(dumy);
            });
        },
        bsSelect : function() {
            $('.bs-select').selectpicker({
                iconBase: 'fa',
                tickIcon: 'fa-check'
            });
        }

    };

    function formatResult(result){
        return result.text;
    }

}();

var Datepicker = function () {
    return {
        picker : function( clas, attr ) {
            $(clas).datepicker(attr);
        }
    }
}();

var Daterangepicker = function () {
    return {
        rangepicker : function( clas, attrrange) {
            $(clas).daterangepicker(attrrange);

            if(typeof attrrange != 'undefined'){
                if(attrrange.autoUpdateInput == false){
                    $(clas).on('apply.daterangepicker', function(ev, picker) {
                        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                    });

                    $(clas).on('cancel.daterangepicker', function(ev, picker) {
                        $(this).val('');
                    });
                }
            }
        }
    }
}();

jQuery(document).ready(function() {  
    $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }
    });
});

/***
Usage
***/
//Custom.doSomeStuff();

// Custom Function

