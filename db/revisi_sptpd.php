<?php

/*========================
// query get tarif persentase
==========================*/
public function ref_rek_6($id)
{
    $query = $this->db->query("SELECT * from ref_rek_6 where id_rek_6 = $id");
    return $query->row();
}

/*========================
// perhitungan di controller
// tambahkan fungsi pembulatan jika belum ada
==========================*/
$rek_6      = $this->mdb->ref_rek_6($this->input->post('id_rek_6'));
$hutang     = $this->input->post('dasar_pengenaan') * $rek_6->tarif / 100;
$total      = $this->pembulatan($hutang, $this->input->post('pembulatan'));


/*========================
// if tombol jika sudah terbit bendahara_penerimaan
==========================*/
$rows->status_sspd == 1 ?
'<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
'<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
'<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'
:

/*========================
// input pembulatan
==========================*/
<div class="col-md-4 form-group">
    <label class="control-label"><strong>Pembulatan</strong>
        <span class="required">*</span>
    </label>
    <!-- <input style="text-transform:uppercase" type="text" class="form-control" name="pembulatan" value="<?=@$data->pembulatan ? @$data->pembulatan : 0?>" required> -->
    <select class="form-control" name="pembulatan" style="text-transform:uppercase">
        <option value="1000" <?=@$data->pembulatan == 1000 ? 'selected' : ''?>>Ribuan</option>
        <option value="100" <?=@$data->pembulatan == 100 ? 'selected' : ''?>>Ratusan</option>
        <option value="0" <?=@$data->pembulatan == 0 ? 'selected' : ''?>>Tanpa Pembulatan</option>
    </select>
    <span class="help-block"></span>
</div>

/*========================
// hilangkan tombol tambah di view show_sptpd_pajak
==========================*/
// di controller show_sptpd_pajak
$data['status_sspd'] = $this->db->select('status_sspd')->where('id', $id)->get('ta_kartu_pajak_pungut')->row();
// di view
<?php if ($status_sspd->status_sspd != 1): ?>
    <a data-original-title="Add" href="<?php echo $url ?>/show_add_sptpd_pajak/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
        <i class="fa fa-plus"></i>
    </a>
    <span class='help-block' style='display: inline;'></span>
<?php endif; ?>
