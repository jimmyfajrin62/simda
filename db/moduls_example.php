<?php

/*========================
// CONTROLLER SELECT()
==========================*/
// deklar table
$this->table_db = 'ref_bank';

// jika ada where
$where_e    = 'kd_bank = 1';

// searching
if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
    $request = $_REQUEST['filterstatus'];
    $where_e = " status = '$request' and kd_bank = 1 ";
}
else {
    $where_e = " status = '1' and kd_bank = 1 ";
}

// bottom active and delete
'<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/nama_table/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
'<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/nama_table/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',



/*==============================================================================
// CONTROLLER fungsi action table
==========================*/
// global actions
public function change_status($status, $id)
{
    $status = explode("/", $status);
    $value  = $status[0];
    $field  = $status[1];
    $table  = $status[2];
    $id     = $id['id IN '];

    $result = $this->db->query("SELECT status from $table where $field in $id")->row();

    if ($result->status == '99' and $value == '99') {
        $query = $this->db->query("DELETE from $table where $field in $id");
    } else {
        $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
    }
}

// global actions
public function change_status_by($id, $table, $status, $stat = false)
{
    if ($stat) {
        $result = $this->m_global->delete($table, ['id' => $id]);
    } else {
        $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
    }

    if ($result) {
        $data['status'] = 1;
    } else {
        $data['status'] = 0;
    }

    echo json_encode($data);
}


/*==============================================================================
// VIEW
==========================*/
// tombol select in view
<div class="table-actions-wrapper">
    <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
        <option>Select...</option>
        <option value="99/id/nama_table">Delete</option>
        <option value="0/id/nama_table">InActive</option>
        <option value="1/id/nama_table">Active</option>
    </select>
    <button class="btn btn-sm btn-icon-only blue table-group-action-submit" onClick="">
        <i class="fa fa-check"></i>
    </button>
</div>

// tombol search active inactive in view
<td class="text-center">
    <div class="clearfix">
        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
            <i class="fa fa-search"></i>
        </button>
        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
            <i class="fa fa-times"></i>
        </button>
        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
    </div>
</td> 
