<?php
(defined('BASEPATH')) or exit('No direct script access allowed');

class Admin_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->cek_login();
    }

    public function cek_login()
    {
        if ($this->session->user_data->user_role != 1 && $this->session->user_data->user_role != 3 ) {
            redirect( base_url().'pendaftaran/pendaftaran_pajak' );
        }
    }
}
