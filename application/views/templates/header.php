<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner ">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="">
                <!-- <img src="<?=base_url()?>/assets/layouts/layout2/img/logo-default.png" alt="logo" class="logo-default" /> -->
                <h2 style="color: white;">SIMDA</h2>
            </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN PAGE TOP -->
        <div class="page-top">
            <!-- END HEADER SEARCH BOX -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->

                    <?php if ($this->session->user_data->user_role != 2) { ?>
                        
                    
                    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <i class="icon-bell"></i>
                            <span class="badge badge-default"> <?=$sum?> </span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="external">
                                <h3>
                                    <span class="bold"><?=$sum?> Execution</span> notifications</h3>
                            </li>
                            <li>
                                <ul class="dropdown-menu-list scroller" style="height: 270px;" data-handle-color="#637283">
                                    <li>
                                        <a href="<?=$url_masa?>">
                                            <span class="time">Open</span>
                                            <span class="details">
                                                    <span class="badge badge-default"> <?=$masa->jumlah?> </span>
                                                </span> SSPD Masa
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=$url_np?>">
                                            <span class="time">Open</span>
                                            <span class="details">
                                                    <span class="badge badge-default"> <?=$np->jumlah ?> </span>
                                                </span> Nota Perhitungan
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=$url_lb?>">
                                            <span class="time">Open</span>
                                            <span class="details">
                                                    <span class="badge badge-default"> <?=$lb->jumlah ?> </span>
                                                </span> SKPD/LB
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=$url_kbt?>">
                                            <span class="time">Open</span>
                                            <span class="details">
                                                    <span class="badge badge-default"> <?=$kbt->jumlah ?> </span>
                                                </span> SKPD/KBT
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?=$url_ijin?>">
                                            <span class="time">Open</span>
                                            <span class="details">
                                                    <span class="badge badge-default"> 
                                                    <?=$ijin->jumlah;?> 
                                                    </span>
                                                </span> Approve User
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>

                    <?php }else { ?>
                    <span></span>
                    <?php } ?>

                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?=base_url()?>/assets/layouts/layout2/img/avatar3_small.jpg" />
                            <span class="username username-hide-on-mobile"> <?=$_SESSION['user_data']->user_name?> </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?=base_url()."login/out"?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END PAGE TOP -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
