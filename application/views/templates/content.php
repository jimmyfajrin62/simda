<?=@$_css?>
<?=@$_js?>

<?php
    if($_SESSION['user_data']->user_role == "1") $home = "parameter/data_umum_pemda";
    else if($_SESSION['user_data']->user_role == "2") $home = "pendaftaran/pendaftaran_pajak";
    else $home = "";
?>

<!-- BEGIN PAGE TITLE-->
<h3 class="page-title"><?=$pagetitle?>
    <small><?=@$subtitle?></small>
</h3>
<!-- END PAGE TITLE-->
<div class="page-bar note note-info">
    <?php if ($breadcrumb): ?>
        <ul class="page-breadcrumb">
            <li>
                <i class="icon-home"></i>
                <a class="ajaxify" href="<?=base_url($home)?>">Home</a>
                <i class="fa fa-angle-right"></i>
            </li>
            <?php
                $i = 0;$len = count($breadcrumb);
                foreach ( $breadcrumb as $key => $value ): ?>
                    <li>
                        <?php if ($value): ?>
                            <a href="<?php echo base_url().$value ?>" class="ajaxify">
                                <?php echo $key ?>
                            </a>
                        <?php else: ?>
                            <span><?=$key?></span>
                        <?php endif; ?>
                        <?php echo $i != $len -1 ? '<i class="fa fa-angle-right"></i>' : ''?>
                    </li>
                    <?php $i++;?>
                <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>
<!-- END PAGE HEADER-->

<!-- CONTENT -->
<?php echo $_content ?>
<!-- END CONTENT -->

<script type="text/javascript">
    $(document).ready(function() {
        // reload init
        App.init();
        document.title = '<?php echo $pagetitle ?> | <?php echo $this->config->item( 'app_name' ) ?>';
    });
</script>
