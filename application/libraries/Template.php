<?php
class Template {

    protected $_ci;
    private $url_kbt  = 'penetapan/penetapan_pajak_skpd_kbt';
    private $url_lb   = 'penetapan/penetapan_pajak_skpd_lb';
    private $url_np   = 'penetapan/penetapan_pajak_np';
    private $url_masa = 'bendahara_penerimaan/penerimaan_pajak_sspd';
    private $url_ijin = 'pendataan/pendataan_approve_wp';

    function __construct()
    {
        $this->_ci = &get_instance();
    }

    function display( $template, $data = NULL, $js = NULL, $css = NULL )
    {
        $url = isset($data['url']) ? $data['url'] : null;

        $status_link    = @$this->_ci->input->post('status_link');
        // if ($this->_check_link($url) === 'false') {
        //     $css['css'] = ['error'];
        //     $template = 'error/error_404';
        // }

        $data['masa']     = $this->_ci->m_global->masa();

        $data['url_masa'] = base_url().$this->url_masa;
        
        $data['np']       = $this->_ci->m_global->np();
        $data['url_np']   = base_url().$this->url_np;
        
        $data['lb']       = $this->_ci->m_global->lb();
        $data['url_lb']   = base_url().$this->url_lb;
        
        $data['kbt']      = $this->_ci->m_global->kbt();
        $data['url_kbt']  = base_url().$this->url_kbt;
        
        $data['ijin']     = $this->_ci->m_global->ijin();
        $data['url_ijin'] = base_url().$this->url_ijin;
        
        $data['sum']      = @$data['masa']->jumlah+ @$data['np']->jumlah + @$data['lb']->jumlah+ @$data['kbt']->jumlah + @$data['ijin']->jumlah ;
        //echo $data['sum']; exit();


        if ( $status_link == 'ajax' )
        {
            $data['_content']       = $this->_ci->load->view($template, $data, TRUE);
            $data['_js']            = $this->_ci->load->view('templates/js', $js, TRUE);
            $data['_css']           = $this->_ci->load->view('templates/css', $css, TRUE);

            $this->_ci->load->view('templates/content', $data);

        }
        else
        {
            $data['_content']       = $this->_ci->load->view($template, $data, TRUE);
            $data['_js']            = $this->_ci->load->view('templates/js', $js, TRUE);
            $data['_css']           = $this->_ci->load->view('templates/css', $css, TRUE);
            $data['_fullcontent']   = $this->_ci->load->view('templates/content', $data, TRUE);
            $data['_header']        = $this->_ci->load->view('templates/header', $data, TRUE);
            $data['_sidebar']       = $this->_ci->load->view('templates/sidebar', $data, TRUE);
            $data['_footer']        = $this->_ci->load->view('templates/footer', $data, TRUE);

            $this->_ci->load->view('templates/template.php', $data);

        }
    }

    function _check_link($data_url) {
        $get_role   = $this->_ci->m_global->get('user', [['roles', 'role_id = user_role']], ['user_id' => login_data('user_id')], 'role_access')[0]->role_access;
        $get_menu   = $this->_ci->m_global->get('menus', null, ['menu_status' => '1'], 'menu_id, menu_link', '`menu_id` IN ('.$get_role.')');
        $link       = $this->_check_acc($get_menu)['link'];
        $url        = str_replace(base_url(), '', $data_url);

        if(in_array($url, $link)) {
            return 'true';
        } else {
            return 'false';
        }
    }

    function _check_acc($data) {
        $link = [];

        foreach ($data as $key => $val) {
            $link['id'][]   = $val->menu_id;
            $link['link'][] = $val->menu_link;
        }

        return $link;
    }

}

?>
