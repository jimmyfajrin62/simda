<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function tgl_id($date){

	$data = explode("-", $date);
	$bulan = $data[1];
	if ($bulan == 01) {
		$bln = "Januari";
	}elseif ($bulan == 02) {
		$bln = "Februari";
	}elseif ($bulan == 03) {
		$bln = "Maret";
	}elseif ($bulan == 04) {
		$bln = "April";
	}elseif ($bulan == 05) {
		$bln = "Mei";
	}elseif ($bulan == 06) {
		$bln = "Juni";
	}elseif ($bulan == 07) {
		$bln = "Juli";
	}elseif ($bulan == '08') {
		$bln = "Agustus";
	}elseif ($bulan == '09') {
		$bln = "September";
	}elseif ($bulan == 10) {
		$bln = "Oktober";
	}elseif ($bulan == 11) {
		$bln = "November";
	}elseif ($bulan == 12) {
		$bln = "Desember";
	}else{
		$bln = "Out Off Range";
	}
	return $data[2]." ".$bln." ".$data[0];
}
function tgl_id_laporan($date){

	$data = explode("-", $date);
	$bulan = $data[1];
	return $data[2]."/".$bulan."/".$data[0];
}