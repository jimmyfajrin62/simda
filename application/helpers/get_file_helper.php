<?php

function get_additional( $add, $tipe ){
	$arr = [
		'datatables' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/datatables/datatables.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				'<script src="'.base_url('assets/global/scripts/datatable.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/datatables/datatables.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js').'" type="text/javascript"></script>'
			]
		],
		'datepicker' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/clockface/css/clockface.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/moment.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/clockface/js/clockface.js').'" type="text/javascript"></script>'
			]
		],
		'select2' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css').'" />',
        		'<link href="'.base_url('assets/global/plugins/select2/css/select2-bootstrap.min.css').'" rel="stylesheet" type="text/css" />'
        	],
        	'js' => [
	        	'<script src="'.base_url('assets/global/plugins/select2/js/select2.full.min.js').'" type="text/javascript"></script>'
        	]
		],
		'validation' => [
			'css' => [],
			'js' => [
		        '<script src="'.base_url('assets/global/plugins/jquery-validation/js/jquery.validate.min.js').'" type="text/javascript"></script>',
		        '<script src="'.base_url('assets/global/plugins/jquery-validation/js/additional-methods.js').'" type="text/javascript"></script>'
		    ]
		],
		'bootstrap-select' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css').'" rel="stylesheet" type="text/css" />',
			],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js').'" type="text/javascript"></script>'
			]
		],
		'mask' => [
			'css' => [],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js').'" type="text/javascript"></script>'
			]
		],
		'toastr' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-toastr/toastr.min.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/bootstrap-toastr/toastr.min.js').'" type="text/javascript"></script>'
			]
		],
		'sweet-alert' => [
			'css' => [
				// '<link href="'.base_url('assets/global/plugins/sweet-alert/sweet-alert.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/sweet-alert/sweetalert2.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				// '<script src="'.base_url('assets/global/plugins/sweet-alert/sweet-alert.min.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/sweet-alert/sweetalert2.min.js').'" type="text/javascript"></script>'
			]
		],
		'ex-modal' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js').'" type="text/javascript"></script>'
			]
		],
		'sparkline' => [
			'css' => [],
			'js' => [
    			'<script src="'.base_url('assets/global/plugins/jquery.sparkline.min.js').'" type="text/javascript"></script>'
			]
		],
		'bootstrap-fileinput' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
    			'<script src="'.base_url('assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js').'" type="text/javascript"></script>'
			]
		],
		'counterup' => [
			'css' => [],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/counterup/jquery.waypoints.min.js').'" type="text/javascript"></script>',
    			'<script src="'.base_url('assets/global/plugins/counterup/jquery.counterup.js').'" type="text/javascript"></script>'
			]
		],
		'dropzone' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/dropzone/dropzone.min.css').'" rel="stylesheet" type="text/css" />',
				'<link href="'.base_url('assets/global/plugins/dropzone/basic.min.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
    			'<script src="'.base_url('assets/global/plugins/dropzone/dropzone.min.js').'" type="text/javascript"></script>'
			]
		],
		'ckeditor' => [
			'css' => [],
			'js' => [
    			'<script src="'.base_url('assets/global/plugins/ckeditor/ckeditor.js').'" type="text/javascript"></script>'
			]
		],
		'bootstrap-table' => [
			'css' => [
				'<link href="'.base_url('assets/global/plugins/bootstrap-table/bootstrap-table.min.css').'" rel="stylesheet" type="text/css" />'
			],
			'js' => [
				'<script src="'.base_url('assets/global/plugins/bootstrap-table/bootstrap-table.js').'" type="text/javascript"></script>'
			]
		],
		'summernote' => [
				'css' => [
					'<link href="'.base_url('assets/global/plugins/bootstrap-summernote/summernote.css').'" rel="stylesheet" type="text/css" />'
				],
				'js' => [
					'<script src="'.base_url('assets/global/plugins/bootstrap-summernote/summernote.js').'" type="text/javascript"></script>'
				]
		],
		'fancytree' => [
	         'css' => [
	           '<link href="'.base_url('assets/global/plugins/fancytree/skin-win8/ui.fancytree.css').'" rel="stylesheet" type="text/css" />'
	         ],
	         'js' => [
	           '<script src="'.base_url('assets/global/plugins/fancytree/js/jquery.fancytree-all.min.js').'" type="text/javascript"></script>'
	         ]
	      ],
		'chart' => [
			'css' => [],
			'js'  =>[
				'<script src="'.base_url('assets/global/plugins/highcharts/js/highcharts.js').'" type="text/javascript"></script>',
		        '<script src="'.base_url('assets/global/plugins/highcharts/js/highcharts-3d.js').'" type="text/javascript"></script>',
		        '<script src="'.base_url('assets/global/plugins/highcharts/js/highcharts-more.js').'" type="text/javascript"></script>',

				'<script src="'.base_url('assets/global/plugins/highcharts/js/modules/exporting.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/highcharts/js/modules/canvas-tools.src.js').'" type="text/javascript"></script>',
				'<script src="'.base_url('assets/global/plugins/highcharts/js/modules/export-csv.js').'" type="text/javascript"></script>',
				'<script type="type="application/javascript"" src="'.base_url('assets/global/plugins/highcharts/js/modules/jspdf.min.js').'"></script>',

				'<script src="'.base_url('assets/global/plugins/highcharts/js/highcharts-export-clientside.js').'" type="text/javascript"></script>',
			]
		]
	];

	$each = @$arr[ $add ][ $tipe ];
	if ( $each )
	{
		foreach ( $each as $key => $value ) {
			echo $value."\n";
		}
	}

}

?>
