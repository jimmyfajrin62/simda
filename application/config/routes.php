<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
*/
$route['default_controller'] = 'dashboard';
$route['404_override'] = 'error/error_404';
$route['translate_uri_dashes'] = FALSE;

# Struktur Wilayah
// $route['sw']             = 'region';
// $route['sw/region']      = 'region';
// $route['sw/region/(.*)'] = 'region/$1';
