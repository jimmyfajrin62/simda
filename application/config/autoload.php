<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------
|  Auto-load Packages
| -------------------------------------------------------------------
| Prototype:
|  $autoload['packages'] = array(APPPATH.'third_party', '/usr/local/shared');
*/
$autoload['packages'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Libraries
| -------------------------------------------------------------------
| Prototype:
|	$autoload['libraries'] = array('database', 'email', 'session');
|	$autoload['libraries'] = array('user_agent' => 'ua');
*/
$autoload['libraries'] = array('template','database','form_validation','session', 'bcrypt');

/*
| -------------------------------------------------------------------
|  Auto-load Drivers
| -------------------------------------------------------------------
| Prototype:
|	$autoload['drivers'] = array('cache');
|	$autoload['drivers'] = array('cache' => 'cch');
|
*/
$autoload['drivers'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Helper Files
| -------------------------------------------------------------------
| Prototype:
|	$autoload['helper'] = array('url', 'file');
*/
$autoload['helper'] = array('security', 'custom_helper', 'url', 'form', 'get_file', 'menu', 'cdn', 'auth','cookie');

/*
| -------------------------------------------------------------------
|  Auto-load Config files
| -------------------------------------------------------------------
| Prototype:
|	$autoload['config'] = array('config1', 'config2');
*/
$autoload['config'] = array('additional');

/*
| -------------------------------------------------------------------
|  Auto-load Language files
| -------------------------------------------------------------------
| Prototype:
|	$autoload['language'] = array('lang1', 'lang2');
*/
$autoload['language'] = array();

/*
| -------------------------------------------------------------------
|  Auto-load Models
| -------------------------------------------------------------------
| Prototype:
|	$autoload['model'] = array('first_model', 'second_model');
|	$autoload['model'] = array('first_model' => 'first');
*/
$autoload['model'] = array('global/m_global', 'global/m_global_new');
