<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| List Plugin
|--------------------------------------------------------------------------
| easypiechart, datatables, datepicker, daterangepicker, flot, fullcalendar, input-mask,
| jstree, jqvmap, profile, pulsate, select2, sparkline, sweet-alert, tasks, toastr, typeahead,
| validation
*/
$config['plugin'] = ['datatables','datepicker','validation','select2','mask','bootstrap-select','toastr','sweet-alert','counterup','bootstrap-table','summernote', 'chart','ckeditor','fancytree'];

/*
|--------------------------------------------------------------------------
| Aplication Name
|--------------------------------------------------------------------------
*/
// $config['base_url'] = 'http://localhost/cendana/metronic/metronici_41/';
$config['base_url']="http://".$_SERVER['HTTP_HOST'].
str_replace(basename($_SERVER['SCRIPT_NAME']),"",
$_SERVER['SCRIPT_NAME']);

$config['app_name'] = 'SIMda';
$config['app_desc'] = 'SISTEM INFORMASI MANAJEMEN  DAERAH <br><br> Silahkan login atau mendaftar untuk pendataan wajib pajak dan pendataan SPTPD secara online';
$config['sess_cookie_name'] = 'MetroniCI41';

/*
	location : assets/pages/img/
	set null to default metronic
*/
$config['logo_img'] = 'logo.png';
