<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

                        <!-- <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="20px"> No </th>
                                <th> No SSPD </th>
                                <th> Masa Pajak </th>
                                <th> Tanggal SSPD </th>
                                <th> Nama Peneyetor </th>
                                <th> Total Bayar </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // table data
        var select_url = '<?php echo $url ?>' + '/select/';
        // console.log(select_url);
        var header     = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-right" },
        ];
        var order = [
            [3, "desc"]
        ];
        var sort = [-1, 0, 1];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
