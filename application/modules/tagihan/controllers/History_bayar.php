<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History_bayar extends CI_Controller
{
    private $prefix         = 'tagihan/History_bayar';
    private $url            = 'tagihan/History_bayar';
    private $path           = 'tagihan/pajak';
    private $table_db       = 'ref_rek_4';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'History Pajak';
        // $data['subtitle']   = 'manage STPD pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['History' => null, 'History Pajak' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( $this->path.'/history', $data, $js, $css );
	}

    public function select()
    {
        $this->table_db = 'ta_sspd';
        $user = $this->session->user_data->user_id;

        $join = [
                    'ta_nota' => ['ta_nota', 'ta_sspd.id = ta_nota.sspd_id', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            // 'nm_rek_4'         => 'ref_rek_4.nm_rek_4',
            // 'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where    = null;
        $where_e  = "ta_nota.user_id = $user";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ta_sspd.no_sspd,ta_sspd.masa1,ta_sspd.masa2,ta_sspd.tgl_sspd,ta_sspd.total_bayar,ta_sspd.nm_penyetor, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query();exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              strtoupper($rows->no_sspd),
              tgl_format($rows->masa1).' S/D '.tgl_format($rows->masa2),
              tgl_format ($rows->tgl_sspd),
              ucwords ($rows->nm_penyetor),
              uang ($rows->total_bayar),
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

}

