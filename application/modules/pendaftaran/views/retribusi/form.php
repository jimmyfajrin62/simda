<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <!-- <i class=" fa fa-plus"></i> -->
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">

                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <h3>Data Umum</h3><hr>
                                <label class="control-label" for="form_control_1"> <strong>Jenis Retribusi</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="mt-radio-inline">
                                    <label class="mt-radio">
                                        <input style="text-transform:uppercase" type="radio" class="jns_retribusi" name="jns_retribusi" value="1" <?=@$data->jns_retribusi == 1 ? 'checked' : '' ?> required> Pribadi
                                        <span class="help-block"></span>
                                    </label>
                                    <label class="mt-radio">
                                        <input style="text-transform:uppercase" type="radio" class="jns_retribusi" name="jns_retribusi" value="2" <?=@$data->jns_retribusi == 2 ? 'checked' : '' ?>> Badan Usaha
                                        <span class="help-block"></span>
                                    </label>
                                </div>
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>No Pendaftaran</strong>
                                    <span class="required">*</span>
                                </label>
                                <?php if (@$np): ?>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="no_daftar" value="<?=$np?>" required placeholder="No Pendaftaran" readonly>
                                <?php else: ?>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="no_daftar" value="<?=@$data->no_daftar?>" required placeholder="No Pendaftaran" readonly>
                                <?php endif; ?>
                                <span class="help-block"></span>
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>Nama Pendaftaran</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="nama_pendaftar" value="<?=@$data->nama_pendaftar?>" required placeholder="Nama Pendaftaran">
                                <!-- <br>
                                <label class="control-label" for="form_control_1"> <strong>NPWPD
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="npwp_pemilik" value="<?=@$data->npwp_pemilik?>" placeholder="NPWP" readonly> -->
                                <br><br>
                                <h4><strong>Alamat</strong></h4><hr>
                                <label class="control-label" for="form_control_1"> <strong>Jalan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="jalan" value="<?=@$data->jalan?>" required placeholder="Jalan">
                                <br>
                                <label class="control-label" id="mask_number" for="form_control_1"> <strong>RT / RW</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="rtrw" value="<?=@$data->rtrw?>" required placeholder="RT / RW">
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>Kecamatan</strong>
                                    <span class="required">*</span>
                                </label>
                                <!-- <select id="kec" name="kd_kec" class="form-control select2-allow-clear" required>
                                    <option></option>
                                    <?php foreach ($kec as $value => $val): ?>
                                        <?php @$data->kd_kec == $val->kd_kec ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kd_kec?>" <?=$selected?>> <?=$val->nm_kec?> </option>
                                    <?php endforeach; ?>
                                </select> -->
                                <select style="text-transform:uppercase" name="kd_kec" class="form-control" id="kecamatan">
                                    <option id="select_kec"> -- Pilih Kecamatan -- </option>
                                    <?php foreach ($kec as $value => $val): ?>
                                        <?php @$data->kd_kec == $val->kd_kec ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kd_kec?>" <?=$selected?>> <?=$val->nm_kec?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>Kelurahan</strong>
                                    <span class="required">*</span>
                                </label>
                                <!-- <select name="kd_kel" class="form-control select2-allow-clear" required>
                                    <option></option>
                                    <?php foreach ($kel as $value => $val): ?>
                                        <?php @$data->kd_kel == $val->kel_id ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kel_id?>" <?=$selected?>> <?=$val->nm_kel?> </option>
                                    <?php endforeach; ?>
                                </select> -->
                                <select style="text-transform:uppercase" name="kd_kel" class="form-control" id="kelurahan">

                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>Kabupaten</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="kabupaten" value="<?=@$data->kabupaten?>" required placeholder="Kabupaten">
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>Kode Pos</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" pattern="\d+" name="kode_pos" value="<?=@$data->kode_pos?>" required placeholder="Kode Pos">
                                <br>
                                <label class="control-label" for="form_control_1"> <strong>No Telepon</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" pattern="\d+" name="no_telpon" value="<?=@$data->no_telpon?>" required placeholder="No Telepon">
                                <br>
                            </div>
                            <div class="col-md-6 <?=@$data->jns_retribusi == 1 ? '' : 'hide' ?>" id="wp">
                                <h3>Identitas Wajib Retribusi</h3><hr>
                                <label class="control-label" for="form_control_1"><strong>Kewarganegaraan</strong>
                                    <span class="required">*</span>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="status_wn" required>
                                    <option value="1" <?=@$data->status_wn == '1' ? 'selected' : '' ?>>WNI</option>
                                    <option value="2" <?=@$data->status_wn == '2' ? 'selected' : '' ?>>WNA</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Jenis Identitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="tanda_bukti" required>
                                    <option value="1" <?=@$data->tanda_bukti == '1' ? 'selected' : '' ?>>KTP</option>
                                    <option value="2" <?=@$data->tanda_bukti == '2' ? 'selected' : '' ?>>SIM</option>
                                    <option value="3" <?=@$data->tanda_bukti == '3' ? 'selected' : '' ?>>Paspor</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Identitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_tanda_bukti" value="<?=@$data->no_tanda_bukti?>" <?=@$data->jns_retribusi == 1 ? 'required' : '' ?> required placeholder="No Tanda Bukti">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Tgl Identitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control date" name="tgl_tanda_bukti" value="<?=@$data->tgl_tanda_bukti?>" <?=@$data->jns_retribusi == 1 ? 'required' : '' ?> placeholder="Tgl Tanda Bukti">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Kartu Keluarga</strong>
                                    <span class="required"></span>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_kk" value="<?=@$data->no_kk?>" placeholder="No Kartu Keluarga">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Tgl Kartu Keluarga</strong>
                                    <span class="required"></span>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control date" name="tgl_kk" value="<?=@$data->tgl_kk == '0000-00-00' ? '' : @$data->tgl_kk?>" placeholder="Tgl Kartu Keluarga">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pekerjaan</strong>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="pekerjaan">
                                    <option value="">pilih pekerjaan</option>
                                    <option value="1" <?=@$data->pekerjaan == '1' ? 'selected' : '' ?>>Pegawai Negri</option>
                                    <option value="2" <?=@$data->pekerjaan == '2' ? 'selected' : '' ?>>Pegawai Swasta</option>
                                    <option value="3" <?=@$data->pekerjaan == '3' ? 'selected' : '' ?>>ABRI</option>
                                    <option value="4" <?=@$data->pekerjaan == '4' ? 'selected' : '' ?>>Pemilik Usaha</option>
                                    <option value="5" <?=@$data->pekerjaan == '5' ? 'selected' : '' ?>>Lainya</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pekerjaan Lainnya</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="pekerjaan_lainnya" value="<?=@$data->pekerjaan_lainnya?>" placeholder="Pekerjaan Lainnya">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Nama Instansi</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="nama_instansi" value="<?=@$data->nama_instansi?>" placeholder="Nama Instansi">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Alamat Instansi</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="alamat_instansi" value="<?=@$data->alamat_instansi?>" placeholder="Alamat Instansi">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Keterangan</strong>
                                </label>
                                <textarea style="text-transform:uppercase" name="keterangan" class="form-control" rows="3" cols="80" placeholder="Keterangan"><?=@$data->keterangan?></textarea>
                                <br>
                            </div>
                            <div class="col-md-6 <?=@$data->jns_retribusi == 2 ? '' : 'hide' ?>" id="pemilik">
                                <h3>Identitas Pemilik / Pimpinan</h3><hr><br>
                                <label class="control-label" for="form_control_1"><strong>Pemilik Badan</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="nama_pemilik" value="<?=@$data->nama_pemilik?>" placeholder="Pemilik Badan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Jabatan</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="jabatan" value="<?=@$data->jabatan?>" placeholder="Jabatan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pimpinan</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="nama_pimpinan" value="<?=@$data->nama_pimpinan?>" placeholder="Pimpinan">
                                <br><br>
                                <h4><strong>Alamat</strong></h4><hr>
                                <label class="control-label" for="form_control_1"><strong>Jalan</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="jalan1" value="<?=@$data->jalan1?>" placeholder="Jalan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>RT / RW</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="rtrw1" value="<?=@$data->rtrw1?>" placeholder="RT / RW">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kecamatan</strong>
                                </label>
                                <select style="text-transform:uppercase"  name="kecamatan1" class="form-control" id="kecamatan1">
                                    <option id="select_kec1" value="">Pilih Kecamatan</option>
                                    <?php foreach ($kec as $value => $val): ?>
                                        <?php @$data->kecamatan1 == $val->kd_kec ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kd_kec?>" <?=$selected?>> <?=$val->nm_kec?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kelurahan</strong>
                                </label>
                                <select style="text-transform:uppercase"  name="kelurahan1" class="form-control" id="kelurahan1">
                                    <option value=""></option>
                                    <?php foreach ($kel2 as $value => $val): ?>
                                        <?php @$data->kelurahan1 == $val->kd_kel ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kd_kel?>" <?=$selected?>> <?=$val->nm_kel?> </option>
                                    <?php endforeach; ?>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kabupaten</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" name="kabupaten1" value="<?=@$data->kabupaten1?>" placeholder="Kabupaten">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kode Pos</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="kode_pos1" value="<?=@$data->kode_pos1?>" placeholder="Kode Pos">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Telepon</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_telpon1" value="<?=@$data->no_telpon1?>" placeholder="No Telepon">
                                <br>
                            </div>
                        </div>

                    </div><br>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8">
                                <button type="submit" class="btn green-seagreen">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form><!-- END FORM-->
            </div>
        </div><!-- END VALIDATION STATES-->
    </div>
</div><!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            placeholder: 'Pilih',
            width: null,
        });

        $('#kelurahan').attr('disabled','disabled');
        $('#kecamatan').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_kec').remove().end();
            var id  = $(this).find(':selected').attr('value');
            $.get({
                url: '<?=$url.'/get_kelurahan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {kd_kec: id},
                success: function(res){
                    console.log(res);
                    $('#kelurahan').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#kelurahan').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
                        })
                    });
                    $('#kelurahan').removeAttr('disabled');
                }
            });
        });

        $('#kelurahan1').attr('disabled','disabled');
        $('#kecamatan1').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_kec1').remove().end();
            var id  = $(this).find(':selected').attr('value');
            $.get({
                url: '<?=$url.'/get_kelurahan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {kd_kec: id},
                success: function(res){
                    console.log(res);
                    $('#kelurahan1').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#kelurahan1').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
                        })
                    });
                    $('#kelurahan1').removeAttr('disabled');
                }
            });
        });

        $('.jns_retribusi').click(function(){
            if($('input:radio[name=jns_retribusi]:checked').val() == 1){
                $("#pemilik").addClass("hide");
                $( "#wp" ).removeClass( "hide" );
            }
            else if($('input:radio[name=jns_retribusi]:checked').val() == 2){
                $("#wp").addClass("hide");
                $( "#pemilik" ).removeClass( "hide" );
            }
        });
    });
</script>
