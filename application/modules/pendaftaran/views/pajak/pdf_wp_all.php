<!-- <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <table border="1">

                <tr>
                    <td align="center">No</td>
                    <td align="center">No.Daftar</td>
                    <td align="center">Nama</td>
                    <td align="center">Alamat</td>
                    <td align="center">Tanggal</td>
                    <td align="center">Keterangan</td>
                </tr>
                    <?php foreach ($wp as $key => $value): ?>
                <tr align="center">
                        <td><?=$value->id?></td>
                        <td><?=$value->no_daftar?></td>
                        <td><?=$value->nama_pendaftar?></td>
                        <td><?=$value->jalan?></td>
                        <td><?=$value->tgl_terdaftar?></td>
                        <td><?=$value->keterangan?></td>
                </tr>
                    <?php endforeach; ?>

        </table>
    </body>
</html> -->

<style type="text/css">
    .wrap_paper{
        margin: 5px 10px;
        background-color: white;
        border:1px solid black;
        padding:5px 5px;
        border-radius: 5px;
        height: 3000px;
        /* width: 3000px; */
    }
    .gambar{
        margin-top: 10px;
        width: 500px;
    }
    .lebar_kolom_besar{
        width: 300px;
    }
    .lebar_kolom_kecil{
        width: 150px;
    }
    .kop_surat td {
        text-align: center;
    }
    .kop_surat td:first-child p{
        /*font-size: 10px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2){
        vertical-align: middle;
    }
    .kop_surat td:nth-child(2) p{
        line-height: 10px;
        /*font-size: 15px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2) h1{
        color: #000;
        font-weight: bold;
    }
    .kolom {
        line-height: 200px;
        padding-left: 30px;
        font-size: 30px;
    }
    table{
        margin-bottom: 0!important;
    }
    table.table-bordered tr td{
        border: 1px solid black !important;
    }
    table .noborder  tr  td{
        border: none !important;
    }
    tr.border_btm  td:not(:first-child){
        border-bottom: 1px solid #E5E5E5;
    }
    table .tbl_perhitungan_njop tr td{
        border: 1px solid black !important;
    }
    table .tbl_perhitungan_njop tr td p,
    table .tbl_perhitungan_njop tr td p b{
        line-height: 10px;
    }
    table.hitung tr td{
        border-left: none;
        border-right: none;
        border-bottom: none;
    }
    tr .tanda_tangan td p{
        line-height: 8px;
    }
    label{
        margin-bottom: 0 !important;
    }
    {
        font-size: 30px;
    }
     @page {
        margin-top: 8mm;
        margin-bottom: 8mm;
        margin-left: 8mm;
        margin-right: 8mm;
    } 
</style>
<link href="<?=base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet');?>" type="text/css" />

<div class="wrap_paper" style="overflow: auto;" >
    <div class="inputform" id="ini_di_print">
        <table class="table">
            <tr class="kop_surat">
                <td>
                    <img src="<?=base_url() . 'assets/img/logo.png' . $this->config->item('LOGO_KOTA');?>" class="gambar" />
                    <p style="margin-top:5px;"><?php echo $this->config->item('NAMA_DINAS');?></p>
                </td>
                <td colspan="2">
                    <p><b>PEMERINTAH KOTA PASURUAN</b></p>
                    <p><b>BADAN PENDAPATAN DAERAH</b></p>
                    <p><b>ALAMAT : MALANG</b></p>
                </td>
                <td>
                    <p><b>Tanggal Daftar : <?php echo @$records->tgl_terdaftar;?></b></p>
                </td>
            </tr>
        </table>
    </div>
</div>
