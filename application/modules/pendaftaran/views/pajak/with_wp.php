<div class="row">
    <div class="col-md-12">
        <div class="portlet light note note-info">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table font-white"></i><?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <a href="<?=$url.'/print_pdf_wp/'.$user_wp?>" target="_blank" class="btn btn-transparent red btn-sm tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i> Print</a>
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Edit" href="<?php echo $url ?>/show_edit/<?=$user_wp?>" class="ajaxify btn btn-transparent red btn-sm tooltips">
                                    <i class="fa fa-edit"></i> Edit WP
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <?php if (@$pesan->status_pajak == 2){ ?>
                        
                        <div class="alert" style="background-color: red">
                        <center><h4 style="color: white"><?=@$pesan->detail?></h4></center>
                        </div>

                    <?php }else { ?>
                        <div>  
                        </div>
                    <?php } ?>

                    <table id="user" class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td colspan="2"><center><h4><strong><br>DATA UMUM WAJIB PAJAK<strong></h4></center></td><br>
                            </tr>
                            <tr>
                                <td style="width:25%"> NOMER DAFTAR </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->no_daftar)?></span></td>
                            </tr>
                            <tr>
                                <td> NPWPD </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->npwpd)?></span></td>
                            </tr>
                            <tr>
                                <td> NPWP USAHA </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->npwp_pemilik)?></span></td>
                            </tr>
                            <tr>
                                <td> NAMA WAJIB PAJAK </td>
                                <td> <?=strtoupper($wp->nama_pendaftar)?> </td>
                            </tr>
                            <tr>
                                <td> JENIS PAJAK </td>
                                <td> <?=strtoupper($wp->jns_pajak) != 2 ? 'PRIBADI' : 'BADAN USAHA'?> </td>
                            </tr>
                            <tr>
                                <td> ALAMAT </td>
                                <td>
                                    <?=strtoupper($wp->jalan)?>&nbsp
                                    RT/RW. <?=strtoupper($wp->rtrw)?> <?=strtoupper($wp->nm_kel)?>, <br><br>
                                    <?=strtoupper($wp->nm_kec)?> - <?=strtoupper($wp->kabupaten)?>, <?=strtoupper($wp->kode_pos)?>
                                </td>
                            </tr>
                            <tr>
                                <td> NOMER TELEPON </td>
                                <td> <?=strtoupper($wp->no_telpon)?> </td>
                            </tr>
                            <tr>
                                <td colspan="2"><center><h4><strong><br>IDENTITAS WAJIB PAJAK<strong></h4></center></td><br>
                            </tr>
                            <tr>
                                <td> WARGA NEGARA </td>
                                <td> <?=strtoupper($wp->status_wn) !=2 ? 'WNI' : 'WNA' ?> </td>
                            </tr>
                            <tr>
                                <td> IDENTITAS </td>
                                <td> <?=$wp->tanda_bukti == 3 ? 'PASPOR' : ($wp->tanda_bukti == 2 ? 'SIM' : 'KTP')?> </td>
                            </tr>
                            <tr>
                                <td> NOMER IDENTITAS </td>
                                <td> <?=strtoupper($wp->no_tanda_bukti)?> </td>
                            </tr>
                            <tr>
                                <td> TANGGAL IDENTITAS </td>
                                <td> <?=$wp->tgl_tanda_bukti == '0000-00-00' || $wp->tgl_tanda_bukti == NULL ? '' : tgl_format($wp->tgl_tanda_bukti)?> </td>
                            </tr>
                            <tr>
                                <td> NOMER KARTU KELUARGA </td>
                                <td> <?=strtoupper($wp->no_kk)?> </td>
                            </tr>
                            <tr>
                                <td> TANGGAL KARTU KELUARGA </td>
                                <td> <?=$wp->tgl_kk == '0000-00-00' || $wp->tgl_kk == NULL ? '' : tgl_format($wp->tgl_kk)?> </td>
                            </tr>
                            <tr>
                                <td> PEKERJAAN </td>
                                <td> <?=$wp->pekerjaan == 5 ? 'LAIN-LAIN' : ($wp->pekerjaan == 4 ? 'PEMILIK USAHA' : ($wp->pekerjaan == 3 ? 'ABRI' : ($wp->pekerjaan == 2 ? 'PEGAWAI SWASTA' : ($wp->pekerjaan == 1 ? 'PEGAWAI NEGRI' : '')))) ?> </td>
                            </tr>
                            <tr>
                                <td> PEKERJAAN LAINNYA </td>
                                <td> <?=strtoupper($wp->pekerjaan_lainnya)?> </td>
                            </tr>
                            <tr>
                                <td> NAMA INSTANSI </td>
                                <td> <?=strtoupper($wp->nama_instansi)?> </td>
                            </tr>
                            <tr>
                                <td> ALAMAT INSTANSI </td>
                                <td> <?=strtoupper($wp->alamat_instansi)?> </td>
                            </tr>
                            <tr>
                                <td> KETERANGAN </td>
                                <td> <?=strtoupper($wp->keterangan)?> </td>
                            </tr>
                            <?php foreach ($ktp as $value): ?>
                            <tr>
                                <td>
                                     <?php if(@$a != '1'){ ?>DOKUMEN PENDUKUNG<?php } 
                                     $a = 1; ?></td>
                                <td> <?=strtoupper($value->type)?>  <a target="_blank" href="<?=base_url('pendaftaran/pendaftaran_pajak/image/'.$value->type.'/'.$value->key)?>" class="fa fa-file-o" ></a> </td>
                            </tr>
                            <?php endforeach ?>

                            <?php if (@$wp->jns_pajak == 1): ?>

                                        <td></td>

                                    <?php else: ?>

                                    <tr>
                                        <td colspan="2"><center><h4><strong><br>IDENTITAS BADAN USAHA<strong></h4></center></td><br>
                                    </tr>
                                    <tr>
                                        <td> NAMA PEMILIK BADAN</td>
                                        <td> <?=strtoupper($wp->nama_pemilik)?> </td>
                                    </tr>
                                    <tr>
                                        <td> JABATAN PEMILIK BADAN</td>
                                        <td> <?=strtoupper($wp->jabatan)?> </td>
                                    </tr>
                                    <tr>
                                        <td> NAMA PIMPINAN BADAN</td>
                                        <td> <?=strtoupper($wp->nama_pimpinan)?> </td>
                                    </tr>
                                    <tr>
                                        <td> ALAMAT PIMPINAN BADAN</td>
                                        <?php if ($wp->jalan1 == NULL): ?>
                                            <td></td>
                                        <?php else: ?>
                                            <td>
                                                <?=strtoupper($wp->jalan1)?>&nbsp
                                                RT/RW. <?=strtoupper($wp->rtrw1)?> <?=strtoupper($wp->kelurahan1)?>, <br><br>
                                                <?=strtoupper($wp->kecamatan1)?> - <?=strtoupper($wp->kabupaten1)?>, <?=strtoupper($wp->kode_pos1)?>
                                            </td>
                                        <?php endif; ?>
                                    </tr>
                                    <tr>
                                        <td> TELPON PIMPINAN BADAN</td>
                                        <td> <?=strtoupper($wp->no_telpon1)?> </td>
                                    </tr>
                            <?php endif; ?>

                            <tr>
                                <td colspan="2"><center><h4><strong><br>OBJEK PAJAK<strong></h4></center></td><br>
                            </tr>
                            <?php foreach (@$jenis_pajak as $id => $value): ?>
                                <?php $jenis_pajak[$value->wp_usaha_id][] = $value; ?>
                            <?php endforeach ?>

                            <?php foreach (@$wp1 as $datax): ?>

                                <tr>
                                    <td colspan="2"><h4><strong><i class="fa fa-folder-o"> </i> <?=strtoupper($datax['nm_usaha'])?><strong></h4></td>
                                </tr>
                                <tr>
                                    <td> JENIS USAHA</td>
                                    <td> <?=strtoupper($datax['nm_usaha_4'])?> </td>
                                </tr>
                                <tr>
                                    <td> ALAMAT USAHA</td>
                                    <td> <?=strtoupper($datax['alamat_usaha'])?> </td>
                                </tr>
                                <tr>
                                    <td> KLASIFIKASI USAHA</td>
                                    <td> <?=strtoupper($datax['nm_usaha_5'])?> </td>
                                </tr>

                                <tr>
                                    <td colspan="2"><h4><strong>JENIS PAJAK<strong></h4></td>
                                </tr>
                                <?php 
                                if(isset($jenis_pajak[$datax['id']])){
                                foreach ($jenis_pajak[$datax['id']] as $data2): ?>

                                <tr>
                                    <td><i class="fa fa-folder"> </i></td>
                                    <td><b> <?=strtoupper($data2->nm_rek_4)?> </b></td>
                                </tr>

                                <tr>
                                    <td><br></td>
                                    <td> <?=strtoupper($data2->tmt_operasional)?> </td>
                                </tr>

                                <tr>
                                    <td><br></td>
                                    <td> <?=strtoupper($data2->nm_jn_pemungutan)?> </td>
                                </tr>

                                <?php endforeach ?>
                                <?php } ?>

                            <?php endforeach ?>

                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        var pajak = $('.pajak').val();
        console.log(pajak);


    });
</script>
