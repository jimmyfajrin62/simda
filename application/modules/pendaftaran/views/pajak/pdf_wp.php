<style type="text/css">
    .wrap_paper{
        margin: 0px 0px;
        background-color: white;
        border:1px solid black;
        padding:0px 0px;
        border-radius: 0px;
        height: 3000px;
        /* width: 3000px; */
    }
    .gambar{
        margin-top: 10px;
        width: 100px;
    }
    .lebar_kolom_besar{
        width: 300px;
    }
    .lebar_kolom_kecil{
        width: 150px;
    }
    .kop_surat td {
        text-align: center;
    }
    .kop_surat td:first-child p{
        /*font-size: 10px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2){
        vertical-align: middle;
    }
    .kop_surat td:nth-child(2) p{
        line-height: 10px;
        /*font-size: 15px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2) h1{
        color: #000;
        font-weight: bold;
    }
    .kolom {
        line-height: 200px;
        padding-left: 30px;
        font-size: 30px;
    }
    table{
        margin-bottom: 0!important;
    }
    table.table-bordered tr td{
        border: 1px solid black !important;
    }
    table .noborder  tr  td{
        border: none !important;
    }
    tr.border_btm  td:not(:first-child){
        border-bottom: 1px solid #E5E5E5;
    }
    table .tbl_perhitungan_njop tr td{
        border: 1px solid black !important;
    }
    table .tbl_perhitungan_njop tr td p,
    table .tbl_perhitungan_njop tr td p b{
        line-height: 10px;
    }
    table.hitung tr td{
        border-left: none;
        border-right: none;
        border-bottom: none;
    }
    tr .tanda_tangan td p{
        line-height: 8px;
    }
    label{
        margin-bottom: 0 !important;
    }
    {
        font-size: 30px;
    }
     @page {
        margin-top: 0mm;
        margin-bottom: 0mm;
        margin-left: 0mm;
        margin-right: 0mm;
    } 
</style>
    <link href="<?=base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet');?>" type="text/css" />

<!-- untuk halaman yang di print -->
<div class="wrap_paper" style="overflow: auto;" >
    <div class="inputform" id="ini_di_print" style="width: 110%">
        <table class="table table-bordered" style="font-size:60px;">
            <tr class="kop_surat">
                <td>
                    <img src="<?=base_url() . 'assets/img/logo.png' . $this->config->item('LOGO_KOTA');?>" class="gambar" />
                </td>
                <td>
                    <h3><b>PEMERINTAH KOTA PASURUAN</b></h3>
                    <h3><b>BADAN PENDAPATAN DAERAH</b></h3>
                    <h3><b>ALAMAT : MALANG</b></h3>
                </td>
                <td>
                    <h3><b>Tanggal Daftar : <?php echo @$records->tgl_terdaftar;?></b></h3>
                </td>
            </tr>
        </table>

        <table class="table table-bordered" style="font-size:60px;">

            <tr class="kop_surat">
                <td>
                    <br><b>SBPWP</b><br>
                    <b>(Surat Bukti Pendaftaran Wajib Pajak)</b>
                </td>
            </tr>
                <tr><td><br></td></tr>

    <tr><td><br>
        <table class="table noborder table-condensed" style="font-size:50px;">
            <tr>
                <td></td>
                <td><b>No. Daftar</td>
                <td><b>:</td>
                <td colspan="7"><b><?php echo @$records->no_daftar;?></td>
            </tr>
            <tr>
                <td></td>
                <td><b>Nama Pendaftar</td>
                <td><b>:</td>
                <td colspan="7"><b><?php echo @$records->nama_pendaftar;?></td>
            </tr>
            <tr class="border_btm">
                <td></td>
                <td><b>Alamat</td>
                <td><b>:</td>
                <td colspan="7"><b><?php echo @$records->jalan;?> RT/RW <?=@$records->rtrw?> Kab.<?=@$records->kabupaten?></td>
            </tr>
            <tr class="border_btm">
                <td></td>
                <td><b>No. Telp</td>
                <td><b>:</td>
                <td colspan="7"><b><?php echo @$records->no_telpon;?></td>
            </tr>
            <tr class="border_btm">
                <td width="5%"></td>
                <td width="13%"></td>
                <td width="2%"></td>
                <td width="15%"></td>
                <td width="5%"></td>
                <td width="2%"></td>
                <td width="10%"></td>
                <td width="8%"></td>
                <td width="2%"></td>
                <td width="10%"></td>
            </tr>
            <tr class="border_btm">
                <td></td>
                <td></td>
                <td ></td>
                <td colspan="2" ></td>
                <td colspan="2" ></td>
                <td ></td>
                <td ></td>
                <td ></td>
            </tr><br>
        </table>
    </td></tr>

            <tr>
                <td colspan="4" style="padding-left: 30px;">
                    <table class="table noborder table-condensed" style="font-size:50px">
                        <tr><td><br></td></tr>
                        <tr>
                            <td></td>
                            <td><b>PERHATIAN :</b></td>
                            <td></td>
                            <td colspan="5"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>1. Form ini harap dibawa untuk melakukan pendataan dan validasi wajib pajak</td>
                            <td></td>
                            <td colspan="5"></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td>2. Form ini di serahkan kepada petugas yang bersangkutan</td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td>3. Beserta fotocopy <b>KTP</b> dan <b>KK</b></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td>4. Pastikan data diri anda sesuai dengan KTP </td>
                        </tr>
                        <tr><td><br></td></tr>

                    </table>
                </td>
            </tr>

        </table>


        </table>

        <table class="table table-bordered" style="font-size:60px;">
                        <tr>
                            <td colspan="4" style="padding-left: 30px;">
                                <span style="margin-left:10px">A. </span>
                                <span style="margin-left:30px"> DIISI OLEH WAJIB PAJAK / PENANGGUNG PAJAK</span>
                            </td>
                        </tr>
                    </td></tr>

            <tr>
                <td colspan="4" style="padding-left: 50px; line-height: 100px;">
                    <table class="table noborder table-condensed" style="font-size:50px;">
                        <tr><td><br></td></tr>
                        <tr>
                            <td>1. Golongan Hotel</td>
                            <td>:</td>
                            <td>Pajak Hotel Bintang Lima Berlian</td>
                        </tr>
                        <tr>
                            <td>2. Menggunakan Cash Register</td>
                            <td>:</td>
                            <td><input type="text" name=""></td>
                            <td>1. Ya</td>
                            <td>2. Tidak</td>
                        </tr>
                        <tr>
                            <td>3. Mengadakan Pembukuan</td>
                            <td>:</td>
                            <td><input type="text" name=""></td>
                            <td>1. Ya</td>
                            <td>2. Tidak</td>
                        </tr>
                        <tr>
                            <td>4 Jumlah dan Tarif Kamar Hotel</td>
                            <td>:</td>
                        </tr>
                        <tr><td><br></td></tr>
                            <table class="table table-bordered" style="font-size:60px;">
                                <tr class="kop_surat" class="table table-bordered">
                                    <td colspan="2">
                                        <p><b>No</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Golongan Kamar</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Tarif(Rp)</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Jumlah Kamar</b></p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <br>
                            </table>

                        <tr>
                            <td colspan="4" style="padding-left: 30px;">
                                <span style="margin-left:10px">B. </span>
                                <span style="margin-left:30px"> DIISI OLEH WAJIB PAJAK / PENANGGUNG PAJAK SELF ASSESMENT</span>
                            </td>
                        </tr>

        </table>
    </div>
</div>
