<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light note note-info">
            <div class="portlet-title">
                <div class="caption">
                    <!-- <i class=" fa fa-plus"></i> -->
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" enctype="multipart/form-data" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">

                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <h3><strong>Data Umum</strong></h3><hr>
                                <label class="control-label" for="form_control_1"><strong>Jenis Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="mt-radio-inline">
                                    <?php if (@$data->jns_pajak): ?>
                                        <label class="mt-radio">
                                            <input type="radio" class="jns_pajak" name="jns_pajak" value="1" <?=$data->jns_pajak == 1 ? 'checked' : '' ?> required disabled> PRIBADI2
                                            <span class="help-block"></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" class="jns_pajak" name="jns_pajak" value="2" <?=$data->jns_pajak == 2 ? 'checked' : '' ?> disabled> BADAN USAHA
                                            <span class="help-block"></span>
                                        </label>
                                    <?php else: ?>
                                        <label class="mt-radio">
                                            <input type="radio" class="jns_pajak" name="jns_pajak" value="1" required> PRIBADI
                                            <span class="help-block"></span>
                                        </label>
                                        <label class="mt-radio">
                                            <input type="radio" class="jns_pajak" name="jns_pajak" value="2"> BADAN USAHA
                                            <span class="help-block"></span>
                                        </label>
                                    <?php endif; ?>
                                </div>
                                <br>

                            <div id='TextBoxesGroup'>
                                <div id="TextBoxDiv1">
                                <label class="control-label" for="form_control_1"><strong>Berkas Pendukung</strong>
                                    <span class="required">*</span>
                                </label>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Jenis Dokumen</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="jn_d1" value="KTP" required placeholder="" readonly>
                                <br>
                                <center>
                                <img class="rounded-circle" alt="File" src="http://via.placeholder.com/350x150" width="40%" id="gambar-preview" name="gambar-preview">
                                </center>
                                <br>
                                <label>
                                <input class="btn btn-primary" type="file" name="KTP" id="image-source" onchange="previewImage()">
                                </label>
                                </div>
                            </div>
                            <br>
                            <a type='button' class="btn btn-info" id='addButton'><i class="fa fa-plus"></i></a>
                            <a type='button' class="btn btn-danger" id='removeButton'><i class="fa fa-trash"></i></a>

                                <br>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Pendaftaran</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="no_daftar" value="<?=$user->no_daftar?>" required placeholder="No Pendaftaran" readonly>
                                <span class="help-block"></span>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Nama Pendaftaran</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase" data-inputmask="'mask' : '* . 9 . 9999999 . 9 . 9'" class="form-control" name="nama_pendaftar" value="<?=@$data->nama_pendaftar?>" required placeholder="Nama Pendaftaran">
                                <br><br>
                                <h4><strong>Alamat</strong></h4><hr>
                                <label class="control-label" for="form_control_1"><strong>Jalan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="jalan" value="<?=@$data->jalan?>" required placeholder="Jalan">
                                <br>
                                <label class="control-label" id="phone" for="form_control_1"><strong>RT / RW</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="rtrw" value="<?=@$data->rtrw?>" required placeholder="RT / RW">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kecamatan</strong>
                                    <span class="required">*</span>
                                </label>
                                <?php if (@$data->kd_kec): ?>
                                    <select style="text-transform:uppercase"  name="kd_kec" class="form-control" id="kecamatan" disabled>
                                        <option id="select_kec" value="">Pilih Kecamatan</option>
                                        <?php foreach ($kec as $value => $val): ?>
                                            <?php @$data->kd_kec == $val->kd_kec ? $selected = 'selected' : $selected = '' ?>
                                            <option value="<?=$val->kd_kec?>" <?=$selected?>> <?=$val->nm_kec?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php else: ?>
                                    <select style="text-transform:uppercase"  name="kd_kec" class="form-control" id="kecamatan">
                                        <option id="select_kec" value="">Pilih Kecamatan</option>
                                        <?php foreach ($kec as $value => $val): ?>
                                            <?php @$data->kd_kec == $val->kd_kec ? $selected = 'selected' : $selected = '' ?>
                                            <option value="<?=$val->kd_kec?>" <?=$selected?>> <?=$val->nm_kec?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php endif; ?>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kelurahan</strong>
                                    <span class="required">*</span>
                                </label>
                                <?php if (@$data->kd_kel): ?>
                                    <select style="text-transform:uppercase"  name="kd_kel" class="form-control" id="kelurahan" disabled>
                                        <?php foreach ($kel as $value => $val): ?>
                                            <?php @$data->kd_kel == $val->kd_kel ? $selected = 'selected' : $selected = '' ?>
                                            <option value="<?=$val->kd_kel?>" <?=$selected?>> <?=$val->nm_kel?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                <?php else: ?>
                                    <select style="text-transform:uppercase"  name="kd_kel" class="form-control" id="kelurahan">

                                    </select>
                                <?php endif; ?>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kabupaten</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="kabupaten" value="<?=@$data->kabupaten?>" required placeholder="Kabupaten">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kode Pos</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="kode_pos" value="<?=@$data->kode_pos?>" placeholder="Kode Pos">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Telepon</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_telpon" value="<?=@$data->no_telpon?>" required placeholder="No Telepon">
                                <br>
                            </div>
                            <div class="col-md-4">
                                <h3><strong>Identitas Wajib Pajak</strong></h3><hr>
                                <label class="control-label" for="form_control_1"><strong>Kewarganegaraan</strong>
                                    <span class="required">*</span>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="status_wn" required>
                                    <option value="1" <?=@$data->status_wn == '1' ? 'selected' : '' ?>>WNI</option>
                                    <option value="2" <?=@$data->status_wn == '2' ? 'selected' : '' ?>>WNA</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Jenis Identitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="tanda_bukti" required>
                                    <option value="1" <?=@$data->tanda_bukti == '1' ? 'selected' : '' ?>>KTP</option>
                                    <option value="2" <?=@$data->tanda_bukti == '2' ? 'selected' : '' ?>>SIM</option>
                                    <option value="3" <?=@$data->tanda_bukti == '3' ? 'selected' : '' ?>>Paspor</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Identitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_tanda_bukti" value="<?=@$data->no_tanda_bukti?>" required placeholder="No Identitas">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Tgl Identitas</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control tgl" name="tgl_tanda_bukti" value="<?=$this->m_global->getdateformat(@$data->tgl_tanda_bukti)?>" placeholder="Tgl Identitas">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Kartu Keluarga</strong>
                                    <span class="required"></span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_kk" value="<?=@$data->no_kk?>" placeholder="No Kartu Keluarga">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Tgl Kartu Keluarga</strong>
                                    <span class="required"></span>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control tgl" name="tgl_kk" value="<?=$this->m_global->getdateformat(@$data->tgl_kk)?>" placeholder="Tgl Kartu Keluarga">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pekerjaan</strong>
                                </label>
                                <select style="text-transform:uppercase"  class="form-control" name="pekerjaan">
                                    <option value="">pilih pekerjaan</option>
                                    <option value="1" <?=@$data->pekerjaan == '1' ? 'selected' : '' ?>>Pegawai Negri</option>
                                    <option value="2" <?=@$data->pekerjaan == '2' ? 'selected' : '' ?>>Pegawai Swasta</option>
                                    <option value="3" <?=@$data->pekerjaan == '3' ? 'selected' : '' ?>>ABRI</option>
                                    <option value="4" <?=@$data->pekerjaan == '4' ? 'selected' : '' ?>>Pemilik Usaha</option>
                                    <option value="5" <?=@$data->pekerjaan == '5' ? 'selected' : '' ?>>Lainya</option>
                                </select>
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pekerjaan Lainnya</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="pekerjaan_lainnya" value="<?=@$data->pekerjaan_lainnya?>" placeholder="Pekerjaan Lainnya">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Nama Instansi</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="nama_instansi" value="<?=@$data->nama_instansi?>" placeholder="Nama Instansi">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Alamat Instansi</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="alamat_instansi" value="<?=@$data->alamat_instansi?>" placeholder="Alamat Instansi">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Keterangan</strong>
                                </label>
                                <textarea style="text-transform:uppercase" name="keterangan" class="form-control" rows="3" cols="80" placeholder="Keterangan"><?=@$data->keterangan?></textarea>
                                <br>
                            </div>
                            <div class="col-md-4 <?=@$data->jns_pajak == 2 ? '' : 'hide' ?>" id="pemilik">
                                <h3><strong>Identitas Pemilik </strong></h3><hr>
                                <label class="control-label" for="form_control_1"><strong>Pemilik Badan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="nama_pemilik" value="<?=@$data->nama_pemilik?>" placeholder="Pemilik Badan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Jabatan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="jabatan" value="<?=@$data->jabatan?>" placeholder="Jabatan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Pimpinan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="nama_pimpinan" value="<?=@$data->nama_pimpinan?>" placeholder="Pimpinan">
                                <br><br>
                                <h4><strong>Alamat</strong></h4><hr>
                                <label class="control-label" for="form_control_1"><strong>Jalan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="jalan1" value="<?=@$data->jalan1?>" placeholder="Jalan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>RT / RW</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="rtrw1" value="<?=@$data->rtrw1?>" placeholder="RT / RW">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kelurahan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="kelurahan1" value="<?=@$data->kelurahan1?>" placeholder="Kelurahan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kecamatan</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="kecamatan1" value="<?=@$data->kecamatan1?>" placeholder="Kecamatan">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kabupaten</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" name="kabupaten1" value="<?=@$data->kabupaten1?>" placeholder="Kabupaten">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>Kode Pos</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="kode_pos1" value="<?=@$data->kode_pos1?>" placeholder="Kode Pos">
                                <br>
                                <label class="control-label" for="form_control_1"><strong>No Telepon</strong>
                                </label>
                                <input type="text" style="text-transform:uppercase"  class="form-control" pattern="\d+" name="no_telpon1" value="<?=@$data->no_telpon1?>" placeholder="No Telepon">
                                <br>
                            </div>
                        </div>

                    </div><br>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-8">
                                <button type="submit" class="btn green-seagreen">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form><!-- END FORM-->
            </div>
        </div><!-- END VALIDATION STATES-->
    </div>
</div><!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>


<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.tgl').datepicker({
            format: 'dd-mm-yyyy'
        });

        $(".select2-allow-clear").select2({
            allowClear: true,
            placeholder: 'Pilih',
            width: null,
        });

        // $('#kelurahan').attr('disabled','disabled');
        $('#kecamatan').on('change', function(){
            var id  = $(this).find(':selected').attr('value');
            $.get({
                url: '<?=$url.'/get_kelurahan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {kd_kec: id},
                success: function(res){
                    console.log(res);
                    $('#kelurahan').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#kelurahan').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
                        })
                    });
                    $('#kelurahan').removeAttr('disabled');
                }
            });
        });

        $('.jns_pajak').click(function(){
            if($('input:radio[name=jns_pajak]:checked').val() == 1){
                $("#pemilik").addClass("hide");
                // $( "#wp" ).removeClass( "hide" );
            }
            else if($('input:radio[name=jns_pajak]:checked').val() == 2){
                // $("#wp").addClass("hide");
                $( "#pemilik" ).removeClass( "hide" );
            }
        });

    });
</script>

<script>
    function previewImage() {
        document.getElementById("gambar-preview").style.display = "block";
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image-source").files[0]);
            oFReader.onload = function(oFREvent) {
            document.getElementById("gambar-preview").src = oFREvent.target.result;
          }
        }
</script>

<script type="text/javascript">

$(document).ready(function(){

    var counter = 2;
        
    $("#addButton").click(function () {
                
    if(counter>10){
            alert("Only 10 textboxes allow");
            return false;
    }   
        
    var newTextBoxDiv = $(document.createElement('div'))
         .attr("id", 'TextBoxDiv' + counter);
                
    newTextBoxDiv.after().html('<br>'+'<label><b>Berkas Pendukung '+ counter + '</b></label>' + '<br>'+'<label><b>Jenis Dokumen'+ counter +'</b></label><br>'+' <input type="text" style="text-transform:uppercase"  class="form-control" name="jn_d'+ counter +'" value="<?=@$data->type?>" required placeholder="">'+
            '<center><img class="rounded-circle" alt="File" src="http://via.placeholder.com/350x150" width="40%" id="gambar-preview" name="gambar-preview">' +'<br>'+'<br>'+
            '<input class="btn btn-primary" type="file" name="KTP' + counter + '" id="image-source' + counter + '" value="" >');
            
    newTextBoxDiv.appendTo("#TextBoxesGroup");
      
    counter++;
     });

     $("#removeButton").click(function () {
    if(counter==1){
          alert("No more textbox to remove");
          return false;
       }   
        
    counter--;
            
        $("#TextBoxDiv" + counter).remove();
            
     });
        
     $("#getButtonValue").click(function () {
        
    var msg = '';
    for(i=1; i<counter; i++){
      msg += "\n Textbox #" + i + " : " + $('#textbox' + i).val();
    }
          alert(msg);
     });
  });
</script>
