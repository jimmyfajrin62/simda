<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Pendaftaran_pajak extends CI_Controller
{
    private $prefix         = 'pendaftaran/pendaftaran_pajak';
    private $url            = 'pendaftaran/pendaftaran_pajak';
    private $table_db       = 'wp_data_umum';
    private $table_db1      = 'file_upload';
    private $path           = 'pendaftaran/pajak/';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pendaftaran_pajak', 'mdb');
    }

    public function index()
    {
        // echo "<pre>", print_r( $this->session->all_userdata() ), exit;
        $data['pagetitle']  = $this->session->user_data->user_role == 2 ? 'Wajib Pajak' : 'Pendaftaran Pajak';
        $data['subtitle']   = $this->session->user_data->user_role == 2 ? 'manage wajib pajak' : 'manage pendaftaran pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = $this->session->user_data->user_role == 2 ? [ 'Wajib Pajak' => $this->url ] : [ 'Pendaftaran Pajak' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        if ($this->session->user_data->user_role == 2) {

            $user = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();
            if ($user->user_wp == null)
            {
                $this->template->display( 'pendaftaran/pajak/no_wp', $data, $js, $css );


            } else {
                $data['wp'] = $this->db->query("SELECT *
                                                FROM wp_data_umum a
                                                JOIN ref_kelurahan g ON a.kd_kel = g.kd_kel
                                                JOIN ref_kecamatan f ON a.kd_kec = f.kd_kec
                                                LEFT JOIN wp_wajib_pajak b ON b.data_umum_id = a.id
                                                LEFT JOIN wp_wajib_pajak_usaha c ON c.wp_id = b.id
                                                LEFT JOIN ref_rek_4 d ON d.id_rek_4 = c.jns_usaha
                                                LEFT JOIN ref_rek_5 e ON e.id_rek_5 = c.klasifikasi_usaha
                                                WHERE a.id = $user->user_wp")->row();

                $data['wp1'] = $this->db->query("SELECT
                                                    `c`.`npwpd`,
                                                    `a`.`id`,
                                                    `a`.`wp_id`,
                                                    `a`.`status`,
                                                    `a`.`nm_usaha`,
                                                    `d`.`nm_usaha_4`,
                                                    `e`.`nm_usaha_5`,
                                                    `a`.`alamat_usaha`
                                                FROM
                                                    `wp_wajib_pajak_usaha` `a`
                                                LEFT JOIN `wp_wajib_pajak` `b` ON `a`.`wp_id` = `b`.`id`
                                                LEFT JOIN `wp_data_umum` `c` ON `b`.`data_umum_id` = `c`.`id`
                                                LEFT JOIN `ref_rek_4` `d` ON `a`.`jns_usaha` = `d`.`id_rek_4`
                                                LEFT JOIN `ref_rek_5` `e` ON `a`.`klasifikasi_usaha` = `e`.`id_rek_5`
                                                WHERE a. STATUS = '1' and c.id = $user->user_wp")->result_array();

                $data['jenis_pajak'] = $this->db->query("SELECT
                                                    `a`.`id`,
                                                    `a`.`wp_usaha_id`,
                                                    `a`.`status`,
                                                    `b`.`wp_id`,
                                                    `e`.`nm_rek_4`,
                                                    `a`.`tmt_operasional`,
                                                    `f`.`nm_jn_pemungutan`
                                                FROM
                                                    `wp_wajib_pajak_usaha_pajak` `a`
                                                LEFT JOIN `wp_wajib_pajak_usaha` `b` ON `a`.`wp_usaha_id` = `b`.`id`
                                                LEFT JOIN `wp_wajib_pajak` `c` ON `b`.`wp_id` = `c`.`id`
                                                LEFT JOIN `wp_data_umum` `d` ON `c`.`data_umum_id` = `d`.`id`
                                                LEFT JOIN `ref_rek_4` `e` ON `a`.`jns_pajak` = `e`.`id_rek_4`
                                                LEFT JOIN `ref_pemungutan` `f` ON `e`.`jns_pemungutan` = `f`.`jn_pemungutan`
                                                WHERE a. STATUS = '1' and d.id = $user->user_wp")->result();


                $data['ktp'] = $this->db->query("SELECT b.key,b.type from file_upload b
                                                WHERE b.key = '".$data['wp']->no_daftar."'")->result();

                $data['pesan'] = $this->db->query("SELECT * from wp_data_umum a
                                                    left join wp_data_umum_log b on a.id = b.id
                                                    where a.id = 7 and b.action = 'Reject'")->row();

                $data['usaha'] =  $this->db->query("select *
                                                            from wp_wajib_pajak_usaha_pajak a
                                                            left join wp_wajib_pajak_usaha b on a.wp_usaha_id = b.id
                                                            join wp_wajib_pajak c on b.wp_id = c.id
                                                            join wp_data_umum d on c.data_umum_id = d.id
                                                            WHERE d.id = $user->user_wp")->result();


                $data['user_wp'] = $user->user_wp;
                $this->template->display( 'pendaftaran/pajak/with_wp', $data, $js, $css );
            }

        } else {
            $this->template->display( 'pendaftaran/pajak/index', $data, $js, $css );
        }
    }

    public function pajak_usaha($id)
    {
        $data['pajak']  =  $this->db->select('*')
                                    ->from('wp_wajib_pajak_usaha_pajak a')
                                    ->join('ref_rek_4 b', 'a.jns_pajak = b.id_rek_4', 'LEFT')
                                    ->where('a.wp_usaha_id', $id)
                                    ->get()->result();

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function image($type,$key){


        $query = $this->db->query("SELECT b.path, b.name from file_upload b
                                    WHERE b.key = '$key' and b.type = '$type' ")->row();

        $file = str_replace('system/','',str_replace('\\','/',BASEPATH)).str_replace('./', '', $query->path).'/'.$query->name;
        $imginfo = getimagesize($file);

        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: '.filesize($file));
        ob_clean();
        flush();
        readfile($file);
    }

    public function show_add()
    {
        // echo "<pre>", print_r( $this->session->all_userdata() ), exit;
        $data['pagetitle']  = $this->session->user_data->user_role == 2 ? 'Wajib Pajak' : 'Pendaftaran Pajak';
        $data['subtitle']   = $this->session->user_data->user_role == 2 ? 'manage wajib pajak' : 'manage pendaftaran pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        // $data['np1']        = $this->mdb->no_npwpd('P');

        $data['user']       = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();

        $data['kec']        = $this->mdb->get_kec();

        $data['breadcrumb'] = $this->session->user_data->user_role == 2 ? [ 'Wajib Pajak' => $this->url, 'Form' => $this->url.'/show_add' ] : [ 'Pendaftaran Pajak' => $this->url, 'Form' => $this->url.'/show_add' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'pendaftaran/pajak/form', $data, $js );
    }

    public function show_edit( $id )
    {
        $data['pagetitle']  = 'Pendaftaran Pajak';
        $data['subtitle']   = 'manage pendaftaran pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data_umum          = $this->mdb->get_data_umum($id);
        $data['user']       = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();
        $data['data']       = $data_umum;
        $data['kec']        = $this->mdb->get_kec();
        $data['kel']        = $this->mdb->get_kel($data_umum->kd_kec);

        $data['breadcrumb'] = [ 'Pendaftaran Pajak' => $this->url, 'Form' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation' ];
        $css['css']         = NULL;

        $this->template->display( 'pendaftaran/pajak/form', $data, $js );
    }

    public function get_kelurahan()
    {
        $table_kel          = 'ref_kelurahan';

        $id = $this->input->post();
        $data['kelurahan']  = $this->m_global->get($table_kel, null, $id, 'kel_id, kd_kec, kd_kel, nm_kel');

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function action_form($id = NULL)
    {
        // echo '<pre>', print_r($this->input->post()), exit();


        $this->form_validation->set_rules('no_daftar', 'No Pendaftaran', 'trim|required');
        $this->form_validation->set_rules('nama_pendaftar', 'Nama Pendaftaran', 'trim|required');
        $this->form_validation->set_rules('jns_pajak', 'Jenis Pajak', 'trim');
        // $this->form_validation->set_rules('jn_d', 'Jenis Dokumen', 'trim|required');
        $this->form_validation->set_rules('jalan', 'Jalan', 'trim|required');
        $this->form_validation->set_rules('rtrw', 'RT/RW', 'trim|required');
        $this->form_validation->set_rules('kd_kel', 'Kelurahan', 'trim');
        $this->form_validation->set_rules('kd_kec', 'Kecamatan', 'trim');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'trim|required');
        $this->form_validation->set_rules('no_telpon', 'No Telepon', 'trim|required');
        $this->form_validation->set_rules('status_wn', 'Kewarganegaraan', 'trim|required');
        $this->form_validation->set_rules('tanda_bukti', 'Tanda Bukti', 'trim|required');
        $this->form_validation->set_rules('no_tanda_bukti', 'No Tanda Bukti', 'trim|required');
        $this->form_validation->set_rules('no_kk', 'No Kartu Keluarga', 'trim');
        $this->form_validation->set_rules('tgl_kk', 'Tgl Kartu Keluarga', 'trim');
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim');
        $this->form_validation->set_rules('pekerjaan_lainnya', 'Pekerjaan Lainnya', 'trim');
        $this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim');
        $this->form_validation->set_rules('alamat_instansi', 'Alamat Instansi', 'trim');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        $this->form_validation->set_rules('nama_pemilik', 'Pemilik Badan', 'trim');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'trim');
        $this->form_validation->set_rules('nama_pimpinan', 'Pimpinan', 'trim');
        $this->form_validation->set_rules('jalan1', 'Jalan', 'trim');
        $this->form_validation->set_rules('rtrw1', 'RT/RW', 'trim');
        $this->form_validation->set_rules('kelurahan1', 'Kelurahan', 'trim');
        $this->form_validation->set_rules('kecamatan1', 'Kecamatan', 'trim');
        $this->form_validation->set_rules('kabupaten1', 'Kabupaten', 'trim');
        $this->form_validation->set_rules('no_telpon1', 'No Telepon', 'trim');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'no_daftar']         = $this->input->post('no_daftar');
            $data[$this->table_prefix.'nama_pendaftar']    = $this->input->post('nama_pendaftar');
            $data[$this->table_prefix.'jalan']             = $this->input->post('jalan');
            // $data[$this->table_prefix.'jn_d']             = $this->input->post('jn_d');
            $data[$this->table_prefix.'rtrw']              = $this->input->post('rtrw');
            $data[$this->table_prefix.'kabupaten']         = $this->input->post('kabupaten');
            $data[$this->table_prefix.'kode_pos']          = $this->input->post('kode_pos');
            $data[$this->table_prefix.'no_telpon']         = $this->input->post('no_telpon');
            $data[$this->table_prefix.'status_wn']         = $this->input->post('status_wn');
            $data[$this->table_prefix.'tanda_bukti']       = $this->input->post('tanda_bukti');
            $data[$this->table_prefix.'no_tanda_bukti']    = $this->input->post('no_tanda_bukti');

            if ($this->input->post('tgl_tanda_bukti') != '') {
                $data[$this->table_prefix.'tgl_tanda_bukti']   = $this->m_global->setdateformat($this->input->post('tgl_tanda_bukti'));
            }else {
                $data[$this->table_prefix.'tgl_tanda_bukti']   = null;
            }
                
            if ($this->input->post('tgl_kk') != '') {
                $data[$this->table_prefix.'tgl_kk']        = $this->m_global->setdateformat($this->input->post('tgl_kk'));
            }else {
                $data[$this->table_prefix.'tgl_kk']        = null; 
            }

            $data[$this->table_prefix.'no_kk']             = $this->input->post('no_kk');
            $data[$this->table_prefix.'pekerjaan']         = $this->input->post('pekerjaan');
            $data[$this->table_prefix.'pekerjaan_lainnya'] = $this->input->post('pekerjaan_lainnya');
            $data[$this->table_prefix.'nama_instansi']     = $this->input->post('nama_instansi');
            $data[$this->table_prefix.'alamat_instansi']   = $this->input->post('alamat_instansi');
            $data[$this->table_prefix.'keterangan']        = $this->input->post('keterangan');
            $data[$this->table_prefix.'nama_pemilik']      = $this->input->post('nama_pemilik');
            $data[$this->table_prefix.'jabatan']           = $this->input->post('jabatan');
            $data[$this->table_prefix.'nama_pimpinan']     = $this->input->post('nama_pimpinan');
            $data[$this->table_prefix.'jalan1']            = $this->input->post('jalan1');
            $data[$this->table_prefix.'rtrw1']             = $this->input->post('rtrw1');
            $data[$this->table_prefix.'kelurahan1']        = $this->input->post('kelurahan1');
            $data[$this->table_prefix.'kecamatan1']        = $this->input->post('kecamatan1');
            $data[$this->table_prefix.'kabupaten1']        = $this->input->post('kabupaten1');
            $data[$this->table_prefix.'kode_pos1']         = $this->input->post('kode_pos1');
            $data[$this->table_prefix.'no_telpon1']        = $this->input->post('no_telpon1');
            $data[$this->table_prefix.'user_id']           = $this->session->user_data->user_id;


                $dir = './upload/'. $data[$this->table_prefix.'no_daftar'];
                if (file_exists($dir) && is_dir($dir)) {
                }else {
                    mkdir($dir, 0777);
                }
                $config['upload_path'] = $dir . "/";
                $config['remove_spaces'] = TRUE;
                $config['allowed_types']  = 'gif|jpg|jpeg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                $i = 1;
                foreach ($_FILES as $a => $b) {

              
                

                    $ar = explode(".", $_FILES[$a]['name']);
                    $ext = $ar[count($ar) - 1];
                    $ext = strtolower($ext);
                     // echo $ext;
                    if ($ext == "pdf" || $ext == "jpg" || $ext == "jpeg" || $ext == "png") {

                        $config['file_name'] = $_POST['jn_d'.$i] . '.' . $ext;
                        $this->upload->initialize($config);
                        $this->upload->display_errors('', '');
                        if (!$this->upload->do_upload($a)) {
                            $error = array('error' => $this->upload->display_errors());
                            if ($error['error'] == "<p>The file you are attempting to upload is larger than the permitted size.</p>") {
                                //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal dikarenakan ukuran file melebihi ketentuan. Silahkan upload file dengan ukuran kurang dari 2MB.";
                                print '<script type="text/javascript">';
                                print 'alert("Maaf, proses upload Data Anda gagal dikarenakan ukuran file melebihi ketentuan.\nSilahkan upload file dengan ukuran kurang dari 2MB.");';
                                print '</script>';
                                die('oke');
                            }
                            else {
                                //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal.\nSilahkan ulangi kembali.";
                                print '<script type="text/javascript">';
                                print 'alert("Maaf, proses upload Data Anda gagal.\nSilahkan ulangi kembali Silahkan upload file dengan ukuran kurang dari 2MB.");';
                                print '</script>';
                                die();
                            }
                        }
                        else {
                            
                            $up = $this->upload->data();

                            $data1['key']  = $data[$this->table_prefix.'no_daftar'];
                            $data1['path'] = $dir;
                            $data1['name'] = str_replace(" ", "_", $config['file_name']) ;
                            $data1['type'] = $_POST['jn_d'.$i];
                            // echo $data1['name'];
                            $this->db->insert( 'file_upload', $data1 );
                        }

                    }
                      $i++;
                }

            if ($id == NULL) {
                $jns_pajak = $this->input->post('jns_pajak');
                $kd_kec    = $this->input->post('kd_kec');
                $kd_kel    = $this->input->post('kd_kel');
                $npwpd     = $this->mdb->no_npwpd("P . $jns_pajak . ", " . $kd_kec . $kd_kel");


                $data[$this->table_prefix.'id']                = $this->mdb->auto_id();
                $data[$this->table_prefix.'npwpd']             = $npwpd;
                $data[$this->table_prefix.'tgl_terdaftar']     = date("Y-m-d");
                $data[$this->table_prefix.'jns_pajak']         = $this->input->post('jns_pajak');
                $data[$this->table_prefix.'kd_kel']            = $this->input->post('kd_kel');
                $data[$this->table_prefix.'kd_kec']            = $this->input->post('kd_kec');

                if ($this->session->user_data->user_role == 2) {
                    $result2        = $this->m_global->insert( $this->table_db, $data );
                    $insert_id      = $this->db->insert_id();

                    $log['id']      = $insert_id;
                    $log['action']  = 'Daftar';
                    $log['detail']  = 'Daftar'.' '.$npwpd;
                    $log['status']  = $this->session->user_data->user_status;
                    $log['user_id'] = $this->session->user_data->user_id;
                    $log['ip']      = $_SERVER['REMOTE_ADDR'];
                    $this->db->insert('wp_data_umum_log', $log);

                    $session_user       = $this->session->userdata['user_data'];
                    $session_wp         = ['user_wp' => $insert_id];
                    $session            = (object) array_merge((array) $session_user, (array) $session_wp);
                    $this->session->set_userdata('user_data', $session);

                    $data2['user_wp']   = $insert_id;
                    $data2['no_daftar'] = $this->input->post('no_daftar');
                    $data2['npwpd']     = $npwpd;
                    $result2            = $this->m_global->update('user', $data2, ['user_id' => $this->session->user_data->user_id]);

                    $data3['data_umum_id'] = $insert_id;
                    $data3['tgl_aktif']    = date('Y-m-d');
                    $data3['keterangan']   = $this->input->post('nama_pendaftar');
                    $result                = $this->m_global->insert( 'wp_wajib_pajak', $data3 );
                } else {
                    $log['id']      = $this->db->insert_id();
                    $log['action']  = 'Daftar';
                    $log['detail']  = 'Daftar'.' '.$npwpd;
                    $log['status']  = $this->session->user_data->user_status;
                    $log['user_id'] = $this->session->user_data->user_id;
                    $log['ip']      = $_SERVER['REMOTE_ADDR'];
                    $this->db->insert('wp_data_umum_log', $log);
                    $result             = $this->m_global->insert( $this->table_db, $data );
                }
            }
            else{
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
                $query = $this->db->query("SELECT * from wp_data_umum where id = $id")->row();

                $log['id']      = $query->id;
                $log['action']  = 'Update';
                $log['detail']  = 'Update'.' '.$query->npwpd;
                $log['status']  = $this->session->user_data->user_status;
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('wp_data_umum_log', $log);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_daftar'      => $this->table_prefix.'no_daftar',
            'nama_pendaftar' => $this->table_prefix.'nama_pendaftar',
            'tgl_terdaftar'  => $this->table_prefix.'tgl_terdaftar',
            'keterangan'     => $this->table_prefix.'keterangan'
        ];

        $where      = NULL;
        $where_e    = 'jns_pajak <> 0';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = "status = '$request' and jns_pajak <> 0";
        }
        else {
            $where_e = "status = '1' and jns_pajak <> 0";
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);

        $records         = array();
        $records["data"] = array();

        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id, status, lastupdate,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                '<span class="label label-m label-primary">'.$rows->no_daftar.'</span>',
                strtoupper($rows->nama_pendaftar),
                $rows->tgl_terdaftar == NULL || $rows->tgl_terdaftar == '0000-00-00 00:00:00' ? '' : tgl_format($rows->tgl_terdaftar),
                strtoupper($rows->keterangan),
                '<a href="'.base_url($this->url.'/print_pdf_wp/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_data_umum/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_data_umum/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );

            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
                $query          = $this->db->query("SELECT npwpd from wp_data_umum where id = $id")->row();
                $log['action']  = 'Delete';
                $log['detail']  = 'Delete'.' '.$query->npwpd;
                $log['status']  = $this->session->user_data->user_status;
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $result2        =   $this->db->insert('wp_data_umum_log', $log);
                $result         = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function print_pdf()
    {

        $data['wp'] = $this->mdb->get_wp_all();
        $kota       = $this->mdb->kota();

        $this->load->library('fpdf_gen');
        $pdf = new fpdf('P','mm','A4');

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('Times', 'B', 11);

        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,12,10,20,20);


        $pdf->Cell(25,7,'', 'LTR', 0, 'C');
        $pdf->Cell(115,7,$kota->nm_pemda, 'TR', 0, 'L');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(20,7,'', 'T', 0, 'L');
        $pdf->Cell(3,7,'', 'T', 0, 'C');
        $pdf->Cell(27,7,'', 'TR', 1, 'L');

        $pdf->Cell(25,7,'', 'LR', 0, 'C');
        $pdf->Cell(115,7,'BADAN PENDAPATAN DAERAH', 'R', 0, 'L');

        $pdf->SetFont('Times', '', 10);

        $pdf->Cell(20,7,'Tanggal', 0, 0, 'C');
        $pdf->Cell(3,7,':', 0, 0, 'L');
        $pdf->Cell(27,7,tgl_format(date("Y-m-d")), 'R', 1, 'L');

        $pdf->Cell(25,7,'', 'LR', 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(115,7,'ALAMAT : '.$kota->alamat, 'R', 0, 'L');
        $pdf->Cell(50,7,'', 'LR', 1, 'C');


        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(190,7,'', 'LTR', 1, 'C');
        $pdf->Cell(190,5,'(Daftar Formulir Wajib Pajak)', 'LR', 1, 'C');
        $pdf->Cell(190,7,'', 'LBR', 1, 'C');


        $pdf->Cell(190,7,'', 'LR', 1, 'c');


        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(5,7,'', 'L', 0, 'c');
        $pdf->Cell(8,7,'No', 1, 0, 'C');
        $pdf->Cell(25,7,'No Daftar', 1, 0, 'C');
        $pdf->Cell(67,7,'Nama', 1, 0, 'C');
        $pdf->Cell(40,7,'Tanggal Mendaftar', 1, 0, 'C');
        $pdf->Cell(40,7,'Keterangan', 1, 0, 'C');
        $pdf->Cell(5,7,'', 'R', 1, 'C');


        $no = 1;
        foreach ($data['wp'] as $value) {

            $pdf->Cell(5,7,'', 'L', 0, 'c');
            $pdf->Cell(8,7,$no++, 1, 0, 'C');
            $pdf->Cell(25,7,$value->no_daftar, 1, 0, 'C');
            $pdf->Cell(67,7,$value->nama_pendaftar, 1, 0, 'L');
            $pdf->Cell(40,7,$value->tgl_terdaftar, 1, 0, 'C');
            $pdf->Cell(40,7,$value->keterangan, 1, 0, 'C');
            $pdf->Cell(5,7,'', 'R', 1, 'C');

        }

        $pdf->Cell(190,7,'', 'LRB', 1, 'c');
        $pdf->Output("Daftar Formulir Pendaftaran.pdf","I");

    }

    public function print_pdf_wp($id)
    {

        $data['records'] = $this->mdb->get_wp($id);

        $kota = $this->mdb->kota();
        // echo "<pre>";
        // print_r ($data['records']);
        // echo "</pre>";exit();

        $this->load->library('fpdf_gen');
        $pdf = new fpdf('P','mm','A4');

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('Times', 'B', 11);

        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,10,10,20,20);

        $pdf->Cell(25,13,'', 'LTR', 0, 'C');
        $pdf->Cell(105,13,$kota->nm_pemda, 'LTR', 0, 'C');

        $pdf->Cell(60,13,'Nomor Formulir', 'TR', 1, 'C');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(25,7,'', 'LBR', 0, 'C');
        $pdf->Cell(105,7,'ALAMAT : '.$kota->alamat, 'LRB', 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(60,7,$data['records']->no_daftar, 'TBR', 1, 'C');

        $pdf->SetFont('Times', 'B', 14);
        $pdf->Cell(190,12,'TANDA TERIMA PENDAFTARAN', 'LR', 1, 'C');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(130,12,'No. Formulir', 'L', 0, 'R');
        $pdf->Cell(10,12,':', '', 0, 'C');
        $pdf->Cell(50,12,$data['records']->no_daftar, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Nama', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->nama_pendaftar, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Alamat', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->jalan.'( Kel. '.$data['records']->nm_kel.' Kec. '.$data['records']->nm_kec.' Kab.'.$data['records']->kabupaten.' Kode Pos '.$data['records']->kode_pos.' )', 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'No. Telpon', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->no_telpon, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Keterangan', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->keterangan, 'R', 1, 'L');

        $pdf->Cell(190,10,'', 'LR', 1, 'L');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(95,7,'Petugas Penerima', 'L', 0, 'C');
        $pdf->Cell(95,7,$kota->ibukota.', '.tgl_format(date("Y-m-d")), 'R', 1, 'C');

        $pdf->Cell(95,7,'', 'L', 0, 'C');
        $pdf->Cell(95,7,'Yang Mendaftar', 'R', 1, 'C');

        $pdf->Cell(95,20,'', 'L', 0, 'C');
        $pdf->Cell(95,20,'', 'R', 1, 'C');

        $pdf->Cell(95,7,'____________________________________', 'L', 0, 'C');
        $pdf->Cell(95,7,'( '.$data['records']->nama_pendaftar.' )', 'R', 1, 'C');

        $pdf->Cell(95,7,'NIP', 'LB', 0, 'C');
        $pdf->Cell(95,7,'', 'RB', 1, 'C');

        $pdf->Cell(60,12,'.........................................................', 'B', 0, 'C');
        $pdf->Cell(70,12,'Gunting Di sini', 'B', 0, 'C');
        $pdf->Cell(60,12,'.........................................................', 'B', 1, 'C');

        $pdf->SetFont('Times', 'B', 11);

        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,10,10,20,20);

        $pdf->Cell(25,13,'', 'LTR', 0, 'C');
        $pdf->Cell(105,13,$kota->nm_pemda, 'LTR', 0, 'C');

        $pdf->Cell(60,13,'Nomor Formulir', 'TR', 1, 'C');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(25,7,'', 'LBR', 0, 'C');
        $pdf->Cell(105,7,'ALAMAT : '.$kota->alamat, 'LRB', 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(60,7,$data['records']->no_daftar, 'TBR', 1, 'C');

        $pdf->SetFont('Times', 'B', 14);
        $pdf->Cell(190,12,'TANDA TERIMA PENDAFTARAN', 'LR', 1, 'C');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(130,12,'No. Formulir', 'L', 0, 'R');
        $pdf->Cell(10,12,':', '', 0, 'C');
        $pdf->Cell(50,12,$data['records']->no_daftar, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Nama', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->nama_pendaftar, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Alamat', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->jalan.'( Kel. '.$data['records']->nm_kel.' Kec. '.$data['records']->nm_kec.' Kab.'.$data['records']->kabupaten.' Kode Pos '.$data['records']->kode_pos.' )', 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'No. Telpon', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->no_telpon, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(25,7,'Keterangan', 'L', 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(3,7,':', '', 0, 'C');
        $pdf->Cell(162,7,$data['records']->keterangan, 'R', 1, 'L');

        $pdf->Cell(190,10,'', 'LR', 1, 'L');


        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(95,7,'Petugas Penerima', 'L', 0, 'C');
        $pdf->Cell(95,7,$kota->ibukota.', '.tgl_format(date("Y-m-d")), 'R', 1, 'C');

        $pdf->Cell(95,7,'', 'L', 0, 'C');
        $pdf->Cell(95,7,'Yang Mendaftar', 'R', 1, 'C');

        $pdf->Cell(95,20,'', 'L', 0, 'C');
        $pdf->Cell(95,20,'', 'R', 1, 'C');

        $pdf->Cell(95,7,'____________________________________', 'L', 0, 'C');
        $pdf->Cell(95,7,'( '.$data['records']->nama_pendaftar.' )', 'R', 1, 'C');

        $pdf->Cell(95,7,'NIP', 'LB', 0, 'C');
        $pdf->Cell(95,7,'', 'RB', 1, 'C');



        $pdf->Output("Formulir Pendaftaran.pdf","I");
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Pendaftaran Report';

        // query
        $data['report']  = $this->db->query("SELECT no_daftar, nama_pendaftar, tgl_terdaftar, keterangan from wp_data_umum where status = '1'")->result();

        $data['namaFile']   ='Pendaftaran_Report_'.date('Y-m-d');
        $data['title']      ='Formulir Pendaftaran Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['No Daftar', '30'], ['Nama', '30'], ['Tgl Mendaftar', '30'], ['Keterangan', '30']
        ];

        $this->load->view('pajak/export',$data);
    }


}
