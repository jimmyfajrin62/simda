<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_retribusi extends Admin_Controller
{
    private $prefix         = 'pendaftaran/pendaftaran_retribusi';
    private $url            = 'pendaftaran/pendaftaran_retribusi';
    private $table_db       = 'wr_data_umum';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_pendaftaran_retribusi', 'mdb');
    }

	public function index()
	{
        $data['pagetitle']  = 'Pendaftaran Retribusi';
        $data['subtitle']   = 'manage pendaftaran retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendaftaran Retribusi' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendaftaran/retribusi/index', $data, $js, $css );
	}

    public function show_add()
    {
        $data['pagetitle']  = 'Pendaftaran Retribusi';
        $data['subtitle']   = 'manage pendaftaran retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['np']         = $this->mdb->no_npwpd('R');
        $data['kec']        = $this->mdb->get_kec();

        $data['breadcrumb'] = [ 'Pendaftaran Retribusi' => $this->url, 'Form' => $this->url.'/show_add' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'pendaftaran/retribusi/form', $data, $js );
    }

    public function show_edit( $id )
    {

        $data['pagetitle']  = 'Pendaftaran Retribusi';
        $data['subtitle']   = 'manage pendaftaran retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data_umum          = $this->mdb->get_data_umum($id);
        $data['data']       = $data_umum;
        $data['kec']        = $this->mdb->get_kec();
        $data['kel']        = $this->mdb->get_kel($data_umum->kd_kec);
        $data['kel2']       = $this->mdb->get_kel($data_umum->kelurahan1);

        $data['breadcrumb'] = [ 'Pendaftaran Retribusi' => $this->url, 'Form' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation' ];
        $css['css']         = NULL;

        $this->template->display( 'pendaftaran/retribusi/form', $data, $js );
    }

    public function get_kelurahan()
    {
        $table_kel          = 'ref_kelurahan';

        $id = $this->input->post();
        $data['kelurahan']  = $this->m_global->get($table_kel, null, $id, 'kel_id, kd_kec, kd_kel, nm_kel');

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function action_form($id = NULL)
    {
        // echo $id; exit();
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->form_validation->set_rules('no_daftar', 'No Pendaftaran', 'trim|required');
        $this->form_validation->set_rules('nama_pendaftar', 'Nama Pendaftaran', 'trim|required');
        $this->form_validation->set_rules('jns_retribusi', 'Jenis Retribusi', 'trim|required');
        $this->form_validation->set_rules('jalan', 'Jalan', 'trim|required');
        $this->form_validation->set_rules('rtrw', 'RT/RW', 'trim|required');
        $this->form_validation->set_rules('kd_kel', 'Kelurahan', 'trim|required');
        $this->form_validation->set_rules('kd_kec', 'Kecamatan', 'trim|required');
        $this->form_validation->set_rules('kabupaten', 'Kabupaten', 'trim|required');
        $this->form_validation->set_rules('kode_pos', 'Kode Pos', 'trim|required');
        $this->form_validation->set_rules('no_telpon', 'No Telepon', 'trim|required');
        $this->form_validation->set_rules('status_wn', 'Kewarganegaraan', 'trim|required');
        $this->form_validation->set_rules('tanda_bukti', 'Tanda Bukti', 'trim|required');
        $this->form_validation->set_rules('no_tanda_bukti', 'No Tanda Bukti', 'trim|required');
        $this->form_validation->set_rules('tgl_tanda_bukti', 'Tgl Tanda Bukti', 'trim|required');
        $this->form_validation->set_rules('no_kk', 'No Kartu Keluarga', 'trim');
        $this->form_validation->set_rules('tgl_kk', 'Tgl Kartu Keluarga', 'trim');
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim');
        $this->form_validation->set_rules('pekerjaan_lainnya', 'Pekerjaan Lainnya', 'trim');
        $this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim');
        $this->form_validation->set_rules('alamat_instansi', 'Alamat Instansi', 'trim');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');
        $this->form_validation->set_rules('nama_pemilik', 'Pemilik Badan', 'trim');
        $this->form_validation->set_rules('jabatan', 'Jabatan', 'trim');
        $this->form_validation->set_rules('nama_pimpinan', 'Pimpinan', 'trim');
        $this->form_validation->set_rules('jalan1', 'Jalan', 'trim');
        $this->form_validation->set_rules('rtrw1', 'RT/RW', 'trim');
        $this->form_validation->set_rules('kelurahan1', 'Kelurahan', 'trim');
        $this->form_validation->set_rules('kecamatan1', 'Kecamatan', 'trim');
        $this->form_validation->set_rules('kabupaten1', 'Kabupaten', 'trim');
        $this->form_validation->set_rules('kode_pos1', 'Kode Pos', 'trim');
        $this->form_validation->set_rules('no_telpon1', 'No Telepon', 'trim');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'no_daftar']         = $this->input->post('no_daftar');
            $data[$this->table_prefix.'nama_pendaftar']    = $this->input->post('nama_pendaftar');
            $data[$this->table_prefix.'jns_retribusi']     = $this->input->post('jns_retribusi');
            $data[$this->table_prefix.'jalan']             = $this->input->post('jalan');
            $data[$this->table_prefix.'rtrw']              = $this->input->post('rtrw');
            $data[$this->table_prefix.'kd_kel']            = $this->input->post('kd_kel');
            $data[$this->table_prefix.'kd_kec']            = $this->input->post('kd_kec');
            $data[$this->table_prefix.'kabupaten']         = $this->input->post('kabupaten');
            $data[$this->table_prefix.'kode_pos']          = $this->input->post('kode_pos');
            $data[$this->table_prefix.'no_telpon']         = $this->input->post('no_telpon');
            $data[$this->table_prefix.'status_wn']         = $this->input->post('status_wn');
            $data[$this->table_prefix.'tanda_bukti']       = $this->input->post('tanda_bukti');
            $data[$this->table_prefix.'no_tanda_bukti']    = $this->input->post('no_tanda_bukti');
            $data[$this->table_prefix.'tgl_tanda_bukti']   = $this->input->post('tgl_tanda_bukti');
            $data[$this->table_prefix.'no_kk']             = $this->input->post('no_kk');
            $data[$this->table_prefix.'tgl_kk']            = $this->input->post('tgl_kk');
            $data[$this->table_prefix.'pekerjaan']         = $this->input->post('pekerjaan');
            $data[$this->table_prefix.'pekerjaan_lainnya'] = $this->input->post('pekerjaan_lainnya');
            $data[$this->table_prefix.'nama_instansi']     = $this->input->post('nama_instansi');
            $data[$this->table_prefix.'alamat_instansi']   = $this->input->post('alamat_instansi');
            $data[$this->table_prefix.'keterangan']        = $this->input->post('keterangan');
            $data[$this->table_prefix.'nama_pemilik']      = $this->input->post('nama_pemilik');
            $data[$this->table_prefix.'jabatan']           = $this->input->post('jabatan');
            $data[$this->table_prefix.'nama_pimpinan']     = $this->input->post('nama_pimpinan');
            $data[$this->table_prefix.'jalan1']            = $this->input->post('jalan1');
            $data[$this->table_prefix.'rtrw1']             = $this->input->post('rtrw1');
            $data[$this->table_prefix.'kelurahan1']        = $this->input->post('kelurahan1');
            $data[$this->table_prefix.'kecamatan1']        = $this->input->post('kecamatan1');
            $data[$this->table_prefix.'kabupaten1']        = $this->input->post('kabupaten1');
            $data[$this->table_prefix.'kode_pos1']         = $this->input->post('kode_pos1');
            $data[$this->table_prefix.'no_telpon1']        = $this->input->post('no_telpon1');

            if ($id == NULL) {
                $jns_retribusi = $this->input->post('jns_retribusi');
                $kd_kec    = $this->input->post('kd_kec');
                $kd_kel    = $this->input->post('kd_kel');
                $npwpd     = $this->mdb->no_npwpd("R . $jns_retribusi . ", " . $kd_kec . $kd_kel");

                $data[$this->table_prefix.'id']            = $this->mdb->auto_id();
                $data[$this->table_prefix.'npwpd']         = $npwpd;
                $data[$this->table_prefix.'tgl_terdaftar'] = date("Y-m-d");

                $result  = $this->m_global->insert( $this->table_db, $data );
            }
            else{
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_daftar'      => $this->table_prefix.'no_daftar',
            'nama_pendaftar' => $this->table_prefix.'nama_pendaftar',
            'tgl_terdaftar'  => $this->table_prefix.'tgl_terdaftar',
            'keterangan'     => $this->table_prefix.'keterangan',
            'lastupdate'     => $this->table_prefix.'lastupdate'
        ];

        $where      = NULL;
        $where_e    = 'jns_retribusi <> 0';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status =']    = '1';
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);

        $records         = array();
        $records["data"] = array();

        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id, status,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->no_daftar,
                strtoupper ($rows->nama_pendaftar),
                $rows->tgl_terdaftar == NULL || $rows->tgl_terdaftar == '0000-00-00 00:00:00' ? '' : tgl_format($rows->tgl_terdaftar),
                strtoupper($rows->keterangan),
                tgl_format($rows->lastupdate),
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/'.
                    ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"' ) ).
                    ' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green-seagreen' ).
                    '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).
                    '" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id.'/99/'.($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
            );

            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function change_status( $status, $where )
    {
        $data[ $this->table_prefix.'status' ]   = $status;

        $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    }

    public function change_status_by( $id, $status, $stat = FALSE )
    {
        if ( $stat )
        {
            $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );
        }
        else
        {
            $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id' => $id]);
        }

        if ( $result )
        {
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }


    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'User Report';

        $where_e = null;
        $where = null;

        if ($this->input->get('name') != '') $where['name LIKE'] = '%'.$this->input->get('name').'%';
        if ($this->input->get('email') != '') $where['start LIKE'] = '%'.$this->input->get('email').'%';
        if ($this->input->get('lastupdate') != '') $where['lastupdate LIKE'] = '%'.$this->input->get('lastupdate').'%';

        $select = 'user_full_name, user_name, user_email, user_lastupdate';

        $data['report']     = $this->m_global->get($this->table_db, NULL, $where, $select);
        $data['namaFile']   ='User_Report_'.date('Y-m-d');
        $data['title']      ='User Report';
        $data['title_2']    ='Periode '.date('Y-m-d');
        $data['header']     = [['No', '8'],['Fullname', '30'],['Name', '30'],['Email', '30'],['Lastupdate', '30']];

        $this->load->view('export',$data);
    }


    public function import_excel()
    {
        $config['upload_path']   = './assets/upload/import/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite']     = TRUE;
        $config['file_name']     = date('Ymdhis').'-area';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')){
            $report['status']   = '0';
            $report['message']  = 'Invalid file uploaded!';
            $report['err']      = $this->upload->display_errors().display($_FILES);
        }
        else{
            $data          = array('upload_data' => $this->upload->data());
            $inputFileName = $data['upload_data']['full_path'];

            $this->load->library('excel');

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            }
            catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            // Read Sheet by Name
            // $sheetName = 'Area';
            // $sheet = $objPHPExcel->getSheetByName($sheetName);

            // Read Sheet By Index
            $sheet = $objPHPExcel->getSheet(0);

            if(!empty($sheet)){
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $startColumn   = 'B';
                $endColumn     = '5';

                $jml           = 0;
                $jmlA          = 0;

                for ($row = $endColumn; $row <= $highestRow; $row++){
                    $dataSheet = $sheet->rangeToArray($startColumn . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $param     = [];
                    $param[$this->table_prefix.'name']        = $dataSheet[0][1];
                    $param[$this->table_prefix.'email']       = $dataSheet[0][2];
                    $param[$this->table_prefix.'salt']        = generate_salt();
                    $param[$this->table_prefix.'pass']        = md5_mod($dataSheet[0][3], $param[$this->table_prefix.'salt'] );
                    $param[$this->table_prefix.'jk']          = $dataSheet[0][4] == 'L' ? '0': '1';
                    $param[$this->table_prefix.'birth_date']  = date('Y-m-d', strtotime($dataSheet[0][5]));
                    $param[$this->table_prefix.'birth_place'] = $dataSheet[0][6];
                    $param[$this->table_prefix.'religion']    = @religion_to_index($dataSheet[0][7]);
                    $param[$this->table_prefix.'biaya']       = str_replace(',', '', $dataSheet[0][8]);
                    $param[$this->table_prefix.'level']       = $dataSheet[0][9] == 'Admin' ? '0' : '1';

                    $validate = $this->m_global->validation($this->table_db,  [$this->table_prefix.'email' => $param[$this->table_prefix.'email']] );

                    if($validate){
                        $this->m_global->insert($this->table_db, $param);
                        $jml++;
                    }
                    else {
                        $jmlA++;
                    }
                }

                $report['status']   = '1';
                $report['message']  = 'Successfully imported file!</br>Record add : <strong>'.$jml.'</strong></br>Another Record Exist : <strong>'.$jmlA.'</strong>';
            }
            else {
                $report['status']   = '0';
                $report['message']  = 'Sheet was not found!';
            }

            unlink($data['upload_data']['full_path']);
        }

        echo json_encode($report);
    }

}
