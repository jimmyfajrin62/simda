<?php
defined('BASEPATH') or exit('No direct script access allowed');
 
class M_pendaftaran_pajak extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_umum($id)
    {
        $query = $this->db->query("select * from wp_data_umum where id = $id");
        return $query->row();
    }

    public function get_kec()
    {
        $query = $this->db->query("select kd_kec, nm_kec from ref_kecamatan");
        return $query->result();
    }

    public function get_kel($kec)
    {
        if ($kec == NULL) {
            $query = $this->db->query("select * from ref_kelurahan");
            return $query->result();
        }
        else {
            $query = $this->db->query("select * from ref_kelurahan where kd_kec = $kec");
            return $query->result();
        }
    }

    public function auto_id()
    {
        $query = $this->db->query("select id from wp_data_umum order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_npwpd($prefix=null, $sufix=null)
    {
        $query = $this->db->query("select id from wp_data_umum order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 7;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }

    public function get_wp_all()
    {
        $query = $this->db->query("select * from wp_data_umum");
        return $query->result();
    }

    public function get_wp($id)
    {
        $query = $this->db->query("select wp_data_umum.*,ref_kelurahan.nm_kel,ref_kecamatan.nm_kec
                                    from wp_data_umum
                                    left join ref_kelurahan on wp_data_umum.kd_kec = ref_kelurahan.kd_kec
                                    left join ref_kecamatan on wp_data_umum.kd_kel = ref_kelurahan.kd_kel
                                    where id = $id");
        return $query->row();
    }

    public function kota()
    {
        $query = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())");
        return $query->row();

    }
}
