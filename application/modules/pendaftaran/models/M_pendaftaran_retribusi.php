<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_pendaftaran_retribusi extends CI_Model {


    public function __construct()
    {
        parent::__construct();
    }

    public function get_data_umum($id)
    {
        $query = $this->db->query("select * from wr_data_umum where id = $id");
        return $query->row();
    }

    public function get_kec()
    {
        $query = $this->db->query("select kd_kec, nm_kec from ref_kecamatan");
        return $query->result();
    }

    public function get_kel($kec)
    {
        if ($kec == NULL) {
            $query = $this->db->query("select * from ref_kelurahan");
            return $query->result();
        }
        else {
            $query = $this->db->query("select * from ref_kelurahan where kd_kec = $kec");
            return $query->result();
        }
    }

    public function auto_id()
    {
        $query = $this->db->query("select id from wr_data_umum order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_npwpd($prefix=null, $sufix=null)
    {
        $query = $this->db->query("select id from wr_data_umum order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 7;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }


}

/* End of file M_pendaftaran_retribusi.php */
/* Location: ./application/modules/pendaftaran/models/M_pendaftaran_retribusi.php */
