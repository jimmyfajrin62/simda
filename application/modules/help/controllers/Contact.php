<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	private $prefix         = 'help/contact';
	private $url            = 'help/contact';
	private $path           = 'help/contact/';
	// private $table_db       = 'menus';
	// private $table_prefix   = 'menu_';
	private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'Contact';
		$data['subtitle']   = 'User';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$data['breadcrumb'] = [ 'Contact' => $this->url ];

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

		$this->template->display($this->path.'index', $data, $js, $css);
	}

}

/* End of file Config_menu.php */
/* Location: ./application/modules/config/controllers/Config_menu.php */
