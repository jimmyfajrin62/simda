<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_menu extends Admin_Controller {

	private $prefix         = 'config/config_menu';
	private $url            = 'config/config_menu';
	private $table_db       = 'menus';
	private $table_prefix   = 'menu_';
	private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'Menu';
		$data['subtitle']   = 'manage menu';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$data['breadcrumb'] = [ 'Menu' => $this->url ];

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

		$this->template->display(strtolower(__CLASS__).'/index', $data, $js, $css );
	}

	public function show_add()
	{
		$data['pagetitle']  = 'Menu';
		$data['subtitle']   = 'add menu';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$getParent 			= [
								'table' => $this->table_db,
								'where' => ['menu_status' => '1']
							  ];

		$data['parent'] 	= $this->m_global_new->get($getParent);

		$data['breadcrumb'] = [ 'Menu' => $this->url, 'Add' => $this->url.'/show_add' ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/add', $data, $js );
	}

	public function show_edit( $id )
	{
		$conf_records['table'] = $this->table_db;
		$conf_records['where'] = ['menu_id' => $id];

		$data['records']    = $this->m_global_new->get($conf_records)[0];

		$data['pagetitle']  = 'Menu';
		$data['subtitle']   = 'manage menu';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;
		$data['id']         = $id;

		$getParent 			= [
								'table' => $this->table_db,
								'where' => ['menu_status' => '1']
							  ];

		$data['parent'] 	= $this->m_global_new->get($getParent);

		$data['breadcrumb'] = [ 'Menu' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/edit', $data, $js );
	}

	public function show_list_menu()
	{
		$data['pagetitle']  = 'Menu';
		$data['subtitle']   = 'List menu';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$data['breadcrumb'] = [ 'Menu' => $this->url, 'List' => $this->url.'/show_list_menu' ];

		$css['css'] 		= ['jquery.nestable'];
		$js['js'] 			= ['jquery.nestable'];

		$this->template->display( strtolower(__CLASS__).'/list_menu', $data, $js, $css );
	}

	public function action_add()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('link', 'Link', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('icon', 'Icon', 'trim');
		$this->form_validation->set_rules('position', 'Position', 'trim');

		if($this->input->post('position') == '0') {
			$this->form_validation->set_rules('parent', 'Parent', 'trim|required');
		} else {
			$this->form_validation->set_rules('parent', 'Parent', 'trim');
		}

		if ( $this->form_validation->run( $this ) )
		{
			$parent = ($this->input->post('position') == '0') ? $this->input->post('parent') : '0';
			$number = $this->m_global_new->get(
				[
					'table'  => 'menus',
					'where'  => ['menu_parent' => $parent],
					'select' => 'MAX(menu_number) as number'
				]
			)[0]->number;

			$data[$this->table_prefix.'name']           = $this->input->post('name');
			$data[$this->table_prefix.'link']           = $this->input->post('link');
			$data[$this->table_prefix.'parent']         = $parent;
			$data[$this->table_prefix.'number']         = ($number + 1);
			$data[$this->table_prefix.'icon']           = $this->input->post('icon');
			$data[$this->table_prefix.'status']         = $this->input->post('status');
			$data[$this->table_prefix.'created_by']     = login_data('user_id');
			$data[$this->table_prefix.'created_date']   = date('Y-m-d');

			$insert = [
				'table' => $this->table_db,
				'datas' => $data
			];

			$result  = $this->m_global_new->insert($insert);

			if ( $result['status'] )
			{
				$data['status']     = 1;
				$data['message']    = 'Successfully add Menu with Name <strong>'.$this->input->post('name').'</strong>';

				echo json_encode( $data );

			} else {

				$data['status']     = 0;
				$data['message']    = 'Failed add Menu with Name <strong>'.$this->input->post('name').'</strong>';
				if(ENVIRONMENT == 'development')
					$data['error']  = $this->db->error();

				echo json_encode( $data );

			}

		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function action_edit( $id )
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('link', 'Link', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('icon', 'Icon', 'trim');
		$this->form_validation->set_rules('position', 'Position', 'trim');

		if($this->input->post('position') == '0') {
			$this->form_validation->set_rules('parent', 'Parent', 'trim|required');
		} else {
			$this->form_validation->set_rules('parent', 'Parent', 'trim');
		}

		if ( $this->form_validation->run( $this ) )
		{
			$parent = ($this->input->post('position') == '0') ? $this->input->post('parent') : '0';

			$data[$this->table_prefix.'name']           = $this->input->post('name');
			$data[$this->table_prefix.'link']           = $this->input->post('link');
			$data[$this->table_prefix.'parent']         = $parent;
			$data[$this->table_prefix.'icon']           = $this->input->post('icon');
			$data[$this->table_prefix.'status']         = $this->input->post('status');
			$data[$this->table_prefix.'created_by']     = login_data('user_id');
			$data[$this->table_prefix.'created_date']   = date('Y-m-d');

			$update = [
				'table' => $this->table_db,
				'datas'	=> $data,
				'where'	=> ['menu_id' => $id]
			];

			$result = $this->m_global_new->update($update);

			if ( $result )
			{
				$data['status']     = 1;
				$data['message']    = 'Successfully edit Menu with Name <strong>'.$this->input->post('name').'</strong>';

				echo json_encode( $data );

			} else {

				$data['status']     = 0;
				$data['message']    = 'Failed edit Menu with Name <strong>'.$this->input->post('name').'</strong>';
				if(ENVIRONMENT == 'development')
					$data['error']  = $this->db->error();

				echo json_encode( $data );

			}

		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function select()
	{
		// jika action checkbox
		if ( @$_REQUEST['customActionType'] == 'group_action' )
		{
			$aChk = [0, 1, 99];

			if ( in_array( @$_REQUEST['customActionName'], $aChk) )
			{
				$this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

				$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
				$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
			}
		}

		$aCari = [
			'name'      => $this->table_prefix.'name',
			'link'      => $this->table_prefix.'link',
			'status'	=> $this->table_prefix.'status'
		];

		$where      = NULL;
		$where_e    = NULL;

		if ( @$_REQUEST['action'] == 'filter')
		{
			$where = [];
			foreach ( $aCari as $key => $value )
			{
				if ( $_REQUEST[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $_REQUEST[$key]);
						$where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {

						$where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

					}
				}
			}
		}

		if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
			$where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
		} else {
			$where[$this->table_prefix.'status <>']    = '99';
		}

		$keys   = array_keys( $aCari );
		@$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

		$arr_config['table'] 	= $this->table_db;
		$arr_config['where'] 	= $where;
		$arr_config['where_e'] 	= $where_e;

		$iTotalRecords  = $this->m_global_new->count($arr_config);
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart  = intval($_REQUEST['start']);
		$sEcho          = intval($_REQUEST['draw']);

		$records        = array();
		$records["data"]= array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$select = 'menu_id,'.implode(',' , $aCari);

		$arr_config['select'] 	= $select;
		$arr_config['order'] 	= $order;
		$arr_config['start'] 	= $iDisplayStart;
		$arr_config['show'] 	= $iDisplayLength;

		$result = $this->m_global_new->get($arr_config);

		$arr_status = [
			'0' => '<span class="label label-warning">InActive</span>',
			'1' => '<span class="label label-primary">Active</span>',
			'99' => '<span class="label label-danger">Delete</span>'
		];

		$i = 1 + $iDisplayStart;
		foreach ( $result as $rows )
		{
			$records["data"][] = array(
			  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->menu_id.'"/><span></span></label>',
			  $i,

			  $rows->menu_name,
			  $rows->menu_link,
			  $arr_status[$rows->menu_status],
			  '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->menu_id.'/'.($rows->menu_status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->menu_status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->menu_status == 0 ? 'InActive' : ($rows->menu_status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->menu_status == 0 ? '-eye-slash' : ($rows->menu_status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
			  '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->menu_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
			  '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->menu_id.'/99/'.($rows->menu_status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode( $records );
	}

	public function change_status( $status, $where )
	{
		$data[ $this->table_prefix.'status' ]   = $status;

		$update = [
			'table' 	=> $this->table_db,
			'datas' 	=> $data,
			'where_e' 	=> $where
		];

		$result = $this->m_global_new->update($update);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		if ( $stat )
		{
			$delete = [
				'table' => $this->table_db,
				'where' => [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->delete($delete);
		} else {
			$update = [
				'table' 	=> $this->table_db,
				'datas' 	=> [$this->table_prefix.'status' => $status],
				'where' 	=> [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->update($update);

		}

		if ( $result )
		{
			$data['status'] = 1;

		} else {

			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

	public function save_list()
	{
		$post 	= $this->input->post('data');
		$data 	= [];

		foreach ($post as $key => $val) {
			$data[] = [
				'parent' => '0',
				'no' 	 => ($key+1),
				'id'	 => $val['id']
			];

			if(isset($val['children'])) {
				$data = array_merge($data, $this->_save_list($val['children'], $val['id']));
			}
		}

		$this->db->trans_begin();

		foreach ($data as $k => $v) {
			$data_update = [
				'table' => $this->table_db,
				'datas' => [
					'menu_parent' => $v['parent'],
					'menu_number' => $v['no']
				],
				'where' => ['menu_id' => $v['id']]
			];

			$this->m_global_new->update($data_update);
		}

		if ($this->db->trans_status() === TRUE) {
			$this->db->trans_commit();

			$result['status']     = 1;
			$result['message']    = 'Successfully edit List Menu';

			echo json_encode( $result );
		} else {
			$this->db->trans_rollback();

			$result['status']     = 0;
			$result['message']    = 'Failed edit List Menu';

			echo json_encode( $result );
		}
	}

	private function _save_list($datas, $id) {
		$data 	= [];

		foreach ($datas as $key => $val) {
			$data[] = [
				'parent' => $id,
				'no' 	 => ($key+1),
				'id'	 => $val['id']
			];

			if(isset($val['children'])) {
				$data = array_merge($data, $this->_save_list($val['children'], $val['id']));
			}
		}

		return $data;
	}

}

/* End of file Config_menu.php */
/* Location: ./application/modules/config/controllers/Config_menu.php */
