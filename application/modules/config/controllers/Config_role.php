<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_role extends Admin_Controller {

	private $prefix         = 'config/config_role';
	private $url            = 'config/config_role';
	private $table_db       = 'roles';
	private $table_prefix   = 'role_';
	private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'Role';
		$data['subtitle']   = 'manage role';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$data['breadcrumb'] = [ 'Role' => $this->url ];

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

		$this->template->display(strtolower(__CLASS__).'/index', $data, $js, $css );
	}

	public function show_add()
	{
		$data['pagetitle']  = 'Role';
		$data['subtitle']   = 'add role';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$data['breadcrumb'] = [ 'Role' => $this->url, 'Add' => $this->url.'/show_add' ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/add', $data, $js );
	}

	public function show_edit( $id )
	{
		$conf_records['table'] = $this->table_db;
		$conf_records['where'] = ['role_id' => $id];

		$data['records']    = $this->m_global_new->get($conf_records)[0];
		
		$data['pagetitle']  = 'Role';
		$data['subtitle']   = 'manage role';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;
		$data['id']         = $id;

		$data['breadcrumb'] = [ 'Role' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/edit', $data, $js );
	}

	public function action_add()
	{
		$post 	= $this->input->post();
		$this->form_validation->set_rules('name', 'Role Name', 'trim|required|is_unique[roles.role_name]', array('is_unique' => 'Nama Role Harus Unique'));
		$this->form_validation->set_rules('status', 'Role Status', 'trim|required');
		$this->form_validation->set_rules('access', 'Role Access', 'trim|required');

		if ( $this->form_validation->run( $this ) )
		{
			$role_data 	= [
						'role_name' 		=> $post['name'],
						'role_access' 		=> $post['access'],
						'role_status'		=> $post['status'],
						'role_created_by'	=> login_data('user_id'),
						'role_created_date'	=> date('Y-m-d')
					  ];

			$insert = [
				'table' => $this->table_db,
				'datas' => $role_data
			];

			$result  = $this->m_global_new->insert($insert);

			if ( $result['status'] )
			{
				$data['status']     = 1;
				$data['message']    = 'Successfully add Role with Name <strong>'.$this->input->post('name').'</strong>';

				echo json_encode( $data );

			} else {

				$data['status']     = 0;
				$data['message']    = 'Failed add Role with Name <strong>'.$this->input->post('name').'</strong>';
				if(ENVIRONMENT == 'development')
					$data['error']  = $this->db->error();

				echo json_encode( $data );

			}

		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function action_edit( $id )
	{
		$post 	= $this->input->post();
		$this->form_validation->set_rules('name', 'Role Name', 'trim|required');
		$this->form_validation->set_rules('status', 'Role Status', 'trim|required');
		$this->form_validation->set_rules('access', 'Role Access', 'trim|required');

		if ( $this->form_validation->run( $this ) )
		{
			$arrConfig = [
				'table' => $this->table_db,
				'where' => [
							'role_name' 	=> $post['name'],
							'role_id <>' 	=> $id
						   ]
			];

			$check = $this->m_global_new->count($arrConfig);

			if ($check == '0') {
				$role_data 	= [
							'role_name' 		=> $post['name'],
							'role_access' 		=> $post['access'],
							'role_status'		=> $post['status'],
							'role_created_by'	=> login_data('user_id'),
						  ];

				$update = [
					'table' => $this->table_db,
					'datas'	=> $role_data,
					'where'	=> ['role_id' => $id]
				];

				$result = $this->m_global_new->update($update);

				if ( $result )
				{
					$data['status']     = 1;
					$data['message']    = 'Successfully edit Role with Name <strong>'.$this->input->post('name').'</strong>';

					echo json_encode( $data );

				} else {

					$data['status']     = 0;
					$data['message']    = 'Failed edit Role with Name <strong>'.$this->input->post('name').'</strong>';
					if(ENVIRONMENT == 'development')
						$data['error']  = $this->db->error();

					echo json_encode( $data );

				}
			} else {
				$data['status']     = 0;
				$data['message']    = 'The Role Name field must contain a unique value.';

				echo json_encode( $data );
			}
		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function select()
	{
		// jika action checkbox
		if ( @$_REQUEST['customActionType'] == 'group_action' )
		{
			$aChk = [0, 1, 99];

			if ( in_array( @$_REQUEST['customActionName'], $aChk) )
			{
				$this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

				$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
				$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
			}
		}

		$aCari = [
			'name'      => $this->table_prefix.'name',
		];

		$where      = NULL;
		$where_e    = NULL;

		if ( @$_REQUEST['action'] == 'filter')
		{
			$where = [];
			foreach ( $aCari as $key => $value )
			{
				if ( $_REQUEST[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $_REQUEST[$key]);
						$where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {

						$where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

					}
				}
			}
		}

		if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
			$where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
		} else {
			$where[$this->table_prefix.'status <>']    = '99';
		}

		$keys   = array_keys( $aCari );
		@$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

		$arr_config['table'] 	= $this->table_db;
		$arr_config['where'] 	= $where;
		$arr_config['where_e'] 	= $where_e;

		$iTotalRecords  = $this->m_global_new->count($arr_config);
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart  = intval($_REQUEST['start']);
		$sEcho          = intval($_REQUEST['draw']);

		$records        = array();
		$records["data"]= array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$select = 'role_id, , role_status,'.implode(',' , $aCari);

		$arr_config['select'] 	= $select;
		$arr_config['order'] 	= $order;
		$arr_config['start'] 	= $iDisplayStart;
		$arr_config['show'] 	= $iDisplayLength;

		$result = $this->m_global_new->get($arr_config);

		$arr_status = [
			'0' => '<span class="label label-warning">InActive</span>',
			'1' => '<span class="label label-primary">Active</span>',
			'99' => '<span class="label label-danger">Delete</span>'
		];

		$i = 1 + $iDisplayStart;
		foreach ( $result as $rows )
		{
			$records["data"][] = array(
			  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->role_id.'"/><span></span></label>',
			  $i,

			  $rows->role_name,
			  $arr_status[$rows->role_status],
			  '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->role_id.'/'.($rows->role_status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->role_status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->role_status == 0 ? 'InActive' : ($rows->role_status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->role_status == 0 ? '-eye-slash' : ($rows->role_status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
			  '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->role_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
			  '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->role_id.'/99/'.($rows->role_status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode( $records );
	}

	public function change_status( $status, $where )
	{
		$data[ $this->table_prefix.'status' ]   = $status;

		$update = [
			'table' 	=> $this->table_db,
			'datas' 	=> $data,
			'where_e' 	=> $where
		];

		$result = $this->m_global_new->update($update);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		if ( $stat )
		{
			$delete = [
				'table' => $this->table_db,
				'where' => [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->delete($delete);
		} else {
			$update = [
				'table' 	=> $this->table_db,
				'datas' 	=> [$this->table_prefix.'status' => $status],
				'where' 	=> [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->update($update);

		}

		if ( $result )
		{
			$data['status'] = 1;

		} else {

			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

}

/* End of file Config_role.php */
/* Location: ./application/modules/config/controllers/Config_role.php */
