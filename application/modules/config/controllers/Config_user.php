<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Config_user extends Admin_Controller {

	private $prefix         = 'config/config_user';
	private $url            = 'config/config_user';
	private $table_db       = 'user';
	private $table_prefix   = 'user_';
	private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct() {
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'User';
		$data['subtitle']   = 'manage user';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$get_rule 			= [
								'table' 	=> 'roles',
								'where' 	=> ['role_status' => '1'],
								'select'	=> 'role_id, role_name'
							  ];
		$data['roles']		= $this->m_global_new->get($get_rule);

		$data['breadcrumb'] = [ 'User' => $this->url ];

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

		$this->template->display(strtolower(__CLASS__).'/index', $data, $js, $css );
	}

	public function show_add()
	{
		$data['pagetitle']  = 'User';
		$data['subtitle']   = 'add user';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

		$get_rule 			= [
								'table' 	=> 'roles',
								'where' 	=> ['role_status' => '1'],
								'select'	=> 'role_id, role_name'
							  ];
		$data['roles']		= $this->m_global_new->get($get_rule);

		$data['breadcrumb'] = [ 'User' => $this->url, 'Add' => $this->url.'/show_add' ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/add', $data, $js );
	}

	public function show_edit( $id )
	{
		$conf_records['table'] = $this->table_db;
		$conf_records['where'] = ['user_id' => $id];

		$data['records']    = $this->m_global_new->get($conf_records)[0];

		$data['pagetitle']  = 'User';
		$data['subtitle']   = 'manage user';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;
		$data['id']         = $id;

		$get_rule 			= [
								'table' 	=> 'roles',
								'where' 	=> ['role_status' => '1'],
								'select'	=> 'role_id, role_name'
							  ];
		$data['roles']		= $this->m_global_new->get($get_rule);

		$data['breadcrumb'] = [ 'User' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/edit', $data, $js );
	}


	public function action_add()
	{
		$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('name', 'Username', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('role', 'Role', 'trim|required');

		if ( $this->form_validation->run( $this ) )
		{
			if ( ! empty( $_FILES ) )
			{
				$config['upload_path']   = './assets/upload/';
				$config['allowed_types'] = 'gif|jpg|png|jpeg';
				$config['max_size']      = '8024';
				$config['file_name']     = time().'_'.$_FILES["image"]['name'];

				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload( 'image' ) )
				{
					$data['status']     = 0;
					$data['message']    = $this->upload->display_errors();

					echo json_encode( $data );

					die();

				} else {

					$upload = $this->upload->data();
					$data[$this->table_prefix.'image']    = $upload['file_name'];

				}
			}

			$data[$this->table_prefix.'full_name']      = $this->input->post('full_name');
			$data[$this->table_prefix.'name']           = $this->input->post('name');
			$data[$this->table_prefix.'email']          = $this->input->post('email');
			$data[$this->table_prefix.'password']       = md5_mod($this->input->post('password'), $this->input->post('name'));
			$data[$this->table_prefix.'status']         = $this->input->post('status');
			$data[$this->table_prefix.'role']           = $this->input->post('role');

			$insert = [
				'table' => $this->table_db,
				'datas' => $data
			];

			$result  = $this->m_global_new->insert($insert);

			if ( $result['status'] )
			{
				$data['status']     = 1;
				$data['message']    = 'Successfully add User with Name <strong>'.$this->input->post('name').'</strong>';

				echo json_encode( $data );

			} else {

				$data['status']     = 0;
				$data['message']    = 'Failed add User with Name <strong>'.$this->input->post('name').'</strong>';
				if(ENVIRONMENT == 'development')
					$data['error']  = $this->db->error();

				echo json_encode( $data );

			}

		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function action_edit( $id )
	{
		$this->form_validation->set_rules('full_name', 'Full Name', 'trim|required');
		$this->form_validation->set_rules('name', 'Username', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$this->form_validation->set_rules('status', 'Status', 'trim|required');
		$this->form_validation->set_rules('role', 'Role', 'trim|required');

		if ( $this->form_validation->run( $this ) )
		{
			if ($this->input->post('password'))
				$data[$this->table_prefix.'password']   = md5_mod($this->input->post('password'), $this->input->post('name'));

			$data[$this->table_prefix.'full_name']      = $this->input->post('full_name');
			$data[$this->table_prefix.'name']           = $this->input->post('name');
			$data[$this->table_prefix.'email']          = $this->input->post('email');
			$data[$this->table_prefix.'status']         = $this->input->post('status');
			$data[$this->table_prefix.'role']           = $this->input->post('role');

			$update = [
				'table' => $this->table_db,
				'datas'	=> $data,
				'where'	=> ['user_id' => $id]
			];

			$result = $this->m_global_new->update($update);

			if ( $result )
			{
				$data['status']     = 1;
				$data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

				echo json_encode( $data );

			} else {

				$data['status']     = 0;
				$data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';
				if(ENVIRONMENT == 'development')
					$data['error']  = $this->db->error();

				echo json_encode( $data );

			}

		} else {

			$data['status']     = 3;
			$str                = ['<p>', '</p>'];
			$str_replace        = ['<li>', '</li>'];
			$data['message']    = str_replace( $str, $str_replace, validation_errors() );

			echo json_encode( $data );

		}
	}

	public function select()
	{
		// jika action checkbox
		if ( @$_REQUEST['customActionType'] == 'group_action' )
		{
			$aChk = [0, 1, 99];

			if ( in_array( @$_REQUEST['customActionName'], $aChk) )
			{
				$this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

				$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
				$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
			}
		}

		$aCari = [
			'name'      => $this->table_prefix.'name',
			'role'      => $this->table_prefix.'role',
			'email'     => $this->table_prefix.'email',
			'lastupdate'=> $this->table_prefix.'lastupdate'
		];

		$where      = NULL;
		$where_e    = NULL;

		$join 		= [
			['roles', 'role_id = user_role']
		];

		if ( @$_REQUEST['action'] == 'filter')
		{
			$where = [];
			foreach ( $aCari as $key => $value )
			{
				if ( $_REQUEST[$key] != '' )
				{
					if ( $key == 'lastupdate' )
					{
						$tmp = explode(' ', $_REQUEST[$key]);
						$where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

					} else {

						$where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

					}
				}
			}
		}

		if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
			$where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
		} else {
			$where[$this->table_prefix.'status <>']    = '99';
		}

		$keys   = array_keys( $aCari );
		@$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-3)]], $_REQUEST['order'][0]['dir']];

		$arr_config['table'] 	= $this->table_db;
		$arr_config['where'] 	= $where;
		$arr_config['where_e'] 	= $where_e;
		$arr_config['join'] 	= $join;

		$iTotalRecords  = $this->m_global_new->count($arr_config);
		$iDisplayLength = intval($_REQUEST['length']);
		$iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart  = intval($_REQUEST['start']);
		$sEcho          = intval($_REQUEST['draw']);

		$records        = array();
		$records["data"]= array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$select = 'user_id, role_name, user_status,'.implode(',' , $aCari);

		$arr_config['select'] 	= $select;
		$arr_config['order'] 	= $order;
		$arr_config['start'] 	= $iDisplayStart;
		$arr_config['show'] 	= $iDisplayLength;

		$result = $this->m_global_new->get($arr_config);
		// echo $this->db->last_query();exit();

		$i = 1 + $iDisplayStart;
		foreach ( $result as $rows )
		{
			$records["data"][] = array(
			  '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->user_id.'"/><span></span></label>',
			  $i,

			  $rows->user_name,
			  $rows->user_email,
			  $rows->role_name,
			  $rows->user_lastupdate,
			  $rows->user_role > 2 ?
			  '<a data-original-title="Role" href="'.base_url().$this->url.'/show_form/'.$rows->user_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-user"></i></a>'.
			  '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->user_id.'/'.($rows->user_status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->user_status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->user_status == 0 ? 'InActive' : ($rows->user_status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->user_status == 0 ? '-eye-slash' : ($rows->user_status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
			  '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->user_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
			  '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->user_id.'/99/'.($rows->user_status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>'
			  :
			  '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->user_id.'/'.($rows->user_status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->user_status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->user_status == 0 ? 'InActive' : ($rows->user_status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->user_status == 0 ? '-eye-slash' : ($rows->user_status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
			  '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->user_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
			  '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->user_id.'/99/'.($rows->user_status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
			  	
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode( $records );
	}

	public function show_form($id)
	{
		$data['id']  		= $id;

		$data['pagetitle']  = 'User';
		$data['subtitle']   = 'Role user';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;

        $data['jns_pajak']  = $this->db->query("SELECT ref_rek_4.jns_pemungutan, ref_rek_4.id_rek_4, ref_rek_4.link,ref_rek_4.						nm_rek_4, ref_pemungutan.nm_jn_pemungutan
								FROM ref_rek_4
								LEFT JOIN ref_pemungutan ON ref_rek_4.jns_pemungutan = ref_pemungutan.jn_pemungutan
								WHERE ref_rek_4.status <> 99
								AND ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 != 72 and id_rek_4 != 15")->result();


        $data['cek']		=  $this->db->query("select * from user_rek_4 where user_id = $id")->row();

		$data['breadcrumb'] = [ 'User' => $this->url, 'Add' => $this->url.'/show_add' ];
		$js['js']           = [ 'form-validation' ];

		$this->template->display( strtolower(__CLASS__).'/role', $data, $js );
	}

    public function action_form()
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'user_rek_4';

        $this->form_validation->set_rules('id_rek_4', 'id_rek_4', 'trim');

        if ($this->form_validation->run($this)) {

	       $id_rek_4          	= $this->input->post('id_rek_4');
	       $a 					=  implode($id_rek_4, ',');
	       
	       $data['user_id']     = $this->input->post('user_id');
	       $data['id_rek_4']    = $a;

                // action user_rek_4
	       		$this->db->where('user_id', $this->input->post('user_id'));
                $result1  = $this->db->delete($this->table_db);
                $result   = $this->m_global->insert($this->table_db, $data);

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

	public function change_status( $status, $where )
	{
		$data[ $this->table_prefix.'status' ]   = $status;

		$update = [
			'table' 	=> $this->table_db,
			'datas' 	=> $data,
			'where_e' 	=> $where
		];

		$result = $this->m_global_new->update($update);
	}

	public function change_status_by( $id, $status, $stat = FALSE )
	{
		if ( $stat )
		{
			$delete = [
				'table' => $this->table_db,
				'where' => [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->delete($delete);
		} else {
			$update = [
				'table' 	=> $this->table_db,
				'datas' 	=> [$this->table_prefix.'status' => $status],
				'where' 	=> [$this->table_prefix.'id' => $id]
			];

			$result = $this->m_global_new->update($update);

		}

		if ( $result )
		{
			$data['status'] = 1;

		} else {

			$data['status'] = 0;
		}

		echo json_encode( $data );
	}

	public function get_treeview()
	{
		$id = $this->input->post('id');

		$data = v_tree_view(menu2('', get_access($id)));

		echo $data;
	}

}

/* End of file Config_user.php */
/* Location: ./application/modules/config/controllers/Config_user.php */
