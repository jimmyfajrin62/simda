
<div class="row">
	<div class="col-md-12">
		<!-- BEGIN VALIDATION STATES-->
		<div class="portlet light portlet-fit portlet-form bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" fa fa-edit font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Edit <?php echo $pagetitle ?></span>
				</div>
				<div class="actions"></div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?php echo $url; ?>/action_edit/<?php echo $id ?>" class="form-horizontal form-edit" method="POST" data-confirm="1" role="form">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Name
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Name" required name="name" value="<?php echo $records->role_name; ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Status
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input value="1" <?php echo ($records->role_status == '1') ? 'checked' : ''; ?> type="radio" required id="status_1" name="status" class="md-radiobtn" checked>
												<label for="status_1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Active </label>
											</div>
											<div class="md-radio has-error">
												<input value="0" <?php echo ($records->role_status == '0') ? 'checked' : ''; ?> type="radio" required id="status_2" name="status" class="md-radiobtn">
												<label for="status_2">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> InActive </label>
											</div>
										</div>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<h4>Menu</h4>
								<div class="row">
									<div class="col-md-12">
										<button type="button" onclick="treeSelected(true);" class="btn btn-sm btn-info"><i class="icon-check"></i> Select All</button>
										<button type="button" onclick="treeSelected(false);" class="btn btn-sm btn-danger"><i class="icon-close"></i> Deselect All</button>
									</div>
									<div class="col-md-12">
										<div id="roles">
											<?php v_tree_view(menu2('all'), $records->role_access); ?>
										</div>
										<input type="hidden" name="access" id="inputTree" value="<?=$records->role_access; ?>">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="text-center">
							<button type="submit" class="btn blue">Submit</button>
							<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url.'/show_edit/'.$id?>" class="ajaxify reload"></a>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// Fungsi Form Validasi
		var rule    = {};
		var message = {};
		var title   = 'Confirmation';
		var text    = 'Are you sure to continue this action?';
		var form    = '.form-edit';
		FormValidation.handleValidation( form, rule, message, title, text );
	
		$('.select2').select2();

		$("#roles").fancytree({
			checkbox: true,
			selectMode: 3,
			// imagePath: "assets/icons/others/",
			extensions: ["dnd"],
			autoScroll: true,
			select: function(event, data) {
				var selKeys2 = $.map(data.tree.getSelectedNodes(), function(node){
					var id_c = node.data.id,
						id_p = node.parent;

					if(id_p.parent !== null) {
						var id_px = id_p.data.id

						return [id_c, id_px];
					} else {
						return [id_c];
					}
				});


				var unique = $.unique(selKeys2);
			
				$("#inputTree").val(unique.join(", "));
			},
		});
	});

	function treeSelected(status) {
		$("#roles").fancytree("getTree").visit(function(node){
			node.setSelected(status);
		});
		
		return false;
	}
</script>