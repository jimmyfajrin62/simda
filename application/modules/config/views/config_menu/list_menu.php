<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- Begin: life time stats -->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table font-blue"></i>
					<span class="caption-subject font-blue sbold uppercase">List <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
					<menu id="nestable-menu">
						<button class="btn btn-sm blue tooltips" data-action="expand-all" data-original-title="Expand All">
							Expand All
						</button>
						<button class="btn btn-sm blue tooltips" data-action="collapse-all" data-original-title="Collapse All">
							Collapse All
						</button>
					</menu>
				</div>
			</div>
			<div class="portlet-body">
				<?php v_list_menu(menu2('all')); ?>
				<hr>
				<div class="text-center">
					<button type="button" id="save_form" class="btn blue">Submit</button>
					<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<a href="<?php echo $url?>/show_list_menu" class="ajaxify reload"></a>

<script type="text/javascript">
	jQuery(document).ready(function() {
		$('.dd').nestable({
			'group' : 0,
			'maxDepth' : 10
		});

		$('#nestable-menu').on('click', function(e) {
			var target = $(e.target),
				action = target.data('action');

			if (action === 'expand-all') {
				$('.dd').nestable('expandAll');
			}
			if (action === 'collapse-all') {
				$('.dd').nestable('collapseAll');
			}
		});

		$('#save_form').on('click', function(e){
			e.preventDefault();

			var data = $('.dd').nestable('serialize');

			$.post('<?php echo $url; ?>/save_list', { data : data } , function(data) {
				if(data.status == '1') {
					toastr['success'](data.message, "Success");
					location.reload();

					// if($('.reload').length) {
					// 	$('.reload').trigger('click');
					// }
				} else {
					toastr['error'](data.message, "Error");
				}
			}, 'json');
		})
	});
</script>