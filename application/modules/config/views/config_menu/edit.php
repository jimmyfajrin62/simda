<div class="row">
	<div class="col-md-12">
		<!-- BEGIN VALIDATION STATES-->
		<div class="portlet light portlet-fit portlet-form bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" fa fa-plus font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Add <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?php echo $url; ?>/action_edit/<?php echo $id ?>" class="form-horizontal form-edit" method="POST" data-confirm="1" role="form">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Name
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Name" required name="name" value="<?php echo $records->menu_name; ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Link
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control no-space" placeholder="Link" required name="link" value="<?php echo $records->menu_link; ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Status
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input value="1" <?php echo ($records->menu_status ==
												 '1') ? 'checked' : ''; ?> type="radio" required id="status_1" name="status" class="md-radiobtn">
												<label for="status_1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Active </label>
											</div>
											<div class="md-radio has-error">
												<input value="0" <?php echo ($records->menu_status ==
												 '0') ? 'checked' : ''; ?> type="radio" required id="status_2" name="status" class="md-radiobtn">
												<label for="status_2">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> InActive </label>
											</div>
										</div>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Position
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input value="1" <?php echo ($records->menu_parent ==
												 '0') ? 'checked' : ''; ?> type="radio" required id="status_3" name="position" class="md-radiobtn">
												<label for="status_3">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Parent </label>
											</div>
											<div class="md-radio has-warning">
												<input value="0" <?php echo ($records->menu_parent !==
												 '0') ? 'checked' : ''; ?> type="radio" required id="status_4" name="position" class="md-radiobtn">
												<label for="status_4">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Children </label>
											</div>
										</div>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div style="display: none;" id="show_parent" class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Parent
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<select id="menu_parent" name="parent" class="form-control select2">
											<option value="">Select Parent</option>
											<?php foreach ($parent as $key => $val): ?>
												<option <?php echo ($records->menu_parent == $val->menu_id) ? 'selected' : ''; ?> value="<?=$val->menu_id; ?>"><?=$val->menu_name; ?></option>
											<?php endforeach; ?>
										</select>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Icon
										<span class="required"></span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Icon" name="icon" value="<?php echo $records->menu_icon; ?>">
										<span class="help-block">  Simple Line Icons  </span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="text-center">
							<button type="submit" class="btn blue">Submit</button>
							<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url.'/show_edit/'.$id?>" class="ajaxify reload"></a>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// Fungsi Form Validasi
		var rule    = {};
		var message = {};
		var title   = 'Confirmation';
		var text    = 'Are you sure to continue this action?';
		var form    = '.form-edit';
		FormValidation.handleValidation( form, rule, message, title, text );

		if("<?php echo $records->menu_parent; ?>" == '0') {
			$('#menu_parent').prop('required', false);
			$('#show_parent').hide();
		} else {
			$('#menu_parent').prop('required', true);
			$('#show_parent').show();
			$('.select2').select2();
		}

		$('input[name="position"]').on('click', function(e){
			var val 	= $(this).val(),
				target  = $('#show_parent'),
				target2 = $('#menu_parent');

			if(val == '0') {
				target2.prop('required', true);
				target.show();
				$('.select2').select2();
			} else {
				target2.prop('required', false);
				target.hide();
			}
		});
	});
</script>