<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
	<div class="col-md-12">
		<!-- Begin: life time stats -->
		<div class="portlet light">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-table font-blue"></i>
					<span class="caption-subject font-blue sbold uppercase">Table <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
					<a href="<?php echo $url ?>/show_add" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Add" >
						<i class="fa fa-plus"></i>
					</a>
					<a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
						<i class="fa fa-refresh"></i>
					</a>
					<a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
						<i class="fa fa-search"></i>
					</a>
					<div class="btn-group">
						<a class="btn red-haze" href="javascript:;" data-toggle="dropdown">
							<i class="fa fa-file-text"></i>
							<span class="hidden-xs"> Tools </span>
							<i class="fa fa-angle-down"></i>
						</a>
						<ul class="dropdown-menu pull-right">
							<li>
								<a href="javascript:;"> Export to Excel </a>
							</li>
							<li>
								<a href="javascript:;"> Export to CSV </a>
							</li>
							<li class="divider"> </li>
							<li>
								<a href="javascript:;"> Print Table </a>
							</li>
						</ul>
						<a class="btn btn-icon-only red fullscreen" href="javascript:;"> </a>
					</div>
				</div>
			</div>
			<div class="portlet-body">
				<div class="table-container">
					<div class="table-actions-wrapper">
						<select class="bs-select table-group-action-input form-control input-small" data-style="blue">
							<option value="">Select...</option>
							<option value="99">Delete</option>
							<option value="0">InActive</option>
							<option value="1">Active</option>
						</select>
						<button class="btn btn-sm btn-icon-only blue table-group-action-submit tooltips" data-original-title="Send">
							<i class="fa fa-check"></i>
						</button>
					</div>
					<table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
						<thead>
							<tr role="row" class="heading">
								<th width="30px">
									<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
										<input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
										<span></span>
									</label>
								</th>
								<th width="20px"> No </th>
								<th> Name </th>
								<th> Email </th>
								<th> Role </th>
								<th width="200px"> Last Update </th>
								<th width="140px"> Action </th>
							</tr>
							<tr role="row" class="filter">
								<td> </td>
								<td> </td>
								<td>
									<input type="text" class="form-control form-filter input-sm" name="name" placeholder="Name"> </td>
								<td>
									<input type="text" class="form-control form-filter input-sm" name="email" placeholder="Email"> </td>
								<td>
									<select name="role" class="form-control form-filter select-filter input-sm">
										<option value="">Select ...</option>
										<?php foreach ($roles as $key => $val): ?>
											<option value="<?=$val->role_id; ?>"><?=$val->role_name; ?></option>
										<?php endforeach; ?>
									</select>
								</td>
								<td>
									<div class="input-group margin-bottom-5" id="defaultrange">
										<input type="text" name="lastupdate_show" class="form-control form-filter">
										<input type="hidden" name="lastupdate" class="form-filter">
										<span class="input-group-btn">
											<button class="btn default date-range-toggle" type="button">
												<i class="fa fa-calendar"></i>
											</button>
										</span>
									</div>
								</td>
								<td class="text-center">
									<div class="clearfix">
										<button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
											<i class="fa fa-search"></i>
										</button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
											<i class="fa fa-times"></i>
										</button><button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
									</div>
								</td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
			<!-- End: life time stats -->
		</div>
	</div>
	<!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
	jQuery(document).ready(function() {

		// table data
		var select_url = '<?php echo $url ?>' + '/select';

		var header = [
			{ "sClass": "text-center" },
			{ "sClass": "text-center" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-left" },
			{ "sClass": "text-center" }
		];
		var order = [
			[2, "asc"]
		];

		var sort = [-1, 0, 1];

		TableDatatablesAjax.handleRecords( select_url, header, order, sort );
		// bs select setelah datatable, bug
		Helper.bsSelect();

	});
</script>
