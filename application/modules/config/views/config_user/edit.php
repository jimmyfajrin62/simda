<div class="row">
	<div class="col-md-12">
		<!-- BEGIN VALIDATION STATES-->
		<div class="portlet light portlet-fit portlet-form bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" fa fa-edit font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Edit <?php echo $pagetitle ?></span>
				</div>
				<div class="actions"></div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?php echo $url; ?>/action_edit/<?php echo $id ?>" class="form-horizontal form-edit" method="POST" data-confirm="1" role="form">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Full Name
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control" placeholder="Full Name" required name="full_name" value="<?php echo $records->user_full_name ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Username
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="text" class="form-control no-space" placeholder="Username" required name="name" value="<?php echo $records->user_name ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label"> Email
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<input type="email" class="form-control" required placeholder="Email" name="email" value="<?php echo $records->user_email ?>">
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Password
										<span class="required"></span>
									</label>
									<div class="col-md-8">
										<input type="password" class="form-control" placeholder="Password" name="password">
										<span class="help-block">Keep empty if no changes</span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Status
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<div class="md-radio-inline">
											<div class="md-radio">
												<input <?php echo ($records->user_status == '1') ? 'checked' : ''; ?> value="1" required type="radio" id="status_1" name="status" class="md-radiobtn">
												<label for="status_1">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> Active </label>
											</div>
											<div class="md-radio has-error">
												<input <?php echo ($records->user_status == '0') ? 'checked' : ''; ?> value="0" required type="radio" id="status_2" name="status" class="md-radiobtn">
												<label for="status_2">
													<span></span>
													<span class="check"></span>
													<span class="box"></span> InActive </label>
											</div>
										</div>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="form-group form-md-line-input">
									<label class="col-md-3 control-label">Role
										<span class="required">*</span>
									</label>
									<div class="col-md-8">
										<select id="user_role" name="role" required class="form-control select2">
											<option value="">Select Role</option>
											<?php $i = 1; foreach ($roles as $key => $val): ?>
												<option <?php echo ($records->user_role == $val->role_id) ? 'selected' : ''; ?> value="<?=$val->role_id; ?>"><?=$i . '. ' . $val->role_name; ?></option>
											<?php $i++; endforeach; ?>
										</select>
										<span class="help-block"></span>
										<div class="form-control-focus"> </div>
									</div>
								</div>
								<div class="col-md-offset-3 col-md-8">
									<div id="roles">
										<?php v_tree_view(menu2('', get_access($records->user_role))); ?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="text-center">
							<button type="submit" class="btn blue">Submit</button>
							<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
						</div>
					</div>
				</form>
				<!-- END FORM-->
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url.'/show_edit/'.$id?>" class="ajaxify reload"></a>

<script type="text/javascript">
	jQuery(document).ready(function() {
		// Fungsi Form Validasi
		var rule    = {};
		var message = {};
		var title   = 'Confirmation';
		var text    = 'Are you sure to continue this action?';
		var form    = '.form-edit';
		FormValidation.handleValidation( form, rule, message, title, text );
	
		$('.select2').select2();

		$('.no-space').on('keydown', function(e){
			if(e.which !== 32){
				return e.which;
			} else {
				return false;
			}
		});

		$("#roles").fancytree({
			checkbox: false,
			selectMode: 3,
			extensions: ["dnd"],
			autoScroll: true,
		});

		$('#user_role').on('change', function(e){
			e.preventDefault();
			var id = $(this).val();
			
			$('#roles').html('');
			$('#roles').fancytree("destroy");
			
			if(id !== '') {
				$.ajax({
					url: base_url+'config/config_user/get_treeview',
					data: {id: id},
					type: 'post',
					dataType: 'html',
					success: function(data) {
						$('#roles').html(data);
					}
				}).done(function(){
					$("#roles").fancytree({
						checkbox: false,
						selectMode: 3,
						extensions: ["dnd"],
						autoScroll: true,
					});
				});
			} else {
				$('#roles').fancytree();
			}
		});
	});
</script>