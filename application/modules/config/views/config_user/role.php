<div class="row">
	<div class="col-md-12">
		<!-- BEGIN VALIDATION STATES-->
		<div class="portlet light portlet-fit portlet-form bordered">
			<div class="portlet-title">
				<div class="caption">
					<i class=" fa fa-plus font-green"></i>
					<span class="caption-subject font-green sbold uppercase">Role <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?= @$url ?>/action_form/<?=@$cek->user_id?>" class="form-horizontal form-add" role="form" method="POST">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>
						<div class="row">
							<div class="col-md-11">
                                <input type="hidden" name="user_id" value="<?=$id?>">
								<table class="table table-striped table-bordered table-hover dt-responsive table-checkable" cellspacing="0" width="100%">
										<tr>
											<th>No</th>
											<th>Jenis Pajak</th>
											<th>Jenis Pemungutan</th>
											<th>Pilih</th>
										</tr>
									</thead>
									<tbody>
										<?php 
										$no=1;
										foreach ($jns_pajak as $value) { ?>
                                            <?php @$selected = @$value->id_rek_4 == @$cek->id_rek_4 ? 'checked' : '' ?>
										<tr>
											<td><?=$no?></td>
											<td><?=$value->nm_rek_4?></td>
											<td><?=$value->nm_jn_pemungutan?></td>
											<td><label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" name="id_rek_4[<?=$value->id_rek_4?>]" class="checkboxes" <?=@$selected?> value="<?=$value->id_rek_4?>"/><span></span></label></td>
										</tr>
										<?php $no++; }?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="form-actions">
						<div class="text-center">
							<button type="submit" class="btn blue">Submit</button>
							<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>


<script type="text/javascript">
	jQuery(document).ready(function() {
		// Fungsi Form Validasi
		var rule = {};
		var message = {};
		var form = '.form-add';
		FormValidation.handleValidation( form, rule, message );

		$('.select2').select2();

		$('.no-space').on('keydown', function(e){
			if(e.which !== 32){
				return e.which;
			} else {
				return false;
			}
		});
	});
</script>