<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends Admin_Controller
{
    private $prefix         = 'landing';

	function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'Welcome to Landing '.date('Y');
        $data['isi']        = 'Dashboard';
        $data['breadcrumb'] = [ 'Landing' => null ];
        $js['js']           = [ ];
        $css['css']         = [ 'about' ];

        $this->template->display( $this->prefix, $data, $js, $css );
	}

    public function change_role( $role )
    {
        $iduser = ['','31','39','51','34','35','43'];
        $select = 'employee_id, employee_name, employee_email, employee_level, employee_foto';
        $result = $this->m_global->get_data_all( 'employee', null, ['employee_id' => $iduser[$role]], $select );

        $this->session->set_userdata('employee_data', $result[0]);
        redirect( $this->input->post('url') );
    }

}
