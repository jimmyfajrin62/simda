<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_penetapan_nota_skpd extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show_nota($id)
    {
        $query = $this->db->query(" SELECT b.no_sptpd, c.nm_penandatangan as nm_penetapan, c.nip_penandatangan as nip_penetapan, e.nm_jab as jbt_penetapan,
                                    		d.nm_penandatangan as nm_perhitungan, d.nip_penandatangan as nip_perhitungan, f.nm_jab as jbt_perhitungan, z.*
                                    FROM ta_nota z
                                    LEFT JOIN ta_sspd a ON z.sspd_id = a.id
                                    LEFT JOIN ta_kartu_pajak_pungut b ON a.pungut_id = b.id
                                    LEFT JOIN ref_penandatangan c ON z.ttd_penetapan = c.no_urut
                                    LEFT JOIN ref_penandatangan d ON z.ttd_perhitungan = d.no_urut
                                    LEFT JOIN ref_jabatan e ON c.jbt_penandatangan = e.kd_jab
                                    LEFT JOIN ref_jabatan f ON d.jbt_penandatangan = f.kd_jab
                                    WHERE z.id = $id");
        return $query->row();
    }

    public function get_sspd($id, $id2)
    {
        $query = $this->db->query("SELECT a.id as sspd_id, b.*
                                    FROM ta_sspd a
                                    LEFT JOIN ta_kartu_pajak_pungut b ON a.pungut_id = b.id
                                    WHERE b.id = $id and a.id = $id2");
        return $query->row();
    }

    public function sum_sspd($id, $table)
    {
        $query = $this->db->query("SELECT sum(pajak_terhutang) as total_pajak
                                    from $table
                                    where pungut_id = $id");
        return $query->row();
    }

    public function show_bank()
    {
        $query = $this->db->query("SELECT id, kd_bank, nm_bank, no_rekening from ref_bank");
        return $query->result();
    }

    public function petugas($id)
    {
        $query = $this->db->query("SELECT a.no_urut, a.nm_penandatangan, a.nip_penandatangan, b.nm_jab
                                    from ref_penandatangan a
                                    left join ref_jabatan b on a.no_urut = b.kd_jab
                                    where no_dok = $id");
        return $query->result();
    }

    public function auto_id()
    {
        $query = $this->db->query("SELECT id from ta_skpd order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_skpd($prefix=null, $sufix=null)
    {
        $query = $this->db->query("SELECT id from ta_skpd order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 6;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }

    public function kota()
    {
        $query = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())");
        return $query->row();

    }

    public function data($id)
    {
        $query = $this->db->query("select
                                     f.nama_pendaftar, f.npwpd, f.jalan, f.rtrw, h.nm_kec, g.nm_kel, f.kabupaten, f.kode_pos,
                                     d.nm_usaha, d.alamat_usaha,
                                     b.no_sptpd, a.jns_pembayaran, a.pungut_id,
                                     a.kd_bank, a.rek_penyetor, a.keterangan, a.no_sspd, i.nm_bank, a.nm_penyetor, j.*,k.nm_penandatangan as nm_penetapan, l.nm_penandatangan as nm_perhitungan,k.nip_penandatangan as nip_penetapan, k.nip_penandatangan as nip_perhitungan
                                    from ta_sspd a
                                    left join ref_bank i on a.kd_bank = i.kd_bank
                                    join ta_kartu_pajak_pungut b on a.pungut_id = b.id
                                    join wp_wajib_pajak_usaha_pajak c on b.wp_usaha_pajak_id = c.id
                                    join wp_wajib_pajak_usaha d on c.wp_usaha_id = d.id
                                    join wp_wajib_pajak e on d.wp_id = e.id
                                    join wp_data_umum f on e.data_umum_id = f.id
                                    join ref_kelurahan g on f.kd_kel = g.kel_id
                                    join ref_kecamatan h on f.kd_kec = h.kd_kec
                                    join ta_nota j on a.id = j.sspd_id
                                    JOIN ref_penandatangan as k ON  k.no_urut = j.ttd_penetapan
                                    JOIN ref_penandatangan as l ON  l.no_urut = j.ttd_perhitungan

                                    WHERE j.id = $id");

        return $query->row();


    }

    public function data2($id)
    {
        $this->db->select(' b.no_sptpd, b.tgl_sptpd, a.pajak_terhutang, d.no_sspd, d.tgl_sspd, d.masa1, d.masa2, d.total_bayar, c.*')
       ->from($this->table_db)
       ->join('ta_kartu_pajak_pungut b', 'a.pungut_id = b.id', 'left')
       ->join('ref_rek_6 c', 'a.id_rek_6 = c.id_rek_6', 'left')
       ->join('ta_sspd d', 'b.id = d.pungut_id', 'left')
       ->where('b.id', $data['records']->pungut_id)->get($this->table_db)->row();

    }

    public function jumlah1($id,$table)
    {

        $query = $this->db->query("select sum(pajak_terhutang ) as jumlah from $table where pungut_id = $id");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

    public function jumlah2($id)
    {

        $query = $this->db->query("select sum(total_bayar ) as jumlah from ta_sspd where id = $id");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

    public function sanksi()
    {
        $query = $this->db->query("select * from ref_sanksi");
        return $query->result();

    }

    public function show_sanksi($id_nota)
    {
        $query = $this->db->query("SELECT `a`.*, `c`.`keterangan`, `a`.`total_sanksi`, `b`.`total_pajak`, `b`.`total_bayar`, `a`.`nilai_sanksi`, `a`.`lastupdate`, `a`.`status`
                                    FROM `ta_nota_rinc` `a`
                                    LEFT JOIN `ta_nota` `b` ON `a`.`nota_id` = `b`.`id`
                                    LEFT JOIN `ref_sanksi` `c` ON `a`.`sanksi_id` = `c`.`id_sanksi`
                                    WHERE a.status = '1' and a.nota_id = $id_nota");
        return $query->row();

    }

    public function get_sanksi($id_nota)
    {
        $query = $this->db->query("SELECT total_pajak
                                        from ta_nota
                                        where id = $id_nota");
        return $query->row();

    }

    public function show_skpd($id)
    {
        $query = $this->db->query("SELECT a.*,c.nip_penandatangan,c.jbt_penandatangan,b.no_nota,b.user_id,b.total_bayar,b.total_pajak_real
                                    FROM ta_skpd a
                                    LEFT JOIN ta_nota b ON a.nota_id = b.id
                                    left join ref_penandatangan c on a.ttd_dok_id = c.no_urut
                                    WHERE a.id = $id");

        return $query->row();

    }

}
