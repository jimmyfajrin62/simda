<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penetapan_pajak_nt_sub extends Admin_Controller
{
    private $prefix         = 'penetapan/penetapan_pajak_nt_sub';
    private $url            = 'penetapan/penetapan_pajak_nt_sub';
    private $path           = 'penetapan/pajak/nota_tambahan';
    private $path_nota      = 'penetapan/penetapan_pajak_nt';
    private $table_db       = '';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_penetapan_nota', 'mdb');

        if ($this->uri->segment(6) == 4) {
            $this->table_db = 'ta_kartu_pajak_hotel';
        } else if ($this->uri->segment(6) == 5) {
            $this->table_db = 'ta_kartu_pajak_restoran';
        } else if ($this->uri->segment(6) == 6) {
            $this->table_db = 'ta_kartu_pajak_hiburan';
        } else if ($this->uri->segment(6) == 7) {
            $this->table_db = 'ta_kartu_pajak_reklame';
        } else if ($this->uri->segment(6) == 8) {
            $this->table_db = 'ta_kartu_pajak_penerangan';
        } else if ($this->uri->segment(6) == 9) {
            $this->table_db = 'ta_kartu_pajak_mineral';
        } else if ($this->uri->segment(6) == 10) {
            $this->table_db = 'ta_kartu_pajak_parkir';
        } else if ($this->uri->segment(6) == 11) {
            $this->table_db = 'ta_kartu_pajak_air';
        } else if ($this->uri->segment(6) == 12) {
            $this->table_db = 'ta_kartu_pajak_walet';
        } else if ($this->uri->segment(6) == 15) {
            $this->table_db = 'ta_kartu_bphtb';
        }
    }

    public function _remap($method, $args)
    {
        if (method_exists($this, $method)) {
            $this->$method($args);
        } else {
            $this->index($method, $args);
        }
    }

    public function index($ids, $title)
    {
        // echo $ids; exit();
        $title = str_replace('%20', ' ', $title[0]);
        $data ['kd_rek_4']  = $ids;
        $data['title']      = $title;

        $data['pagetitle']  = "Nota Tambahan $title";
        $data['subtitle']   = "Nota Tambahan $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => null ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'/wp', $data, $js, $css);
    }

    public function select($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title = str_replace('%20', ' ', $ids[1]);

        $this->table_db = 'ta_sspd a';

        $join = [
                'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut b', 'a.pungut_id = b.id', 'LEFT'],
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak c', 'b.wp_usaha_pajak_id = c.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha d', 'c.wp_usaha_id = d.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak e', 'd.wp_id = e.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum f', 'e.data_umum_id = f.id', 'LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'        => 'f.npwpd',
            'nm_usaha'     => 'd.nm_usaha',
            'alamat_usaha' => 'd.alamat_usaha',
        ];

        $where    = null;
        $where_e  = "a.jns_pajak = $kd_rek_4";
        $group    = "f.id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['a.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['a.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e, $group);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'f.id as wp, b.id as pungut, a.id, a.status, a.lastupdate,f.user_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength, $group);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->npwpd),
                strtoupper($rows->nm_usaha),
                strtoupper($rows->alamat_usaha),
                '<a href="'.base_url($this->url.'/show_nota/'.$kd_rek_4.'/'.$title.'/'.$rows->wp.'/'.$rows->user_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SSPD"><i class="fa fa-file-text"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    public function show_nota($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[3];

        $data ['kd_rek_4']  = $kd_rek_4;
        $data ['title']     = $title;
        $data ['wp']        = $wp;
        $data ['user_id']   = $user_id;

        $data['pagetitle']   = "Nota Tambahan Pajak $title";
        $data['subtitle']    = "Nota Tambahan Pajak $title";

        $data['url']         = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'Nota/WP' => null];

        $js['js']            = [ 'table-datatables-ajax' ];
        $css['css']          = null;

        $this->template->display($this->path.'/nota', $data, $js, $css);
    }

    public function select_nota($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4  = $ids[0];
        $title     = str_replace('%20', ' ', $ids[1]);
        $wp        = $ids[2];
        $user_id   = $ids[3];

        $this->table_db = 'ta_nota a';

        $join = [
                'ta_sspd'                    => ['ta_sspd b', 'a.sspd_id = b.id', 'LEFT'],
                'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut c', 'b.pungut_id = c.id', 'LEFT'],
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak d', 'c.wp_usaha_pajak_id = d.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha e', 'd.wp_usaha_id = e.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak f', 'e.wp_id = f.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum g', 'f.data_umum_id = g.id', 'LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_nota'         => 'a.no_nota',
            'total_pajak'     => 'a.total_pajak',
            'total_bayar'     => 'a.total_bayar',
            'pajak_terhutang' => 'a.pajak_terhutang',
            'keterangan'      => 'a.keterangan',
        ];

        $where    = null;
        $where_e  = "a.jns_pajak = $kd_rek_4 AND g.id = $wp and a.jenis_nota = 'tambahan'";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['a.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['a.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.jenis_nota, total_pajak_real, b.pungut_id, a.status, a.lastupdate, g.user_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->no_nota),
                uang($rows->total_pajak),
                uang($rows->total_bayar),
                // $rows->pajak_terhutang == 0 ? 'LUNAS' : uang($rows->pajak_terhutang),
                uang($rows->total_pajak_real),
                strtoupper($rows->keterangan),
                $this->session->user_data->user_role != 1?
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$title.'/'.$kd_rek_4.'/'.$wp.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_nota_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->pungut_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Rincian Nota"><i class="fa fa-folder-open"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->pungut_id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_nota/99'.
                    ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'
                :
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$title.'/'.$kd_rek_4.'/'.$wp.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_nota_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->pungut_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Rincian Nota"><i class="fa fa-folder-open"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_admin/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->pungut_id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_nota/99'.
                    ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // global actions
    public function change_status_by($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $id     = $ids[0];
        $table  = $ids[1];
        $status = $ids[2];
        $stat   = $ids[3];

        if ($stat == 'true') {
            $data['status_nota'] = 0;
            // update sptpd
            $pungut               = $this->db->query("select b.pungut_id
                                    from ta_nota a JOIN ta_sspd b ON a.sspd_id = b.id
                                    where a.id = $id")->row();
            $result2              = $this->m_global->update('ta_kartu_pungut', $data, ['id' => $pungut->pungut_id]);

            $result               = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result               = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function show_add($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4                = $ids[0];
        $title                   = str_replace('%20', ' ', $ids[1]);
        $wp                      = $ids[2];
        $user_id                 = $ids[3];

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;
        $data['wp']              = $wp;
        $data['user_id']         = $user_id;

        $data['bank']            = $this->mdb->show_bank();
        $data['sanksi']          = $this->mdb->sanksi();
        $data['ttd_penetapan']   = $this->mdb->petugas(3);
        $data['ttd_perhitungan'] = $this->mdb->petugas(4);

        $data['pagetitle']       = "Add Pajak $title";
        $data['subtitle']        = "Add Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'Nota/WP' => $this->url.'/show_nota/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id,'Add' =>null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form_nota', $data, $js, $css);
    }

    public function show_edit($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4                = $ids[0];
        $title                   = str_replace('%20', ' ', $ids[1]);
        $wp                      = $ids[2];
        $user_id                 = $ids[3];
        $pungut_id               = $ids[4];
        $id                      = $ids[5];

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;
        $data['wp']              = $wp;
        $data['user_id']         = $user_id;
        $data['pungut_id']       = $pungut_id;
        $data['id']              = $id;

        $data['bank']            = $this->mdb->show_bank();
        $data['sanksi']          = $this->mdb->sanksi();
        $data['ttd_penetapan']   = $this->mdb->petugas(3);
        $data['ttd_perhitungan'] = $this->mdb->petugas(4);
        $data['data']            = $this->mdb->show_nota($id);

        $data['pagetitle']       = "Add Pajak $title";
        $data['subtitle']        = "Add Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'Nota/WP' => $this->url.'/show_nota/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id,'Edit' =>null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form_nota', $data, $js, $css);
    }

    public function show_edit_admin($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4                = $ids[0];
        $title                   = str_replace('%20', ' ', $ids[1]);
        $wp                      = $ids[2];
        $user_id                 = $ids[3];
        $pungut_id               = $ids[4];
        $id                      = $ids[5];

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;
        $data['wp']              = $wp;
        $data['user_id']         = $user_id;
        $data['pungut_id']       = $pungut_id;
        $data['id']              = $id;

        $data['bank']            = $this->mdb->show_bank();
        $data['sanksi']          = $this->mdb->sanksi();
        $data['ttd_penetapan']   = $this->mdb->petugas(3);
        $data['ttd_perhitungan'] = $this->mdb->petugas(4);
        $data['data']            = $this->mdb->show_nota($id);

        $data['pagetitle']       = "Add Pajak $title";
        $data['subtitle']        = "Add Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'Nota/WP' => $this->url.'/show_nota/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id,'Edit' =>null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form_nota_admin', $data, $js, $css);
    }

    public function action_form($ids = null)
    { 
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'ta_nota';

        $this->form_validation->set_rules('tgl_nota', 'Tgl Nota', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

        if ($this->form_validation->run($this)) {
            // perhitungan pajak
            $pajak      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_pajak'));
            $bayar      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_bayar'));
            $pajak_real = str_replace(['Rp', ',', ' '], '', $this->input->post('total_pajak_real'));
            $nilai      = $this->input->post('nilai');
            $hutang     = $pajak - $bayar;
            $real       = ((int)$nilai + 100) * (int)$pajak / 100;

            if ($pajak > $pajak_real) {
            $data[$this->table_prefix.'jenis_skpd']  = 'LB';
                
            }else {
            $data[$this->table_prefix.'jenis_skpd']  = 'KBT';
                
            }

            $data[$this->table_prefix.'pajak_terhutang']  = str_replace(['Rp', ',', ' '], '', $hutang);
            $data[$this->table_prefix.'jns_pajak']        = $this->input->post('jns_pajak');
            $data[$this->table_prefix.'tgl_nota']         = $this->m_global->setdateformat($this->input->post('tgl_nota'));
            $data[$this->table_prefix.'masa1']            = $this->m_global->setdateformat($this->input->post('masa1'));
            $data[$this->table_prefix.'masa2']            = $this->m_global->setdateformat($this->input->post('masa2'));
            $data[$this->table_prefix.'keterangan']       = $this->input->post('keterangan');
            $data[$this->table_prefix.'total_pajak']      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_pajak'));
            $data[$this->table_prefix.'total_bayar']      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_bayar'));
            $data[$this->table_prefix.'total_pajak_real'] = str_replace(['Rp', ',', ' '], '', $this->input->post('total_pajak_real'));
            $data[$this->table_prefix.'ttd_penetapan']    = $this->input->post('ttd_penetapan');
            $data[$this->table_prefix.'jenis_nota']       = $this->input->post('jenis_nota');
            $data[$this->table_prefix.'sanksi_id']        = $this->input->post('sanksi_id');
            $data[$this->table_prefix.'user_id']          = $this->input->post('user_id');
            $data[$this->table_prefix.'sptpd_id']         = $this->input->post('pungut_id');
            $data[$this->table_prefix.'ttd_perhitungan']  = $this->input->post('ttd_perhitungan');

            if ($ids == null) {
                $date = date("d/m/Y");

                $no_nota = $this->m_global->nomor('nota');
                $data[$this->table_prefix.'id']      = $this->mdb->auto_id();
                $data[$this->table_prefix.'sspd_id'] = $this->input->post('sspd_id');
                $data[$this->table_prefix.'tahun']   = date('Y');
                $data[$this->table_prefix.'no_nota'] = $no_nota;
                // update sspd
                $id_pungut = $this->input->post('pungut_id');
                $data2[$this->table_prefix.'status_nota'] = 1;
                $result2 = $this->m_global->update('ta_sspd', $data2, ['pungut_id' => $id_pungut]);
                $result3 = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);
                // instert nota

                $result  = $this->m_global->insert($this->table_db, $data);
                $log['id']      = $this->input->post('user_id');
                $log['action']  = 'Tambah pajak ';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $no_nota.' jenis_nota : '.$this->input->post('jenis_nota').'input user = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('nota_log', $log);
                // echo $this->db->last_query();exit();
            } else {
                $id = $ids[0];
                // update nota
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
                $log['id']      = $ids[0];
                $log['action']  = 'Edit pajak ';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $this->input->post('no_nota').' jenis_nota = '.$this->input->post('jenis_nota').', input user = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('nota_log', $log);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function show_nota_rinci($ids)
    {
         //echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[3];
        $id_pungut          = $ids[4];

        $data ['kd_rek_4']  = $kd_rek_4;
        $data ['nm_rek_4']  = $title;
        $data ['wp']        = $wp;
        $data ['id_pungut'] = $id_pungut;

        $data['pagetitle']   = "SSPD Pajak Hotel $title Rinci";
        $data['subtitle']    = "SSPD Pajak Hotel $title Rinci";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->path_nota, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'Nota/WP' => $this->url.'/show_nota/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id,'Rincian' =>null];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'/nota_rinci', $data, $js, $css);
    }

    public function select_nota_rinci($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $nm_rek_4           = $ids[1];
        $wp                 = $ids[2];
        $id_pungut          = $ids[3];

        if ($kd_rek_4 == 4) {
            $this->table_db = 'ta_kartu_pajak_hotel';
        } else if ($kd_rek_4 == 5) {
            $this->table_db = 'ta_kartu_pajak_restoran';
        } else if ($kd_rek_4 == 6) {
            $this->table_db = 'ta_kartu_pajak_hiburan';
        } else if ($kd_rek_4 == 7) {
            $this->table_db = 'ta_kartu_pajak_reklame';
        } else if ($kd_rek_4 == 8) {
            $this->table_db = 'ta_kartu_pajak_penerangan';
        } else if ($kd_rek_4 == 9) {
            $this->table_db = 'ta_kartu_pajak_mineral';
        } else if ($kd_rek_4 == 10) {
            $this->table_db = 'ta_kartu_pajak_parkir';
        } else if ($kd_rek_4 == 11) {
            $this->table_db = 'ta_kartu_pajak_air';
        } else if ($kd_rek_4 == 12) {
            $this->table_db = 'ta_kartu_pajak_walet';
        } else if ($kd_rek_4 == 15) {
            $this->table_db = 'ta_kartu_bphtb';
        }

        $this->table_db = "$this->table_db a";

        $join = [
                'ref_rek_6' => ['ref_rek_6 b', 'a.id_rek_6 = b.id_rek_6', 'LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6'        => 'b.nm_rek_6',
            'tarif_pajak'     => 'a.tarif_pajak',
            'dasar_pengenaan' => 'a.dasar_pengenaan',
            'pajak_terhutang' => 'a.pajak_terhutang'
        ];

        $where    = null;
        $where_e  = "a.pungut_id = $id_pungut";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['a.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['a.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.status, a.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->nm_rek_6),
                $rows->tarif_pajak.'%',
                uang($rows->dasar_pengenaan),
                uang($rows->pajak_terhutang),
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // show modal
    public function select_sptpd($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $wp                 = $ids[1];

        $this->table_db = 'ta_sspd z';

        $join = [
                'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut a', 'z.pungut_id = a.id', 'LEFT'],
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak b', 'a.wp_usaha_pajak_id = b.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha c', 'b.wp_usaha_id = c.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak d', 'c.wp_id = d.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum e', 'd.data_umum_id = e.id', 'LEFT']
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_sptpd'  => 'a.no_sptpd',
            'tgl_sptpd' => 'a.tgl_sptpd',
            'nm_usaha'  => 'c.nm_usaha',
        ];

        $where    = null;
        $where_e  = "e.id = $wp and b.jns_pajak = $kd_rek_4 and a.status_sspd = 1";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['a.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['a.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.wp_usaha_pajak_id, a.id, z.id as sspd_id, a.status, a.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->no_sptpd),
                tgl_format($rows->tgl_sptpd),
                strtoupper($rows->nm_usaha),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.','.$rows->wp_usaha_pajak_id.','.$rows->sspd_id.','.$kd_rek_4.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    public function get_sptpd()
    {
        // echo "<pre>",print_r($this->input->post()),exit();
        $id                = $this->input->post('id');
        $wp_usaha_pajak_id = $this->input->post('wp_usaha_pajak_id');
        $sspd_id           = $this->input->post('sspd_id');
        $kd_rek_4          = $this->input->post('kd_rek_4');
        $sptpd             = $this->mdb->get_sspd($id, $sspd_id);
        $total_pajak       = $this->mdb->sum_sspd($sptpd->id, $this->table_db);

        $data['records']   = $sptpd;
        $data['masa1'] = date("d-m-Y", strtotime($data['records']->masa1));
        $data['masa2'] = date("d-m-Y", strtotime($data['records']->masa2));
        $data['records2']  = $total_pajak;

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function print_pdf($ids){

    // echo $this->uri->segment(6),exit();
    $kd_rek_4                = $ids[0];
    $title                   = str_replace('%20', ' ', $ids[1]);
    $wp                      = $ids[3];
    $id                      = $ids[4];

    $data['records'] = $this->mdb->data($id);
    // echo "<pre>";
    // print_r ($data['records']);exit();
    // echo "</pre>";

    $data['jumlah1'] = $this->mdb->jumlah1($data['records']->pungut_id,$this->table_db);
    $data['jumlah2'] = $this->mdb->jumlah2($data['records']->sspd_id);

    //$hasil = $data['jumlah1']->jumlah - $data['jumlah2']->jumlah;
    // echo $data['jumlah1']->jumlah;exit;
    // echo $this->db->last_query();

    // echo $data['records']->pungut_id; exit();
    // echo $this->table_db;exit;

   $query = $this->db->query("select b.no_sptpd, b.tgl_sptpd, a.pajak_terhutang,a.dasar_pengenaan,a.pajak_terhutang, e.no_nota, e.tgl_nota, e.total_pajak_real, c.*, e.total_bayar, f.tagihan
            from $this->table_db a
            left join ta_kartu_pajak_pungut b on a.pungut_id = b.id
            left join ref_rek_6 c on a.id_rek_6 = c.id_rek_6
            left join ta_sspd d on b.id = d.pungut_id
            left join ta_nota e on b.id = e.sptpd_id
            left join ta_skpd f on e.id = f.nota_id
            where d.jns_sspd = 1 and b.id = ".$data['records']->pungut_id);


   $sptpd = $query->result();

   $query1 = $this->db->query("select a.total_bayar,a.total_pajak_real,a.no_nota,a.tgl_nota from ta_nota a where a.id = $id");   

   $sspd = $query1->row();


    $this->load->library('fpdf_gen');
    $pdf = new fpdf('P','mm','A4');

    $kota = $this->mdb->kota();

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(true, 0);

    $image = base_url('./assets/img/'.$kota->logo);
    $pdf->Image($image,10,10,20,20);

    $pdf->SetFont('Times', 'B', 12);
    $pdf->Cell(25,7,'', 'LTR', 0, 'C');
    $pdf->Cell(165,7,$kota->nm_pemda, 'LTR', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(25,7,'', 'LR', 0, 'C');
    $pdf->Cell(165,7,'BADAN PENDAPATAN DAERAH', 'LR', 1, 'C');

    $pdf->SetFont('Times', '', 11);
    $pdf->Cell(25,7,'', 'LBR', 0, 'C');
    $pdf->Cell(165,7,$kota->alamat, 'LRB', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(25,7,'', 'L', 0, 'C');
    $pdf->Cell(165,7,'NOTA TAMBAHAN PAJAK DAERAH', 'R', 1, 'C');

    $pdf->SetFont('Times', 'I', 11);
    $pdf->Cell(25,5,'', 'L', 0, 'C');
    $pdf->Cell(165,5,'(Self Assesment)', 'R', 1, 'C');

    $pdf->Cell(60,5,'', 'L', 0, 'L');
    $pdf->Cell(5,5,'', '', 0, 'C');
    $pdf->Cell(125,5,'', 'R', 1, 'L');

    $pdf->SetFont('Times', '', 10);
    $pdf->Cell(60,5,'           Nomor', 'L', 0, 'L');
    $pdf->Cell(5,5,':', '', 0, 'C');
    $pdf->Cell(125,5,$data['records']->no_nota, 'R', 1, 'L');

    $pdf->Cell(60,7,'           Masa Pajak', 'L', 0, 'L');
    $pdf->Cell(5,7,':', '', 0, 'C');
    $pdf->Cell(125,7,$data['records']->masa1.' s.d '.$data['records']->masa2, 'R', 1, 'L');

    $pdf->Cell(60,5,'           Tahun Pajak', 'L', 0, 'L');
    $pdf->Cell(5,5,':', '', 0, 'C');
    $pdf->Cell(125,5,$data['records']->tahun, 'R', 1, 'L');

    $pdf->Cell(60,7,'           Nama Wajib Pajak', 'L', 0, 'L');
    $pdf->Cell(5,7,':', '', 0, 'C');
    $pdf->Cell(125,7,$data['records']->nama_pendaftar, 'R', 1, 'L');

    $pdf->Cell(60,5,'           Alamat', 'L', 0, 'L');
    $pdf->Cell(5,5,':', '', 0, 'C');
    $pdf->Cell(125,5,$data['records']->jalan.',RT/RW '.$data['records']->rtrw.',Kel. '.$data['records']->nm_kel.',Kec.'.$data['records']->nm_kec, 'R', 1, 'L');

    $pdf->Cell(60,5,'', 'L', 0, 'L');
    $pdf->Cell(5,5,'', '', 0, 'C');
    $pdf->Cell(125,5,' Kab/Kota. '.$data['records']->kabupaten.', Kode Pos. '.$data['records']->kode_pos, 'R', 1, 'L');

    $pdf->Cell(60,7,'           Nama Usaha', 'L', 0, 'L');
    $pdf->Cell(5,7,':', '', 0, 'C');
    $pdf->Cell(125,7,$data['records']->nm_usaha, 'R', 1, 'L');

    $pdf->Cell(60,5,'           Alamat Usaha', 'L', 0, 'L');
    $pdf->Cell(5,5,':', '', 0, 'C');
    $pdf->Cell(125,5,$data['records']->alamat_usaha, 'R', 1, 'L');

    $pdf->Cell(60,7,'           NPWPD', 'L', 0, 'L');
    $pdf->Cell(5,7,':', '', 0, 'C');
    $pdf->Cell(125,7,$data['records']->npwpd, 'R', 1, 'L');

    $pdf->Cell(60,5,'           Uraian', 'L', 0, 'L');
    $pdf->Cell(5,5,':', 0, 0, 'C');
    $pdf->Cell(125,5,$data['records']->keterangan, 'R', 1, 'L');

    $pdf->Cell(60,3,'', 'LB', 0, 'L');
    $pdf->Cell(5,3,'', 'B', 0, 'C');
    $pdf->Cell(125,3,'', 'BR', 1, 'L');

    $pdf->Cell(60,7,'A. Dasar Pengenaan Pajak', 'L', 0, 'L');
    $pdf->Cell(5,7,'', 0, 0, 'C');
    $pdf->Cell(125,7,'', 'R', 1, 'L');

    $pdf->Cell(15,7,'No', 'LTR', 0, 'C');
    $pdf->Cell(65,7,'SPTPD', 1, 0, 'C');
    $pdf->Cell(35,7,'Kode Rekening', 'T', 0, 'C');
    $pdf->Cell(40,7,'Uraian', 'LTR', 0, 'C');
    $pdf->Cell(35,7,'Pajak Terhutang', 'TR', 1, 'C');

    $pdf->Cell(15,5,'', 'LBR', 0, 'C');
    $pdf->Cell(40,5,'Nomor', 1, 0, 'C');
    $pdf->Cell(25,5,'Tanggal', 1, 0, 'C');
    $pdf->Cell(35,5,'', 'B', 0, 'C');
    $pdf->Cell(40,5,'', 'LBR', 0, 'C');
    $pdf->Cell(35,5,'', 'BR', 1, 'C');

    $no = 1;
    $i = 0;
    foreach ($sptpd as $value) {

        if(++$i > 1)
            break;
    $rekening =
        $pdf->SetFont('Times', '', 9);
        $pdf->Cell(15,7,$no++, 'LR', 0, 'C');
        $pdf->Cell(40,7,$value->no_sptpd, 'LR', 0, 'C');
        $pdf->Cell(25,7,$value->tgl_sptpd, 'LR', 0, 'C');
        $pdf->Cell(35,7,$value->kd_rek_1.'.'.$value->kd_rek_2.'.'.$value->kd_rek_3.'.'.$value->kd_rek_4.'.'.$value->kd_rek_5.'.'.$value->kd_rek_6, '', 0, 'C');
        $pdf->Cell(40,7,$value->nm_rek_6, 'LR', 0, 'C');
        $pdf->Cell(35,7,uang($value->pajak_terhutang), 'R', 1, 'C');

        $pdf->Cell(15,7,'', 'LBR', 0, 'C');
        $pdf->Cell(40,7,'', 'LRB', 0, 'C');
        $pdf->Cell(25,7,'', 'LRB', 0, 'C');
        $pdf->Cell(35,7,'', 'B', 0, 'C');
        $pdf->Cell(40,7,uang($value->dasar_pengenaan).' X '.$value->tarif.'.00'.' %', 'LBR', 0, 'C');
        $pdf->Cell(35,7,'', 'BR', 1, 'C');

    }

    $pdf->SetFont('Times', 'B', 10);
    $pdf->Cell(15,7,'', 'LB', 0, 'C');
    $pdf->Cell(40,7,'', 'B', 0, 'C');
    $pdf->Cell(25,7,'', 'B', 0, 'C');
    $pdf->Cell(35,7,'', 'B', 0, 'C');
    $pdf->Cell(40,7,'JUMLAH (A)', 'BR', 0, 'R');
    $pdf->Cell(35,7,uang($data['jumlah1']->jumlah), 'BR', 1, 'C');

    $pdf->SetFont('Times', '', 10);
    $pdf->Cell(190,8,'B. Setoran yang ditambahkan', 'LRB', 1, 'L');

    $pdf->Cell(15,7,'No', 'LTR', 0, 'C');
    $pdf->Cell(65,7,'Nota Tambahan', 1, 0, 'C');
    $pdf->Cell(35,7,'Pajak yang Dibayar', 'TR', 0, 'C');
    $pdf->Cell(40,7,'Pajak Real', 'T', 0, 'C');
    $pdf->Cell(35,7,'Setoran', 'LTR', 1, 'C');

    $pdf->Cell(15,5,'', 'LBR', 0, 'C');
    $pdf->Cell(40,5,'Nomor', 1, 0, 'C');
    $pdf->Cell(25,5,'Tanggal', 1, 0, 'C');
    $pdf->Cell(35,5,'', 'BR', 0, 'C');
    $pdf->Cell(40,5,'', 'B', 0, 'C');
    $pdf->Cell(35,5,'', 'LBR', 1, 'C');


    $pdf->SetFont('Times', '', 9);
    $pdf->Cell(15,7,'1', 'LR', 0, 'C');
    $pdf->Cell(40,7,$sspd->no_nota, 'LR', 0, 'C');
    $pdf->Cell(25,7,$sspd->tgl_nota, 'LR', 0, 'C');
    $pdf->Cell(35,7,uang($sspd->total_bayar), 'R', 0, 'C');
    $pdf->Cell(40,7,uang($sspd->total_pajak_real), '', 0, 'C');
    $pdf->Cell(35,7,uang($sspd->total_pajak_real - $sspd->total_bayar), 'LR', 1, 'C');


    $pdf->SetFont('Times', 'B', 10);
    $pdf->Cell(15,7,'', 'LBT', 0, 'C');
    $pdf->Cell(40,7,'', 'BT', 0, 'C');
    $pdf->Cell(25,7,'', 'BT', 0, 'C');
    $pdf->Cell(75,7,'JUMLAH (B)', 'BT', 0, 'R');
    $pdf->Cell(35,7,uang($sspd->total_pajak_real - $sspd->total_bayar), 1, 1, 'C');


    $pdf->Cell(190,8,'D. Sanksi administrasi :', 'LR', 1, 'L');

    $pdf->Cell(40,7,'', 'L', 0, 'C');
    $pdf->Cell(40,7,'1. Denda', 0, 0, 'L');
    $pdf->Cell(50,7,'0%', 0, 0, 'C');
    $pdf->Cell(60,7,'', 'R', 1, 'C');

    $pdf->Cell(40,7,'', 'L', 0, 'C');
    $pdf->Cell(40,7,'', 0, 0, 'L');
    $pdf->Cell(50,7,'Jumlah Sanksi(D)', 0, 0, 'C');
    $pdf->Cell(60,7,'0,00', 'BR', 1, 'R');

    $pdf->Cell(40,7,'', 'L', 0, 'C');
    $pdf->Cell(40,7,'', 0, 0, 'L');
    $pdf->Cell(50,7,'Total', 0, 0, 'C');
    $pdf->Cell(60,7,uang($sspd->total_pajak_real - $sspd->total_bayar), 'BR', 1, 'R');

    $pdf->Cell(40,8,'Jumlah Dengan huruf   :', 'BL', 0, 'L');
    $pdf->Cell(150,8,'', 'BR', 1, 'L');
    //$pdf->Cell(150,8,ucwords(terbilang($hasil)), 'BR', 1, 'L');

    $pdf->Cell(95,8,'Mengetahui,', 'L', 0, 'C');
    $pdf->Cell(95,8,ucwords($kota->ibukota).', '.tgl_format(date("Y-m-d")), 'R', 1, 'C');

    $pdf->Cell(190,15,'', 'LR', 1, 'C');

    $pdf->Cell(95,5,$data['records']->nm_penetapan.',', 'L', 0, 'C');
    $pdf->Cell(95,5,$data['records']->nm_perhitungan.',', 'R', 1, 'C');

    $pdf->Cell(95,5,'NIP. '.$data['records']->nip_penetapan, 'L', 0, 'C');
    $pdf->Cell(95,5,'NIP. '.$data['records']->nip_perhitungan, 'R', 1, 'C');

    $pdf->Cell(190,2,'', 'LBR', 1, 'C');

    $pdf->Output("Nota Perhitungan.pdf","I");

    }
}
