<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penetapan_pajak_np extends Admin_Controller
{
    private $prefix         = 'penetapan/penetapan_pajak_np';
    private $url            = 'penetapan/penetapan_pajak_np';
    private $path           = 'penetapan/pajak/nota_perhitungan';
    private $table_db       = 'ref_rek_4';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'Nota Perhitungan Pajak';
        $data['subtitle']   = 'manage nota perhitungan pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'Nota' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( $this->path.'/index', $data, $js, $css );
	}

    public function select()
    {

        $user_id = $this->session->user_data->user_id;
        
        $join = [
                    'ref_pemungutan' => ['ref_pemungutan', 'ref_rek_4.jns_pemungutan = ref_pemungutan.jn_pemungutan', 'LEFT'],
                    'ta_sspd' => ['ta_sspd z', 'z.jns_pajak = ref_rek_4.id_rek_4 AND z.status <> 99 AND z.status_nota = 0 AND z.jns_sspd = 1', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'ref_rek_4.nm_rek_4',
            'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where    = null;
        // query session petugas

        $query = $this->db->query("select * from user_rek_4 where user_id = $user_id")->row();

        if ($this->session->user_data->user_role == 1) {
        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 != 72 and id_rek_4 != 15";
            
        } else {

        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 in ($query->id_rek_4)";
            
        }
        
        $group    = "ref_rek_4.id_rek_4";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e, $group);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_rek_4.jns_pemungutan, ref_rek_4.nm_usaha_4, ref_rek_4.id_rek_4, ref_rek_4.link, COUNT(z.id) as jumlah, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength, $group);
        // echo $this->db->last_query();exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              ucwords($rows->nm_rek_4),
              ucwords ($rows->nm_jn_pemungutan),
              '<a href="'.base_url('penetapan/penetapan_pajak_np_sub/'.$rows->id_rek_4.'/'.$rows->nm_usaha_4).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="SPTPD Masa"><i class="fa fa-file-text"></i></a><span class="badge badge-default">'.$rows->jumlah.'</span>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

}

/* End of file Penetapan_pajak_np.php */
/* Location: ./application/modules/data_entry/controllers/Penetapan_pajak_np.php */
