<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penetapan_pajak_skpd_sub extends Admin_Controller
{
    private $prefix         = 'penetapan/penetapan_pajak_skpd_sub';
    private $url            = 'penetapan/penetapan_pajak_skpd_sub';
    private $path           = 'penetapan/pajak/skpd';
    private $path_skpd      = 'penetapan/penetapan_pajak_skpd';
    private $table_db       = '';
    private $table_db_kartu = '';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_penetapan_nota_skpd', 'mdb');

        if ($this->uri->segment(4) == 4) {
            $this->table_db_kartu = 'ta_kartu_pajak_hotel';
        } else if ($this->uri->segment(4) == 5) {
            $this->table_db_kartu = 'ta_kartu_pajak_restoran';
        } else if ($this->uri->segment(4) == 6) {
            $this->table_db_kartu = 'ta_kartu_pajak_hiburan';
        } else if ($this->uri->segment(4) == 7) {
            $this->table_db_kartu = 'ta_kartu_pajak_reklame';
        } else if ($this->uri->segment(4) == 8) {
            $this->table_db_kartu = 'ta_kartu_pajak_penerangan';
        } else if ($this->uri->segment(4) == 9) {
            $this->table_db_kartu = 'ta_kartu_pajak_mineral';
        } else if ($this->uri->segment(4) == 10) {
            $this->table_db_kartu = 'ta_kartu_pajak_parkir';
        } else if ($this->uri->segment(4) == 11) {
            $this->table_db_kartu = 'ta_kartu_pajak_air';
        } else if ($this->uri->segment(4) == 12) {
            $this->table_db_kartu = 'ta_kartu_pajak_walet';
        } else if ($this->uri->segment(4) == 15) {
            $this->table_db_kartu = 'ta_kartu_bphtb';
        }
    }

    public function skpd($kd_rek_4, $nm_rek_4)
    {
        $title               = str_replace('%20', ' ', $nm_rek_4);
        $data['kd_rek_4']    = $kd_rek_4;
        $data['title']       = $title;

        $data['pagetitle']   = "SKPD $title";
        $data['subtitle']    = "SKPD $title";

        $data['url']         = base_url().$this->url;
        $data['breadcrumb']  = ['Penetapan' => null, 'SKPD' => null];

        $js['js']            = [ 'table-datatables-ajax' ];
        $css['css']          = null;

        $this->template->display($this->path.'/skpd', $data, $js, $css);
    }

    public function select($kd_rek_4, $nm_rek_4)
    {
        $title          = str_replace('%20', ' ', $nm_rek_4);
        $this->table_db = 'ta_skpd a';

		$join = [
			'ta_nota'                    => ['ta_nota b', 'a.nota_id                              = b.id', 'LEFT'],
			'ta_sspd'                    => ['ta_sspd c', 'b.sspd_id                              = c.id', 'LEFT'],
			'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut d', 'c.pungut_id              = d.id', 'LEFT'],
			'ta_kartu_pajak'             => ["$this->table_db_kartu i", 'i.pungut_id              = d.id', 'LEFT'],
			'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak e', 'd.wp_usaha_pajak_id = e.id', 'LEFT'],
			'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha f', 'e.wp_usaha_id             = f.id', 'LEFT'],
			'wp_wajib_pajak'             => ['wp_wajib_pajak g', 'f.wp_id                         = g.id', 'LEFT'],
			'wp_data_umum'               => ['wp_data_umum h', 'g.data_umum_id                    = h.id', 'LEFT'],
		];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];
            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_skpd'        => 'a.no_skpd',
            'npwpd'          => 'h.npwpd',
            'nama_pendaftar' => 'h.nama_pendaftar',
            'nm_usaha'       => 'f.nm_usaha',
            'alamat_usaha'   => 'f.alamat_usaha',
            'keterangan'     => 'a.keterangan'
        ];

        $where      = NULL;
        $where_e    = "b.jenis_nota = 'perhitungan' and a.status = '1' and a.jns_skpd = 1 and i.id IS NOT NULL";

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = "b.jenis_nota = 'perhitungan' and a.status = '$request' and a.jns_skpd = 1 and i.id IS NOT NULL";
        }
        else {
            $where_e = "b.jenis_nota = 'perhitungan' and a.status = '1' and a.jns_skpd = 1 and i.id IS NOT NULL";
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count( $this->table_db, $join, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'f.alamat_usaha, d.no_sptpd, c.no_sspd, b.no_nota, a.*, '.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query();exit();
        // echo "<pre>", print_r($result), exit;

        $i               = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                '<span class="label label-m label-primary">'.$rows->no_skpd.'</span>',
                '<span class="label label-m label-primary">'.$rows->npwpd.'</span>',
                strtoupper($rows->nama_pendaftar),
                strtoupper($rows->nm_usaha),
                strtoupper($rows->alamat_usaha),
                strtoupper($rows->keterangan),
                ($this->session->user_data->user_role !=1)?
                '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'/'.$kd_rek_4.'/'.$title.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                 '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_skpd/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 1 ? 'grey-cascade hide' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_skpd/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'.
                '<a data-original-title="Ubah Status" href="'.base_url().$this->url.'/show_ubah/'.$rows->id.'/'.$kd_rek_4.'/'.$title.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-refresh"></i></a>'
                : 
                '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit_admin/'.$rows->id.'/'.$kd_rek_4.'/'.$title.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_skpd/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 1 ? 'grey-cascade hide' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_skpd/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'.
                '<a data-original-title="Ubah Status" href="'.base_url().$this->url.'/show_ubah/'.$rows->id.'/'.$kd_rek_4.'/'.$title.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-refresh"></i></a>',
                '<span class="label label-m label-primary">'.$rows->no_sptpd.'</span>',
                '<span class="label label-m label-primary">'.$rows->no_sspd.'</span>',
                '<span class="label label-m label-primary">'.$rows->no_nota.'</span>',
            );

            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add($kd_rek_4, $nm_rek_4)
    {
        // echo "<pre>",print_r($ids),exit();
        $title                   = str_replace('%20', ' ', $nm_rek_4);

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;

        $data['bank']            = $this->mdb->show_bank();
        $data['ttd_penetapan']   = $this->mdb->petugas(2);

        $data['pagetitle']       = "Add Pajak $title";
        $data['subtitle']        = "Add Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb']      = ['Penetapan' => null, 'SKPD' => null,'Jenis Pajak' => $this->url.'/skpd/'.$kd_rek_4.'/'.$title, 'Add' => null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form', $data, $js, $css);
    }

    public function show_edit($id, $kd_rek_4, $nm_rek_4)
    {
        // echo "<pre>",print_r($ids),exit();
        $title                   = str_replace('%20', ' ', $nm_rek_4);

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;

        $data['bank']            = $this->mdb->show_bank();
        $data['ttd_penetapan']   = $this->mdb->petugas(2);
        $data['skpd']            = $this->mdb->show_skpd($id);
        
        $data['pagetitle']       = "Edit Pajak $title";
        $data['subtitle']        = "Edit Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb']      = ['Penetapan' => null, 'SKPD' => null,'Jenis Pajak' => $this->url.'/skpd/'.$kd_rek_4.'/'.$title,'Edit' => null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form', $data, $js, $css);
    }

    public function show_edit_admin($id, $kd_rek_4, $nm_rek_4)
    {
        // echo "<pre>",print_r($ids),exit();
        $title                   = str_replace('%20', ' ', $nm_rek_4);

        $data['kd_rek_4']        = $kd_rek_4;
        $data['title']           = $title;

        $data['bank']            = $this->mdb->show_bank();
        $data['ttd_penetapan']   = $this->mdb->petugas(2);
        $data['skpd']            = $this->mdb->show_skpd($id);
        
        $data['pagetitle']       = "Edit Pajak $title";
        $data['subtitle']        = "Edit Pajak $title";

        $data['url']             = base_url().$this->url;
        $data['breadcrumb']      = ['Penetapan' => null, 'SKPD' => null,'Jenis Pajak' => $this->url.'/skpd/'.$kd_rek_4.'/'.$title,'Edit' => null];

        $js['js']                = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']              = null;

        $this->template->display($this->path.'/form_admin', $data, $js, $css);
    }

    public function action_form($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'ta_skpd';

        $this->form_validation->set_rules('tgl_skpd', 'Tgl SKPD', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

        if ($this->form_validation->run($this)) {

            $data[$this->table_prefix.'nota_id']         = $this->input->post('nota_id');
            $data[$this->table_prefix.'user_id']         = $this->input->post('user_id');
            $data[$this->table_prefix.'tgl_skpd']        = $this->m_global->setdateformat($this->input->post('tgl_skpd'));
            $data[$this->table_prefix.'masa1']           = $this->m_global->setdateformat($this->input->post('masa1'));
            $data[$this->table_prefix.'masa2']           = $this->m_global->setdateformat($this->input->post('masa2'));
            $data[$this->table_prefix.'keterangan']      = $this->input->post('keterangan');
            $data[$this->table_prefix.'ttd_dok_id']      = $this->input->post('ttd_penetapan') == '' ? null : $this->input->post('ttd_penetapan');
            $data[$this->table_prefix.'tgl_jatuh_tempo'] = $this->m_global->setdateformat($this->input->post('tgl_jatuh_tempo'));
            $data[$this->table_prefix.'kd_bank']         = $this->input->post('kd_bank') == '' ? null : $this->input->post('kd_bank');
            $data[$this->table_prefix.'jns_skpd']        = 1;

            if ($ids == null) {
                $date    = date("d/m/Y");
                $no_skpd = $this->mdb->no_skpd('S', "/SKPD/$date");

                $data[$this->table_prefix.'id']      = $this->mdb->auto_id();
                $data[$this->table_prefix.'tahun']   = date('Y');
                $data[$this->table_prefix.'no_skpd'] = $no_skpd;

                // update ta_nota
                $data2[$this->table_prefix.'status_skpd'] = 1;
                $result2 = $this->db->where('id',$this->input->post('nota_id'))->update('ta_nota',['status_skpd'=>1]);

                // update ta_nota
                $data2[$this->table_prefix.'skpdn_status'] = 1;
                $result2 = $this->db->where('id',$this->input->post('sspd_id'))->update('ta_sspd',['skpdn_status'=>1]);

                // instert ta_skpd
                $result  = $this->m_global->insert($this->table_db, $data);

                //data log
                $log['id']      = $this->input->post('user_id');
                $log['action']  = 'Add Pajak SKPD ';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'No. SKPD : '.$this->input->post('no_skpd').' Jenis SKPD : Umum, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('skpd_log', $log);

            } else {
                $id = $ids;

                //data log
                $log['id']      = $this->input->post('user_id');
                $log['action']  = 'Edit Pajak SKPD ';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'No. SKPD : '.$this->input->post('no_skpd').' Jenis SKPD : Umum, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('skpd_log', $log);

                // update ta_skpd
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // SSPD ubah admin
    public function show_ubah($id, $kd_rek_4, $nm_rek_4)
    {
        // echo "<pre>",print_r($ids),exit();
        $title                   = str_replace('%20', ' ', $nm_rek_4);

        $data['kd_rek_4'] = $kd_rek_4;
        $data['title']    = $title;
        $data['id']       = $id;
        $data['label']      = $this->db->query("select status_label, status_tipe, status_detail, status_group from status WHERE status_group ='ta_skpd' GROUP BY status_label")->result_array();

        foreach($data['label'] as $val){

            $data['isi'][$val['status_tipe']]= $this->db->query("SELECT status_detail, status_id, status_label FROM status where status_tipe in ('".$val['status_tipe']."') and status_group = '$val[status_group]'")->result();

        }
        
        $data['pagetitle']  = "Pajak $title";
        $data['subtitle']   = "Pajak $title";
        
        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = ['Penetapan' => null, 'SKPD' => null];
        
        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('penetapan/pajak/skpd/form_ubah', $data, $js, $css);
    }

    // SSPD ubah admin
    public function action_ubah($ids)
    {
        // echo '<pre>', print_r($this->input->post()),exit();
        $this->form_validation->set_rules('id', 'ID', 'trim');

        if ($this->form_validation->run($this)) {
            $detail = 'Ubah Status : ';
            foreach($this->input->post('combo') as $key => $val){
                $data[$key] = $val;
                $detail = $detail.$key.' = '.$val.', ';
            }

            $query = $this->db->query("select * from wp_data_umum where id = $ids[0]")->row();
            
            $log['id']      = $ids[0];
            $log['action']  = 'Ubah Status';
            $log['detail']  = $query->npwpd.' '.$detail;
            $log['status']  = '1';
            $log['user_id'] = $this->session->user_data->user_id;
            $log['ip']      = $_SERVER['REMOTE_ADDR'];
            $result2        = $this->db->insert('skpd_log', $log);

            $result         = $this->m_global->update('ta_skpd', $data, ['id' => $ids[0]]);


            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // show modal
    public function select_nota($kd_rek_4)
    {
        $this->table_db = 'ta_nota b';

		$join = [
			'ta_sspd'                    => ['ta_sspd c', 'b.sspd_id                              = c.id', 'LEFT'],
			'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut d', 'c.pungut_id              = d.id', 'LEFT'],
			'ta_kartu_pajak'             => ["$this->table_db_kartu i", 'i.pungut_id              = d.id', 'LEFT'],
			'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak e', 'd.wp_usaha_pajak_id = e.id', 'LEFT'],
			'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha f', 'e.wp_usaha_id             = f.id', 'LEFT'],
			'wp_wajib_pajak'             => ['wp_wajib_pajak g', 'f.wp_id                         = g.id', 'LEFT'],
			'wp_data_umum'               => ['wp_data_umum h', 'g.data_umum_id                    = h.id', 'LEFT'],
		];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_nota'  => 'b.no_nota',
            'tgl_nota' => 'b.tgl_nota',
            'nm_usaha' => 'f.nm_usaha',
        ];

        $where    = null;
        $where_e  = "b.jenis_nota = 'perhitungan' and b.status = '1' and b.status_skpd = 0 AND i.id IS NOT NULL";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = "b.jenis_nota = 'perhitungan' and b.status = '$request' and b.status_skpd = 0 AND i.id IS NOT NULL";
        }
        else {
            $where_e = "b.jenis_nota = 'perhitungan' and b.status = '1' and b.status_skpd = 0 AND i.id IS NOT NULL";
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'b.id, b.total_bayar,h.user_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                $i,
                strtoupper($rows->no_nota),
                strtoupper($rows->tgl_nota),
                strtoupper($rows->nm_usaha),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function get_nota()
    {
        // echo "<pre>",print_r($this->input->post()),exit();
        $id                  = $this->input->post('id');
        $nota                = $this->db->where('id', $id)->get('ta_nota')->row();
        
        $data['records']     = $nota;
        $data['masa1']       = date("d-m-Y", strtotime($data['records']->masa1));
        $data['masa2']       = date("d-m-Y", strtotime($data['records']->masa2));
        $data['total_bayar'] = $data['records']->total_bayar;
        $data['sspd_id']     = $data['records']->sspd_id;
        $data['user_id']     = $data['records']->user_id;

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];

        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id,$table, $status,$stat = false)
    {
        // echo "<pre>",print_r($id),exit();
        // $id     = $ids[0];
        // $table  = $ids[1];
        // $status = $ids[2];
        // $stat   = $ids[3];

        if ($stat == 'true') {
            // update sptpd
            $pungut               = $this->db->query("SELECT nota_id from ta_skpd  where id = $id")->row();
            $nota                 = $pungut->id;
            $data2['status_skpd'] = 0;
            $result2              = $this->m_global->update('ta_nota', $data2, ['id' => $nota]);

            $result               = $this->m_global->delete('ta_skpd', ['id' => $id]);
        } else {
            $result               = $this->m_global->update('ta_skpd', ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

}
