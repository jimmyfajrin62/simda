<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=$records->nm_usaha?>">
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>SPTPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->no_sptpd?>" name="no_sptpd" id="no_sptpd" readonly required>
                                    <span class="input-group-btn">
                                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening" id="add_rekening">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Masa Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group input-large date-picker input-daterange date">
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa1" id="masa1" value="<?=@$data->masa1?>" readonly required>
                                    <span class="input-group-addon"> s/d </span>
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa2" id="masa2" value="<?=@$data->masa2?>" readonly required>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nota SKPD</strong>
                                </label>
                                <input style="text-transform:uppercase" type="hidden" name="sspd_id" value="" id="sspd_id">
                                <input style="text-transform:uppercase" type="hidden" name="pungut_id" value="" id="pungut_id">
                                <input style="text-transform:uppercase" type="hidden" name="jns_pajak" value="<?=$kd_rek_4?>">
                                <input style="text-transform:uppercase" type="hidden" name="jenis_nota" value="jabatan">
                                <input style="text-transform:uppercase" type="hidden" name="tahun" value="<?=date('Y')?>">
                                <input style="text-transform:uppercase" type="hidden" name="wp_usaha_pajak_id" value="" id="wp_usaha_pajak_id">
                                <input style="text-transform:uppercase" type="text" class="form-control" name="no_nota" readonly value="<?=@$data->no_nota?>" placeholder="Nomer Nota Perhitungan Generate">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal SKPD/N</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->tgl_nota?>" name="tgl_nota" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>Uraian</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="keterangan" value="<?=@$data->keterangan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Bayar</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="total_bayar" name="total_bayar" value="<?=@$data->total_bayar?>" placeholder="Total Bayar" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Pajak</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="total_pajak" value="<?=@$data->total_pajak?>" name="total_pajak" id="total_pajak" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Pajak Terhutang</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="pajak_terhutang" name="pajak_terhutang" value="<?=@$data->pajak_terhutang?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Penandatangan Penetapan</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="ttd_penetapan" id="ttd_penetapan" style="text-transform:uppercase">
                                    <option value="">Pilih Petugas</option>
                                    <?php foreach ($ttd_penetapan as $val): ?>
                                        <?php @$data->ttd_penetapan == $val->no_urut ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->no_urut?>" data-1="<?=$val->nip_penandatangan?>" data-2="<?=$val->nm_jab?>" <?=$selected?>><?=$val->nm_penandatangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nip_penetapan" id="nip_penetapan" readonly value="<?=@$data->nip_penetapan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jbt_penetapan" id="jbt_penetapan" readonly value="<?=@$data->jbt_penetapan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Penandatangan Perhitungan</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="ttd_perhitungan" id="ttd_perhitungan" style="text-transform:uppercase">
                                    <option value="">Pilih Petugas</option>
                                    <?php foreach ($ttd_perhitungan as $val): ?>
                                        <?php @$data->ttd_perhitungan == $val->no_urut ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->no_urut?>" data-1="<?=$val->nip_penandatangan?>" data-2="<?=$val->nm_jab?>" <?=$selected?>><?=$val->nm_penandatangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nip_perhitungan" id="nip_perhitungan" readonly value="<?=@$data->nip_perhitungan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jbt_perhitungan" id="jbt_perhitungan" readonly value="<?=@$data->jbt_perhitungan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/skpd/<?=$kd_rek_4?>/<?=$title?>/<?=$jns_skpd?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/skpd/<?=$kd_rek_4?>/<?=$title?>/<?=$jns_skpd?>" class="ajaxify reload"></a>

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <!-- <div class="action pull-right">
                            <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div> -->
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> No.SPTPD </th>
                                    <th width="100px"> Tanggal </th>
                                    <th> Keterangan </th>
                                    <th width="50px"> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="no_sptpd" placeholder="No.SPTPD">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="tanggal" placeholder="Tanggal">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nama_usaha" placeholder="Nama Usaha">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->


<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_sptpd/<?=$kd_rek_4?>/<?=$wp?>/<?=$jns_skpd?>';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });

    function pilih(id, wp_usaha_pajak_id, sspd_id, kd_rek_4){

        $.get({
            url:        '<?=$url?>/get_sptpd/'+id+'/'+wp_usaha_pajak_id+'/'+kd_rek_4+'/'+sspd_id,
            type:       'POST',
            dataType:   'JSON',
            data:       {
                id: id,
                wp_usaha_pajak_id: wp_usaha_pajak_id,
                sspd_id: sspd_id,
                kd_rek_4: kd_rek_4
            },
            success: function(res){
                console.log(res);
                $('#no_sptpd').val(res.records.no_sptpd);
                $('#masa1').val(res.records.masa1);
                $('#masa2').val(res.records.masa2);
                $('#wp_usaha_pajak_id').val(res.records.wp_usaha_pajak_id);
                $('#sspd_id').val(res.records.sspd_id);
                $('#pungut_id').val(res.records.id);
                $('#total_pajak').val(res.records2.total_pajak);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#ttd_penetapan').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penetapan').val(data1);
            $('#jbt_penetapan').val(data2);
        });

        $('#ttd_perhitungan').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_perhitungan').val(data1);
            $('#jbt_perhitungan').val(data2);
        });

      $("#total_bayar,#total_pajak").on('keyup',function(){
          var bayar = $("#total_bayar").val().replace(/[Rp \.\$,]/g, '');
          var pajak  = $("#total_pajak").val().replace(/[Rp \.\$,]/g, '');
          var jumlah   = bayar - pajak;
          $("#pajak_terhutang").val(jumlah);
      });

      $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }
    });

    });
</script>
