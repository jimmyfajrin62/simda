<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $url ?>/show_add/<?=$kd_rek_4?>/<?=$title?>" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

                        <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + 'pendaftaran/pendaftaran_pajak/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:window.open(base_url + 'pendaftaran/pendaftaran_pajak/print_pdf')"> Print PDF </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="20px"> No </th>
                                <th> SKPD </th>
                                <th> NPWPD </th>
                                <th> Nama </th>
                                <th> Usaha </th>
                                <th> Alamat </th>
                                <th> Keterangan </th>
                                <th width="140px"> Action </th>
                                <th class="none"> SPTPD </th>
                                <th class="none"> SSPD </th>
                                <th class="none"> NOTA </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="no_skpd" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="npwpd" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nama_pendaftar" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nm_usaha" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="alamat_usaha" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="keterangan" placeholder=""> </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                    </div>
                                </td>
                                <td class="hide"> </td>
                                <td class="hide"> </td>
                                <td class="hide"> </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // table data
        var select_url = '<?php echo $url ?>' + '/select/<?=$kd_rek_4?>/<?=$title?>';
        var header     = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
        ];
        var order = [
            [4, "DESC"]
        ];
        var sort = [-1, 0];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
