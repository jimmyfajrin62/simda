<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form_sanksi/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=$records->nm_usaha?>">
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Nama Sanksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="keterangan" id="keterangan" style="text-transform:uppercase">
                                    <option value="">--Pilih--</option>
                                    <?php foreach ($sanksi as $val): ?>
                                        <?php @$data->sanksi_id == $val->id_sanksi ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->id_sanksi?>" data-1="<?=$val->id_sanksi?>" data-2="<?=$val->no_urut?>" data-3="<?=$val->nilai?>" data-4="<?=$val->satuan?>" <?=$selected?>><?=$val->keterangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Kode Sanksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="sanksi_id" id="sanksi_id" readonly value="<?=@$data->keterangan?>">
                                <input style="text-transform:uppercase" type="hidden" class="form-control" name="nota_id" value="<?=$id_nota?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Nilai Sanksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="number" class="form-control" name="nilai_sanksi" id="nilai" value="<?=@$data->nilai_sanksi?>" readonly="" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Satuan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" value="%" name="" id="satuan" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Jumlah Bulan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" id="jml_bulan" type="text" class="form-control" name="jml_bulan" value="<?=@$data->jml_bulan?>" placeholder="" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Satuan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->satuan?>" name="satuan" id="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Total Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" id="total_pajak" class="form-control uang" name="total_pajak" readonly value="<?=@$get->total_pajak?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Jumlah Sanksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="jml_sanksi" name="total_sanksi" value="<?=@$data->total_sanksi?>" placeholder="" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_nota_sanksi/<?=$kd_rek_4?>/<?=$nm_rek_4?>/<?=$wp?>/<?=$id_pungut?>/<?=$id_nota?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/show_nota_sanksi/<?=$kd_rek_4?>/<?=$nm_rek_4?>/<?=$wp?>/<?=$id_pungut?>/<?=$id_nota?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#keterangan').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            var data3  = $(this).find(':selected').attr('data-3');
            // var data4  = $(this).find(':selected').attr('data-4');
            $('#sanksi_id').val(data1);
            $('#no_urut').val(data2);
            $('#nilai').val(data3);
            // $('#satuan').val(data4);
        });

      $("#jml_bulan").on('keyup',function(){
          var nilai_sanksi = $("#nilai").val();
          var total_pajak  = $("#total_pajak").val().replace(/[Rp \.\$,]/g, '');
          var jml_bulan    = $("#jml_bulan").val().replace(/[Rp \.\$,]/g, '');
          var jml_sanksi   = ((total_pajak * nilai_sanksi) / 100) * jml_bulan;
          $("#jml_sanksi").val(jml_sanksi);
      });

      $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }

         });
    });
</script>
