<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $url ?>/show_add_sanksi/<?=$kd_rek_4?>/<?=$nm_rek_4?>/<?=$wp?>/<?=$id_pungut?>/<?=$id_nota?>" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <a data-original-title="Reload" href="<?php echo $url ?>/show_sspd_rinci/<?=$kd_rek_4?>/<?=$wp?>/<?=$id_pungut?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

                        <!-- <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <!-- <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue-steel">
                            <option value="ta_teguran_rinc">Delete Multiple</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue-steel table-group-action-submit" onClick="">
                            <i class="fa fa-check"></i>
                        </button>
                    </div> -->
                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th> Kode </th>
                                <th> Uraian </th>
                                <th> No </th>
                                <th> Total Pajak </th>
                                <!-- <th> Setoran </th> -->
                                <th> Nilai </th>
                                <th> Jumlah </th>
                                <th> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td><input type="text" class="form-control form-filter input-sm" name="id_sanks" placeholder="uraian"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="keterangan" placeholder=""> </td>
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <!-- <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td> -->
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="" placeholder=""> </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // table data
        var select_url = '<?php echo $url ?>'+'/select_nota_sanksi/<?=$kd_rek_4?>/<?=$nm_rek_4?>/<?=$wp?>/<?=$id_pungut?>/<?=$id_nota?>';
        console.log(select_url);
        var header     = [
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" }
        ];
        var order = [
            [2, "asc"]
        ];
        var sort = [-1, 0, 1];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
