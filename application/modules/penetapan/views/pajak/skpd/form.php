<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$skpd->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NOTA</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$skpd->no_nota?>" name="no_nota" id="no_nota" readonly required>
                                    <span class="input-group-btn">
                                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening" id="add_rekening">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Masa Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group input-large">
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa1" id="masa1" value="<?=$this->m_global->getdateformat(@$skpd->masa1)?>" readonly required>
                                    <span class="input-group-addon"> s/d </span>
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa2" id="masa2" value="<?=$this->m_global->getdateformat(@$skpd->masa2)?>" readonly required>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nomer SKPD</strong>
                                </label>
                                <input style="text-transform:uppercase" type="hidden" name="nota_id" value="<?=@$skpd->nota_id?>" id="nota_id">
                                <input style="text-transform:uppercase" type="hidden" name="user_id" value="<?=@$skpd->user_id?>" id="user_id">
                                <input style="text-transform:uppercase" type="hidden" name="sspd_id" value="<?=@$skpd->sspd_id?>" id="sspd_id">
                                <input style="text-transform:uppercase" type="hidden" name="tahun" value="<?=date('Y')?>">
                                <input style="text-transform:uppercase" type="text" class="form-control" name="no_skpd" readonly value="<?=@$skpd->no_skpd?>" placeholder="Nomer SKPD Generate">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal SKPD/N</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=$this->m_global->getdateformat(@$skpd->tgl_skpd)?>" name="tgl_skpd" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jatuh Tempo</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=$this->m_global->getdateformat(@$skpd->tgl_jatuh_tempo)?>" name="tgl_jatuh_tempo" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Bank</strong>
                                </label>
                                <select class="form-control" name="kd_bank" id="bank_id" style="text-transform:uppercase">
                                    <option value="">Pilih Bank</option>
                                    <?php foreach ($bank as $val): ?>
                                        <?php @$skpd->kd_bank == $val->id ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->id?>" data-1="<?=$val->no_rekening?>" <?=$selected?>><?=$val->nm_bank?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label"><strong>Uraian</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="keterangan" value="<?=@$skpd->keterangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                </label>
                                <input style="text-transform:uppercase" type="hidden" class="form-control" id="total_bayar" name="total_bayar" value="<?=@$skpd->total_bayar?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Penandatangan Penetapan</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="ttd_penetapan" id="ttd_penetapan" style="text-transform:uppercase">
                                    <option value="">Pilih Petugas</option>
                                    <?php foreach ($ttd_penetapan as $val): ?>
                                        <?php @$skpd->ttd_dok_id == $val->no_urut ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->no_urut?>" data-1="<?=$val->nip_penandatangan?>" data-2="<?=$val->nm_jab?>" <?=$selected?>><?=$val->nm_penandatangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="nip_penetapan" id="nip_penetapan" readonly value="<?=@$skpd->nip_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="jbt_penetapan" id="jbt_penetapan" readonly value="<?=@$skpd->jbt_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/skpd/<?=$kd_rek_4?>/<?=$title?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/skpd/<?=$kd_rek_4?>/<?=$title?>" class="ajaxify reload"></a>

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> NOTA </th>
                                    <th width="100px"> Tanggal </th>
                                    <th> Nama Usaha </th>
                                    <th width="50px"> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="no_nota" placeholder="No.SPTPD">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="tgl_nota" placeholder="Tanggal">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nm_usaha" placeholder="Nama Usaha">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->


<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_nota/<?=$kd_rek_4?>';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });

    function pilih(id){

        $.get({
            url:        '<?=$url?>/get_nota/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {
                id: id
            },
            success: function(res){
                console.log(res);
                $('#nota_id').val(res.records.id);
                $('#no_nota').val(res.records.no_nota);
                $('#masa1').val(res.masa1);
                $('#masa2').val(res.masa2);
                $('#total_bayar').val(res.total_bayar);
                $('#user_id').val(res.user_id);
                $('#sspd_id').val(res.sspd_id);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#ttd_penetapan').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penetapan').val(data1);
            $('#jbt_penetapan').val(data2);
        });

        $('#ttd_perhitungan').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_perhitungan').val(data1);
            $('#jbt_perhitungan').val(data2);
        });

      $("#total_bayar,#total_pajak").on('keyup',function(){
          var bayar = $("#total_bayar").val().replace(/[Rp \.\$,]/g, '');
          var pajak  = $("#total_pajak").val().replace(/[Rp \.\$,]/g, '');
          var jumlah   = bayar - pajak;
          $("#pajak_terhutang").val(jumlah);
      });

      $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }
    });

    });
</script>
