<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends Admin_Controller
{
    private $prefix = 'dashboard';
    private $url    = 'dashboard';

	function __construct()
    {
        parent::__construct();
        $this->load->model('m_dashboard');
    }

	public function index()
	{
        $data['pagetitle']  = 'Dashboard';
        $data['subtitle']   = 'Statistics Analysis';

        $data['url']        = base_url().$this->prefix;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Dashboard' => $this->prefix ];

        $data['pendapatan'] = $this->db->select('sum(total_bayar) as total_bayar')
                                        ->from('ta_sspd')
                                        ->where('tahun', date('Y'))
                                        ->get()->row();

        $data['total_wp']   = $this->db->where('status', '1')
                                        ->get('wp_wajib_pajak')->num_rows();

        $data['wp_pending'] = $this->db->select('a.id')->from('wp_data_umum a')
                                        ->join('wp_wajib_pajak b', 'a.id = b.data_umum_id', 'LEFT')
                                        ->where('a.status_pajak', 0)
                                        ->where('a.status', '1')
                                        ->get()->num_rows();

        $js['js']           = null;
        $css['css']         = null;

        $this->template->display( $this->prefix, $data, $js, $css );
	}

    public function chart_bulan()
    {
        $date = date('Y');

        $sspd = $this->db->query("SELECT m.month,d.total_pajak
                                    FROM (
                                    SELECT 'January' AS MONTH UNION
                                    SELECT 'February' AS MONTH UNION
                                    SELECT 'March' AS MONTH UNION
                                    SELECT 'April' AS MONTH UNION
                                    SELECT 'May' AS MONTH UNION
                                    SELECT 'June' AS MONTH UNION
                                    SELECT 'July' AS MONTH UNION
                                    SELECT 'August' AS MONTH UNION
                                    SELECT 'September' AS MONTH UNION
                                    SELECT 'October' AS MONTH UNION
                                    SELECT 'November' AS MONTH UNION
                                    SELECT 'December' AS MONTH
                                    ) AS m
                                    LEFT JOIN
                                    (
                                    SELECT MONTHNAME(tgl_sspd) AS MONTH,total_pajak, tgl_sspd
                                    FROM ta_sspd
                                    WHERE YEAR(tgl_sspd) = '$date'
                                    GROUP BY MONTH(tgl_sspd))d ON m.MONTH = d.MONTH");

         $data['tgl_sspd']    = array_column($sspd->result_array(), 'tgl_sspd');
         $total_pajak         = array_column($sspd->result_array(), 'total_pajak');
         $data['total_pajak'] = array_map('intval', $total_pajak);

        header("Content-Type:application/json");
        echo json_encode($data);

    }

    public function chart_tahun()
    {
        $date = date('Y');

        $sspd = $this->db->query("SELECT YEAR(tgl_sspd) AS tahun,SUM(total_pajak) as total_pajak
                                    FROM ta_sspd
                                    WHERE YEAR(tgl_sspd) BETWEEN YEAR(current_date)-10 AND YEAR(current_date)
                                    GROUP BY YEAR(tgl_sspd)
                                    ORDER BY TAHUN ASC");

         $data['tahun']       = array_column($sspd->result_array(), 'tahun');
         $total_pajak         = array_column($sspd->result_array(), 'total_pajak');
         $data['total_pajak'] = array_map('intval', $total_pajak);

        header("Content-Type:application/json");
        echo json_encode($data);

    }

}
