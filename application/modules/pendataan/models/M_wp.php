<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_wp extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function cek_add_wp()
    {
        $query = $this->db->query("SELECT id from wp_wajib_pajak");
        return $query->num_rows();
    }

    public function show_wp_row($id)
    {
        $query = $this->db->query("SELECT wp_data_umum.no_daftar, wp_wajib_pajak.*, wp_data_umum.nama_pendaftar, wp_data_umum.npwpd from wp_wajib_pajak
                                    join wp_data_umum on wp_wajib_pajak.data_umum_id = wp_data_umum.id
                                    where wp_wajib_pajak.id = $id");
        return $query->row();
    }

    public function header($id)
    {
        $query = $this->db->query("SELECT wp_data_umum.nama_pendaftar, wp_data_umum.npwpd from wp_wajib_pajak
                                    join wp_data_umum on wp_wajib_pajak.data_umum_id = wp_data_umum.id
                                    where wp_wajib_pajak.id = $id");
        return $query->row();
    }

    public function header2($id)
    {
        $query = $this->db->query("SELECT d.npwpd, d.nama_pendaftar, b.nm_usaha, b.alamat_usaha
                                    from wp_wajib_pajak_usaha b
                                    left join wp_wajib_pajak c on b.wp_id = c.id
                                    left join wp_data_umum d on c.data_umum_id = d.id
                                    where b.id = $id");
        return $query->row();
    }

    public function organisasi_row($id = '')
    {
        if ($id == '') {
            $query = $this->db->query("SELECT * from ref_sub_unit where id_organisasi = 87");
            return $query->row();
        } else {
            $query = $this->db->query("SELECT * from ref_sub_unit where id_organisasi = $id");
            return $query->row();
        }
    }

    public function show_wp_usaha_row($id)
    {

        $query = $this->db->query("SELECT * from wp_wajib_pajak_usaha where id = $id");
        return $query->row();
    }

    public function jns_usaha()
    {
        $query = $this->db->query("SELECT * from ref_rek_4 where id_rek_kegunaan = 2");
        return $query->result();
    }

    public function klasifikasi_usaha()
    {
        $query = $this->db->query("SELECT * from ref_rek_5 where id_rek_kegunaan = 3");
        return $query->result();
    }

    public function klasifikasi_usaha_row($id)
    {
        $usaha = $this->db->query("SELECT kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4 from ref_rek_4 where id_rek_4 = $id ")->row();
        $query = $this->db->query("SELECT id_rek_5, nm_usaha_5 from ref_rek_5
                                    where kd_rek_1 = $usaha->kd_rek_1 and kd_rek_2 = $usaha->kd_rek_2 and kd_rek_3 = $usaha->kd_rek_3 and kd_rek_4 = $usaha->kd_rek_4");
        return $query->result();
    }

    public function kecamatan()
    {
        $query = $this->db->query("SELECT kd_kec, nm_kec from ref_kecamatan where status = '1'");
        return $query->result();
    }

    public function kelurahan_row($id)
    {
        $query = $this->db->query("SELECT kd_kel, nm_kel from ref_kelurahan where kd_kec = $id");
        return $query->result();
    }

    public function show_pajak_usaha_row($id)
    {
        $query = $this->db->query("SELECT c.jn_pemungutan, c.nm_jn_pemungutan, a.*
                                    from wp_wajib_pajak_usaha_pajak a
                                    left join ref_rek_4 b on a.jns_pajak = b.id_rek_4
                                    left join ref_pemungutan c on b.jns_pemungutan = c.jn_pemungutan
                                    where a.id = $id");
        return $query->row();
    }

    public function show_izin($id)
    {
        $query = $this->db->query("SELECT * from wp_wajib_pajak_izin where id = $id");
        return $query->row();
    }

}

/* End of file M_sptpd_bangunan.php */
/* Location: ./application/modules/data_entry/models/pendataan_pajak_sptpd_sub/M_sptpd_bangunan.php */
