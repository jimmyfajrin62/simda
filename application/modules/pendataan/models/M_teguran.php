<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_teguran extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function show_data($id)
    {
        $query = $this->db->query("SELECT ref_rek_4.nm_rek_4,ta_teguran.*,`wp_wajib_pajak_usaha_pajak`.`status`, `wp_wajib_pajak_usaha_pajak`.`lastupdate`, `wp_data_umum`.`npwpd`, `wp_data_umum`.`nama_pendaftar`, `wp_wajib_pajak_usaha`.`nm_usaha`, `wp_wajib_pajak_usaha`.`alamat_usaha`,wp_data_umum.jalan,wp_data_umum.rtrw,wp_data_umum.kabupaten,wp_data_umum.kode_pos
                            FROM `ta_teguran_rinc`
                            LEFT JOIN `ta_teguran` ON `ta_teguran_rinc`.`teguran_id` = `ta_teguran`.`id`
                            LEFT JOIN `ref_rek_4` ON `ta_teguran`.`kd_pajak` = `ref_rek_4`.`id_rek_4`
                            LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON `ta_teguran_rinc`.`wp_usaha_pajak_id` = `wp_wajib_pajak_usaha_pajak`.`id`
                            LEFT JOIN `wp_wajib_pajak_usaha` ON `wp_wajib_pajak_usaha_pajak`.`wp_usaha_id` = `wp_wajib_pajak_usaha`.`id`
                            LEFT JOIN `wp_wajib_pajak` ON `wp_wajib_pajak_usaha`.`wp_id` = `wp_wajib_pajak`.`id`
                            LEFT JOIN `wp_data_umum` ON `wp_wajib_pajak`.`data_umum_id` = `wp_data_umum`.`id`
                            WHERE ta_teguran.id = $id");
        return $query->row();
    }

    public function kota()
    {
        $query = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())");
        return $query->row();

    }

    public function get_npwpd($id)
    {
        $query = $this->db->query("SELECT wp_wajib_pajak_usaha_pajak.id, wp_data_umum.npwpd, wp_data_umum.nama_pendaftar, wp_wajib_pajak_usaha.nm_usaha, wp_wajib_pajak_usaha.alamat_usaha
                                    from wp_wajib_pajak_usaha_pajak
                                    LEFT JOIN wp_wajib_pajak_usaha ON wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
                                    LEFT JOIN wp_wajib_pajak ON wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
                                    LEFT JOIN wp_data_umum ON wp_wajib_pajak.data_umum_id = wp_data_umum.id
                                    where wp_wajib_pajak_usaha_pajak.id = $id");
        return $query->row();
    }

    public function show_teguran($id)
    {
        $query = $this->db->query("SELECT t.*, p.no_urut, p.nm_penandatangan, p.nip_penandatangan, j.nm_jab
                                    from ta_teguran t
                                    left join ref_penandatangan p on t.ttd_dok_id = p.no_urut
                                    left join ref_jabatan j on p.jbt_penandatangan = j.kd_jab
                                    where t.id = $id");
        return $query->row();
    }

    public function petugas()
    {
        $query = $this->db->query("SELECT rp.no_urut, rp.nm_penandatangan, rp.nip_penandatangan, rj.nm_jab
                                    from ref_penandatangan rp
                                    left join ref_jabatan rj on rp.jbt_penandatangan = rj.kd_jab
                                    where no_dok = 12");
        return $query->result();
    }

    public function auto_id()
    {
        $query = $this->db->query("SELECT id from ta_teguran order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_teguran($prefix=null, $sufix=null)
    {
        $query = $this->db->query("SELECT id from ta_teguran order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 6;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }

    public function no_urut($id)
    {
        $query = $this->db->query("SELECT * from ta_kartu_pajak_hotel_rinc where hotel_id = $id order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $data->no_urut + 1;
        }
    }
}
