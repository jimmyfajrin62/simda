<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_sptpd_restoran extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function ref_rek_6($id)
    {
        $query = $this->db->query("SELECT * from ref_rek_6 where id_rek_6 = $id");
        return $query->row();
    }

    public function menu($jns)
    {
        $query = $this->db->query("select * from ref_rek_4 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 1 and kd_rek_4 <> 13 and jns_pemungutan = $jns");
        return $query->result();
    }

    public function show_npwpd($id)
    {
        $query    = $this->db->query("select wp_data_umum.npwpd, wp_data_umum.nama_pendaftar, wp_wajib_pajak_usaha.nm_usaha
                                        from wp_wajib_pajak_usaha_pajak
                                        left join wp_wajib_pajak_usaha on wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
                                        left join wp_wajib_pajak on wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
                                        left join wp_data_umum on wp_wajib_pajak.data_umum_id = wp_data_umum.id
                                        where wp_wajib_pajak_usaha_pajak.id = $id");
        return $query->row();
    }

    public function show_data_1($id)
    {
        $query    = $this->db->query("SELECT wp_data_umum.npwpd, wp_data_umum.nama_pendaftar, wp_wajib_pajak_usaha.nm_usaha,ta_kartu_pajak_pungut.keterangan,ta_kartu_pajak_pungut.masa1,ta_kartu_pajak_pungut.masa2,ta_kartu_pajak_pungut.tahun,ta_kartu_pajak_restoran.kas,ta_kartu_pajak_restoran.pembukuan,wp_data_umum.jalan,wp_data_umum.kabupaten,wp_data_umum.kode_pos,wp_data_umum.rtrw,ref_penandatangan.nm_penandatangan,ref_penandatangan.nip_penandatangan,ta_data_umum_pemda.logo
                                        from ta_kartu_pajak_restoran
                                        left join ta_kartu_pajak_pungut on ta_kartu_pajak_restoran.pungut_id = ta_kartu_pajak_pungut.id
                                        left join ref_penandatangan on ta_kartu_pajak_pungut.ttd_dok_id = ref_penandatangan.no_urut
                                        left join wp_wajib_pajak_usaha_pajak on ta_kartu_pajak_pungut.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id
                                        left join wp_wajib_pajak_usaha on wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
                                        left join wp_wajib_pajak on wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
                                        left join wp_data_umum on wp_wajib_pajak.data_umum_id = wp_data_umum.id
                                        left join ta_data_umum_pemda on ta_kartu_pajak_restoran.tahun = ta_data_umum_pemda.tahun
                                        where wp_wajib_pajak_usaha_pajak.id = $id");
        return $query->row();

    }

    public function petugas()
    {
        $query = $this->db->query("select rp.no_urut, rp.nm_penandatangan, rp.nip_penandatangan, rj.nm_jab
                                    from ref_penandatangan rp
                                    left join ref_jabatan rj on rp.jbt_penandatangan = rj.kd_jab
                                    where no_dok = 1");
        return $query->result();
    }

    public function show_pungut($id)
    {
        $query = $this->db->query("select kp.*, rp.nm_penandatangan, rp.nip_penandatangan, rj.nm_jab
                                    from ta_kartu_pajak_pungut kp
                                    left join ref_penandatangan rp on kp.ttd_dok_id = rp.no_urut
                                    left join ref_jabatan rj on rp.jbt_penandatangan = rj.kd_jab
                                    where id = $id");
        return $query->row();
    }
     public function show_pajak($id2)
    {
        $query = $this->db->query("select kp.*, rp.kd_rek_1, rp.kd_rek_2,rp.kd_rek_3,rp.kd_rek_4,rp.kd_rek_5,rp.kd_rek_6, rp.nm_rek_6,kp.dasar_pengenaan, kp.pajak_terhutang
                                    from ta_kartu_pajak_restoran kp
                                    left join ref_rek_6 rp on kp.id_rek_6 = rp.id_rek_6
                                    where pungut_id = $id2");
        // echo $this->db->last_query($query);exit();
        return $query->result();
    }

    public function show_sptpd_pajak($id2)
    {
        $query = $this->db->query("select kp.*, rp.kd_rek_1, rp.kd_rek_2, rp.kd_rek_3, rp.kd_rek_4, rp.kd_rek_5, rp.kd_rek_6,rp.nm_rek_6
                                    from ta_kartu_pajak_restoran kp
                                    left join ref_rek_6 rp on kp.id_rek_6 = rp.id_rek_6
                                    where id =$id2");
        return $query->row();
    }

     public function show_pajak2($id2)
    {
        $query = $this->db->query("select kp.*, rp.kd_rek_1, rp.kd_rek_2,rp.kd_rek_3,rp.kd_rek_4,rp.kd_rek_5,rp.kd_rek_6, rp.nm_rek_6,kp.dasar_pengenaan, kp.pajak_terhutang
                                    from ta_kartu_pajak_restoran kp
                                    left join ref_rek_6 rp on kp.id_rek_6 = rp.id_rek_6
                                    where pungut_id = $id2");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

     public function jumlah($id2)
    {

        $query = $this->db->query("select sum(pajak_terhutang ) as jumlah from ta_kartu_pajak_restoran where pungut_id = $id2");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

    public function kota()
    {
        $query = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())");
        return $query->row();

    }

    public function show_sptpd($id)
    {
        $query = $this->db->query("select kp.*, rp.nm_penandatangan, rp.nip_penandatangan, rj.nm_jab
                                    from ta_kartu_pajak_pungut kp
                                    left join ref_penandatangan rp on kp.ttd_dok_id = rp.no_urut
                                    left join ref_jabatan rj on rp.jbt_penandatangan = rj.kd_jab
                                    where wp_usaha_pajak_id = $id");
        return $query->row();
    }

    public function show_rinci_sptpd($id)
    {
        $query = $this->db->query("select * from ta_kartu_pajak_restoran_rinc where id = $id");
        return $query->row();
    }

    public function auto_id()
    {
        $query = $this->db->query("select id from ta_kartu_pajak_pungut order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_sptpd($prefix=null, $sufix=null)
    {
        $query = $this->db->query("select id from ta_kartu_pajak_pungut order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 6;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }

    public function no_urut($id)
    {
        $query = $this->db->query("select * from ta_kartu_pajak_restoran_rinc where restoran_id = $id order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $data->no_urut + 1;
        }
    }
}
