<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form_teguran_wp/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="hidden" name="teguran_id" value="<?=$id?>">
                                    <input style="text-transform:uppercase" type="hidden" name="wp_usaha_pajak_id" value="<?=@$records->wp_usaha_pajak_id?>" id="wp_usaha_pajak_id">
                                    <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=@$records->npwpd?>" id="npwpd">
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=@$records->nama_pendaftar?>" id="nama_pendaftar">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=@$records->nm_usaha?>" id="nm_usaha">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Alamat Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="alamat_usaha" readonly value="<?=@$records->alamat_usaha?>" id="alamat_usaha">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_teguran_wp/<?=$kd_rek_4?>/<?=$title?>/<?=$id?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<a href="<?php echo $url ?>/show_teguran_wp/<?=$kd_rek_4?>/<?=$title?>/<?=$id?>" class="ajaxify reload"></a>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->
<div id="large" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="200px"> NPWPD </th>
                                    <th> Wajib Pajak </th>
                                    <th> Usaha </th>
                                    <th> Alamat </th>
                                    <th width="100px"> Action </th>
                                </tr>
                                <tr role="row">
                                    <td><input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="npwpd" placeholder="NPWPD"></td>
                                    <td><input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nama_pendaftar" placeholder="Wajib Pajak"></td>
                                    <td><input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nm_usaha" placeholder="Usaha"></td>
                                    <td><input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="alamat_usaha" placeholder="Alamat"></td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#modal_show').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            var select_url = '<?php echo $url ?>/get_npwpd/<?=$kd_rek_4?>';
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [0, "asc"]
            ];
            var sort = [-1];
            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            Helper.bsSelect();
        });

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

    });

    function pilih(id){
        $.get({
            url:        '<?=$url?>/get_npwpd_row/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res);
                $('#wp_usaha_pajak_id').val(res.records.id);
                $('#npwpd').val(res.records.npwpd);
                $('#nama_pendaftar').val(res.records.nama_pendaftar);
                $('#nm_usaha').val(res.records.nm_usaha);
                $('#alamat_usaha').val(res.records.alamat_usaha);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
