<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nomer Teguran</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="hidden" name="kd_pajak" value="<?=$kd_rek_4?>">
                                <input style="text-transform:uppercase" type="text" class="form-control" name="no_teguran" readonly value="<?=@$data->no_teguran?>" placeholder="Nomer Teguran Generate">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal Teguran</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control tgl" value="<?=$this->m_global->getdateformat(@$data->tgl_teguran)?>" name="tgl_teguran" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal Jatuh Tempo</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control tgl" value="<?=$this->m_global->getdateformat(@$data->tgl_jatuh_tempo)?>" name="tgl_jatuh_tempo" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Masa Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group input-large date-picker input-daterange date">
                                    <input style="text-transform:uppercase" type="text" class="form-control tgl" name="masa1" value="<?=$this->m_global->getdateformat(@$data->masa1)?>" required>
                                    <span class="input-group-addon"> s/d </span>
                                    <input style="text-transform:uppercase" type="text" class="form-control tgl" name="masa2" value="<?=$this->m_global->getdateformat(@$data->masa2)?>" required>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>Keterangan</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="keterangan" value="<?=@$data->keterangan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <!-- <hr><h4><strong>Petugas Penerima</strong></h4> -->
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="ttd_dok_id" id="ttd_dok_id" style="text-transform:uppercase">
                                    <option value="">Pilih Petugas</option>
                                    <?php foreach ($dokumen as $val): ?>
                                        <?php @$data->ttd_dok_id == $val->no_urut ? $selected='selected' : $selected='' ?>
                                        <option value="<?=$val->no_urut?>" data-1="<?=$val->nip_penandatangan?>" data-2="<?=$val->nm_jab?>" <?=$selected?>><?=$val->nm_penandatangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nip_penandatangan" id="nip_penandatangan" readonly required value="<?=@$data->nip_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jbt_penandatangan" id="jbt_penandatangan" readonly required value="<?=@$data->nm_jab?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/<?=$kd_rek_4?>/<?=$title?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<a href="<?php echo $url ?>/<?=$kd_rek_4?>/<?=$title?>" class="ajaxify reload"></a>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );


        $('.tgl').datepicker({
            format: 'dd-mm-yyyy'
        });


        $('#ttd_dok_id').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penandatangan').val(data1);
            $('#jbt_penandatangan').val(data2);
        });

    });
</script>
