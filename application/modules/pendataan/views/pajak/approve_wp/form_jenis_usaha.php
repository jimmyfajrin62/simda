<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_jenis_usaha/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>NPWPD</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="hidden" name="wp_id" value="<?=$wp_id?>">
                                <input type="text" class="form-control" name="npwpd" readonly="readonly" id="npwpd" value="<?=$records->npwpd?>" style="text-transform:uppercase">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>Nama Wajib Pajak</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_pendaftar" readonly="readonly" id="nama_pendaftar" value="<?=$records->nama_pendaftar?>" style="text-transform:uppercase">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Jenis Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-3">
                                <select name="jns_usaha" class="form-control" id="jns_usaha" required style="text-transform:uppercase">
                                    <option id="select_usaha"> Pilih Jenis Usaha </option>
                                    <?php foreach ($jns_usaha as $key => $value) :?>
                                        <?php $selected = @$data->jns_usaha == $value->id_rek_4 ? 'selected' : '' ?>
                                        <option value="<?=$value->id_rek_4?>" data-1="<?=$value->kd_rek_1?>" data-2="<?=$value->kd_rek_2?>" data-3="<?=$value->kd_rek_3?>" data-4="<?=$value->kd_rek_4?>" <?=$selected?>> <?=$value->nm_usaha_4?> </option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>Klasifikasi Usaha</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-3">
                                <select name="klasifikasi_usaha" class="form-control" id="klasifikasi" required style="text-transform:uppercase">
                                    <?php foreach ($klasifikasi_usaha as $key => $value) :?>
                                        <?php $selected = @$data->klasifikasi_usaha == $value->id_rek_5 ? 'selected' : '' ?>
                                        <option value="<?=$value->id_rek_5?>" <?=$selected?>> <?=$value->nm_usaha_5?> </option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>Nama Usaha</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" required name="nm_usaha" value="<?=@$data->nm_usaha?>"  id="nm_usaha" style="text-transform:uppercase">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>Alamat</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" name="alamat_usaha" required style="text-transform:uppercase"><?=@$data->alamat_usaha?></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>NPWP Usaha</strong>
                                <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="npwp_usaha" value="<?=@$data->npwp_usaha?>" id="npwp_usaha" style="text-transform:uppercase">
                                <span class="help-block"></span>
                            </div>
                        </div>
                          <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1"><strong>Kec / Kel</strong>
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-3">
                                <select name="kd_kec" class="form-control" id="kecamatan" required style="text-transform:uppercase">
                                    <option id="select_kec"> Pilih Kecamatan </option>
                                    <?php foreach ($kec as $key => $value): ?>
                                        <?php $selected = @$data->kd_kec == $value->kd_kec ? 'selected' : '' ?>
                                        <option value="<?=$value->kd_kec?>" <?=$selected?>><?=$value->nm_kec?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3">
                                <select name="kd_kel" class="form-control" id="kelurahan" style="text-transform:uppercase">
                                    <?php foreach ($kel as $key => $value): ?>
                                        <?php $selected = @$data->kd_kel == $value->kd_kel ? 'selected' : '' ?>
                                        <option value="<?=$value->kd_kel?>" <?=$selected?>><?=$value->nm_kel?></option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?=$url?>/show_jenisUsaha/<?=$wp_id?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url ?>/show_jenisUsaha/<?=$wp_id?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#kecamatan').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_kec').remove().end();
            var id  = $(this).find(':selected').attr('value');
            $.get({
                url: '<?=$url.'/get_kelurahan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {kd_kec: id},
                success: function(res){
                    console.log(res);
                    $('#kelurahan').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#kelurahan').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
                        })
                    });
                    $('#kelurahan').removeAttr('disabled');
                }
            });
        });

        $('#jns_usaha').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_usaha').remove().end();
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            var data3  = $(this).find(':selected').attr('data-3');
            var data4  = $(this).find(':selected').attr('data-4');
            $.get({
                url: '<?=$url.'/get_klasifikasi/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    kd_rek_1: data1,
                    kd_rek_2: data2,
                    kd_rek_3: data3,
                    kd_rek_4: data4
                },
                success: function(res){
                    console.log(res);
                    $('#klasifikasi').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#klasifikasi').append('<option value="'+value2.id_rek_5+'">'+value2.nm_usaha_5+'</option>');
                        })
                    });
                    $('#klasifikasi').removeAttr('disabled');
                }
            });
        });

    });
</script>
