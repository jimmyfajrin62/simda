<div class="row">
    <div class="col-md-12">
        <div class="portlet light note note-info">

            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table font-white"></i><?php echo $pagetitle ?>
                </div>
            </div>


            <div class="portlet-body">
                <div class="table-container">

                    <table id="user" class="table table-bordered table-striped">
                        <tbody>
                            <tr>
                                <td colspan="2"><center><h4><strong><br>DATA PENGAJUAN WAJIB PAJAK<strong></h4></center></td><br>
                            </tr>
                            <tr>
                                <td style="width:25%"> NOMER DAFTAR </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->no_daftar)?></span></td>
                            </tr>
                            <tr>
                                <td> NPWPD </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->npwpd)?></span></td>
                            </tr>
                            <tr>
                                <td> NPWP USAHA </td>
                                <td><span class="label label-m label-primary"><?=strtoupper($wp->npwp_pemilik)?></span></td>
                            </tr>
                            <tr>
                                <td> NAMA WAJIB PAJAK </td>
                                <td> <?=strtoupper($wp->nama_pendaftar)?> </td>
                            </tr>
                            <tr>
                                <td> JENIS PAJAK </td>
                                <td> <?=strtoupper($wp->jns_pajak) != 2 ? 'PRIBADI' : 'BADAN USAHA'?> </td>
                            </tr>
                            <tr>
                                <td> ALAMAT </td>
                                <td>
                                    <?=strtoupper($wp->jalan)?>&nbsp
                                    RT/RW. <?=strtoupper($wp->rtrw)?> <?=strtoupper($wp->nm_kel)?>, <br><br>
                                    <?=strtoupper($wp->nm_kec)?> - <?=strtoupper($wp->kabupaten)?>, <?=strtoupper($wp->kode_pos)?>
                                </td>
                            </tr>
                            <tr>
                                <td> NOMER TELEPON </td>
                                <td> <?=strtoupper($wp->no_telpon)?> </td>
                            </tr>
                            <tr>
                                <td colspan="2"><center><h4><strong><br>IDENTITAS WAJIB PAJAK<strong></h4></center></td><br>
                            </tr>
                            <tr>
                                <td> WARGA NEGARA </td>
                                <td> <?=strtoupper($wp->status_wn) !=2 ? 'WNI' : 'WNA' ?> </td>
                            </tr>
                            <tr>
                                <td> IDENTITAS </td>
                                <td> <?=$wp->tanda_bukti == 3 ? 'PASPOR' : ($wp->tanda_bukti == 2 ? 'SIM' : 'KTP')?> </td>
                            </tr>
                            <tr>
                                <td> NOMER IDENTITAS </td>
                                <td> <?=strtoupper($wp->no_tanda_bukti)?> </td>
                            </tr>
                            <tr>
                                <td> TANGGAL IDENTITAS </td>
                                <td> <?=$wp->tgl_tanda_bukti == '0000-00-00' || $wp->tgl_tanda_bukti == NULL ? '' : tgl_format($wp->tgl_tanda_bukti)?> </td>
                            </tr>
                            <tr>
                                <td> NOMER KARTU KELUARGA </td>
                                <td> <?=strtoupper($wp->no_kk)?> </td>
                            </tr>
                            <tr>
                                <td> TANGGAL KARTU KELUARGA </td>
                                <td> <?=$wp->tgl_kk == '0000-00-00' || $wp->tgl_kk == NULL ? '' : tgl_format($wp->tgl_kk)?> </td>
                            </tr>
                            <tr>
                                <td> PEKERJAAN </td>
                                <td> <?=$wp->pekerjaan == 5 ? 'LAIN-LAIN' : ($wp->pekerjaan == 4 ? 'PEMILIK USAHA' : ($wp->pekerjaan == 3 ? 'ABRI' : ($wp->pekerjaan == 2 ? 'PEGAWAI SWASTA' : ($wp->pekerjaan == 1 ? 'PEGAWAI NEGRI' : '')))) ?> </td>
                            </tr>
                            <tr>
                                <td> PEKERJAAN LAINNYA </td>
                                <td> <?=strtoupper($wp->pekerjaan_lainnya)?> </td>
                            </tr>
                            <tr>
                                <td> NAMA INSTANSI </td>
                                <td> <?=strtoupper($wp->nama_instansi)?> </td>
                            </tr>
                            <tr>
                                <td> ALAMAT INSTANSI </td>
                                <td> <?=strtoupper($wp->alamat_instansi)?> </td>
                            </tr>
                            <tr>
                                <td> KETERANGAN </td>
                                <td> <?=strtoupper($wp->keterangan)?> </td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>

                            <form action="<?=$url.'/change_reject/'.$id?>" class="form-horizontal form-add" role="form" method="POST">
                            <tr>
                                <td>
                                <textarea style="text-transform:uppercase" name="keterangan" class="form-control" rows="5" cols="80" placeholder="Keterangan"></textarea></td>
                            </tr>

                        </tbody>
                    </table>
                </div>
            </div>

                <div class="tools">
                    <div class="actions">
                        <a href='<?=$url.'/change_approve/'.$id?>' onClick="return f_approve(1, this, event)" class="btn green-seagreen tooltips" class="fa fa-check" data-original-title="Approve"><i class="fa fa-check"></i> Approve</a>
                            <button type="submit" class="btn blue"><i class="fa fa-close"></i> Reject</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>

<script type="text/javascript">
    function f_approve(stat, ele, e, dtele){
    e.preventDefault();
    dtGrid = (dtele != null ? $("#"+dtele): $("#datatable_ajax") );

    if(stat == 1){
        var mes = "Are you sure want to change Status ?";
        var sus = "Successfully Change Status!";
        var err = "Error Change Status!";
        var html = false;
    }else if(stat == 2){
        var mes = "Are you sure want to Delete data ?";
        var sus = "Successfully Delete data!";
        var err = "Error Delete data!";
        var html = false;
    }else if(stat == 3){
        var mes = "<b>This will delete all related Subscription too!</b></br>Are you sure want to Delete data ?";
        var sus = "Successfully Delete data!";
        var err = "Error Delete data!";
        var html = true;
    }
    swal({
        title: "Are you sure?",
        text: mes,
        html: html,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#01579B',
        confirmButtonText: 'Yes',
        closeOnConfirm: false
    }).then(function(isConfirm){
        if(isConfirm){
            var href = $(ele).attr('href');
            $.post(href, function(data, textStatus, xhr) {
                if(data.status == 1){
                    dtGrid.DataTable().ajax.reload();
                    swal("Success", sus, "success");
                    swal({
                        title: "Success!",
                        text: sus,
                        type: "success"
                    }).then(function() {
                        window.location.href = "<?php echo base_url('pendataan/pendataan_approve_wp'); ?>";
                    });
                }else{
                    swal("Error", err, "error");
                }
            }, 'json');
        }
    });
};

</script>
