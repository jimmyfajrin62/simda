<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light note note-info">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $url ?>/show_add_surat_izin/<?=$id?>/<?=$wp_id?>" class="tooltips ajaxify btn btn-circle btn-icon-only btn-default">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Excel" href="javascript:window.location.assign(base_url + '/user/export_data')" class="tooltips ajaxify btn btn-circle btn-icon-only btn-default">
                                    <i class="fa fa-file-excel-o"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?=$url?>" class="tooltips ajaxify btn btn-circle btn-icon-only btn-default">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips ajaxify btn btn-circle btn-icon-only btn-default" id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
                            <option>Select...</option>
                            <!-- id / field-table / table -->
                            <option value="99/id/wp_wajib_pajak_izin">Delete</option>
                            <option value="0/id/wp_wajib_pajak_izin">InActive</option>
                            <option value="1/id/wp_wajib_pajak_izin">Active</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue table-group-action-submit">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th colspan="7" class="text-center"><h4><strong><?=strtoupper($head->nama_pendaftar)?> &nbsp (<?=$head->npwpd?>)</strong></h4></th>
                            </tr>
                            <tr role="row" class="heading">
                                <th width="30px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th> No</th>
                                <th> No.Perizinan</th>
                                <th> Nama Perizinan </th>
                                <th> Tanggal Perizinan </th>
                                <th width="140px"> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="no_izin" placeholder="No. Izin"> </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="nm_izin" placeholder="Nama Izin"> </td>
                                    <td>
                                        <div class="input-group">
                                            <input type="text" name="tgl_izin" placeholder="Tgl Terdaftar" class="form-control form-filter date">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-calendar"></i></span>
                                        </div>
                                    </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm blue btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
    jQuery(document).ready(function() {

        // table data
        var select_url = '<?php echo $url ?>/select_suratIjin/<?=$id?>';
        console.log(select_url);
        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" }
        ];
        var order = [
            [3, "DESC"]
        ];

        var sort = [-1, 0, 1];

        TableDatatablesAjax.handleRecords(select_url, header, order, sort);
        // bs select setelah datatable, bug
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

    });
</script>
