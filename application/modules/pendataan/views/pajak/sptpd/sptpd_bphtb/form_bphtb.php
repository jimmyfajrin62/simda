<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <fieldset>
                            <legend>BPHTB</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NOP</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Objek Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" placeholder="Objek Pajak" name="obyek_pajak" id="obyek_pajak">
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset>
                            <legend>PBB</legend>
                            <center>
                        <div class="row">
                            <div class="col-md-2 form-group">
                                    <span><strong>Uraian</strong></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                    <span ><strong>Luas Tanah Dan Bangunan</strong></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                    <span ></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                    <span ><strong>NJOP PBB / M2</strong></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                    <span ></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                    <span ><strong>Luas x NJOP PBB / M2</strong></span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </center>
                        <div class="row">
                            <center>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>Tanah</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>X</strong>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>=</strong>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                            </center>
                        </div>
                        <div class="row">
                            <center>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>Bangunan</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>X</strong>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>=</strong>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                            </center>
                        </div>

                        <div class="row">
                            <center>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>Tarif</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>%</strong>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            </center>
                        </div>

                        <div class="row">
                            <center>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>NJOP PBB</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                            </center>
                        </div>

                        <div class="row">
                            <center>
                            <div class="col-md-2 form-group">
                                <label class="control-label"><strong>Harga Transaksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-2 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" >
                                <span class="help-block"></span>
                            </div>
                            </center>
                        </div>

                        <fieldset>
                            <legend>Pajak</legend>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>1. Nilai Perolehan Objek Pajak ( NPOP )</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar"readonly="" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>2. Nilai Perolehan Objek Pajak Tidak Kena Pajak ( NPOPTKP )</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>3. Nilai Perolehan Objek Pajak Kena Pajak ( NPOPKP )</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>4. Bea Perolehan Hak atas Tanah dan Bangunan Yang Terhutang</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>5. Pengenaan 50% Karena Waris/Hibah Wasiat/Pemberian Hak pengelolaan</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>6. Pengurangan BPHTB</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>7. Bea Perolehan Hak atas tanah dan atau bangunan yang harus di bayar</strong>
                                    <span class="required">*</span>
                                </label>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly="" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/index/" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#ttd_dok_id').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penandatangan').val(data1);
            $('#jbt_penandatangan').val(data2);
        });

    });
</script>
