<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <fieldset>
                            <legend>BPHTB</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" placeholder="NPWPD" name="obyek_pajak" id="obyek_pajak">
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NPWP</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIK</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jalan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>RT/RW</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Kecamatan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Kelurahan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Kab / Kota</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Kode Pos</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset>
                            <legend>NOP</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.Objek Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.SPTPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Dasar Pengenaan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jenis Perolehan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.Sertifikat/RW</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal Terima</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->tgl_terima?>" name="tgl_terima" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 form-group">
                                <label class="control-label"><strong>Uraian</strong>
                                    <span class="required">*</span>
                                </label>
                                <textarea class="form-control" style="text-transform:uppercase"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset>
                            <legend>Letak Tanah Dan Bangunan</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jalan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Kecamatan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Kelurahan</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>RT/RW</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Kode Pos</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" value="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Kab / Kota</strong>
                                    <span class="required">*</span>
                                </label>
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->tgl_terima?>" name="tgl_terima" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        </fieldset>

                        <fieldset>
                            <legend>Petugas Penerima</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="nip_penandatangan" id="nip_penandatangan" value="<?=@$data->nip_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="nip_penandatangan" id="nip_penandatangan" value="<?=@$data->nip_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="jbt_penandatangan" id="jbt_penandatangan" value="<?=@$data->nm_jab?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/index/" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#ttd_dok_id').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penandatangan').val(data1);
            $('#jbt_penandatangan').val(data2);
        });

    });
</script>
