<style type="text/css">
    .wrap_paper{
        margin: 0px 0px;
        background-color: white;
        border:1px solid black;
        padding:0px 0px;
        border-radius: 0px;
        height: 3000px;
        /* width: 3000px; */
    }
    .gambar{
        margin-top: 10px;
        padding: 10px;
        width: 100px;
    }
    .lebar_kolom_besar{
        width: 300px;
    }
    .lebar_kolom_kecil{
        width: 150px;
    }
    .kop_surat td {
        text-align: center;
        padding: 10px;
    }
    .kop_surat td:first-child p{
        /*font-size: 10px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2){
        vertical-align: middle;
    }
    .kop_surat td:nth-child(2) p{
        line-height: 10px;
        /*font-size: 15px;*/
        font-weight: bold;
    }
    .kop_surat td:nth-child(2) h1{
        color: #000;
        font-weight: bold;
    }
    .kolom {
        line-height: 200px;
        padding-left: 30px;
        font-size: 30px;
    }
    table{
        margin-bottom: 0!important;
    }
    table.table-bordered tr td{
        border: 1px solid black !important;
    }
    table .noborder  tr  td{
        border: none !important;
    }
    tr.border_btm  td:not(:first-child){
        border-bottom: 1px solid #E5E5E5;
    }
    table .tbl_perhitungan_njop tr td{
        border: 1px solid black !important;
    }
    table .tbl_perhitungan_njop tr td p,
    table .tbl_perhitungan_njop tr td p b{
        line-height: 10px;
    }
    table.hitung tr td{
        border-left: none;
        border-right: none;
        border-bottom: none;
    }
    tr .tanda_tangan td p{
        line-height: 8px;
    }
    label{
        margin-bottom: 0 !important;
    }
    {
        font-size: 30px;
    }
     @page {
        margin-top: 0mm;
        margin-bottom: 0mm;
        margin-left: 0mm;
        margin-right: 0mm;
    } 
</style>
    <link href="<?=base_url('assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet');?>" type="text/css" />

<!-- untuk halaman yang di print -->
<div class="wrap_paper" style="overflow: auto;" >
    <div class="inputform" id="ini_di_print" style="width: 110%">
        <table class="table table-bordered" style="font-size:60px;">
            <tr class="kop_surat">
                <td>
                    <img src="<?=base_url() . 'assets/img/logo.png' . $this->config->item('LOGO_KOTA');?>" class="gambar" />
                </td>
                <td style="padding: 10px; width: 400px">
                    <h4><b>PEMERINTAH KOTA PASURUAN</b></h4>
                    <h4><b>BADAN PENDAPATAN DAERAH</b></h4>
                    <h4><b>ALAMAT : MALANG</b></h4>
                </td>
                <td>
                    <h4><b>Tahun pajak :<?php echo @$sptpd->tahun;?></b></h4>
                </td>
            </tr>
        </table>

        <table class="table table-bordered" style="font-size:50px;">

            <tr class="kop_surat">
                <td style="padding: 50px">
                    <b><?php echo @$sptpd->no_sptpd;?></b><br>
                    <b>Masa pajak : <?php echo @$sptpd->masa1;?> s.d <?php echo @$sptpd->masa2;?> </b>
                </td>
            </tr>
                
                <tr><td align="right" style="padding-right: 200px;padding-top: 50px;padding-bottom: 50px">
                    <b>Kepada: Yth.</b><br>
                    <b>Kabban</b><br>
                    <b>Di - PASURUAN</b>
                </td></tr>

    <tr><td><br>
        <table class="table noborder table-condensed" style="font-size:50px;">
                        <tr>
                            <td></td>
                            <td><b>NPWPD</td>
                            <td><b> :</td>
                            <td colspan="7"><b><?php echo $npwpd->npwpd;?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Nama WP</td>
                            <td><b> :</td>
                            <td colspan="7"><b><?php echo $npwpd->nama_pendaftar;?></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><b>Nama Usaha</td>
                            <td><b> :</td>
                            <td colspan="7"><b><?php echo $npwpd->nm_usaha;?></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td><b>Keterangan</td>
                            <td><b> :</td>
                            <td colspan="7"><b><?php echo @$npwpd->keterangan;?></td>
                        </tr>
                        <tr class="border_btm">
                            <td width="5%"></td>
                            <td width="13%"></td>
                            <td width="2%"></td>
                            <td width="15%"></td>
                            <td width="5%"></td>
                            <td width="2%"></td>
                            <td width="10%"></td>
                            <td width="8%"></td>
                            <td width="2%"></td>
                            <td width="10%"></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td></td>
                            <td ></td>
                            <td colspan="2" ></td>
                            <td colspan="2" ></td>
                            <td ></td>
                            <td ></td>
                            <td ></td>
                        </tr><br>
        </table>
    </td></tr>

            <tr>
                <td colspan="4" style="padding-left: 30px;">
                    <table class="table noborder table-condensed" style="font-size:50px">
                        <tr><td><br></td></tr>
                        <tr>
                            <td></td>
                            <td><b>PERHATIAN :</b></td>
                            <td></td>
                            <td colspan="5"></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td><h1>1. Harap diisi dalam rangkap 2 (dua) dan ditulis dengan huruf CETAK</h1></td>
                            <td></td>
                            <td colspan="5"></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td><h1>2. Beri Nomor pada Kotak yang tersedia untuk jawaban yang diberikan</h1></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td><h1>3. Setelah diisi dan ditandatangani harap diserahkan kembali kepada <b>BADAN PENDAPATAN DAERAH</b>, paling lambat tanggal <b>15</b> bulan berikutnya.</h1></td>
                        </tr>
                        <tr class="border_btm">
                            <td></td>
                            <td><h1>4. Keterlambatan penyerahan dari tanggal tersebut di atas akan dilakukan Penerbitan Surat Teguranewfew</h1></td>
                        </tr>
                    </table>
                </td>
            </tr>
                        <tr>
                            <td colspan="4" style="padding-left: 30px;">
                                <span style="margin-left:10px">A. </span>
                                <span style="margin-left:30px"> DIISI OLEH WAJIB PAJAK / PENANGGUNG PAJAK</span>
                            </td>
                        </tr>
                    </td></tr>
        </table>
    </td></tr>

            <tr>
                <td colspan="4" style="padding-left: 30px;">
                    <table class="table noborder table-condensed" style="font-size:50px">
                        <tr>
                            <td>1. Golongan Hotel</td>
                            <td>:</td>
                            <td>Pajak Hotel Bintang Lima Berlian</td>
                        </tr>
                        <tr>
                            <td>2. Menggunakan Cash Register</td>
                            <td>:</td>
                            <td><input type="text" name=""></td>
                            <td>1. Ya</td>
                            <td>2. Tidak</td>
                        </tr>
                        <tr>
                            <td>3. Mengadakan Pembukuan</td>
                            <td>:</td>
                            <td><input type="text" name=""></td>
                            <td>1. Ya</td>
                            <td>2. Tidak</td>
                        </tr>
                        <tr>
                            <td>4 Jumlah dan Tarif Kamar Hotel</td>
                            <td>:</td>
                        </tr>
                        
                            <table class="table table-bordered" style="font-size:60px;">
                                <tr class="kop_surat" class="table table-bordered">
                                    <td colspan="2">
                                        <p><b>No</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Golongan Kamar</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Tarif(Rp)</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>Jumlah Kamar</b></p>
                                    </td>
                                </tr>

                                    <?php $no = 1; foreach ($pajak as $key => $value): ?>
                                <tr>
                                    <td colspan="2">
                                        <p><b><?=$no;?></b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                    <td colspan="2">
                                        <p><b>dinamis</b></p>
                                    </td>
                                </tr>

                                        <?php $no++; endforeach ?>
                                    <br>
                            </table> 
                </td>
            </tr>


        </table>
    </div>
</div>

