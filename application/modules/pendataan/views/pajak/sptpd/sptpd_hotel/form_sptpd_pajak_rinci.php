<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form_rinci/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Nama Kamar</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="hidden" name="hotel_id" value="<?=$id2?>">
                                <!-- <input style="text-transform:uppercase" type="hidden" name="tahun" value="<?=date('Y')?>"> -->
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_kamar" value="<?=@$data->nm_kamar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Jumlah Kapasitas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jml_kapasitas" value="<?=@$data->jml_kapasitas?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Jumlah Tamu</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jml_tamu" value="<?=@$data->jml_tamu?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-6 form-group">
                                <label class="control-label"><strong>Tarif</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="tarif" value="<?=@$data->tarif?>" placeholder="Tarif">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_rinci/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>/<?=$id2?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<a href="<?php echo $url ?>/show_rinci/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>/<?=$id2?>" class="ajaxify reload"></a>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

    });
</script>
