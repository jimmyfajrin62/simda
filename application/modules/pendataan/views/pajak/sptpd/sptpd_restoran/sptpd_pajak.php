<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <?php if ($status_sspd->status_sspd != 1): ?>
                                    <a data-original-title="Add" href="<?php echo $url ?>/show_add_sptpd_pajak/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                        <i class="fa fa-plus"></i>
                                    </a>
                                    <span class='help-block' style='display: inline;'></span>
                                <?php endif; ?>
                                <a data-original-title="Reload" href="<?php echo $url ?>/show_sptpd_pajak/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

                        <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
                            <option>Select...</option>
                            <!-- id / field-table / table -->
                            <option value="99/id/ta_kartu_pajak_hotel">Delete</option>
                            <option value="0/id/ta_kartu_pajak_hotel">InActive</option>
                            <option value="1/id/ta_kartu_pajak_hotel">Active</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue table-group-action-submit" onClick="">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>

                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th colspan="8" class="text-center"><h4><strong><?=strtoupper($head->nama_pendaftar)?> &nbsp (<?=$head->npwpd?>)</strong></h4></th>
                            </tr>
                            <tr role="row" class="heading">
                                <th width="30px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="20px"> No </th>
                                <th width="100px"> Rekening </th>
                                <th> Keterangan </th>
                                <th> Dasar Pengenaaan </th>
                                <th> Tarif Pajak </th>
                                <th> Pajak Terhutang </th>
                                <th width="150px"> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nm_rek_6" placeholder="Keterangan"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="dasar_pengenaan" placeholder="Dasar Pengenaaan"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="tarif_pajak" placeholder="Tarif Pajak"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="pajak_terhutang" placeholder="Pajak Terhutang"> </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // table data
        var select_url = '<?php echo $url ?>'+'/select_sptpd_pajak/'+<?=$kd_rek_4?>+'/'+'<?=$wp?>'+'/'+<?=$id?>;
        // console.log(select_url);
        var header     = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" }
        ];
        var order = [
            [2, "asc"]
        ];
        var sort = [-1, 0, 1];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
