<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form_sptpd_pajak/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=$records->nm_usaha?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Obyek Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="hidden" name="pungut_id" name="pungut_id" value="<?=$id?>">
                                    <input style="text-transform:uppercase" type="hidden" name="id_rek_6" name="id_rek_6" value="<?=@$data->id_rek_6?>" id="id_rek_6">
                                    <input style="text-transform:uppercase" type="text" class="form-control" placeholder="Obyek Pajak" value="<?=@$data->nm_rek_6?>" name="obyek_pajak" id="obyek_pajak" readonly>
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Rekening Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <?php if (@$data->id_rek_6): ?>
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->kd_rek_1?> . <?=@$data->kd_rek_2?> . <?=@$data->kd_rek_3?> . <?=@$data->kd_rek_4?> . <?=@$data->kd_rek_5?> . <?=@$data->kd_rek_6?>" name="rek_pajak" id="rek_pajak" readonly required>
                                <?php else: ?>
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="" name="rek_pajak" id="rek_pajak" readonly required>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tarif Pajak (%)</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="tarif_pajak" value="<?=@$data->tarif_pajak?>" id="tarif_pajak" readonly required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jangka Waktu</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group input-large date-picker input-daterange date">
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="periode1" value="<?=@$data->periode1?>" required>
                                    <span class="input-group-addon"> s/d </span>
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="periode2" value="<?=@$data->periode2?>" required>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Lokasi</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="kd_kelas" id="kd_kelas" value="<?=@$data->kd_kelas?>" readonly>
                                    <span class="input-group-addon"></span>
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="kd_lokasi" id="kd_lokasi" value="<?=@$data->kd_lokasi?>" readonly>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Lokasi</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <select class="form-control" name="lokasi_reklame" id="lokasi_reklame" style="text-transform:uppercase">
                                        <option value="">--Pilih--</option>
                                        <?php foreach ($lokasi as $value): ?>
                                            <?php $selected = $data->lokasi_reklame == $value->id ? $selected = 'selected' : $selected = '' ?>
                                            <option value="<?=$value->id?>" data-1="<?=$value->kd_kelas?>" data-2="<?=$value->kd_lokasi?>" <?=$selected?>> <?=$value->nm_lokasi?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold" data-toggle="modal" href="#lokasi" id="modal_lokasi">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="row">                            
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Panjang</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="panjang" value="<?=@$data->panjang?>" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Lebar</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="lebar" value="<?=@$data->lebar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong></strong>
                                    <span class="required"></span>
                                </label>
                                <div class="input-group"><input style="text-transform:uppercase" type="number" class="form-control" name="jml_waktu" value="<?=@$data->jml_waktu?>" required>
                                    <span class="input-group-addon"></span>
                                    <select class="form-control" name="satuan_waktu" id="satuan_waktu" style="text-transform:uppercase">
                                        <option value="">-</option>
                                        <?php foreach ($waktu as $value): ?>
                                            <?php $selected = @$data->id == $value->id ? 'selected' : '' ?>
                                            <option value="<?=$value->id?>" <?=$selected?>> <?=$value->nm_satuan?> </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="row">                            
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tarif</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="tarif" value="<?=@$data->tarif?>" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Judul</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="judul" value="<?=@$data->judul?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nilai Sewa</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" name="nilai_sewa" id="nilai_sewa" value="<?=@$data->nilai_sewa?>">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Jumlah Reklame</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="jumlah" id="jumlah_reklame" value="<?=@$data->jumlah?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Dasar Pengenaan (Rp)</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" name="dasar_pengenaan" id="dasar_pengenaan" value="<?=@$data->dasar_pengenaan?>" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3 form-group">
                                <label class="control-label"><strong>Pajak Terhutang (Rp)</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" name="pajak_terhutang" id="pajak_terhutang" value="<?=@$data->pajak_terhutang?>" readonly>
                                <span class="help-block"></span>
                            </div>
                                <div class="col-md-3 form-group">
                                    <label class="control-label"><strong>Pembulatan</strong>
                                        <span class="required">*</span>
                                    </label>
                                    <!-- <input style="text-transform:uppercase" type="text" class="form-control" name="pembulatan" value="<?=@$data->pembulatan ? @$data->pembulatan : 0?>" required> -->
                                    <select class="form-control" name="pembulatan" style="text-transform:uppercase">
                                        <option value="1000" <?=@$data->pembulatan == 1000 ? 'selected' : ''?>>Ribuan</option>
                                        <option value="100" <?=@$data->pembulatan == 100 ? 'selected' : ''?>>Ratusan</option>
                                        <option value="0" <?=@$data->pembulatan == 0 ? 'selected' : ''?>>Tanpa Pembulatan</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                        </div>
                        <!-- <hr><h4><strong>Petugas Penerima</strong></h4> -->
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_sptpd_pajak/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
        <a href="<?php echo $url ?>//show_sptpd_pajak/<?=$kd_rek_4?>/<?=$wp?>/<?=$id?>" class="ajaxify reload"></a>

        <!-- START MODAL -->
        <div id="large" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">Wajib Pajak</h4>
                    </div>
                    <div class="modal-body">
                        <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                            <div class="row" style="padding: 25px">
                                <!-- MODAL CONTENT AREA -->
                                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                                    <thead>
                                        <tr role="row" class="heading">
                                            <th width="200px"> Rekening Pajak </th>
                                            <th> Obyek Pajak </th>
                                            <th width="100px"> Action </th>
                                        </tr>
                                        <tr role="row">
                                            <td></td>
                                            <td>
                                                <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nm_rek_6" placeholder="Obyek Pajak">
                                            </td>
                                            <td class="text-center">
                                                <div class="clearfix">
                                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                                <!-- End Modal Content -->
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END MODAL -->

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#modal_show').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            var select_url = '<?php echo $url ?>/select_rek_6';
            var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" }
            ];
            var order = [
            [0, "asc"]
            ];
            var sort = [-1];
            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            Helper.bsSelect();
        });

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#lokasi_reklame').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#kd_kelas').val(data1);
            $('#kd_lokasi').val(data2);
        });

    $("#nilai_sewa,#jumlah_reklame").on('keyup',function(){
          var nilai   = $("#nilai_sewa").val().replace(/[Rp \.\$,]/g, '');
          var reklame = $("#jumlah_reklame").val().replace(/[Rp \.\$,]/g, '');
          var tarif   = $("#tarif_pajak").val().replace(/[Rp \.\$,]/g, '');
          var jumlah  = nilai * reklame;
          var hutang  = jumlah * tarif /100;
          $("#dasar_pengenaan").val(jumlah);
          $("#pajak_terhutang").val(hutang);
      });

    });

    function pilih(id){
        $.get({
            url:        '<?=$url?>/get_rek_6/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                $('#id_rek_6').val(res.records.id_rek_6);
                $('#obyek_pajak').val(res.records.nm_rek_6);
                $('#tarif_pajak').val(res.records.tarif);
                $('#rek_pajak').val(res.records.kd_rek_1+' . '+res.records.kd_rek_2+' . '+res.records.kd_rek_3+' . '+res.records.kd_rek_4+' . '+res.records.kd_rek_5+' . '+res.records.kd_rek_6);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
