<?php if ($status_pajak): ?>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-bordered">
                <h4 class="block">NPWPD Wajib Pajak Belum di Validasi</h4>
                <p> NPWPD wajib pajak Anda belum di validasi oleh petugas, untuk dapat menggunakan menu ini silahkan hubungi admin atau datang langsung ke Kantor Badan Pendapatan Daerah Pasuruan
                    <br><br><center><a href="<?=base_url('help/contact')?>" class="btn btn-outline red"> Contact Us</a>
                </p>
            </div>
        </div>
    </div>
<?php else: ?>
    <div class="row">
        <div class="col-md-12">
            <div class="note note-success note-bordered">
                <h4 class="block">Anda Belum Mendaftar Wajib Pajak</h4>
                <p> Anda belum mendaftar wajib pajak, silahkan daftar wajib pajak untuk dapat menggunakan menu ini, atau jika anda sudah terdaftar sebagai wajib pajak dan sudah mempunyai NPWPD silahkan hubungi admin atau datang langsung ke Kantor Badan Pendapatan Daerah Pasuruan
                    <br><br><center><a href="<?=base_url('pendaftaran/pendaftaran_pajak/show_add')?>" class="btn btn-outline red"> Daftar Wajib Pajak</a>
                </p>
            </div>
        </div>
    </div>
<?php endif; ?>
