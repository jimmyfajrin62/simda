<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
<!--                                 <a data-original-title="Add" href="<?php echo $url ?>/show_add_sts" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a> -->
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

                        <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <!-- <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="table-container">
                    <table border="0">
                        <tr>
                             
                                  
                        </tr>
                    </table>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                      
                        <thead>
                            <tr>
                                <th colspan="8">
                                     <h2 align="center"><b><?php echo $nama->nm_unit?></b></h2>        
                                </th>
                            </tr>
                            <tr role="row" class="heading">
                                <th width="20px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="20px"> No </th>
                                <th width="20px"> Urusan </th>
                                <th width="40px"> Bidang </th>
                                <th width="30px"> Unit </th>
                                <th width="50px"> Sub Unit </th>
                                <th width="240px"> Uraian Unit </th>
                                <th > Last Update </th>
                                <th> Actioin </th>
                            </tr>

                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="urusan" placeholder="Urusan"> </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="bidang" placeholder="Bidang"> </td> 
                                 <td>
                                    <input type="text" class="form-control form-filter input-sm" name="unit" placeholder="Unit">
                                 </td>
                       
                                 <td>
                                    <input type="text" class="form-control form-filter input-sm" name="sub_unit" placeholder="Sub Unit"> </td>

                                 <td>
                                    <input type="text" class="form-control form-filter input-sm" name="uraian_unit" placeholder="Uraian Unit"> </td>

                                  <td>
                                      <div class="input-group margin-bottom-5" id="defaultrange">
                                        <input type="text" name="lastupdate_show" class="form-control form-filter">
                                        <input type="hidden" name="lastupdate" class="form-filter">
                                        <span class="input-group-btn">
                                            <button class="btn default date-range-toggle" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                  </td>
                                 <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                       
                                    </div>
                                </td>

                                
                            </tr>
                        </thead>
                        <tbody> 
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
    jQuery(document).ready(function() {

        // table data
        var select_url = '<?php echo $url ?>/select_sub/<?=$kd_urusan?>/<?=$kd_bidang?>/<?=$kd_unit?>';

        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" }
        ];
        var order = [
            [2, "asc"]
        ];

        var sort = [-1,0,1];

        TableDatatablesAjax.handleRecords( select_url, header, order, sort);
        // bs select setelah datatable, bug
        Helper.bsSelect();
        
    });
</script>