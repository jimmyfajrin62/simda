<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus "></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add_wajib_retribusi/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">NPWPD
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <?php if (isset($np)): ?>
                                <input type="text" class="form-control" id="npwpd" name="npwpd" value="" placeholder="NPWPD Generate" readonly>
                                <input type="hidden" name="data_umum_id" id="id">
                                <?php else: ?>
                                <input type="text" class="form-control" name="npwpd" value="<?=@$data->npwpd?>" required placeholder="NPWPD" readonly>
                                <input type="hidden" name="data_umum_id" value="<?=@$data->data_umum_id?>">
                                <?php endif; ?>
                                <span class="help-block"></span>

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Wajib retribusi
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Nama Wajib retribusi" required value="<?=@$data->nama_pendaftar?>" name="nama_pendaftar" id="nama_pendaftar" readonly>
                                    <span class="input-group-btn">
                                        <a class="btn blue btn-outline sbold <?=@$data->nama_pendaftar ? 'hide' : ''?>" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tanggal Terdaftar
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="tgl_aktif" value="<?=@$data->tgl_aktif?>">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Status
                                <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                                <div class="mt-checkbox-list">
                                    <label class="mt-checkbox mt-checkbox-outline"> Aktif
                                        <input type="checkbox" value="1" <?=@$data->status_wr == 1 ? 'checked' : ''?> name="status_wr">
                                        <span></span>
                                    </label>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Keterangan
                                <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" name="keterangan"><?=@$data->keterangan?></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?=$url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->
<div id="large" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib retribusi</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="200px"> No.Registrasi </th>
                                    <th> Nama Wajib retribusi </th>
                                    <th> Tgl Terdaftar </th>
                                    <th width="100px"> Action </th>
                                </tr>
                                <tr role="row">
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="no_daftar" placeholder="No.Registrasi">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="nama_pendaftar" placeholder="Nama Pendaftar">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="tgl_terdaftar" placeholder="Tgl Terdaftar">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<a href="<?php echo $url ?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#modal_show').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_data_umum_wr';
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [0, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });


    });

    function pilih(id){

        $.get({
            url:        '<?=$url?>/get_wajib_retribusi/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res.records.npwp_pemilik);
                $('#nama_pendaftar').val(res.records.nama_pendaftar);
                $('#npwpd').val(res.records.npwpd);
                $('#id').val(res.records.id);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
