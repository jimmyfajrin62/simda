<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_jenis_retribusi/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <div class="form-group">

                            <label class="col-md-3 control-label" for="form_control_1">NPWPD
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="hidden" name="wr_usaha_id" value="<?=$wr_usaha_id?>">
                                <input type="text" class="form-control" required name="npwpd" readonly="readonly" id="npwpd" value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Usaha
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required name="nama_pendaftar" readonly="readonly" id="nama_pendaftar" value="<?= $records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                        </div>


                        <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Obyek
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                <select name="jns_retribusi" class="form-control" id="jenis_retribusi">
                                    <option id="select_retribusi"> -- Pilih Jenis retribusi -- </option>
                                    <?php foreach ($jns_retribusi as $key => $value) :?>
                                        <option value="<?=$value->id_rek_5?>" data-id="<?=$value->jns_pemungutan?>" data-kd5="<?=$value->kd_rek_5?>"> <?=$value->nm_rek_5?> </option>
                                    <?php endforeach ?>
                                </select>
                                <input type="hidden" name="kd_retribusi" value="" id="kd_retribusi">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Jenis Pemungutan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <select name="jn_pemungutan" class="form-control" id="jn_pemungutan">

                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Pengelola
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_urusan" value="<?=@$data->kd_urusan ? @$data->kd_urusan : $organisasi->kd_urusan?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_bidang" value="<?=@$data->kd_bidang ? @$data->kd_bidang : $organisasi->kd_bidang?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_unit" value="<?=@$data->kd_unit ? @$data->kd_unit : $organisasi->kd_unit?>" readonly>
                            </div>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_sub" value="<?=@$data->kd_sub ? @$data->kd_sub : $organisasi->kd_sub?>" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Status
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="mt-checkbox-list">
                                    <label class="mt-checkbox mt-checkbox-outline"> Aktif
                                        <input type="checkbox" value="1" <?=@$data->status_wr == 1 ? 'checked' : ''?> name="status_wr">
                                        <span></span>
                                    </label>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">TMT Operasional
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="tmt_operasional" value="<?=@$data->tmt_operasional?>">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?=$url?>/show_jenisretribusi/<?=$wr_usaha_id?>/<?=$wr_id?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url ?>/show_jenisretribusi/<?=$wr_usaha_id?>/<?=$wr_id?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#modal_show').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_wajib_retribusi';
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [0, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });


</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#kelurahan').attr('disabled','disabled');

        $('#kecamatan').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_kec').remove().end();
            var id  = $(this).find(':selected').attr('value');
            $.get({
                url: '<?=$url.'/get_kelurahan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {kd_kec: id},
                success: function(res){
                    console.log(res);
                    $('#kelurahan').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#kelurahan').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
                        })
                    });
                    $('#kelurahan').removeAttr('disabled');
                }
            });
        });

        $('#jenis_retribusi').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_retribusi').remove().end();
            var id  = $(this).find(':selected').attr('data-id');
            var id2 = $(this).find(':selected').attr('data-kd4');
            $.get({
                url: '<?=$url.'/get_pemungutan/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {jn_pemungutan: id},
                success: function(res){
                    console.log(res);
                    $('#kd_retribusi').val(id2);
                    $('#jn_pemungutan').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#jn_pemungutan').append('<option value="'+value2.jn_pemungutan+'">'+value2.nm_jn_pemungutan+'</option>');
                        })
                    });
                    $('#jn_pemungutan').removeAttr('disabled');
                }
            });
        });

    });
</script>
