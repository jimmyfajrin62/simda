<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_jenis_usaha/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">NPWPD
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="hidden" name="wr_id" value="<?=$wr_id?>">
                                <input type="text" class="form-control" name="npwpd" readonly="readonly" id="npwpd" value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Wajib Retribusi
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_pendaftar" readonly="readonly" id="nama_pendaftar" value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Jenis Usaha
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-3">
                                <select name="jns_usaha" class="form-control" id="jns_usaha" required>
                                    <option id="select_usaha"> -- Pilih Jenis Usaha -- </option>
                                    <?php foreach ($jns_usaha as $key => $value) :?>
                                        <option value="<?=$value->id_rek_5?>" data-1="<?=$value->kd_rek_1?>" data-2="<?=$value->kd_rek_2?>" data-3="<?=$value->kd_rek_3?>" data-4="<?=$value->kd_rek_4?>" data-5="<?=$value->kd_rek_5?>"> <?=$value->nm_rek_5?> </option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Klasifikasi Usaha
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-3">
                                <select name="klasifikasi_usaha" class="form-control" id="klasifikasi" required>

                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Usaha
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" required name="nm_usaha" value="<?=@$data->nm_usaha?>"  id="nm_usaha">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Alamat
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <textarea class="form-control" rows="3" name="alamat_usaha" required><?=@$data->alamat_usaha?></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">NPWP Usaha
                                <span class="required"></span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="npwp_usaha" value="<?=@$data->npwp_usaha?>" id="npwp_usaha">
                                <span class="help-block"></span>
                            </div>
                        </div>
<!--                           <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kec / Kel
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-3">
                                <select name="kd_kec" class="form-control" id="kecamatan" required>
                                    <option id="select_kec"> -- Pilih Kecamatan -- </option>
                                    <?php foreach ($kec as $key => $value): ?>
                                        <option value="<?=$value->kd_kec?>">
                                            <?=$value->nm_kec?>
                                        </option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-3">
                                <select name="kd_kel" class="form-control" id="kelurahan">

                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?=$url?>/show_jenisUsaha/<?=$wr_id?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url ?>/show_jenisUsaha/<?=$wr_id?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        // $('#kelurahan').attr('disabled','disabled');
        // $('#kecamatan').on('change', function(){
        //     // console.log($(this).find(':selected').attr('value'));
        //     $('#select_kec').remove().end();
        //     var id  = $(this).find(':selected').attr('value');
        //     $.get({
        //         url: '<?=$url.'/get_kelurahan/'?>',
        //         type: 'POST',
        //         dataType: 'JSON',
        //         data: {kd_kec: id},
        //         success: function(res){
        //             console.log(res);
        //             $('#kelurahan').find('option').remove().end();
        //             $.each(res, function(key, value){
        //                 $.each(value, function(key2, value2) {
        //                     // console.log(value2.nm_kel);
        //                     $('#kelurahan').append('<option value="'+value2.kd_kel+'" >'+value2.nm_kel+'</option>');
        //                 })
        //             });
        //             $('#kelurahan').removeAttr('disabled');
        //         }
        //     });
        // });

        $('#jns_usaha').on('change', function(){
            // console.log($(this).find(':selected').attr('value'));
            $('#select_usaha').remove().end();
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            var data3  = $(this).find(':selected').attr('data-3');
            var data4  = $(this).find(':selected').attr('data-4');
            var data5  = $(this).find(':selected').attr('data-5');
            $.get({
                url: '<?=$url.'/get_klasifikasi/'?>',
                type: 'POST',
                dataType: 'JSON',
                data: {
                    kd_rek_1: data1,
                    kd_rek_2: data2,
                    kd_rek_3: data3,
                    kd_rek_4: data4,
                    kd_rek_5: data5
                },
                success: function(res){
                    console.log(res);
                    $('#klasifikasi').find('option').remove().end();
                    $.each(res, function(key, value){
                        $.each(value, function(key2, value2) {
                            // console.log(value2.nm_kel);
                            $('#klasifikasi').append('<option value="'+value2.id_rek_6+'">'+value2.nm_rek_6+'</option>');
                        })
                    });
                    $('#klasifikasi').removeAttr('disabled');
                }
            });
        });

    });
</script>
