<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usaha_detail_hotel extends CI_Controller
{
    private $prefix            = 'pendataan/usaha_detail_hotel';
    private $url               = 'pendataan/usaha_detail_hotel';
    private $url1              = 'pendataan/objek_pajak_wp';
    private $path              = 'pendataan/pajak/usaha_detail_hotel/';
    private $table_db          = 'wp_wajib_pajak';
    private $rule_valid        = 'xss_clean|encode_php_tags';
    private $table_prefix      = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_wp', 'mdb');
    }

    // detail usaha
    public function show_detail_usaha($id, $wp_id)
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;
        $data['head']       = $this->mdb->header2($id);

        $data['pagetitle']  = 'Detail Usaha';
        $data['subtitle']   = 'manage Detail Usaha';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url1, 'Detail Usaha' => $this->url.'/show_detail_usaha'.'/'.$id.'/'.$wp_id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'show_detail', $data, $js, $css);
    }

    // pajak usaha
    public function select_detail_usaha($id, $wp_id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_hotel_detail';

        // $join = [
        //             'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
        //             'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
        //             'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
        //             'ref_rek_4'            => ['ref_rek_4 e', 'a.jns_pajak = e.id_rek_4', 'LEFT'],
        //             'ref_pemungutan'       => ['ref_pemungutan f', 'e.jns_pemungutan = f.jn_pemungutan', 'LEFT']
        //         ];

        // jika action checkbox
        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'id'       => 'id',
            'nm_kamar' => 'nm_kamar',
        ];

        $where      = NULL;
        $where_e    = "usaha_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = null;
        } else {
            $where_e = "usaha_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, null, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id, usaha_id, tahun, nm_kamar,tamu,jml_kapasitas,status,lastupdate,tarif,keterangan, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query();exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->nm_kamar),
              strtoupper($rows->jml_kapasitas),
              uang($rows->tarif),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'/'.$id.'/'.$wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_hotel_detail/'.
                                       ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                       ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                       ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                       ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_hotel_detail/99'.
                                       ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // pajak usaha
    public function show_add($id, $wp_id )
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;


        $data['pagetitle']  = 'Jenis Pajak';
        $data['subtitle']   = 'manage jenis pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url1, 'Detail Usaha' => $this->url.'/show_detail_usaha'.'/'.$id.'/'.$wp_id, 'Form' => $this->url.'/show_add'.'/'.$id.'/'.$wp_id];

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js, $css);
    }

    // pajak usaha
    public function show_edit($id, $id2, $wp_id)
    {
        $data['id']         = $id;
        $data['id']         = $id2;
        $data['wp_id']      = $wp_id;

        $data['pagetitle']  = 'Detail Usaha';
        $data['subtitle']   = 'manage Detail Usaha';
        $this->db->where('id', $id);
        $data['data']       = $this->db->get('wp_wajib_pajak_usaha_hotel_detail')->row();

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url1, 'Detail Usaha' => $this->url.'/show_detail_usaha'.'/'.$id2.'/'.$wp_id, 'Form' =>null];

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js, $css);
    }


    public function action_form($id = null)
    {

        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'wp_wajib_pajak_usaha_hotel_detail';

        $this->form_validation->set_rules('usaha_id', 'usaha_id', 'trim');
        $this->form_validation->set_rules('nm_kamar', 'nm_kamar', 'trim');
        $this->form_validation->set_rules('tahun', 'tahun', 'trim');
        $this->form_validation->set_rules('jml_kapasitas', 'jml_kapasitas', 'trim');
        // $this->form_validation->set_rules('tamu', 'tamu', 'trim|required');
        $this->form_validation->set_rules('tarif', 'tarif', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'nm_kamar']      = $this->input->post('nm_kamar');
            $data[$this->table_prefix.'usaha_id']      = $this->input->post('usaha_id');
            $data[$this->table_prefix.'jml_kapasitas'] = $this->input->post('jml_kapasitas');
            $data[$this->table_prefix.'tamu']          = $this->input->post('tamu');
            $data[$this->table_prefix.'tarif']         = str_replace(['Rp', ',', ' '], '', $this->input->post('tarif'));

            if ($id == null) {
                $data[$this->table_prefix.'tahun']     = date('Y');
                $result  = $this->m_global->insert($this->table_db, $data);
            } else {
                $id = $id[0];
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_kamar').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_kamar').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // pajak usaha get pungut
    public function get_pemungutan()
    {
        $table = 'ref_pemungutan';
        $id    = $this->input->post();

        $data['pemungutan']  = $this->m_global->get($table, null, $id);

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }
      
        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Pendataan Report';

        // query
        $data['report']  = $this->db->query("SELECT data_umum_id, tgl_aktif, keterangan, user_lastupdate from user")->result();

        $data['namaFile']   ='User_Report_'.date('Y-m-d');
        $data['title']      ='User Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['Fullname', '30'], ['Name', '30'], ['Email', '30'], ['Lastupdate', '30']
        ];

        $this->load->view('export',$data);
    }
}

/* End of file Pendataan_pajak_wp.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_wp.php */
