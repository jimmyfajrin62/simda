<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pendataan_approve_wp extends Admin_Controller
{
    private $prefix            = 'pendataan/pendataan_approve_wp';
    private $url               = 'pendataan/pendataan_approve_wp';
    private $path              = 'pendataan/pajak/approve_wp/';
    private $table_db          = 'wp_wajib_pajak';
    private $rule_valid        = 'xss_clean|encode_php_tags';
    private $table_prefix      = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_wp', 'mdb');
    }

    // wajib pajak
    public function index()
    {
        $data['pagetitle']  = 'Wajib Pajak';
        $data['subtitle']   = 'manage wajib pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'index', $data, $js, $css);
    }

    // wajib pajak
    public function select()
    {
        $this->table_db = 'wp_wajib_pajak a';

        $join = [
                    'wp_data_umum_join' => ['wp_data_umum b', 'a.data_umum_id = b.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'     => 'b.npwpd',
            'nama_wp'   => 'b.nama_pendaftar',
            'tgl_aktif' => 'a.tgl_aktif'
        ];

        $where      = null;
        $where_e    = 'b.status_pajak = 0 or b.status_pajak = 2';

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and b.status_pajak = 0 or b.status_pajak = 2";
        } else {
            $where_e = "a.status = '1' and b.status_pajak = 0 or b.status_pajak = 2";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.status, b.id as dataum_id, b.status_pajak,'.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                '<span class="label label-m label-primary">'.strtoupper($rows->npwpd).'</span>',
                strtoupper($rows->nama_pendaftar),
                $rows->status_pajak == 0 ? '<span class="label label-m label-warning">Pending</span>' : '<span class="label label-m label-danger">Reject</span>',
                tgl_format($rows->tgl_aktif),
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak/'.
                //     ($rows->status == 1 ? '0" data-original-title="Approve"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                //     ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                //     ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                //     ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/detail_wp/'.$rows->dataum_id).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="Lihat Detail"><i class="fa fa-user"></i></a>'.
                // '<a href="'.base_url($this->url.'/change_approve/'.$rows->dataum_id).'" onClick="return f_approve(1, this, event)" class="btn btn-icon-only green-seagreen tooltips" data-original-title="Approve"><i class="fa fa-check"></i></a>'.
                // '<a href="'.base_url($this->url.'/change_reject/'.$rows->dataum_id).'" onClick="return f_approve(1, this, event)" class="btn btn-icon-only red tooltips" data-original-title="Reject"><i class="fa fa-close"></i></a>'.
                '<a href="'.base_url($this->url.'/show_jenisUsaha/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Jenis Usaha"><i class="fa fa-folder-open"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function detail_wp($id)
    {

        $data['id']         = $id;
        $data['pagetitle']  = 'Wajib Pajak';
        $data['subtitle']   = 'manage wajib pajak';

        $data['wp'] = $this->db->query("SELECT * 
                                                FROM wp_data_umum a
                                                JOIN ref_kelurahan g ON a.kd_kel = g.kd_kel
                                                JOIN ref_kecamatan f ON a.kd_kec = f.kd_kec WHERE id = $id")->row();

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Detail WP' =>null];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'detail_wp', $data, $js, $css);
    }

    // approve
    public function change_approve($id)
    {
        // echo '<pre>', print_r($this->input->post()), exit();

        $query = $this->db->query("select *
                                    from wp_data_umum
                                    where id = $id")->row();

        $log['id']      = $id;
        $log['action']  = 'Approve';
        $log['detail']  = $query->npwpd;
        $log['status']  = '1';
        $log['user_id'] = $this->session->user_data->user_id;
        $log['ip']      = $_SERVER['REMOTE_ADDR'];
        $result2        = $this->db->insert('wp_data_umum_log', $log);

        $result = $this->db->where('id', $id)->update('wp_data_umum', ['status_pajak' => 1]);

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
       

        echo json_encode($data);
    }

    // reject
    public function change_reject($id)
    {

        $query = $this->db->query("select *
                                    from wp_data_umum
                                    where id = $id")->row();

        $log['id']      = $id;
        $log['action']  = 'Reject';
        $log['detail']  = $query->npwpd.' '.$this->input->post('keterangan');
        $log['status']  = '1';
        $log['user_id'] = $this->session->user_data->user_id;
        $log['ip']      = $_SERVER['REMOTE_ADDR'];
        $result2        = $this->db->insert('wp_data_umum_log', $log);

        $result = $this->db->where('id', $id)->update('wp_data_umum', ['status_pajak' => 2]);

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }
        redirect($this->url);

        echo json_encode($data);
    }

    // wajib pajak
    public function show_add()
    {
        $data['np']         = $this->mdb->cek_add_wp();

        $data['pagetitle']  = 'Wajib Pajak';
        $data['subtitle']   = 'manage wajib pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Form' => $this->url.'/show_add'];

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_wp', $data, $js, $css);
    }

    // wajib pajak
    public function show_edit($id)
    {
        $data['id']         = $id;
        $data['data']       = $this->mdb->show_wp_row($id);

        $data['pagetitle']  = 'Wajib Pajak';
        $data['subtitle']   = 'manage wajib pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Form' => $this->url.'/show_edit'.'/'.$id];

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_wp', $data, $js, $css);
    }

    // wajib pajak
    public function action_add_wajib_pajak($id = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->form_validation->set_rules('tgl_aktif', 'Tanggal Aktif', 'trim|required');
        $this->form_validation->set_rules('status', 'Status WP', 'trim');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

        if ($this->form_validation->run($this)) {
            $data['data_umum_id'] = $this->input->post('data_umum_id');
            $data['tgl_aktif']    = $this->m_global->setdateformat($this->input->post('tgl_aktif'));
            $data['status']       = $this->input->post('status') == NULL ? '0' : $this->input->post('status');
            $data['keterangan']   = $this->input->post('keterangan');

            if ($id == null) {
                // update pendaftaran
                $data2['status_pajak'] = 1;
                $result2 = $this->m_global->update('wp_data_umum', $data2, ['id' => $this->input->post('data_umum_id')]);

                $result  = $this->m_global->insert($this->table_db, $data);
            } else {
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran pajak with Name <strong>'.$this->input->post('nama_wp').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran Pajak with Name <strong>'.$this->input->post('nama_wp').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // wajib pajak modal
    public function select_data_umum_wp()
    {
        $this->table_db = 'wp_data_umum';

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_daftar'      => 'no_daftar',
            'nama_pendaftar' => 'nama_pendaftar',
            'tgl_terdaftar'  => 'tgl_terdaftar',
        ];

        $where      = null;
        $where_e    = 'status_pajak = 0';

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['status']   = $_REQUEST['filterstatus'];
        } else {
            $where['status ='] = '1';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords   = $this->m_global->count($this->table_db, null, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);

        $records         = array();
        $records["data"] = array();

        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id, status,'.implode(',', $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ($result as $rows) {
            $records["data"][] = array(
                $i,
                strtoupper($rows->no_daftar),
                strtoupper($rows->nama_pendaftar),
                $rows->tgl_terdaftar == null || $rows->tgl_terdaftar == '0000-00-00 00:00:00' ? 'Pending' : tgl_format($rows->tgl_terdaftar),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i>'.
                '</a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // wajib pajak modal
    public function get_wajib_pajak()
    {
        $this->table_db  = 'wp_data_umum';
        $id              = $this->input->post('id');

        $data['records'] = $this->m_global->get($this->table_db, null, ['id' => $id])[0];
        header("Content-Type:application/json");
        echo json_encode($data);
    }

    // usaha
    public function show_jenisUsaha($id)
    {
        $data['id']         = $id;
        $data['head']       = $this->mdb->header($id);

        $data['pagetitle']  = 'Jenis Usaha';
        $data['subtitle']   = 'manage jenis usaha';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'jenis_usaha', $data, $js, $css);
    }

    // usaha
    public function select_jenisUsaha($id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha a';

        $join = [
                    'wp_wajib_pajak' => ['wp_wajib_pajak b', 'a.wp_id = b.id', 'LEFT'],
                    'wp_data_umum'   => ['wp_data_umum c', 'b.data_umum_id = c.id', 'LEFT'],
                    'ref_rek_4'      => ['ref_rek_4 d', 'a.jns_usaha = d.id_rek_4', 'LEFT'],
                    'ref_rek_5'      => ['ref_rek_5 e', 'a.klasifikasi_usaha = e.id_rek_5', 'LEFT'],
                ];

        // jika action checkbox
        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_usaha'     => 'a.nm_usaha',
            'nm_usaha_4'   => 'd.nm_usaha_4',
            'nm_usaha_5'   => 'e.nm_usaha_5',
            'alamat_usaha' => 'a.alamat_usaha'
        ];

        $where      = NULL ;

        $where_e    = "a.wp_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'c.npwpd, a.id, a.wp_id, a.status, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->nm_usaha),
              strtoupper($rows->nm_usaha_4),
              strtoupper($rows->nm_usaha_5),
              strtoupper($rows->alamat_usaha),
              '<a href="'.base_url($this->url.'/show_jenisPajak/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Jenis Pajak"><i class="fa fa-folder-open"></i></a>'.
              '<a href="'.base_url($this->url.'/show_suratIjin/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Surat Izin Yang Dimiliki">
                   <i class="fa fa-file-text"></i></a>'.
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_usaha/'.$rows->id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // usaha
    public function show_add_jenis_usaha($id)
    {
        $data['wp_id']             = $id;
        $data['records']           = $this->mdb->show_wp_row($id);
        $data['kec']               = $this->mdb->kecamatan();
        $data['jns_usaha']         = $this->mdb->jns_usaha();

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$id, 'Form' => $this->url.'/show_add_jenis_usaha'.'/'.$id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_usaha', $data, $js, $css);
    }

    // usaha
    public function show_edit_jenis_usaha($id, $wp_id)
    {
        $data['wp_id']             = $wp_id;
        $data['data']              = $this->mdb->show_wp_usaha_row($id);
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['kec']               = $this->mdb->kecamatan();
        $data['kel']               = $this->mdb->kelurahan_row($data['data']->kd_kec);
        $data['jns_usaha']         = $this->mdb->jns_usaha();
        $data['klasifikasi_usaha'] = $this->mdb->klasifikasi_usaha_row($data['data']->jns_usaha);

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Form' => $this->url.'/show_edit_jenis_usaha'.'/'.$id.'/'.$wp_id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_usaha', $data, $js, $css);
    }

    // usaha
    public function action_jenis_usaha($id = null)
    {
        // print_r($this->input->post()); exit();
        $this->form_validation->set_rules('jns_usaha', 'Jenis Usaha', 'trim|required');
        $this->form_validation->set_rules('klasifikasi_usaha', 'Klasifikasi Usaha', 'trim|required');
        $this->form_validation->set_rules('nm_usaha', 'Nama Usaha', 'trim|required');
        $this->form_validation->set_rules('alamat_usaha', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('kd_kec', 'Kecamatan', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_id']             = $this->input->post('wp_id');
            $data[$this->table_prefix.'jns_usaha']         = $this->input->post('jns_usaha');
            $data[$this->table_prefix.'klasifikasi_usaha'] = $this->input->post('klasifikasi_usaha');
            $data[$this->table_prefix.'npwp_usaha']        = $this->input->post('npwp_usaha');
            $data[$this->table_prefix.'nm_usaha']          = $this->input->post('nm_usaha');
            $data[$this->table_prefix.'alamat_usaha']      = $this->input->post('alamat_usaha');
            $data[$this->table_prefix.'kd_kec']            = $this->input->post('kd_kec');
            $data[$this->table_prefix.'kd_kel']            = $this->input->post('kd_kel');

            if ($id == null) {
                $result  = $this->m_global->insert('wp_wajib_pajak_usaha', $data);
            } else {
                $result = $this->m_global->update('wp_wajib_pajak_usaha', $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // usaha option
    public function get_kelurahan()
    {
        $table_kel          = 'ref_kelurahan';

        $id = $this->input->post();
        $data['kelurahan']  = $this->m_global->get($table_kel, null, $id, 'kel_id, kd_kec, kd_kel, nm_kel');

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // usaha option
    public function get_klasifikasi()
    {
        $kd_rek_1 = $this->input->post('kd_rek_1');
        $kd_rek_2 = $this->input->post('kd_rek_2');
        $kd_rek_3 = $this->input->post('kd_rek_3');
        $kd_rek_4 = $this->input->post('kd_rek_4');

        $data['klasifikasi']  = $this->db->query("select * from ref_rek_5 where kd_rek_1 = $kd_rek_1 and kd_rek_2 = $kd_rek_2 and kd_rek_3 = $kd_rek_3 and kd_rek_4 = $kd_rek_4")->result();

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // pajak usaha
    public function show_jenisPajak($id, $wp_id)
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;
        $data['head']       = $this->mdb->header2($id);

        $data['pagetitle']  = 'Jenis Pajak';
        $data['subtitle']   = 'manage jenis pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$id.'/'.$wp_id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function select_jenis_pajak($id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_pajak a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                    'ref_rek_4'            => ['ref_rek_4 e', 'a.jns_pajak = e.id_rek_4', 'LEFT'],
                    'ref_pemungutan'       => ['ref_pemungutan f', 'e.jns_pemungutan = f.jn_pemungutan', 'LEFT']
                ];

        // jika action checkbox
        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'e.nm_rek_4',
            'tmt_operasional'  => 'a.tmt_operasional',
            'nm_jn_pemungutan' => 'f.nm_jn_pemungutan'
        ];

        $where      = NULL ;

        $where_e    = "a.wp_usaha_id' = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_usaha_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_usaha_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.wp_usaha_id, a.status, b.wp_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->nm_rek_4),
              tgl_format($rows->tmt_operasional),
              strtoupper($rows->nm_jn_pemungutan),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_pajak/'.$rows->id.'/'.$rows->wp_usaha_id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_pajak/'.
                                       ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                       ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                       ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                       ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_pajak/99'.
                                       ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // pajak usaha
    public function show_add_jenis_pajak($id, $wp_id)
    {
        $data['wp_usaha_id']       = $id;
        $data['wp_id']             = $wp_id;
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['jns_pajak']         = $this->mdb->jns_usaha();
        $data['organisasi']        = $this->mdb->organisasi_row($this->session->user_data->user_organisasi);

        $data['pagetitle']         = 'Jenis Pajak';
        $data['subtitle']          = 'manage jenis pajak';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$id.'/'.$wp_id, 'Form' => $this->url.'/show_add_jenis_pajak'.'/'.$id.'/'.$wp_id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function show_edit_jenis_pajak($id, $wp_usaha_id, $wp_id)
    {
        $data['wp_usaha_id']       = $wp_usaha_id;
        $data['wp_id']             = $wp_id;
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['jns_pajak']         = $this->mdb->jns_usaha();
        $data['organisasi']        = $this->mdb->organisasi_row($this->session->user_data->user_organisasi);
        $data['data']              = $this->mdb->show_pajak_usaha_row($id);

        $data['pagetitle']         = 'Jenis Pajak';
        $data['subtitle']          = 'manage jenis pajak';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$wp_usaha_id.'/'.$wp_id, 'Form' => $this->url.'/show_edit_jenis_pajak'.'/'.$id.'/'.$wp_usaha_id.'/'.$wp_id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function action_jenis_pajak($id = null)
    {
        // print_r($this->input->post()); exit();
       $this->form_validation->set_rules('tmt_operasional', 'TMT Operasional', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_usaha_id']     = $this->input->post('wp_usaha_id');
            $data[$this->table_prefix.'jns_pajak']       = $this->input->post('jns_pajak');
            $data[$this->table_prefix.'kd_urusan']       = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']       = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']         = $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']          = $this->input->post('kd_sub');
            $data[$this->table_prefix.'tmt_operasional'] = $this->input->post('tmt_operasional');
            $data[$this->table_prefix.'status']          = $this->input->post('status') == NULL ? '0' : $this->input->post('status');

            if ($id == null) {
                $result  = $this->m_global->insert('wp_wajib_pajak_usaha_pajak', $data);
            } else {
                $result = $this->m_global->update('wp_wajib_pajak_usaha_pajak', $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // pajak usaha get pungut
    public function get_pemungutan()
    {
        $table = 'ref_pemungutan';
        $id    = $this->input->post();

        $data['pemungutan']  = $this->m_global->get($table, null, $id);

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // ijin usaha
    public function show_suratIjin($id, $wp_id)
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;
        $data['head']       = $this->mdb->header2($id);

        $data['pagetitle']  = 'Surat Izin';
        $data['subtitle']   = 'manage surat izin';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id.'/'.$wp_id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'surat_ijin', $data, $js, $css);
    }

    // ijin usaha
    public function select_suratIjin($id)
    {
        // select table urusan
        $this->table_db = 'wp_wajib_pajak_izin a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_izin'  => 'a.no_izin',
            'nm_izin'  => 'a.nm_izin',
            'tgl_izin' => 'a.tgl_izin'
        ];

        $where      = null;
        $where_e    = "a.wp_usaha_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_usaha_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_usaha_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'b.wp_id, a.id, wp_usaha_id, a.wp_usaha_id, a.status, a.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->no_izin),
              strtoupper($rows->nm_izin),
              tgl_format($rows->tgl_izin),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_surat_izin/'.$rows->id.'/'.$rows->wp_usaha_id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_izin/'.
                                       ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                       ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                       ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                       ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_izin/99'.
                                       ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // ijin usaha
    public function show_add_surat_izin($id, $wp_id)
    {
        $data['wp_usaha_id'] = $id;
        $data['wp_id']       = $wp_id;
        $data['records']     = $this->mdb->show_wp_row($wp_id);

        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['breadcrumb']  = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id.'/'.$wp_id, 'Form' => $this->url.'/show_add_surat_izin'.'/'.$id.'/'.$wp_id];

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_surat_izin', $data, $js, $css);
    }

    // ijin usaha
    public function show_edit_surat_izin($id, $wp_usaha_id, $wp_id)
    {
        $data['wp_usaha_id'] = $wp_usaha_id;
        $data['wp_id']       = $wp_id;
        $data['records']     = $this->mdb->show_wp_row($wp_id);
        $data['data']        = $this->mdb->show_izin($id);

        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['breadcrumb']  = [ 'Pendataan' => null, 'Pajak' => null, 'Wajib Pajak' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wp_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$wp_usaha_id.'/'.$wp_id, 'Form' => $this->url.'/show_edit_surat_izin'.'/'.$id.'/'.$wp_usaha_id.'/'.$wp_id];

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_surat_izin', $data, $js, $css);
    }

    // ijin usaha
    public function action_surat_izin($id = null)
    {
        $this->table_db_izin = 'wp_wajib_pajak_izin';

        $this->form_validation->set_rules('no_izin', 'No.Izin', 'trim|required');
        $this->form_validation->set_rules('nm_izin', 'Nama Izin', 'trim|required');
        $this->form_validation->set_rules('tgl_izin', 'Tanggal Izin', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_usaha_id']    = $this->input->post('wp_usaha_id');
            $data[$this->table_prefix.'no_izin']        = $this->input->post('no_izin');
            $data[$this->table_prefix.'nm_izin']        = $this->input->post('nm_izin');
            $data[$this->table_prefix.'tgl_izin']       = $this->input->post('tgl_izin');


            if ($id == null) {
                $result  = $this->m_global->insert($this->table_db_izin, $data);
            } else {
                $result = $this->m_global->update($this->table_db_izin, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Pendataan Report';

        // query
        $data['report']  = $this->db->query("SELECT data_umum_id, tgl_aktif, keterangan, user_lastupdate from user")->result();

        $data['namaFile']   ='User_Report_'.date('Y-m-d');
        $data['title']      ='User Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['Fullname', '30'], ['Name', '30'], ['Email', '30'], ['Lastupdate', '30']
        ];

        $this->load->view('export',$data);
    }
}

/* End of file Pendataan_pajak_wp.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_wp.php */
