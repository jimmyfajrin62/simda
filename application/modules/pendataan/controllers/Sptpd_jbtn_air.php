<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sptpd_jbtn_air extends Admin_Controller
{
    private $prefix         = 'pendataan/sptpd_jbtn_air';
    private $url            = 'pendataan/sptpd_jbtn_air';
    private $table_db       = 'ta_kartu_pajak_air';
    private $path           = 'pendataan/pajak/sptpd/sptpd_air/';
    private $path_sptpd     = 'pendataan/pendataan_pajak_sptpd';
    private $rule_valid     = 'xss_clean|encode_php_tags';
    private $table_prefix   = '';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_sptpd_air', 'mdb');
    }

    function _remap($method,$args)
    {
        if (method_exists($this, $method)){
            $this->$method($args);
        }
        else{
            $this->index($method,$args);
        }
   }

    public function index($kd_rek_4)
    {
        $data['kd_rek_4']   = $kd_rek_4;

        $data['pagetitle']  = 'Pendataan SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4 ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'index', $data, $js, $css);
    }

    public function select($kd_rek_4)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_pajak a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'          => 'd.npwpd',
            'nama_pendaftar' => 'd.nama_pendaftar',
            'nm_usaha'       => 'b.nm_usaha',
            'alamat_usaha'   => 'b.alamat_usaha'
        ];

        $where    = null;
        $id       = $kd_rek_4[0];
        $where_e  = "a.jns_pajak = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.jns_pajak = $id";
        } else {
            $where_e = "a.status = '1' and a.jns_pajak = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.status, c.tgl_aktif, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              '<span class="label label-m label-primary">'.strtoupper($rows->npwpd).'</span>',
              strtoupper($rows->nama_pendaftar),
              strtoupper($rows->nm_usaha),
              strtoupper($rows->alamat_usaha),
              tgl_format($rows->tgl_aktif),
               '<a href="'.base_url($this->url.'/show_sptpd/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SPTPD"><i class="fa fa-folder-open"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_sptpd($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $data['pagetitle']  = 'SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['id']         = $id;

        $data['head']    = $this->mdb->show_npwpd($id);

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sptpd', $data, $js, $css);
    }

    public function select_sptpd($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $this->table_db = 'ta_kartu_pajak_pungut';

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_sptpd'   => 'no_sptpd',
            'tgl_sptpd'  => 'tgl_sptpd',
            'masa2'      => 'masa2',
            'keterangan' => 'keterangan'
        ];

        $where    = null;
        $where_e  = "wp_usaha_pajak_id = $id and jns_sptpd = 2";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "status = '$request' and wp_usaha_pajak_id = $id and jns_sptpd = 2";
        } else {
            $where_e = "status = '1' and wp_usaha_pajak_id = $id and jns_sptpd = 2";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, null, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id, wp_usaha_pajak_id, masa1, status_sspd, status, lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            if(($this->session->userdata('user_data')->user_role == 1)){
                $ubah_status = '<a data-original-title="Ubah status" href="'.base_url().$this->url.'/show_ubah/'.$kd_rek_4.'/'.$id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-refresh"></i></a>';
            }else{
                $ubah_status = '';
            }
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              '<span class="label label-m label-primary">'.strtoupper($rows->no_sptpd).'</span>',
              tgl_format($rows->tgl_sptpd),
              tgl_format($rows->masa1).'<br> s/d <br>'.tgl_format($rows->masa2),
              strtoupper($rows->keterangan),
                $rows->status_sspd == 1 && $this->session->user_data->user_role != 1  ?
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_sptpd_pajak/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Pajak SPTPD"><i class="fa fa-folder-open"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'
                :
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_sptpd_pajak/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Pajak SPTPD"><i class="fa fa-folder-open"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$kd_rek_4.'/'.$id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_pungut/'.
                    ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                    ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                    ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                    ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_pungut/99'.
                    ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'.
                $ubah_status,
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $data['pagetitle']  = 'SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['records']    = $this->mdb->show_npwpd($id);
        $data['dokumen']    = $this->mdb->petugas();
        $data['bulan']      = $this->db->get('bulan')->result();

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js);
    }

    public function show_edit( $ids )
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];
        $id2      = $ids[2];

        $data['pagetitle']  = 'SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['records']    = $this->mdb->show_npwpd($id);
        $data['data']       = $this->mdb->show_pungut($id2);
        $data['bulan']      = $this->db->get('bulan')->result(); 
        $data['bulan_id']   = date('m', strtotime($data['data']->masa1));
        $data['dokumen']    = $this->mdb->petugas();

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id.'/'.$id2];
        $js['js']           = [ 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js);
    }

    public function action_form($ids = NULL)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        // $id = $ids[0]; echo $id;exit();
        $this->table_db = 'ta_kartu_pajak_pungut';

        $this->form_validation->set_rules('wp_usaha_pajak_id', 'wp_usaha_pajak_id', 'trim');
        $this->form_validation->set_rules('ttd_dok_id', 'ttd_dok_id', 'trim');
        $this->form_validation->set_rules('tahun', 'tahun', 'trim');
        $this->form_validation->set_rules('no_sptpd', 'no_sptpd', 'trim');
        $this->form_validation->set_rules('tgl_sptpd', 'Tgl SPTPD', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim');
        $this->form_validation->set_rules('tgl_terima', 'Tgl Terima', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $last     = date('t',strtotime(date('Y').'-'.$this->input->post('masa').'-'.date('d')));
            $firstday = date('Y').'-'.$this->input->post('masa').'-01';
            $lastday  = date('Y').'-'.$this->input->post('masa').'-'.$last;

            $data[$this->table_prefix.'wp_usaha_pajak_id'] = $this->input->post('wp_usaha_pajak_id');
            $data[$this->table_prefix.'jns_sptpd']         = 2;
            $data[$this->table_prefix.'ttd_dok_id']        = $this->input->post('ttd_dok_id');
            $data[$this->table_prefix.'tahun']             = $this->input->post('tahun');
            $data[$this->table_prefix.'tgl_sptpd']         = $this->m_global->setdateformat($this->input->post('tgl_sptpd'));
            $data[$this->table_prefix.'masa1']             = $firstday;
            $data[$this->table_prefix.'masa2']             = $lastday;
            $data[$this->table_prefix.'keterangan']        = $this->input->post('keterangan');
            $data[$this->table_prefix.'tgl_terima']        = $this->m_global->setdateformat($this->input->post('tgl_terima'));

            if ($ids == NULL) {
                $date     = date("d/m/Y");
                $no_sptpd = $this->mdb->no_sptpd('S', "/SPTPD/$date");

                $data[$this->table_prefix.'id']                = $this->mdb->auto_id();
                $data[$this->table_prefix.'tahun']             = date('Y');
                $data[$this->table_prefix.'no_sptpd']          = $no_sptpd;
                $result  = $this->m_global->insert( $this->table_db, $data );

                $log['id']      = $data[$this->table_prefix.'id'];
                $log['action']  = 'Add SPTPD Jabatan - Air Tanah';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'NPWPD : '.$this->input->post('npwpd').' Jenis SPTPD = 2, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sptpd_log', $log);
            }
            else{
                $id = $ids[0];
                $data[$this->table_prefix.'no_sptpd']          = $this->input->post('no_sptpd');
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

                $log['id']      = $ids[0];
                $log['action']  = 'Edit SPTPD Jabatan - Air Tanah';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'NPWPD : '.$this->input->post('npwpd').' Jenis SPTPD = 2, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sptpd_log', $log);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    //ubah status -> admin
    public function show_ubah($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];
        $id2      = $ids[2];

        $data['pagetitle']  = 'SPTPD Air';
        $data['subtitle']   = 'manage SPTPD Air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['id2']        = $id2;

        $data['label']      = $this->db->query("select status_label, status_tipe, status_detail, status_group from status WHERE status_group ='ta_kartu_pajak_pungut' GROUP BY status_label")->result_array();

        foreach($data['label'] as $val){

            $data['isi'][$val['status_tipe']]= $this->db->query("SELECT status_detail, status_id, status_label FROM status where status_tipe in ('".$val['status_tipe']."') and status_group = '$val[status_group]'")->result();

        }

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Air' => $this->url.'/'.$kd_rek_4, 'SPTPD Air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id.'/'.$id2];
        $js['js']           = [ 'form-validation' ];

        $this->template->display('pendataan/pajak/sptpd/form_ubah', $data, $js);
    }

    //action status -> admin
    public function action_ubah($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'ta_kartu_pajak_pungut';

        $this->form_validation->set_rules('id', 'ID', 'trim');

        if ($this->form_validation->run($this)) {
            $detail = 'Ubah Status : ';
            foreach($this->input->post('combo') as $key => $val){
                $data[$key] = $val;
                $detail = $detail.$key.' = '.$val.', ';
            }

            $query = $this->db->query("select * from wp_data_umum where id = $ids[0]")->row();
            
            $log['id']      = $ids[0];
            $log['action']  = 'Ubah Status';
            $log['detail']  = $query['npwpd'].$detail;
            $log['status']  = '1';
            $log['user_id'] = $this->session->user_data->user_id;
            $log['ip']      = $_SERVER['REMOTE_ADDR'];
            $result2        = $this->db->insert('sptpd_log', $log);

            $result = $this->m_global->update('ta_kartu_pajak_pungut', $data, ['id' => $ids[0]]);


            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function show_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $data['pagetitle']  = 'SPTPD Pajak air';
        $data['subtitle']   = 'manage Jabatan SPTPD pajak air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak air' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;

        $data['head']    = $this->mdb->show_npwpd($wp);
        $data['status_sspd'] = $this->db->select('status_sspd')->where('id', $id)->get('ta_kartu_pajak_pungut')->row();

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sptpd_pajak', $data, $js, $css);
    }

    public function select_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $this->table_db = 'ta_kartu_pajak_air';

        $join = [
                    'ref_rek_6' => ['ref_rek_6', 'ta_kartu_pajak_air.id_rek_6 = ref_rek_6.id_rek_6', 'LEFT'],
                    'ta_kartu_pajak_pungut' => ['ta_kartu_pajak_pungut', 'ta_kartu_pajak_air.pungut_id = ta_kartu_pajak_pungut.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
             'nm_rek_6'        => 'ref_rek_6.nm_rek_6',
             // 'uraian'     => 'ta_kartu_pajak_air.uraian',
             'pajak_terhutang' => 'ta_kartu_pajak_air.pajak_terhutang'
         ];

        $where    = null;
        $where_e  = "ta_kartu_pajak_air.pungut_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "ta_kartu_pajak_air.status = '$request' and ta_kartu_pajak_air.pungut_id = $id";
        } else {
            $where_e = "ta_kartu_pajak_air.status = '1' and ta_kartu_pajak_air.pungut_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ta_kartu_pajak_pungut.status_sspd, ref_rek_6.kd_rek_1, ref_rek_6.kd_rek_2, ref_rek_6.kd_rek_3, ref_rek_6.kd_rek_4, ref_rek_6.kd_rek_5, ref_rek_6.kd_rek_6,ref_rek_6.nm_rek_6, ta_kartu_pajak_air.id,ta_kartu_pajak_air.lokasi,ta_kartu_pajak_air.pajak_terhutang, ta_kartu_pajak_air.status, ta_kartu_pajak_air.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                 '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
               $i,
               $rows->lokasi,
               '<span class="label label-m label-primary">'.$rows->kd_rek_1.' . '.$rows->kd_rek_2.' . '.$rows->kd_rek_3.' . '.$rows->kd_rek_4.' . '.$rows->kd_rek_5.' . '.$rows->kd_rek_6.'</span>',
               strtoupper($rows->nm_rek_6),
                uang($rows->pajak_terhutang),
                $rows->status_sspd == 1 ?
                // '<a href="'.base_url($this->url.'/show_rinci/'.$kd_rek_4.'/'.$wp.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Kamar air"><i class="fa fa-folder-open"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'
                :
                // '<a href="'.base_url($this->url.'/show_rinci/'.$kd_rek_4.'/'.$wp.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Kamar air"><i class="fa fa-folder-open"></i></a>'.
                '<a href="'.base_url($this->url.'/show_edit_sptpd_pajak/'.$kd_rek_4.'/'.$wp.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue tooltips" data-original-title="Edit Pajak SPTPD"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_air/'.
                    ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_air/99'.
                    ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $data['pagetitle']  = 'SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;
        $data['records']    = $this->mdb->show_npwpd($wp);

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => null, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak air' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display('pendataan/pajak/sptpd/sptpd_air/form_sptpd_pajak', $data, $js);
    }

    public function show_edit_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];
        $id2      = $ids[3];

        $data['pagetitle']  = 'SPTPD air';
        $data['subtitle']   = 'manage Jabatan SPTPD air';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;
        $data['records']    = $this->mdb->show_npwpd($wp);
        $data['data']       = $this->mdb->show_sptpd_pajak($id2);

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => null, 'Pendataan SPTPD air' => $this->url.'/'.$kd_rek_4, 'SPTPD air' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak air' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display('pendataan/pajak/sptpd/sptpd_air/form_sptpd_pajak', $data, $js);
    }

    public function action_form_sptpd_pajak($ids = null)
    {
        $this->table_db = 'ta_kartu_pajak_air';

        $this->form_validation->set_rules('id_rek_6', 'Obyek Pajak', 'trim|required');
        // $this->form_validation->set_rules('dasar_pengenaan', 'Dasar Pengenaan', 'trim|required');
        // $this->form_validation->set_rules('tarif_pajak', 'Tarif Pajak', 'trim|required');
        $this->form_validation->set_rules('pembulatan', 'Pembulatan', 'trim|required');

        if ($this->form_validation->run($this)) {
            // perhitungan pajak
            $tarif           = $this->input->post('tarifpajak');
            $npa             = $this->input->post('npa');
            $hutang          = $npa * $tarif / 100;

            $data[$this->table_prefix.'pajak_terhutang'] = $hutang;
            $data[$this->table_prefix.'pungut_id']       = $this->input->post('pungut_id');
            $data[$this->table_prefix.'id_rek_6']        = $this->input->post('id_rek_6');
            $data[$this->table_prefix.'tarifpajak']     = $this->input->post('tarifpajak');
            $data[$this->table_prefix.'volume']          = $this->input->post('volume');
            $data[$this->table_prefix.'npa']             = $this->input->post('npa');
            $data[$this->table_prefix.'pembulatan']      = $this->input->post('pembulatan');
            $data[$this->table_prefix.'lokasi']          = $this->input->post('lokasi');

        // echo '<pre>', print_r($data), exit();
            if ($ids == null) {
                $data[$this->table_prefix.'tahun']       = date('Y');
                $result  = $this->m_global->insert($this->table_db, $data);

                $log['id']      = $this->db->insert_id();
                $log['action']  = 'Add SPTPD Jabatan Rinci - Air';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'NPWPD : '.$this->input->post('npwpd').' Jenis SPTPD = 2, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sptpd_log', $log);

            } else {
                $id     = $ids[0];
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

                $log['id']      = $ids[0];
                $log['action']  = 'Edit SPTPD Jabatan Rinci - Air';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'NPWPD : '.$this->input->post('npwpd').' Jenis SPTPD = 2, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sptpd_log', $log);

            }
            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";exit();

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function select_rek_6()
    {
        $this->table_db = 'ref_rek_6';

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => $this->table_prefix.'nm_rek_6',
        ];

        $where      = NULL;
        $where_e    = 'kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 1 and kd_rek_4 = 8';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status =']    = '1';
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6, status, lastupdate,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $rows->kd_rek_1.' . '.$rows->kd_rek_2.' . '.$rows->kd_rek_3.' . '.$rows->kd_rek_4.' . '.$rows->kd_rek_5.' . '.$rows->kd_rek_6,
                strtoupper($rows->nm_rek_6),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i>'.
                '</a>',
            );
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function get_rek_6()
    {
        $this->table_db  = 'ref_rek_6';
        $id              = $this->input->post('id');

        $data['records'] = $this->m_global->get($this->table_db, null, ['id_rek_6' => $id] )[0];
        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function pembulatan($hutang, $pembulatan)
    {
        $ratusan = substr($hutang, -3);

        if ($pembulatan == 100) {
            return $akhir = $hutang + (100-$ratusan);
        }
        else if ($pembulatan == 1000) {
            return $akhir = $hutang + (1000-$ratusan);
        }
        else {
            return $hutang;
        }
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($ids)
    {
        // echo "<pre>",print_r($id),exit();
        $id     = $ids[0];
        $table  = $ids[1];
        $status = $ids[2];
        $stat   = $ids[3];

        if ($stat == 'true') {
            // update sptpd
            $pungut               = $this->db->query("SELECT pungut_id from ta_sspd  where id = $id")->row();
            $id_pungut            = $pungut->pungut_id;
            $data2['status_sspd'] = 0;
            $result2              = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);

            $result               = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result               = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

}
