<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendataan_pajak_sptpd extends CI_Controller
{
    private $prefix         = 'pendataan/pendataan_pajak_sptpd';
    private $url            = 'pendataan/pendataan_pajak_sptpd';
    private $table_db       = 'ref_rek_4';
    private $path           = 'pendataan/pajak/sptpd/';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        if ($this->session->user_data->user_role != 1 && $this->session->user_data->user_role != 3) {
            redirect( base_url().'pendaftaran/pendaftaran_pajak' );
        }

        $data['pagetitle']  = 'Pendataan Pajak SPTPD';
        $data['subtitle']   = 'manage pendataan pajak SPTPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        if ($this->session->user_data->user_role == 2) {

            $user = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();

            if ($user->user_wp == null)
            {
                $data['status_pajak'] = FALSE;
                $this->template->display( 'pendataan/pajak/sptpd/no_wp', $data, $js, $css );

            } else {
                $data['wp'] = $this->db->query("SELECT * from wp_data_umum where id = $user->user_wp")->row();

                if ($data['wp']->status_pajak == 0) {
                    $data['status_pajak'] = TRUE;
                    $this->template->display( 'pendataan/pajak/sptpd/no_wp', $data, $js, $css );
                } else {
                    $this->sptpd_user();
                    // $this->template->display( 'pendataan/pajak/sptpd/sptpd_user', $data, $js, $css );
                }
            }
        } else {
            $this->template->display( 'pendataan/pajak/sptpd/index', $data, $js, $css );
        }

	}

    public function select()
    {
        $user_id = $this->session->user_data->user_id;
        
        $join = [
                    'ref_pemungutan' => ['ref_pemungutan', 'ref_rek_4.jns_pemungutan = ref_pemungutan.jn_pemungutan', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'ref_rek_4.nm_rek_4',
            'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where    = null;
        // query session petugas

        $query = $this->db->query("select * from user_rek_4 where user_id = $user_id")->row();

        if ($this->session->user_data->user_role == 1) {
        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 != 72 and id_rek_4 != 15";
            
        } else {
        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 in ($query->id_rek_4)";
            
        }

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_rek_4.jns_pemungutan, ref_rek_4.id_rek_4, ref_rek_4.link, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              ucwords($rows->nm_rek_4),
              ucwords ($rows->nm_jn_pemungutan),
              $rows->jns_pemungutan != 2 ?
               '<a href="'.base_url('pendataan/sptpd_'.$rows->link.'/'.$rows->id_rek_4).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="SPTPD Masa"><i class="fa fa-folder-open"></i></a>'.
               '<a href="'.base_url('pendataan/sptpd_jbtn_'.$rows->link.'/'.$rows->id_rek_4).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="SPTPD Jabatan"><i class="fa fa-file-text"></i></a>'
               :
               '<a class="btn btn-icon-only grey tooltips" disabled><i class="fa fa-lock"></i></a>'.
               '<a href="'.base_url('pendataan/sptpd_jbtn_'.$rows->link.'/'.$rows->id_rek_4).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="SPTPD Jabatan"><i class="fa fa-file-text"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function sptpd_user()
    {
		$data['pagetitle']  = 'SPTPD';
		$data['subtitle']   = 'manage sptpd';

		$data['url']        = base_url().$this->url;
		$data['breadcrumb'] = [ 'SPTPD' => $this->url.'/sptpd_user' ];
		$data['user_id']    = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

        $user = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();

        if ($user->user_wp == null)
        {
            $data['status_pajak'] = FALSE;
            $this->template->display( 'pendataan/pajak/sptpd/no_wp', $data, $js, $css );

        } else {
            $data['wp'] = $this->db->query("SELECT * from wp_data_umum where id = $user->user_wp")->row();

            if ($data['wp']->status_pajak == 0) {
                $data['status_pajak'] = TRUE;
                $this->template->display( 'pendataan/pajak/sptpd/no_wp', $data, $js, $css );
            } else {
                // $this->template->display( 'pendataan/pajak/sptpd/index', $data, $js, $css );
                $this->template->display($this->path.'sptpd_user', $data, $js, $css);
            }
        }

    }

    public function select_sptpd_user($id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_pajak a';

		$join = [
			'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
			'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
			'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
			'ref_rek_4'            => ['ref_rek_4 e', 'a.jns_pajak = e.id_rek_4', 'LEFT'],
		];

		if (@$_REQUEST['customActionType'] == 'group_action') {
			$aChk = [0, 1, 99];

			if (in_array(@$_REQUEST['customActionName'], $aChk)) {
				$this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
				$records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
				$records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
			}
		}

		$aCari = [
			'nm_usaha'       => 'b.nm_usaha',
			'alamat_usaha'   => 'b.alamat_usaha',
			'nm_rek_4'       => 'e.nm_rek_4',
		];

		$where    = null;
		$where_e  = "d.id = $id";

		if (@$_REQUEST['action'] == 'filter') {
			$where = [];
			foreach ($aCari as $key => $value) {
				if ($_REQUEST[$key] != '') {
					if ($key == 'lastupdate') {
						$tmp = explode(' ', $_REQUEST[$key]);
						$where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
					} else {
						$where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
					}
				}
			}
		}

		if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
			$request = $_REQUEST['filterstatus'];
			$where_e = "a.status = '$request' and d.id = $id";
		} else {
			$where_e = "a.status = '1' and d.id = $id";
		}

		$keys             = array_keys($aCari);
		@$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
		$iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
		$iDisplayLength   = intval($_REQUEST['length']);
		$iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
		$iDisplayStart    = intval($_REQUEST['start']);
		$sEcho            = intval($_REQUEST['draw']);
		$records          = array();
		$records["data"]  = array();

		$end = $iDisplayStart + $iDisplayLength;
		$end = $end > $iTotalRecords ? $iTotalRecords : $end;

		$select = 'a.id, a.jns_pajak, e.link, a.status, a.lastupdate, '.implode(',', $aCari);
		$result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
		// echo $this->db->last_query(); exit();

		$i = 1 + $iDisplayStart;
		foreach ($result as $rows) {
			$records["data"][] = array(
				$i,
				strtoupper($rows->nm_usaha),
				strtoupper($rows->alamat_usaha),
				strtoupper($rows->nm_rek_4),
				'<a href="'.base_url('pendataan/sptpd_'.$rows->link.'/show_sptpd/'.$rows->jns_pajak.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SPTPD"><i class="fa fa-folder-open"></i></a>',
			);
			$i++;
		}

		$records["draw"]            = $sEcho;
		$records["recordsTotal"]    = $iTotalRecords;
		$records["recordsFiltered"] = $iTotalRecords;

		echo json_encode($records);
    }

}

/* End of file Pendataan_pajak_sptpd.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_sptpd.php */
