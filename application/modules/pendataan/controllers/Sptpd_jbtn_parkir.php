<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sptpd_jbtn_parkir extends Admin_Controller
{
    private $prefix         = 'pendataan/sptpd_jbtn_parkir';
    private $url            = 'pendataan/sptpd_jbtn_parkir';
    private $table_db       = 'ta_kartu_pajak_parkir';
    private $path           = 'pendataan/pajak/sptpd/sptpd_parkir/';
    private $path_sptpd     = 'pendataan/pendataan_pajak_sptpd';
    private $rule_valid     = 'xss_clean|encode_php_tags';
    private $table_prefix   = '';

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_sptpd_parkir', 'mdb');
    }

    function _remap($method,$args)
    {
        if (method_exists($this, $method)){
            $this->$method($args);
        }
        else{
            $this->index($method,$args);
        }
   }

    public function index($kd_rek_4)
    {
        $data['kd_rek_4']   = $kd_rek_4;

        $data['pagetitle']  = 'Pendataan SPTPD Parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan Parkir';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4 ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'index', $data, $js, $css);
    }

    public function select($kd_rek_4)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_pajak a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'          => 'd.npwpd',
            'nama_pendaftar' => 'd.nama_pendaftar',
            'nm_usaha'       => 'b.nm_usaha',
            'alamat_usaha'   => 'b.alamat_usaha'
        ];

        $where    = null;
        $id       = $kd_rek_4[0];
        $where_e  = "a.jns_pajak = $id and status_teguran = 1";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.jns_pajak = $id and status_teguran = 1";
        } else {
            $where_e = "a.status = '1' and a.jns_pajak = $id and status_teguran = 1";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.status, c.tgl_aktif, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              '<span class="label label-m label-primary">'.strtoupper($rows->npwpd).'</span>',
              strtoupper($rows->nama_pendaftar),
              strtoupper($rows->nm_usaha),
              strtoupper($rows->alamat_usaha),
              tgl_format($rows->tgl_aktif),
               '<a href="'.base_url($this->url.'/show_sptpd/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SPTPD"><i class="fa fa-folder-open"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_sptpd($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $data['pagetitle']  = 'SPTPD parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD Parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['id']         = $id;

        $data['head']    = $this->mdb->show_npwpd($id);

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sptpd', $data, $js, $css);
    }

    public function select_sptpd($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $this->table_db = 'ta_kartu_pajak_pungut';

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_sptpd'   => 'no_sptpd',
            'tgl_sptpd'  => 'tgl_sptpd',
            'masa2'      => 'masa2',
            'keterangan' => 'keterangan'
        ];

        $where    = null;
        $where_e  = "wp_usaha_pajak_id = $id and jns_sptpd = 2";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "status = '$request' and wp_usaha_pajak_id = $id and jns_sptpd = 2";
        } else {
            $where_e = "status = '1' and wp_usaha_pajak_id = $id and jns_sptpd = 2";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, null, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id, wp_usaha_pajak_id, masa1, status_sspd, status, lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);


        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            if(($this->session->userdata('user_data')->user_role == 1)){
                $ubah_status = '<a data-original-title="Ubah status" href="'.base_url().$this->url.'/show_ubah/'.$kd_rek_4.'/'.$id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-refresh"></i></a>';
            }else{
                $ubah_status = '';
            }
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              '<span class="label label-m label-primary">'.strtoupper($rows->no_sptpd).'</span>',
              tgl_format($rows->tgl_sptpd),
              tgl_format($rows->masa1).'<br> s/d <br>'.tgl_format($rows->masa2),
              strtoupper($rows->keterangan),
                $rows->status_sspd == 1 && $this->session->user_data->user_role != 1 ?
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_sptpd_pajak/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Pajak SPTPD"><i class="fa fa-folder-open"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'
                :
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a href="'.base_url($this->url.'/show_sptpd_pajak/'.$kd_rek_4.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Pajak SPTPD"><i class="fa fa-folder-open"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$kd_rek_4.'/'.$id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_pungut/'.
                    ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                    ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                    ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                    ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_pungut/99'.
                    ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'.$ubah_status,
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];

        $data['pagetitle']  = 'SPTPD parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['records']    = $this->mdb->show_npwpd($id);
        $data['dokumen']    = $this->mdb->petugas();
        $data['bulan']      = $this->db->get('bulan')->result();

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => null, 'Pendataan SPTPD parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'pendataan/pajak/sptpd/sptpd_parkir/form', $data, $js );
    }

    public function show_edit( $ids )
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];
        $id2      = $ids[2];

        $data['pagetitle']  = 'SPTPD parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['records']    = $this->mdb->show_npwpd($id);
        $data['data']       = $this->mdb->show_pungut($id2);
        $data['bulan']      = $this->db->get('bulan')->result(); 
        $data['bulan_id']   = date('m', strtotime($data['data']->masa1));
        $data['dokumen']    = $this->mdb->petugas();

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => null, 'Pendataan SPTPD parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id.'/'.$id2];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'pendataan/pajak/sptpd/sptpd_parkir/form', $data, $js );
    }

    public function action_form($ids = NULL)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        // $id = $ids[0];
        $this->table_db = 'ta_kartu_pajak_pungut';

        $this->form_validation->set_rules('wp_usaha_pajak_id', 'wp_usaha_pajak_id', 'trim');
        $this->form_validation->set_rules('ttd_dok_id', 'ttd_dok_id', 'trim');
        $this->form_validation->set_rules('tahun', 'tahun', 'trim');
        $this->form_validation->set_rules('no_sptpd', 'no_sptpd', 'trim');
        $this->form_validation->set_rules('tgl_sptpd', 'Tgl SPTPD', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim');
        $this->form_validation->set_rules('tgl_terima', 'Tgl Terima', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $last     = date('t',strtotime(date('Y').'-'.$this->input->post('masa').'-'.date('d')));
            $firstday = date('Y').'-'.$this->input->post('masa').'-01';
            $lastday  = date('Y').'-'.$this->input->post('masa').'-'.$last;
            
            $data[$this->table_prefix.'wp_usaha_pajak_id'] = $this->input->post('wp_usaha_pajak_id');
            $data[$this->table_prefix.'jns_sptpd']         = 2;
            $data[$this->table_prefix.'ttd_dok_id']        = $this->input->post('ttd_dok_id');
            $data[$this->table_prefix.'tahun']             = $this->input->post('tahun');
            $data[$this->table_prefix.'tgl_sptpd']         = $this->m_global->setdateformat($this->input->post('tgl_sptpd'));
            $data[$this->table_prefix.'masa1']             = $firstday;
            $data[$this->table_prefix.'masa2']             = $lastday;
            $data[$this->table_prefix.'keterangan']        = $this->input->post('keterangan');
            $data[$this->table_prefix.'tgl_terima']        = $this->m_global->setdateformat($this->input->post('tgl_terima'));

            if ($ids == NULL) {
                $date     = date("d/m/Y");
                $no_sptpd = $this->mdb->no_sptpd('S', "/SPTPD/$date");

                $data[$this->table_prefix.'id']                = $this->mdb->auto_id();
                $data[$this->table_prefix.'tahun']             = date('Y');
                $data[$this->table_prefix.'no_sptpd']          = $no_sptpd;
                $result  = $this->m_global->insert( $this->table_db, $data );

                $log['id']      = $data[$this->table_prefix.'id'];
                $log['action']  = 'Tambah sptpd jabatan parkir';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $this->input->post('npwpd').' jns_sptpd = 2, input user = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $result2        = $this->db->insert('sptpd_log', $log);
            }
            else{
                $id = $ids[0];
                $data[$this->table_prefix.'no_sptpd']          = $this->input->post('no_sptpd');
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

                $log['id']      = $ids[0];
                $log['action']  = 'Ubah sptpd jabatan parkir';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $this->input->post('npwpd').' jns_sptpd = 2, input user = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $result2        = $this->db->insert('sptpd_log', $log);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    //ubah status -> admin
    public function show_ubah($ids)
    {
        $kd_rek_4 = $ids[0];
        $id       = $ids[1];
        $id2      = $ids[2];

        $data['pagetitle']  = 'SPTPD Parkir';
        $data['subtitle']   = 'manage SPTPD Parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $data['kd_rek_4']   = $kd_rek_4;
        $data['id2']        = $id2;

        $data['label']      = $this->db->query("select status_label, status_tipe, status_detail, status_group from status WHERE status_group ='ta_kartu_pajak_pungut' GROUP BY status_label")->result_array();

        foreach($data['label'] as $val){

            $data['isi'][$val['status_tipe']]= $this->db->query("SELECT status_detail, status_id, status_label FROM status where status_tipe in ('".$val['status_tipe']."') and status_group = '$val[status_group]'")->result();

        }

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD Parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id.'/'.$id2];
        $js['js']           = [ 'form-validation' ];

        $this->template->display('pendataan/pajak/sptpd/form_ubah', $data, $js);
    }

    //action status -> admin
    public function action_ubah($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'ta_kartu_pajak_pungut';

        $this->form_validation->set_rules('id', 'ID', 'trim');

        if ($this->form_validation->run($this)) {
            $detail = 'Ubah Status : ';
            foreach($this->input->post('combo') as $key => $val){
                $data[$key] = $val;
                $detail = $detail.$key.' = '.$val.', ';
            }

            $query = $this->db->query("select * from wp_data_umum where id = $ids[0]")->row();
            
            $log['id']      = $ids[0];
            $log['action']  = 'Ubah Status';
            $log['detail']  = $query['npwpd'].$detail;
            $log['status']  = '1';
            $log['user_id'] = $this->session->user_data->user_id;
            $log['ip']      = $_SERVER['REMOTE_ADDR'];
            $result2        = $this->db->insert('sptpd_log', $log);

            $result = $this->m_global->update('ta_kartu_pajak_pungut', $data, ['id' => $ids[0]]);


            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function show_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $data['pagetitle']  = 'SPTPD Pajak parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan pajak parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD Parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak Parkir' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;
        $data['head']       = $this->mdb->show_npwpd($wp);
        $data['status_sspd'] = $this->db->select('status_sspd')->where('id', $id)->get('ta_kartu_pajak_pungut')->row();

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sptpd_pajak', $data, $js, $css);
    }

    public function select_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $this->table_db = 'ta_kartu_pajak_parkir';

        $join = [
                    'ref_rek_6' => ['ref_rek_6', 'ta_kartu_pajak_parkir.id_rek_6 = ref_rek_6.id_rek_6', 'LEFT'],
                    'ta_kartu_pajak_pungut' => ['ta_kartu_pajak_pungut', 'ta_kartu_pajak_parkir.pungut_id = ta_kartu_pajak_pungut.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
             'nm_rek_6'        => 'ref_rek_6.nm_rek_6',
             'dasar_pengenaan' => 'ta_kartu_pajak_parkir.dasar_pengenaan',
             'tarif_pajak'     => 'ta_kartu_pajak_parkir.tarif_pajak',
             'pajak_terhutang' => 'ta_kartu_pajak_parkir.pajak_terhutang'
         ];

        $where    = null;
        $where_e  = "ta_kartu_pajak_parkir.pungut_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "ta_kartu_pajak_parkir.status = '$request' and ta_kartu_pajak_parkir.pungut_id = $id";
        } else {
            $where_e = "ta_kartu_pajak_parkir.status = '1' and ta_kartu_pajak_parkir.pungut_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ta_kartu_pajak_pungut.status_sspd, ref_rek_6.kd_rek_1, ref_rek_6.kd_rek_2, ref_rek_6.kd_rek_3, ref_rek_6.kd_rek_4, ref_rek_6.kd_rek_5, ref_rek_6.kd_rek_6, ta_kartu_pajak_parkir.id, ta_kartu_pajak_parkir.status, ta_kartu_pajak_parkir.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                 '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
               $i,
               '<span class="label label-m label-primary">'.$rows->kd_rek_1.' . '.$rows->kd_rek_2.' . '.$rows->kd_rek_3.' . '.$rows->kd_rek_4.' . '.$rows->kd_rek_5.' . '.$rows->kd_rek_6.'</span>',
               strtoupper($rows->nm_rek_6),
               uang($rows->dasar_pengenaan),
               $rows->tarif_pajak.'%',
               uang($rows->pajak_terhutang),
                // '<a href="'.base_url($this->url.'/show_rinci/'.$kd_rek_4.'/'.$wp.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Kamar parkir"><i class="fa fa-folder-open"></i></a>'.
                '<a href="'.base_url($this->url.'/show_edit_sptpd_pajak/'.$kd_rek_4.'/'.$wp.'/'.$id.'/'.$rows->id).'" class="btn btn-icon-only blue tooltips" data-original-title="Edit Pajak SPTPD"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_parkir/'.
                                        ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_kartu_pajak_parkir/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
             );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];

        $data['pagetitle']  = 'SPTPD parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;
        $data['records']    = $this->mdb->show_npwpd($wp);

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD Parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak Parkir' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_sptpd_pajak', $data, $js);
    }

    public function show_edit_sptpd_pajak($ids)
    {
        $kd_rek_4 = $ids[0];
        $wp       = $ids[1];
        $id       = $ids[2];
        $id2      = $ids[3];

        $data['pagetitle']  = 'SPTPD parkir';
        $data['subtitle']   = 'manage SPTPD Jabatan parkir';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['wp']         = $wp;
        $data['id']         = $id;
        $data['records']    = $this->mdb->show_npwpd($wp);
        $data['data']       = $this->mdb->show_sptpd_pajak($id2);

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_sptpd, 'Pendataan SPTPD Parkir' => $this->url.'/'.$kd_rek_4, 'SPTPD Parkir' => $this->url.'/show_sptpd'.'/'.$kd_rek_4.'/'.$wp, 'SPTPD Pajak Parkir' => $this->url.'/show_sptpd_pajak'.'/'.$kd_rek_4.'/'.$wp.'/'.$id, 'Form' => $this->url.'/show_add'.'/'.$kd_rek_4.'/'.$id];
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_sptpd_pajak', $data, $js);
    }

    public function action_form_sptpd_pajak($ids = null)
    {
        $this->table_db = 'ta_kartu_pajak_parkir';

        $this->form_validation->set_rules('id_rek_6', 'Obyek Pajak', 'trim|required');
        // $this->form_validation->set_rules('dasar_pengenaan', 'Dasar Pengenaan', 'trim|required');
        $this->form_validation->set_rules('tarif_pajak', 'Tarif Pajak', 'trim|required');
        $this->form_validation->set_rules('pembulatan', 'Pembulatan', 'trim|required');

        if ($this->form_validation->run($this)) {
            // perhitungan pajak
            $tarif_parkir  = str_replace(['Rp', ',', ' '], '', $this->input->post('tarif_parkir'));
            $tarif_pajak   = $this->input->post('tarif_pajak');
            $jml_kendaraan = $this->input->post('jml_kendaraan');
            $pembulatan    = $this->input->post('pembulatan');
            $pengenaan     = $tarif_parkir * $jml_kendaraan;

            $hutang        = $pengenaan * $tarif_pajak / 100;
            $total         = $this->pembulatan($hutang, $pembulatan);

            $data[$this->table_prefix.'pajak_terhutang'] = str_replace(['Rp', ',', ' '], '', $total);
            $data[$this->table_prefix.'pungut_id']       = $this->input->post('pungut_id');
            $data[$this->table_prefix.'tarif_parkir']    = str_replace(['Rp', ',', ' '], '', $this->input->post('tarif_parkir'));
            $data[$this->table_prefix.'jml_kendaraan']   = $this->input->post('jml_kendaraan');
            $data[$this->table_prefix.'id_rek_6']        = $this->input->post('id_rek_6');
            $data[$this->table_prefix.'dasar_pengenaan'] = str_replace(['Rp', ',', ' '], '', $pengenaan);
            $data[$this->table_prefix.'tarif_pajak']     = $this->input->post('tarif_pajak');
            $data[$this->table_prefix.'keterangan']      = $this->input->post('keterangan');
            $data[$this->table_prefix.'pembulatan']      = $this->input->post('pembulatan');

            if ($ids == null) {
                $data[$this->table_prefix.'tahun']       = date('Y');
                $result  = $this->m_global->insert($this->table_db, $data);
                $log['id'] = $this->db->insert_id();
                $log['action']  = 'Tambah sptpd jbtn parkir form rinci';

                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $this->input->post('npwpd').' jns_sptpd = 1, input user = '.$detail;

                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $result2        = $this->db->insert('sptpd_log', $log);
            } else {
                $id     = $ids[0];
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
                $log['id']      = $ids[0];
                $log['action']  = 'Ubah sptpd jbtn parkir form rinci';

                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = $this->input->post('npwpd').' jns_sptpd = 1, input user = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $result2        = $this->db->insert('sptpd_log', $log);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function select_rek_6()
    {
        $this->table_db = 'ref_rek_6';

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => $this->table_prefix.'nm_rek_6',
        ];

        $where      = NULL;
        $where_e    = 'kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 1 and kd_rek_4 = 7';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status =']    = '1';
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6, status, lastupdate,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $rows->kd_rek_1.' . '.$rows->kd_rek_2.' . '.$rows->kd_rek_3.' . '.$rows->kd_rek_4.' . '.$rows->kd_rek_5.' . '.$rows->kd_rek_6,
                strtoupper($rows->nm_rek_6),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i>'.
                '</a>',
            );
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function get_rek_6()
    {
        $this->table_db  = 'ref_rek_6';
        $id              = $this->input->post('id');

        $data['records'] = $this->m_global->get($this->table_db, null, ['id_rek_6' => $id] )[0];
        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function pembulatan($hutang, $pembulatan)
    {
        $ratusan = substr($hutang, -3);

        if ($pembulatan == 100) {
            return $akhir = $hutang + (100-$ratusan);
        }
        else if ($pembulatan == 1000) {
            return $akhir = $hutang + (1000-$ratusan);
        }
        else {
            return $hutang;
        }
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($ids)
    {
        // echo "<pre>",print_r($id),exit();
        $id     = $ids[0];
        $table  = $ids[1];
        $status = $ids[2];
        $stat   = $ids[3];

        if ($stat == 'true') {
            // update sptpd
            $pungut               = $this->db->query("SELECT pungut_id from ta_sspd  where id = $id")->row();
            $id_pungut            = $pungut->pungut_id;
            $data2['status_sspd'] = 0;
            $result2              = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);

            $result               = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result               = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function print_pdf($ids){

    $kd_rek_4 = $ids[0];
    $id       = $ids[1];
    $id2      = $ids[2];



    $this->load->library('Pdf');
    $pdf = $this->pdf->load();

    $data['npwpd'] = $this->mdb->show_data_1($id);
    if ($data['npwpd'] == null) {
        echo "<script type='text/javascript'>alert('Silahkan isi dahulu rincian SPTPD');</script>"; exit();
    }

    $data['sptpd'] = $this->mdb->show_sptpd($id);
    $data['pajak'] = $this->mdb->show_pajak($id2);

    $data['pajak2'] = $this->mdb->show_pajak2($id2);
    $data['jumlah'] = $this->mdb->jumlah($id2);

    $data['petugas'] = $this->mdb->petugas($id2);

    $kota = $this->mdb->kota();

    $this->load->library('fpdf_gen');
    $pdf = new fpdf('P','mm','A4');

    $pdf->AddPage();
    $pdf->SetAutoPageBreak(true, 0);

    $w = 0;
    $image = base_url('./assets/img/'.$data['npwpd']->logo);

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Image($image,10,10,20,20);

    $pdf->Cell(25,7,'', 'LTR', 0, 'C');
    $pdf->Cell(115,7,'PEMERINTAH KOTA PASURUAN', 'LTR', 0, 'C');

    $pdf->Cell(50,7,$data['sptpd']->no_sptpd, 1, 1, 'C');

    $pdf->Cell(25,7,'', 'LR', 0, 'C');
    $pdf->Cell(115,7,'BADAN PENDAPATAN DAERAH', 'LR', 0, 'C');

    $pdf->SetFont('Times', '', 10);
    $pdf->Cell(50,7,'Masa : '.$data['npwpd']->masa1. 's.d ' .$data['npwpd']->masa2, 'R', 1, 'L');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(25,7,'', 'LBR', 0, 'C');
    $pdf->Cell(115,7,'ALAMAT :'.$kota->ibukota, 'LRB', 0, 'C');

    $pdf->SetFont('Times', '', 10);
    $pdf->Cell(50,7,'Tahun : '.$data['npwpd']->tahun, 'BR', 1, 'L');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(190,10,'SPTPD', 'LTR', 1, 'C');
    $pdf->Cell(190,5,'(Surat Pemberitahuan Pajak Daerah)', 'LR', 1, 'C');
    $pdf->Cell(190,10,'Pajak Parkir', 'LBR', 1, 'C');

    $pdf->SetFont('Times', '', 11);
    $pdf->Cell(150,7,'Kepada : Yth.', 'LT', 0, 'R');
    $pdf->Cell(40,7,'', 'TR', 1, 'C');
    $pdf->Cell(150,7,'Kabban', 'L', 0, 'R');
    $pdf->Cell(40,7,'', 'R', 1, 'R');
    $pdf->Cell(150,7,'Di - PASURUAN', 'LB', 0, 'R');
    $pdf->Cell(40,7,'', 'RB', 1, 'R');

    $pdf->Cell(30,7,'NPWPD', 'LT', 0, 'c');
    $pdf->Cell(160,7,':'.$data['npwpd']->npwpd, 'TR', 1, 'c');
    $pdf->Cell(30,7,'Nama Usaha', 'L', 0, 'c');
    $pdf->Cell(160,7,':'.$data['npwpd']->nm_usaha, 'R', 1, 'c');
    $pdf->Cell(30,7,'Keterangan', 'BL', 0, 'c');
    $pdf->Cell(160,7,':'.$data['npwpd']->keterangan, 'RB', 1, 'c');

    $pdf->Cell(190,10,'PERHATIAN :', 'LTR', 1, 'c');

    $pdf->Cell(190,5,'1. Harap diisi dalam rangkap 2 (dua) dan ditulis dengan huruf CETAK', 'LR', 1, 'c');
    $pdf->Cell(190,5,'2. Beri Nomor pada Kotak yang tersedia untuk jawaban yang diberikan', 'LR', 1, 'c');
    $pdf->Cell(107,5,'3. Setelah diisi dan ditandatangani harap diserahkan kembali kepada ', 'L', 0, 'c');
    $pdf->SetFont('Times', 'B', 10);
    $pdf->Cell(83,5,' BADAN PENDAPATAN DAERAH, ', 'R', 1, 'c');
    $pdf->SetFont('Times', '', 11);
    $pdf->Cell(190,5,'    paling lambat tanggal 15 bulan berikutnya.', 'LR', 1, 'c');
    $pdf->Cell(190,5,'4. Keterlambatan penyerahan dari tanggal tersebut di atas akan dilakukan Penerbitan Surat Teguran', 'LR', 1, 'c');

    $pdf->Cell(190,2,'', 'LRB', 1, 'c');

    $pdf->Cell(190,10,'A. DIISI OLEH WAJIB PAJAK / PENANGUNG PAJAK', 1, 1, 'L');

    $pdf->Cell(70,10,'Data Objek Pajak', 'L', 0, 'c');
    $pdf->Cell(5,10,'', 0, 0, 'c');
    $pdf->Cell(115,10,'', 'R', 1, 'c');

    $pdf->Cell(5,7,'', 'L', 0, 'c');
    $pdf->Cell(15,7,'No', 1, 0, 'C');
    $pdf->Cell(43,7,'Rekening', 1, 0, 'C');
    $pdf->Cell(42,7,'Keterangan', 1, 0, 'C');
    $pdf->Cell(40,7,'Jml Kendaraan', 1, 0, 'C');
    $pdf->Cell(40,7,'Tarif Parkir', 1, 0, 'C');
    $pdf->Cell(5,7,'', 'R', 1, 'C');

    $pdf->Cell(5,7,'', 'L', 0, 'c');
    $pdf->Cell(15,7,'1', 1, 0, 'R');
    $pdf->Cell(43,7,$data['pajak']->kd_rek_1.'.'.$data['pajak']->kd_rek_2.'.'.$data['pajak']->kd_rek_3.'.'.$data['pajak']->kd_rek_4.'.'.$data['pajak']->kd_rek_5.'.'.$data['pajak']->kd_rek_6, 1, 0, 'C');
    $pdf->Cell(42,7,$data['npwpd']->keterangan, 1, 0, 'L');
    $pdf->Cell(40,7,$data['pajak']->jml_kendaraan, 1, 0, 'R');
    $pdf->Cell(40,7,$data['pajak']->tarif_parkir, 1, 0, 'R');
    $pdf->Cell(5,7,'', 'R', 1, 'C');

    $pdf->Cell(190,7,'', 'LRB', 1, 'c');

    $pdf->Cell(190,10,'B. DIISI OLEH WAJIB PAJAK / PENANGGUNG PAJAK SELF ASSESMENT', 1, 1, 'L');

    $pdf->Cell(5,10,'', 'LT', 0, 'L');
    $pdf->Cell(180,10,'Jumlah Pembayaran dan pajak terhutang untuk masa pajak sekarang (lampiran foto copy dokumen)', 'T', 0, 'C');
    $pdf->Cell(5,10,'', 'TR', 1, 'L');

    $pdf->Cell(5,7,'', 'L', 0, 'c');
    $pdf->Cell(15,7,'No', 1, 0, 'C');
    $pdf->Cell(50,7,'Rekening', 1, 0, 'C');
    $pdf->Cell(50,7,'Dasar Pengenaan & Tarif', 1, 0, 'C');
    $pdf->Cell(32,7,'Pajak Terhutang', 1, 0, 'C');
    $pdf->Cell(33,7,'Jumlah Dibayar', 1, 0, 'C');
    $pdf->Cell(5,7,'', 'R', 1, 'c');

        $no = 1;
        foreach ($data['pajak2'] as $value) {
        $rekening =
        $pdf->Cell(5,7,'', 'L', 0, 'c');
        $pdf->Cell(15,14,$no++, LTR, 0, 'C');
        $pdf->Cell(50,7,$value->kd_rek_1.'.'.$value->kd_rek_2.'.'.$value->kd_rek_3.'.'.$value->kd_rek_4.'.'.$value->kd_rek_5.'.'.$value->kd_rek_6, 'LTR', 0, 'C');
        $pdf->Cell(50,7,$value->dasar_pengenaan.' / '.$value->tarif_pajak, 1, 0, 'C');
        $pdf->Cell(32,7,$value->pajak_terhutang, 1, 0, 'C');
        $pdf->Cell(33,7,$value->pajak_terhutang, 1, 0, 'C');
        $pdf->Cell(5,7,'', 'R', 1, 'c');

        $pdf->Cell(5,7,'', 'L', 0, 'c');
        $pdf->Cell(15,7,'', LBR, 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(50,7,$value->nm_rek_6, 'LBR', 0, 'C');

        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(50,7,'', 'LTB', 0, 'C');
        $pdf->Cell(32,7,'', 'TB', 0, 'C');
        $pdf->Cell(33,7,'', 'TBR', 0, 'C');
        $pdf->Cell(5,7,'', 'R', 1, 'c');

      }

    $pdf->Cell(5,7,'', 'L', 0, 'c');
    $pdf->Cell(15,7,'', 0, 0, 'C');
    $pdf->Cell(50,7,'', 0, 0, 'C');
    $pdf->Cell(50,7,'', 0, 0, 'C');
    $pdf->Cell(32,7,'Jumlah :', 'LB', 0, 'C');
    $pdf->Cell(33,7,$data['jumlah']->jumlah, 'RB', 0, 'C');
    $pdf->Cell(5,7,'', 'R', 1, 'c');


    $pdf->Cell(190,5,'', 'LRB', 1, 'c');

    //FORM 2
    $pdf->AddPage();
    $pdf->Cell(190,10,'C. PERNYATAAN', 1, 1, 'L');

    $pdf->Cell(190,2,'', 'LR', 1, 'c');
    $pdf->Cell(190,5,'   Dengan menyadari sepenuhnya akan akibat termasuk sanksi-sanksi sesuai dengan ketentuan perundang-undangan ', 'LR', 1, 'L');
    $pdf->Cell(190,5,'   yang berlaku, saya atau yang saya berkuasa menyatakan apa yang telah kami beritahukan tersebut diatas beserta', 'LR', 1, 'L');
    $pdf->Cell(190,5,'   lampiran-lampirannya adalah benar, lengkap dan jelas ', 'LR', 1, 'L');

    $pdf->Cell(190,3,'', 'LR', 1, 'c');
    $pdf->Cell(100,7,'', 'L', 0, 'c');
    $pdf->Cell(70,7,$kota->ibukota.', '.tgl_format(date("Y-m-d")), 0, 0, 'C');
    $pdf->Cell(20,7,'', 'R', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(100,7,'', 'L', 0, 'c');
    $pdf->Cell(70,4,$data['npwpd']->nama_pendaftar, 0, 0, 'C');
    $pdf->Cell(20,7,'', 'R', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(100,20,'', 'L', 0, 'c');
    $pdf->Cell(70,20,'', 'B', 0, 'C');
    $pdf->Cell(20,20,'', 'R', 1, 'C');

    $pdf->SetFont('Times', '', 11);
    $pdf->Cell(100,6,'', 'L', 0, 'c');
    $pdf->Cell(70,6,'Nama Jelas', 0, 0, 'C');
    $pdf->Cell(20,6,'', 'R', 1, 'C');

    $pdf->Cell(190,2,'', 'LRB', 1, 'C');

    $pdf->Cell(190,10,'D. DIISI OLEH PETUGAS PENERIMA', 1, 1, 'L');

    $pdf->Cell(190,2,'', 'LTR', 1, 'C');
    $pdf->Cell(40,7,'   Diterima Tanggal', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.tgl_format(date("Y-m-d")), 'R', 1, 'c');
    $pdf->Cell(40,7,'   Nama Petugas', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.$data['npwpd']->nm_penandatangan, 'R', 1, 'c');
    $pdf->Cell(40,7,'   NIP', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.$data['npwpd']->nip_penandatangan, 'R', 1, 'c');

    $pdf->Cell(190,15,'', 'LR', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(100,20,'', 'LB', 0, 'c');
    $pdf->Cell(70,20,'(.......................................................)', '', 0, 'C');
    $pdf->Cell(20,20,'', 'BR', 1, 'C');
    $pdf->Cell(60,10,'.........................................................', 'LB', 0, 'C');
    $pdf->Cell(70,10,'Gunting Di sini', 'TB', 0, 'C');
    $pdf->Cell(60,10,'.........................................................', 'TRB', 1, 'C');

    $pdf->Cell(190,3,'', 'LR', 1, 'c');
    $pdf->Cell(90,7,'', 'L', 0, 'c');
    $pdf->Cell(35,7,'No. Formulir', 0, 0, 'C');
    $pdf->Cell(55,7,': '.$data['sptpd']->no_sptpd, 0, 0, 'L');
    $pdf->Cell(10,7,'', 'R', 1, 'C');

    $pdf->SetFont('Times', 'B', 14);
    $pdf->Cell(190,20,'TANDA TERIMA', 'LR', 1, 'C');
    $pdf->Cell(190,2,'', 'LR', 1, 'C');

    $pdf->SetFont('Times', '', 11);
    $pdf->Cell(40,7,'   Nama', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.$data['npwpd']->nama_pendaftar, 'R', 1, 'c');
    $pdf->Cell(40,7,'   Alamat', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.$data['npwpd']->jalan.' RT/RW. '.$data['npwpd']->rtrw.' Kab/Kota. '.$data['npwpd']->kabupaten.', Kode Pos '.$data['npwpd']->kode_pos, 'R', 1, 'c');
    $pdf->Cell(40,7,'   Nama Usaha', 'L', 0, 'c');
    $pdf->Cell(150,7,': '.$data['npwpd']->nm_usaha, 'R', 1, 'c');

    $pdf->Cell(190,15,'', 'LR', 1, 'C');

    $pdf->Cell(190,3,'', 'LR', 1, 'c');
    $pdf->Cell(100,7,'', 'L', 0, 'c');
    $pdf->Cell(70,7,$kota->ibukota.', '.tgl_format(date("Y-m-d")), 0, 0, 'C');
    $pdf->Cell(20,7,'', 'R', 1, 'C');

    $pdf->Cell(100,7,'', 'L', 0, 'c');
    $pdf->Cell(70,4,'Yang Menerima', 0, 0, 'C');
    $pdf->Cell(20,7,'', 'R', 1, 'C');

    $pdf->SetFont('Times', 'B', 11);
    $pdf->Cell(100,20,'', 'L', 0, 'c');
    $pdf->Cell(70,20,'(.......................................................)', '', 0, 'C');
    $pdf->Cell(20,20,'', 'R', 1, 'C');

    $pdf->Cell(100,2,'', 'LB', 0, 'c');
    $pdf->Cell(70,2,'', 'B', 0, 'C');
    $pdf->Cell(20,2,'', 'BR', 1, 'C');



    $pdf->Output("SPTPD.pdf","I");
  }

}
