<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendataan_retribusi_wr extends Admin_Controller {

	private $prefix       = 'pendataan/pendataan_retribusi_wr';
    private $url          = 'pendataan/pendataan_retribusi_wr';
	private $table_db     = 'wr_wajib_retribusi';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
		$data['pagetitle']  = 'Wajib Retribusi';
        $data['subtitle']   = 'manage wajib Retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'Retribusi' => null, 'Wajib Retribusi' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/index', $data, $js, $css );

	}

	public function select()
    {
        $this->table_db = 'wr_wajib_retribusi';

        $join = [
                    'wr_data_umum_join' => ['wr_data_umum', 'wr_wajib_retribusi.data_umum_id = wr_data_umum.id', 'LEFT'],
                ];

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'     => 'wr_data_umum.npwpd',
            'nama_wr'   => 'wr_data_umum.nama_pendaftar',
            'tgl_aktif' => 'wr_wajib_retribusi.tgl_aktif'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, $join, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wr_wajib_retribusi.id, wr_wajib_retribusi.status, '.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              $rows->npwpd,
              $rows->nama_pendaftar,
              tgl_format($rows->tgl_aktif),
               '<a href="'.base_url($this->url.'/show_jenisUsaha/'.$rows->id).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="Lihat Jenis Usaha"><i class="fa fa-check"></i></a>'.
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a data-original-title="Hapus" href="'.base_url().$this->url.'/delete/'.$rows->id.'" class=" btn red btn-icon-only tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add()
    {
        $data['pagetitle']  = 'Wajib retribusi';
        $data['subtitle']   = 'manage wajib retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Form' => $this->url.'/show_add'];

        $data['np']         = $this->db->query("select id from wr_wajib_retribusi")->num_rows();

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];


        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_wr', $data, $js, $css );
    }

    public function show_edit($id)
    {
        $data['pagetitle']  = 'Wajib retribusi';
        $data['subtitle']   = 'manage wajib retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Form' => $this->url.'/show_edit'.'/'.$id];

        $data['data']       = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $id")->row();
        $data['id']         = $id;

        $css['css']         = null;
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_wr', $data, $js, $css );
    }

    public function action_add_wajib_retribusi($id = NULL)
    {
        // // echo '<pre>', print_r($this->input->post()), exit();
        // $this->form_validation->set_rules('npwpd',          'Nomer NPWPD',                   'trim|required');
        // $this->form_validation->set_rules('no_daftar',      'Nomer Daftar',                  'trim|required');
        // $this->form_validation->set_rules('nama_wr',        'Nama Wajib retribusi',              'trim|required');
        $this->form_validation->set_rules('tgl_aktif',      'Tanggal Aktif',                 'trim|required');
        $this->form_validation->set_rules('status_wr',      'Status WR',                     'trim');
        $this->form_validation->set_rules('keterangan',     'Keterangan',                    'trim');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'data_umum_id'] = $this->input->post('data_umum_id');
            $data[$this->table_prefix.'tgl_aktif']    = $this->input->post('tgl_aktif');
            $data[$this->table_prefix.'status_wr']    = $this->input->post('status_wr');
            $data[$this->table_prefix.'keterangan']   = $this->input->post('keterangan');

            if ($id == NULL) {
                $data2[$this->table_prefix.'status_retribusi'] = 1;
                $result1 = $this->m_global->update('wr_data_umum', $data2, ['id' => $this->input->post('data_umum_id')]);

                $result  = $this->m_global->insert( $this->table_db, $data );
            }
            else{
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ( $result ){
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nama_wr').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nama_wr').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo $this->db->last_query();
                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function delete( $id )
    {
        $this->table_db = 'wr_wajib_retribusi';

        $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );

        if ( $result )
        {
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function show_jenisUsaha($id)
    {
        $data['pagetitle']  = 'Jenis Usaha';
        $data['subtitle']   = 'manage jenis usaha';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$id];
        $data['id']         = $id;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/jenis_usaha', $data, $js, $css );
    }

    public function select_jenisUsaha($id)
    {
        $this->table_db = 'wr_wajib_retribusi_usaha';

        $join = [
                    'wr_wajib_retribusi_join' => ['wr_wajib_retribusi', 'wr_wajib_retribusi_usaha.wr_id = wr_wajib_retribusi.id', 'LEFT'],
                    'wr_data_umum_join' => ['wr_data_umum', 'wr_wajib_retribusi.data_umum_id = wr_data_umum.id', 'LEFT'],
                    'ref_rek_5_join' => ['ref_rek_5', 'wr_wajib_retribusi_usaha.jns_usaha = ref_rek_5.id_rek_5', 'LEFT'],
                    'ref_rek_6_join' => ['ref_rek_6', 'wr_wajib_retribusi_usaha.klasifikasi_usaha = ref_rek_6.id_rek_6', 'LEFT'],
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_usaha'     => 'wr_wajib_retribusi_usaha.nm_usaha',
            'nm_jns_usaha' => 'ref_rek_5.nm_rek_5',
            'nm_rek_6'     => 'ref_rek_6.nm_rek_6',
            'alamat_usaha' => 'wr_wajib_retribusi_usaha.alamat_usaha'
        ];

        $where      = [ $this->table_db.'.wr_id' => $id] ;

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, $join, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wr_data_umum.npwpd,nm_usaha, wr_wajib_retribusi_usaha.id, wr_wajib_retribusi_usaha.wr_id, '.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              $rows->npwpd,
              $rows->nm_usaha,
              $rows->nm_rek_5,
              $rows->nm_rek_6,
              $rows->alamat_usaha,
              '<a href="'.base_url($this->url.'/show_suratIjin/'.$rows->id.'/'.$rows->wr_id).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="Lihat Surat Izin Yang Dimiliki">
                   <i class="fa fa-refresh"></i></a>'.
              '<a href="'.base_url($this->url.'/show_jenisretribusi/'.$rows->id.'/'.$rows->wr_id).'" class="btn btn-icon-only green-seagreen tooltips" data-original-title="Lihat Jenis retribusi">
                   <i class="fa fa-check"></i></a>'.
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_usaha/'.$rows->id.'/'.$rows->wr_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a data-original-title="Hapus" href="'.base_url().$this->url.'/delete_jenis_usaha/'.$rows->id.'" class=" btn red btn-icon-only tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        //echo $this->db->last_query();

        echo json_encode( $records );
    }

    public function show_add_jenis_usaha($id)
    {
        $this->table_db            = 'wr_wajib_retribusi';

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['prefix']            = $this->prefix;

        $data['breadcrumb']        = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$id, 'Form' => $this->url.'/show_add_jenis_usaha'.'/'.$id];

        $data['wr_id']             = $id;
        $data['records']           = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $id")->row();
        // $data['kec']               = $this->db->query("select kd_kec, nm_kec from ref_kecamatan where status = '1'")->result();
        $data['jns_usaha']         = $this->db->query("select * from ref_rek_5 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();
        // $data['klasifikasi_usaha'] = $this->db->query("select * from ref_rek_6 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_jenis_usaha', $data, $js, $css);
    }

    public function show_edit_jenis_usaha($id, $wr_id)
    {
        $this->table_db            = 'wr_wajib_retribusi';

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['prefix']            = $this->prefix;

        $data['breadcrumb']        = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Form' => $this->url.'/show_edit_jenis_usaha'.'/'.$id.'/'.$wr_id];

        $data['wr_id']             = $wr_id;
        $data['data']              = $this->db->query("select * from wr_wajib_retribusi_usaha where id = $id")->row();
        $data['records']           = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $wr_id")->row();
        // $data['kec']               = $this->db->query("select kd_kec, nm_kec from ref_kecamatan where status = '1'")->result();
        $data['jns_usaha']         = $this->db->query("select * from ref_rek_5 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();
        // $data['klasifikasi_usaha'] = $this->db->query("select * from ref_rek_6 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_jenis_usaha', $data, $js, $css);
    }

    public function action_jenis_usaha($id = NULL)
    {
        // print_r($this->input->post()); exit();
        $this->form_validation->set_rules('jns_usaha',              'Jenis Usaha',                  'trim|required');
        $this->form_validation->set_rules('klasifikasi_usaha',      'Klasifikasi Usaha',            'trim|required');
        $this->form_validation->set_rules('nm_usaha',               'Nama Usaha',                   'trim|required');
        $this->form_validation->set_rules('alamat_usaha',           'Alamat',                       'trim|required');
        // $this->form_validation->set_rules('kd_kec',                 'Kecamatan',                    'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'wr_id']             = $this->input->post('wr_id');
            $data[$this->table_prefix.'jns_usaha']         = $this->input->post('jns_usaha');
            $data[$this->table_prefix.'klasifikasi_usaha'] = $this->input->post('klasifikasi_usaha');
            $data[$this->table_prefix.'npwp_usaha']        = $this->input->post('npwp_usaha');
            $data[$this->table_prefix.'nm_usaha']          = $this->input->post('nm_usaha');
            $data[$this->table_prefix.'alamat_usaha']      = $this->input->post('alamat_usaha');
            // $data[$this->table_prefix.'kd_kec']            = $this->input->post('kd_kec');
            // $data[$this->table_prefix.'kd_kel']            = $this->input->post('kd_kel');

            if ($id == NULL) {
                $result  = $this->m_global->insert( 'wr_wajib_retribusi_usaha', $data );
            }
            else{
                $result = $this->m_global->update( 'wr_wajib_retribusi_usaha', $data, ['id' => $id]);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';

                echo json_encode( $data );

            } else {

                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );

            }

        } else {

            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );

        }
    }

    public function delete_jenis_usaha( $id )
    {
        $this->table_db = 'wr_wajib_retribusi_usaha';

        $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );

        if ( $result )
        {
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function get_kelurahan()
    {
        $table_kel          = 'ref_kelurahan';

        $id = $this->input->post();
        $data['kelurahan']  = $this->m_global->get($table_kel, null, $id, 'kel_id, kd_kec, kd_kel, nm_kel');

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function get_klasifikasi()
    {
        $kd_rek_1 = $this->input->post('kd_rek_1');
        $kd_rek_2 = $this->input->post('kd_rek_2');
        $kd_rek_3 = $this->input->post('kd_rek_3');
        $kd_rek_4 = $this->input->post('kd_rek_4');
        $kd_rek_5 = $this->input->post('kd_rek_5');

        $data['klasifikasi']  = $this->db->query("select * from ref_rek_6 where kd_rek_1 = $kd_rek_1 and kd_rek_2 = $kd_rek_2 and kd_rek_3 = $kd_rek_3 and kd_rek_4 = $kd_rek_4 and kd_rek_5 = $kd_rek_5" )->result();

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function get_pemungutan()
    {
        $table_kel          = 'ref_pemungutan';

        $id = $this->input->post();
        $data['pemungutan']  = $this->m_global->get($table_kel, null, $id);

        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function show_suratIjin($id, $wr_id)
    {
        $this->table_ijin    = 'wr_wajib_retribusi_izin';

        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['prefix']      = $this->prefix;

        $data['id']          = $id;
        $data['wr_id']       = $wr_id;
        $wpu                 = $this->db->query("select wr_id from wr_wajib_retribusi_usaha where id = $id")->row();
        $data['breadcrumb']  = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wpu->wr_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id];

        $js['js']            = [ 'table-datatables-ajax' ];
        $css['css']          = null;

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/surat_ijin', $data, $js, $css );
    }

    public function select_suratIjin($id)
    {
        // select table urusan
        $this->table_db = 'wr_wajib_retribusi_izin';

		$join = [
                    'wr_wajib_retribusi_usaha_join' => ['wr_wajib_retribusi_usaha', 'wr_wajib_retribusi_izin.wr_usaha_id = wr_wajib_retribusi_usaha.id', 'LEFT'],
                    'wr_wajib_retribusi_join' => ['wr_wajib_retribusi', 'wr_wajib_retribusi_usaha.wr_id = wr_wajib_retribusi.id', 'LEFT'],
                    'wr_data_umum_join' => ['wr_data_umum', 'wr_wajib_retribusi.data_umum_id = wr_data_umum.id', 'LEFT'],
                ];

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'    => 'wr_data_umum.npwpd',
            'nm_usaha' => 'wr_wajib_retribusi_usaha.nm_usaha',
            'no_izin'  => 'wr_wajib_retribusi_izin.no_izin',
            'nm_izin'  => 'wr_wajib_retribusi_izin.nm_izin',
            'tgl_izin' => 'wr_wajib_retribusi_izin.tgl_izin'
        ];

        $where      = NULL;
        $where_e    = "wr_usaha_id = $id";

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where['wr_wajib_retribusi_izin.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['wr_wajib_retribusi_izin.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wr_wajib_retribusi_usaha.wr_id, wr_wajib_retribusi_izin.id, wr_usaha_id, wr_wajib_retribusi_izin.wr_usaha_id, wr_wajib_retribusi_izin.status, wr_wajib_retribusi_izin.lastupdate, '.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              $rows->npwpd,
              $rows->nm_usaha,
              $rows->no_izin,
              $rows->nm_izin,
              tgl_format($rows->tgl_izin),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_surat_izin/'.$rows->id.'/'.$rows->wr_usaha_id.'/'.$rows->wr_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a data-original-title="Hapus" href="'.base_url().$this->url.'/delete_surat_izin/'.$rows->id.'" class=" btn red btn-icon-only tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
			);
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_surat_izin($id, $wr_id)
    {
        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['prefix']      = $this->prefix;

        $data['breadcrumb']  = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id, 'Form' => $this->url.'/show_add_surat_izin'.'/'.$id];

        $data['wr_usaha_id'] = $id;
        $data['wr_id']       = $wr_id;
        $data['records']     = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $wr_id")->row();

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_surat_izin', $data, $js, $css);
    }

    public function show_edit_surat_izin($id, $wr_usaha_id, $wr_id)
    {
        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['prefix']      = $this->prefix;

        $data['breadcrumb']  = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$wr_usaha_id.'/'.$wr_id, 'Form' => $this->url.'/show_edit_surat_izin'.'/'.$id.'/'.$wr_usaha_id.'/'.$wr_id];

        $data['wr_usaha_id'] = $wr_usaha_id;
        $data['wr_id']       = $wr_id;
        $data['records']     = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $wr_id")->row();
        $data['data']        = $this->db->query("select * from wr_wajib_retribusi_izin where id = $id")->row();

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_surat_izin', $data, $js, $css);
    }

    public function action_surat_izin($id = NULL)
    {
        $this->table_db_izin = 'wr_wajib_retribusi_izin';

        $this->form_validation->set_rules('no_izin',               'No.Izin',                     'trim|required');
        $this->form_validation->set_rules('nm_izin',               'Nama Izin',                   'trim|required');
        $this->form_validation->set_rules('tgl_izin',              'Tanggal Izin',                'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'wr_usaha_id']    = $this->input->post('wr_usaha_id');
            $data[$this->table_prefix.'no_izin']        = $this->input->post('no_izin');
            $data[$this->table_prefix.'nm_izin']        = $this->input->post('nm_izin');
            $data[$this->table_prefix.'tgl_izin']       = $this->input->post('tgl_izin');


            if ($id == NULL) {
                $result  = $this->m_global->insert( $this->table_db_izin, $data );
            }
            else{
                $result = $this->m_global->update($this->table_db_izin, $data, ['id' => $id]);
            }

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';

                echo json_encode( $data );

            } else {

                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );

            }

        } else {

            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );

        }
    }

    public function delete_surat_izin( $id )
    {
        $this->table_db = 'wr_wajib_retribusi_izin';

        $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );

        if ( $result )
        {
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function show_jenisretribusi($id, $wr_id)
    {
        $data['pagetitle']  = 'Jenis retribusi';
        $data['subtitle']   = 'manage jenis retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb']  = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Jenis retribusi' => $this->url.'/show_jenisretribusi'.'/'.$id.'/'.$wr_id];
        $data['id']         = $id;
        $data['wr_id']      = $wr_id;
        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/wajib_retribusi/jenis_retribusi', $data, $js, $css );
    }

    public function select_jenis_retribusi($id)
    {
        $this->table_db = 'wr_wajib_retribusi_usaha_retribusi';

        $join = [
                    'wr_wajib_retribusi_usaha_join' => ['wr_wajib_retribusi_usaha', 'wr_wajib_retribusi_usaha_retribusi.wr_usaha_id = wr_wajib_retribusi_usaha.id', 'LEFT'],
                    'wr_wajib_retribusi_join' => ['wr_wajib_retribusi', 'wr_wajib_retribusi_usaha.wr_id = wr_wajib_retribusi.id', 'LEFT'],
                    'wr_data_umum_join' => ['wr_data_umum', 'wr_wajib_retribusi.data_umum_id = wr_data_umum.id', 'LEFT'],
                    'ref_rek_5_join' => ['ref_rek_5', 'wr_wajib_retribusi_usaha_retribusi.jns_retribusi = ref_rek_5.id_rek_5', 'LEFT'],
                    'ref_pemungutan_join' => ['ref_pemungutan', 'ref_rek_5.jns_pemungutan = ref_pemungutan.jn_pemungutan', 'LEFT']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'            => 'wr_data_umum.npwpd',
            'nm_usaha'         => 'wr_wajib_retribusi_usaha.nm_usaha',
            'nm_rek_5'         => 'ref_rek_5.nm_rek_5',
            'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where      = [ $this->table_db.'.wr_usaha_id' => $id] ;

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wr_wajib_retribusi_usaha_retribusi.id, wr_wajib_retribusi_usaha_retribusi.wr_usaha_id, wr_wajib_retribusi_usaha_retribusi.tmt_operasional, wr_wajib_retribusi_usaha_retribusi.status, wr_wajib_retribusi_usaha_retribusi.lastupdate,
                    wr_wajib_retribusi_usaha.wr_id, ref_rek_5.nm_rek_5, ref_pemungutan.nm_jn_pemungutan, '.implode(',' , $aCari);
        // $this->db->last_query();exit();
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              $rows->npwpd,
              $rows->nm_usaha,
              $rows->nm_rek_5,
              $rows->nm_jn_pemungutan,
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_retribusi/'.$rows->id.'/'.$rows->wr_usaha_id.'/'.$rows->wr_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a data-original-title="Hapus" href="'.base_url().$this->url.'/delete_jenis_retribusi/'.$rows->id.'" class=" btn red btn-icon-only tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
   }

   public function show_add_jenis_retribusi($id, $wr_id)
   {
       $this->table_db            = 'wr_wajib_retribusi_usaha_retribusi';

       $data['pagetitle']         = 'Jenis retribusi';
       $data['subtitle']          = 'manage jenis retribusi';

       $data['url']               = base_url().$this->url;
       $data['prefix']            = $this->prefix;

       $data['breadcrumb']        = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Jenis retribusi' => $this->url.'/show_jenisretribusi'.'/'.$id.'/'.$wr_id, 'Form' => $this->url.'/show_add_jenis_retribusi'.'/'.$id.'/'.$wr_id];

       $data['wr_usaha_id']       = $id;
       $data['wr_id']             = $wr_id;
       $data['records']           = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $wr_id")->row();
       $data['jns_retribusi']         = $this->db->query("select * from ref_rek_5 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();
       $data['organisasi']        = $this->db->query("select * from ref_sub_unit where nm_sub_unit = 'BADAN PENDAPATAN DAERAH'")->row();

       $css['css']                = null;
       $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

       $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_jenis_retribusi', $data, $js, $css);
   }

   public function show_edit_jenis_retribusi($id, $wr_usaha_id, $wr_id)
   {
       $this->table_db            = 'wr_wajib_retribusi_usaha_retribusi';

       $data['pagetitle']         = 'Jenis retribusi';
       $data['subtitle']          = 'manage jenis retribusi';

       $data['url']               = base_url().$this->url;
       $data['prefix']            = $this->prefix;

       $data['breadcrumb']        = [ 'Data Entry' => null, 'Pendataan' => null, 'retribusi' => null, 'Wajib retribusi' => $this->url, 'Jenis Usaha' => $this->url.'/show_jenisUsaha'.'/'.$wr_id, 'Jenis retribusi' => $this->url.'/show_jenisretribusi'.'/'.$wr_usaha_id.'/'.$wr_id, 'Form' => $this->url.'/show_add_jenis_retribusi'.'/'.$id.'/'.$wr_usaha_id.'/'.$wr_id];

       $data['wr_usaha_id']       = $wr_usaha_id;
       $data['wr_id']             = $wr_id;
       $data['records']           = $this->db->query("select wr_wajib_retribusi.*, wr_data_umum.nama_pendaftar, wr_data_umum.npwpd from wr_wajib_retribusi join wr_data_umum on wr_wajib_retribusi.data_umum_id = wr_data_umum.id where wr_wajib_retribusi.id = $wr_id")->row();
       $data['jns_retribusi']         = $this->db->query("select * from ref_rek_5 where kd_rek_1 = 4 and kd_rek_2 = 1 and kd_rek_3 = 2 and kd_rek_4 = 1")->result();
       $data['organisasi']        = $this->db->query("select * from ref_sub_unit where nm_sub_unit = 'BADAN PENDAPATAN DAERAH'")->row();
       $data['data']              = $this->db->query("select * from wr_wajib_retribusi_usaha_retribusi where id = $id")->row();

       $css['css']                = null;
       $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

       $this->template->display( 'pendataan/retribusi/wajib_retribusi/form_jenis_retribusi', $data, $js, $css);
   }

   public function action_jenis_retribusi($id = NULL)
   {
       // print_r($this->input->post()); exit();
       $this->form_validation->set_rules('tmt_operasional',       'TMT Operasional',             'trim|required');

       if ( $this->form_validation->run( $this ) )
       {
           $data[$this->table_prefix.'wr_usaha_id']     = $this->input->post('wr_usaha_id');
           $data[$this->table_prefix.'jns_retribusi']       = $this->input->post('jns_retribusi');
           $data[$this->table_prefix.'kd_urusan']       = $this->input->post('kd_urusan');
           $data[$this->table_prefix.'kd_bidang']       = $this->input->post('kd_bidang');
           $data[$this->table_prefix.'kd_unit']         = $this->input->post('kd_unit');
           $data[$this->table_prefix.'kd_sub']          = $this->input->post('kd_sub');
           $data[$this->table_prefix.'tmt_operasional'] = $this->input->post('tmt_operasional');
           $data[$this->table_prefix.'status_wr']       = $this->input->post('status_wr');

           if ($id == NULL) {
               $result  = $this->m_global->insert( 'wr_wajib_retribusi_usaha_retribusi', $data );
           }
           else{
               $result = $this->m_global->update( 'wr_wajib_retribusi_usaha_retribusi', $data, ['id' => $id]);
           }

           if ( $result )
           {
               $data['status']     = 1;
               $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';

               echo json_encode( $data );

           } else {

               $data['status']     = 0;
               $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_retribusi').'</strong>';
               if(ENVIRONMENT == 'development')
                   $data['error']  = $this->db->error();

               echo json_encode( $data );

           }

       } else {

           $data['status']     = 3;
           $str                = ['<p>', '</p>'];
           $str_replace        = ['<li>', '</li>'];
           $data['message']    = str_replace( $str, $str_replace, validation_errors() );

           echo json_encode( $data );

       }
    }

    public function delete_jenis_retribusi( $id )
    {
        $this->table_db = 'wr_wajib_retribusi_usaha_retribusi';

        $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );

        if ( $result )
        {
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function select_data_umum_wr()
    {
        $this->table_db = 'wr_data_umum';

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_daftar'      => $this->table_prefix.'no_daftar',
            'nama_pendaftar' => $this->table_prefix.'nama_pendaftar',
            'tgl_terdaftar'  => $this->table_prefix.'tgl_terdaftar',
        ];

        $where      = NULL;
        $where_e    = 'status_retribusi != 1';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status =']    = '1';
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);

        $records         = array();
        $records["data"] = array();

        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id, status,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $rows->no_daftar,
                $rows->nama_pendaftar,
                $rows->tgl_terdaftar == NULL || $rows->tgl_terdaftar == '0000-00-00 00:00:00' ? 'Pending' : tgl_format($rows->tgl_terdaftar),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i>'.
                '</a>',
            );
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function get_wajib_retribusi()
    {
        $this->table_db  = 'wr_data_umum';
        $id              = $this->input->post('id');

        $data['records'] = $this->m_global->get($this->table_db, null, ['id' => $id] )[0];
        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function change_status( $status, $where )
    {
        foreach ($where as $value) {
            $result = $this->db->query("delete from $status where id in $value");
        }
    }

}

/* End of file Pendataan_retribusi_wr.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_retribusi_wr.php */
