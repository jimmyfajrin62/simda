<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Objek_pajak_wp extends CI_Controller
{
    private $prefix            = 'pendataan/objek_pajak_wp';
    private $url               = 'pendataan/objek_pajak_wp';
    private $path              = 'pendataan/pajak/objek_pajak/';
    private $table_db          = 'wp_wajib_pajak';
    private $rule_valid        = 'xss_clean|encode_php_tags';
    private $table_prefix      = '';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_wp', 'mdb');
    }

    // wajib pajak usaha
    public function index()
    {
        $data['pagetitle']  = 'Jenis Usaha';
        $data['subtitle']   = 'manage jenis usaha';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $user = $this->db->query("SELECT * from user where user_id = ".$this->session->user_data->user_id)->row();
        if ($user->user_wp == null)
        {
            $this->template->display( 'pendaftaran/pajak/no_wp', $data, $js, $css );
        } else {
            $data['head']       = $this->db->select('a.nama_pendaftar, a.npwpd, b.id as wp_id')
                                            ->from('wp_data_umum a')
                                            ->join('wp_wajib_pajak b', 'a.id = b.data_umum_id', 'LEFT')
                                            ->where('a.id', $this->session->user_data->user_wp)
                                            ->get()->row();
            $data['id']         = $data['head']->wp_id;

            $this->template->display($this->path.'jenis_usaha', $data, $js, $css);
        }
    }

    // usaha
    public function select_jenisUsaha($id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha a';

        $join = [
                    'wp_wajib_pajak' => ['wp_wajib_pajak b', 'a.wp_id = b.id', 'LEFT'],
                    'wp_data_umum'   => ['wp_data_umum c', 'b.data_umum_id = c.id', 'LEFT'],
                    'ref_rek_4'      => ['ref_rek_4 d', 'a.jns_usaha = d.id_rek_4', 'LEFT'],
                    'ref_rek_5'      => ['ref_rek_5 e', 'a.klasifikasi_usaha = e.id_rek_5', 'LEFT'],
                ];

        // jika action checkbox
        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_usaha'     => 'a.nm_usaha',
            'nm_usaha_4'   => 'd.nm_usaha_4',
            'nm_usaha_5'   => 'e.nm_usaha_5',
            'alamat_usaha' => 'a.alamat_usaha'
        ];

        $where      = NULL ;

        $where_e    = "a.wp_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'c.npwpd, c.no_daftar, a.id, a.wp_id, a.status, a.jns_usaha, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {

            if ($rows->jns_usaha == 4) {
                $link_menu = '<a href="'.base_url('pendataan/usaha_detail_hotel/show_detail_usaha/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Detail Usaha"><i class="fa fa-bookmark"></i></a>';
            // } elseif ($rows->jns_usaha == 5) {
            //     $link_menu = '<a href="'.base_url('pendataan/usaha_detail_restoran/show_detail_usaha/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Detail Usaha"><i class="fa fa-bookmark"></i></a>';
            } elseif ($rows->jns_usaha == 6) {
                $link_menu = '<a href="'.base_url('pendataan/usaha_detail_hiburan/show_detail_usaha/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Detail Usaha"><i class="fa fa-bookmark"></i></a>';
            } elseif ($rows->jns_usaha == 8) {
                $link_menu = '<a href="'.base_url('pendataan/usaha_detail_penerangan/show_detail_usaha/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Detail Usaha"><i class="fa fa-bookmark"></i></a>';
            } else {
                $link_menu = '<a class="btn btn-icon-only blue-steel tooltips" disabled data-original-title="Lihat Detail Usaha"><i class="fa fa-bookmark"></i></a>';
            }

            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->nm_usaha),
              strtoupper($rows->nm_usaha_4),
              strtoupper($rows->nm_usaha_5),
              strtoupper($rows->alamat_usaha),
              $link_menu.
              '<a href="'.base_url($this->url.'/show_jenisPajak/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Jenis Pajak"><i class="fa fa-folder-open"></i></a>'.
              // '<a href="'.base_url($this->url.'/show_suratIjin/'.$rows->id.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Surat Izin Yang Dimiliki">
              //      <i class="fa fa-file-text"></i></a>'.
              '<a target="_blank" href="'.base_url($this->url.'/image/'.$rows->no_daftar.'/'.$rows->wp_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SIUP">
                   <i class="fa fa-file-text"></i></a>'.
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_usaha/'.$rows->id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // usaha
    public function show_add_jenis_usaha($id)
    {
        $data['wp_id']             = $id;
        $data['records']           = $this->mdb->show_wp_row($id);

        $data['kec']               = $this->mdb->kecamatan();
        $data['jns_usaha']         = $this->mdb->jns_usaha();

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Objek Pajak' => $this->url, 'Form' => null];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_usaha', $data, $js, $css);
    }

    // usaha
    public function show_edit_jenis_usaha($id, $wp_id)
    {
        $data['wp_id']             = $wp_id;
        $data['data']              = $this->mdb->show_wp_usaha_row($id);
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['kec']               = $this->mdb->kecamatan();
        $data['kel']               = $this->mdb->kelurahan_row($data['data']->kd_kec);
        $data['jns_usaha']         = $this->mdb->jns_usaha();
        $data['klasifikasi_usaha'] = $this->mdb->klasifikasi_usaha_row($data['data']->jns_usaha);

        $data['query'] = $this->db->query("SELECT b.path, b.name from file_upload b
                                    WHERE b.key = '".$data['records']->no_daftar.'/'.$wp_id."'")->row();
        // echo "<pre>";
        // print_r ($data['records']);exit();
        // echo "</pre>";

        $data['pagetitle']         = 'Jenis Usaha';
        $data['subtitle']          = 'manage jenis usaha';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Objek Pajak' => $this->url, 'Form' => null];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_usaha', $data, $js, $css);
    }

    // usaha
    public function action_jenis_usaha($id = null)
    {
        // print_r($this->input->post()); exit();
        $this->form_validation->set_rules('jns_usaha', 'Jenis Usaha', 'trim|required');
        $this->form_validation->set_rules('klasifikasi_usaha', 'Klasifikasi Usaha', 'trim|required');
        $this->form_validation->set_rules('nm_usaha', 'Nama Usaha', 'trim|required');
        $this->form_validation->set_rules('alamat_usaha', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('kd_kec', 'Kecamatan', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_id']             = $this->input->post('wp_id');
            $data[$this->table_prefix.'jns_usaha']         = $this->input->post('jns_usaha');
            $data[$this->table_prefix.'klasifikasi_usaha'] = $this->input->post('klasifikasi_usaha');
            $data[$this->table_prefix.'npwp_usaha']        = $this->input->post('npwp_usaha');
            $data[$this->table_prefix.'nm_usaha']          = $this->input->post('nm_usaha');
            $data[$this->table_prefix.'alamat_usaha']      = $this->input->post('alamat_usaha');
            $data[$this->table_prefix.'kd_kec']            = $this->input->post('kd_kec');
            $data[$this->table_prefix.'kd_kel']            = $this->input->post('kd_kel');

            $dir = './upload/'. $this->input->post('no_daftar').'/'.$this->input->post('wp_id');
                if (! file_exists($dir) && is_dir($dir)) {

                    echo $dir;exit();
                    mkdir($dir, 0777);
                }
                $config['upload_path'] = $dir . "/";
                $config['remove_spaces'] = TRUE;
                $config['allowed_types']  = 'gif|jpg|jpeg|png';
                $config['max_size'] = '10000';
                $config['overwrite'] = FALSE;
                $this->load->library('upload', $config);
                // echo "<pre>";
                // print_r ($_FILES);exit();
                // echo "</pre>";
                foreach ($_FILES as $a => $b) {
                    $ar = explode(".", $_FILES[$a]['name']);
                    $ext = $ar[count($ar) - 1];
                    $ext = strtolower($ext);
                     // echo $ext;
                    if ($ext == "pdf" || $ext == "jpg" || $ext == "jpeg" || $ext == "png") {

                        $config['file_name'] = $a . '.' . $ext;
                        $this->upload->initialize($config);
                        $this->upload->display_errors('', '');
                        if (!$this->upload->do_upload($a)) {
                            $error = array('error' => $this->upload->display_errors());
                            if ($error['error'] == "<p>The file you are attempting to upload is larger than the permitted size.</p>") {
                                //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal dikarenakan ukuran file melebihi ketentuan. Silahkan upload file dengan ukuran kurang dari 2MB.";
                                print '<script type="text/javascript">';
                                print 'alert("Maaf, proses upload Data Anda gagal dikarenakan ukuran file melebihi ketentuan.\nSilahkan upload file dengan ukuran kurang dari 2MB.");';
                                print '</script>';
                                die('oke');
                            }
                            else {
                                //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal.\nSilahkan ulangi kembali.";
                                print '<script type="text/javascript">';
                                print 'alert("Maaf, proses upload Data Anda gagal.\nSilahkan ulangi kembali Silahkan upload file dengan ukuran kurang dari 2MB.");';
                                print '</script>';
                            }
                        }
                        else {

                            $up = $this->upload->data();

                            $data1[$this->table_prefix.'key'] = $this->input->post('no_daftar').'/'.$this->input->post('wp_id');
                            $data1['path']                    = $dir;
                            $data1['name']                    = $config['file_name'];
                            $data1['type']                    = $a;
                            // echo  $data1['key'];exit();
                            $this->db->insert( 'file_upload', $data1 );
                        }

                    }
                }

            if ($id == null) {
                $result  = $this->m_global->insert('wp_wajib_pajak_usaha', $data);
            } else {
                $result = $this->m_global->update('wp_wajib_pajak_usaha', $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add  with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // usaha option
    public function get_kelurahan()
    {
        $table_kel          = 'ref_kelurahan';

        $id = $this->input->post();
        $data['kelurahan']  = $this->m_global->get($table_kel, null, $id, 'kel_id, kd_kec, kd_kel, nm_kel');

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // usaha option
    public function get_klasifikasi()
    {
        $kd_rek_1 = $this->input->post('kd_rek_1');
        $kd_rek_2 = $this->input->post('kd_rek_2');
        $kd_rek_3 = $this->input->post('kd_rek_3');
        $kd_rek_4 = $this->input->post('kd_rek_4');

        $data['klasifikasi']  = $this->db->query("select * from ref_rek_5 where kd_rek_1 = $kd_rek_1 and kd_rek_2 = $kd_rek_2 and kd_rek_3 = $kd_rek_3 and kd_rek_4 = $kd_rek_4")->result();

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // pajak usaha
    public function show_jenisPajak($id, $wp_id)
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;
        $data['head']       = $this->mdb->header2($id);

        $data['pagetitle']  = 'Jenis Pajak';
        $data['subtitle']   = 'manage jenis pajak';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$id.'/'.$wp_id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function select_jenis_pajak($id)
    {
        $this->table_db = 'wp_wajib_pajak_usaha_pajak a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                    'ref_rek_4'            => ['ref_rek_4 e', 'a.jns_pajak = e.id_rek_4', 'LEFT'],
                    'ref_pemungutan'       => ['ref_pemungutan f', 'e.jns_pemungutan = f.jn_pemungutan', 'LEFT']
                ];

        // jika action checkbox
        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'e.nm_rek_4',
            'tmt_operasional'  => 'a.tmt_operasional',
            'nm_jn_pemungutan' => 'f.nm_jn_pemungutan'
        ];

        $where      = NULL ;

        $where_e    = "a.wp_usaha_id' = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_usaha_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_usaha_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.id, a.wp_usaha_id, a.status, b.wp_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->nm_rek_4),
              tgl_format($rows->tmt_operasional),
              strtoupper($rows->nm_jn_pemungutan),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis_pajak/'.$rows->id.'/'.$rows->wp_usaha_id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_pajak/'.
                                       ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                       ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                       ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                       ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_usaha_pajak/99'.
                                       ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

              );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // pajak usaha
    public function show_add_jenis_pajak($id, $wp_id)
    {
        $data['wp_usaha_id']       = $id;
        $data['wp_id']             = $wp_id;
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['jns_pajak']         = $this->mdb->jns_usaha();
        $data['organisasi']        = $this->mdb->organisasi_row($this->session->user_data->user_organisasi);

        $data['pagetitle']         = 'Jenis Pajak';
        $data['subtitle']          = 'manage jenis pajak';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Objek Pajak' => $this->url, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$id.'/'.$wp_id, 'Form' => $this->url.'/show_add_jenis_pajak'.'/'.$id.'/'.$wp_id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function show_edit_jenis_pajak($id, $wp_usaha_id, $wp_id)
    {
        $data['wp_usaha_id']       = $wp_usaha_id;
        $data['wp_id']             = $wp_id;
        $data['records']           = $this->mdb->show_wp_row($wp_id);
        $data['jns_pajak']         = $this->mdb->jns_usaha();
        $data['organisasi']        = $this->mdb->organisasi_row($this->session->user_data->user_organisasi);
        $data['data']              = $this->mdb->show_pajak_usaha_row($id);

        $data['pagetitle']         = 'Jenis Pajak';
        $data['subtitle']          = 'manage jenis pajak';

        $data['url']               = base_url().$this->url;
        $data['breadcrumb']        = [ 'Objek Pajak' => $this->url, 'Jenis Pajak' => $this->url.'/show_jenisPajak'.'/'.$wp_usaha_id.'/'.$wp_id, 'Form' => $this->url.'/show_edit_jenis_pajak'.'/'.$id.'/'.$wp_usaha_id.'/'.$wp_id];

        $css['css']                = null;
        $js['js']                  = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_jenis_pajak', $data, $js, $css);
    }

    // pajak usaha
    public function action_jenis_pajak($id = null)
    {
        // print_r($this->input->post()); exit();
       $this->form_validation->set_rules('tmt_operasional', 'TMT Operasional', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_usaha_id']     = $this->input->post('wp_usaha_id');
            $data[$this->table_prefix.'jns_pajak']       = $this->input->post('jns_pajak');
            $data[$this->table_prefix.'kd_urusan']       = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']       = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']         = $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']          = $this->input->post('kd_sub');
            $data[$this->table_prefix.'tmt_operasional'] = $this->m_global->setdateformat($this->input->post('tmt_operasional'));
            $data[$this->table_prefix.'status']          = $this->input->post('status') == NULL ? '0' : $this->input->post('status');

            if ($id == null) {
                $result  = $this->m_global->insert('wp_wajib_pajak_usaha_pajak', $data);
            } else {
                $result = $this->m_global->update('wp_wajib_pajak_usaha_pajak', $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // pajak usaha get pungut
    public function get_pemungutan()
    {
        $table = 'ref_pemungutan';
        $id    = $this->input->post();

        $data['pemungutan']  = $this->m_global->get($table, null, $id);

        header('Content-type: application/json');
        echo json_encode($data);
    }

    // ijin usaha
    public function show_suratIjin($id, $wp_id)
    {
        $data['id']         = $id;
        $data['wp_id']      = $wp_id;
        $data['head']       = $this->mdb->header2($id);

        $data['pagetitle']  = 'Surat Izin';
        $data['subtitle']   = 'manage surat izin';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Objek Pajak' => $this->url, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id.'/'.$wp_id];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'surat_ijin', $data, $js, $css);
    }

    // ijin usaha
    public function select_suratIjin($id)
    {
        // select table urusan
        $this->table_db = 'wp_wajib_pajak_izin a';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha b', 'a.wp_usaha_id = b.id', 'LEFT'],
                    'wp_wajib_pajak'       => ['wp_wajib_pajak c', 'b.wp_id = c.id', 'LEFT'],
                    'wp_data_umum'         => ['wp_data_umum d', 'c.data_umum_id = d.id', 'LEFT'],
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_izin'  => 'a.no_izin',
            'nm_izin'  => 'a.nm_izin',
            'tgl_izin' => 'a.tgl_izin'
        ];

        $where      = null;
        $where_e    = "a.wp_usaha_id = $id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.wp_usaha_id = $id";
        } else {
            $where_e = "a.status = '1' and a.wp_usaha_id = $id";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'b.wp_id, a.id, wp_usaha_id, a.wp_usaha_id, a.status, a.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              strtoupper($rows->no_izin),
              strtoupper($rows->nm_izin),
              tgl_format($rows->tgl_izin),
               '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_surat_izin/'.$rows->id.'/'.$rows->wp_usaha_id.'/'.$rows->wp_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_izin/'.
                                       ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                       ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                       ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                       ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
               '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/wp_wajib_pajak_izin/99'.
                                       ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    // ijin usaha
    public function show_add_surat_izin($id, $wp_id)
    {
        $data['wp_usaha_id'] = $id;
        $data['wp_id']       = $wp_id;
        $data['records']     = $this->mdb->show_wp_row($wp_id);

        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['breadcrumb']  = [ 'Objek Pajak' => $this->url, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$id.'/'.$wp_id, 'Form' => $this->url.'/show_add_surat_izin'.'/'.$id.'/'.$wp_id];

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_surat_izin', $data, $js, $css);
    }

    // ijin usaha
    public function show_edit_surat_izin($id, $wp_usaha_id, $wp_id)
    {
        $data['wp_usaha_id'] = $wp_usaha_id;
        $data['wp_id']       = $wp_id;
        $data['records']     = $this->mdb->show_wp_row($wp_id);
        $data['data']        = $this->mdb->show_izin($id);

        $data['pagetitle']   = 'Surat Izin';
        $data['subtitle']    = 'manage surat izin';

        $data['url']         = base_url().$this->url;
        $data['breadcrumb']  = [ 'Objek Pajak' => $this->url, 'Surat Izin' => $this->url.'/show_suratIjin'.'/'.$wp_usaha_id.'/'.$wp_id, 'Form' => $this->url.'/show_edit_surat_izin'.'/'.$id.'/'.$wp_usaha_id.'/'.$wp_id];

        $css['css']          = null;
        $js['js']            = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_surat_izin', $data, $js, $css);
    }

    // ijin usaha
    public function action_surat_izin($id = null)
    {
        $this->table_db_izin = 'wp_wajib_pajak_izin';

        $this->form_validation->set_rules('no_izin', 'No.Izin', 'trim|required');
        $this->form_validation->set_rules('nm_izin', 'Nama Izin', 'trim|required');
        $this->form_validation->set_rules('tgl_izin', 'Tanggal Izin', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'wp_usaha_id']    = $this->input->post('wp_usaha_id');
            $data[$this->table_prefix.'no_izin']        = $this->input->post('no_izin');
            $data[$this->table_prefix.'nm_izin']        = $this->input->post('nm_izin');
            $data[$this->table_prefix.'tgl_izin']       = $this->input->post('tgl_izin');


            if ($id == null) {
                $result  = $this->m_global->insert($this->table_db_izin, $data);
            } else {
                $result = $this->m_global->update($this->table_db_izin, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed add pendaftaran retribusi with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Pendataan Report';

        // query
        $data['report']  = $this->db->query("SELECT data_umum_id, tgl_aktif, keterangan, user_lastupdate from user")->result();

        $data['namaFile']   ='User_Report_'.date('Y-m-d');
        $data['title']      ='User Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['Fullname', '30'], ['Name', '30'], ['Email', '30'], ['Lastupdate', '30']
        ];

        $this->load->view('export',$data);
    }

    public function image($type,$key){

        $query = $this->db->query("SELECT b.path, b.name from file_upload b
                                    WHERE b.key = '$type/$key'")->row();

        $file = str_replace('system/','',str_replace('\\','/',BASEPATH)).str_replace('./', '', $query->path).'/'.$query->name;
        $imginfo = getimagesize($file);

        header("Content-type: $imginfo[mime]");
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: '.filesize($file));
        ob_clean();
        flush();
        readfile($file);
    }
    
    public function delete_file($type,$key){


        $query = $this->db->query("SELECT b.path, b.name from file_upload b
                                    WHERE b.key = '$key' and b.type = '$type' ")->row();

        $file = str_replace('./', '', $query->path).'/'.$query->name;        
        
        unlink($file);
        print '<script type="text/javascript">';
        print 'window.history.back();';
        print '</script>';
        die();
    }
}

/* End of file Pendataan_pajak_wp.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_wp.php */
