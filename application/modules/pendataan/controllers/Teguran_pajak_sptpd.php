<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teguran_pajak_sptpd extends Admin_Controller
{
    private $prefix         = 'pendataan/teguran_pajak_sptpd';
    private $url            = 'pendataan/teguran_pajak_sptpd';
    private $table_db       = 'ref_rek_4';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'Teguran Pajak SPTPD';
        $data['subtitle']   = 'manage teguran pajak SPTPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Teguran' => null, 'Pajak' => null, 'SPTPD' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/pajak/teguran_sptpd/index', $data, $js, $css );
	}

    public function select()
    {
        $join = [
                    'ref_pemungutan' => ['ref_pemungutan', 'ref_rek_4.jns_pemungutan = ref_pemungutan.jn_pemungutan', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'ref_rek_4.nm_rek_4',
            'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where    = null;
        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 != 72 and id_rek_4 != 15 and jns_pemungutan = 1 ";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_rek_4.jns_pemungutan, ref_rek_4.id_rek_4, ref_rek_4.nm_usaha_4, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              ucwords($rows->nm_rek_4),
              ucwords ($rows->nm_jn_pemungutan),
               '<a href="'.base_url('pendataan/teguran_pajak_sptpd_sub/'.$rows->id_rek_4.'/'.$rows->nm_usaha_4).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Teguran SPTPD"><i class="fa fa-folder-open"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

}

/* End of file Pendataan_pajak_sptpd.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_sptpd.php */
