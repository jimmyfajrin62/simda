<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendataan_retribusi_sptrd extends Admin_Controller {

	private $prefix                 = 'pendataan/Pendataan_retribusi_sptrd ';
    private $url                    = 'pendataan/Pendataan_retribusi_sptrd';
    private $table_db   	        = 'ref_unit';
    private $table_sub_unit         = 'ref_sub_unit';
    private $table_jns_retribusi	= 'ref_jenis_retribusi';
    private $table_prefix           = '';
    private $rule_valid             = 'xss_clean|encode_php_tags';

   function __construct() {
        parent::__construct();
    }

    public function index()
    {

        $data['pagetitle']  = 'Pendataan Retribusi';
        $data['subtitle']   = 'SPTRD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Unit' => $this->url  ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/sptrd/index', $data, $js, $css );
    }

    public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'unit_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);


                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'         => 'kd_urusan',
            'bidang'         => 'kd_bidang',
            'unit'           => 'kd_unit',
            'uraian_unit'    => 'nm_unit',
            'lastupdate'     => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys   = array_keys( $aCari );
        @$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords  = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $records        = array();
        $records["data"]= array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,unit_id,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->unit_id.'"/><span></span></label>',
              $i,

              $rows->kd_urusan,
              $rows->kd_bidang,
              $rows->kd_unit,
              $rows->nm_unit,
              $rows->lastupdate,


              '<a data-original-title="pilih" href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_urusan.'/'.$rows->unit_id.'/" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_sub_unit($kd_urusan,$kd_bidang,$kd_unit,$id)
    {

        $data['pagetitle']  = 'Pendataan Retribusi';
        $data['subtitle']   = 'SPTRD';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$id ];
        $data['prefix']     = $this->prefix;
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;
        $data['id']         =$id;
        $data['nama']       = $this->m_global->get( $this->table_db, null, ['unit_id' => $id] )[0];
        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/sptrd/sub_unit', $data, $js, $css  );
    }

    public function select_sub($kd_urusan='',$kd_bidang='',$kd_unit='')
    {

        if ( @$_REQUEST['customActionType'] == 'group_action' )

        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'unit_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'         => 'kd_urusan',
            'bidang'         => 'kd_bidang',
            'unit'           => 'kd_unit',
            'sub_unit'       => 'kd_sub',
            'uraian_unit'    => 'nm_sub_unit',
            'lastupdate'     => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = 'kd_urusan='.$kd_urusan.' and kd_bidang='.$kd_bidang.' and kd_unit='.$kd_unit;
        // $where_e    = NULL;
        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys   = array_keys( $aCari );
        @$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords  = $this->m_global->count( $this->table_sub_unit, null, $where, $where_e );
        $iDisplayLength = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $records        = array();
        $records["data"]= array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,kd_unit,kd_bidang,kd_urusan,kd_sub,nm_sub_unit,lastupdate,sub_unit_id,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_sub_unit, null,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->sub_unit_id.'"/><span></span></label>',
              $i,

              $rows->kd_urusan,
              $rows->kd_bidang,
              $rows->kd_unit,
              $rows->kd_sub,
              $rows->nm_sub_unit,
              $rows->lastupdate,

               '<a data-original-title="pilih" href="'.base_url().$this->url.'/show_rekening/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->kd_sub.'/'.$rows->sub_unit_id.'/" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_rekening($kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id)
    {

        $data['pagetitle']  = 'Jenis Retribusi';
        $data['subtitle']   = 'Jenis Retsribusi';

        $data['url']        = base_url().$this->url;;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.                  '/'.$kd_unit.'/'.$id, 'Rekening' => $this->url."/show_penandatanganan/".$kd_urusan.                  '/'.$kd_bidang.'/'.$kd_unit.'/'.$id ];
        $data['prefix']     = $this->prefix;
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;
        $data['kd_sub']    = $kd_sub;
        $data['nama']       = $this->m_global->get( $this->table_sub_unit, null, ['sub_unit_id' => $id] )[0];
        $data['id']         =$id;
        $data['nama']       = $this->m_global->get( $this->table_db, null, ['unit_id' => $id] )[0];
        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/sptrd/rekening', $data, $js, $css  );
    }

    public function select_rekening($kd_urusan='',$kd_bidang='',$kd_unit='',$kd_sub='')
    {

        if ( @$_REQUEST['customActionType'] == 'group_action' )

        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'unit_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_jenis'         => 'kd_jenis',
            'nm_retribusi'     => 'nm_retribusi',
            'lastupdate'       => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = 'kd_urusan='.$kd_urusan.' and kd_bidang='.$kd_bidang.' and kd_unit='.$kd_unit.' and kd_sub='.$kd_sub;
        // $where_e    = NULL;
        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys   = array_keys( $aCari );
        @$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords  = $this->m_global->count( $this->table_jns_retribusi, null, $where, $where_e );
        $iDisplayLength = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $records        = array();
        $records["data"]= array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,kd_unit,kd_bidang,kd_urusan,kd_sub,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_jns_retribusi, null,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_jenis.'"/><span></span></label>',
              $i,

              $rows->kd_jenis,
              $rows->nm_retribusi,
              $rows->lastupdate,

               '<a data-original-title="pilih" href="'.base_url().$this->url.'/show_/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->kd_sub.'/" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>',

            );
            $i++;
        }
        // echo $this->db->last_query();
        // exit();
        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_($kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id)
    {

        $data['pagetitle']  = 'Jenis Retribusi';
        $data['subtitle']   = 'Jenis Retsribusi';

        $data['url']        = base_url().$this->url;;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.                  '/'.$kd_unit.'/'.$id, 'Rekening' => $this->url."/show_penandatanganan/".$kd_urusan.                  '/'.$kd_bidang.'/'.$kd_unit.'/'.$id ];
        $data['prefix']     = $this->prefix;
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;
        $data['kd_sub']    = $kd_sub;
        $data['nama']       = $this->m_global->get( $this->table_sub_unit, null, ['sub_unit_id' => $id] )[0];
        $data['id']         =$id;
        $data['nama']       = $this->m_global->get( $this->table_db, null, ['unit_id' => $id] )[0];
        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'pendataan/retribusi/sptrd/rekening', $data, $js, $css  );
    }


}

/* End of file Pendaftaran_retribusi_sptrd.php */
/* Location: ./application/modules/data_entry/controllers/Pendaftaran_retribusi_sptpd.php */
