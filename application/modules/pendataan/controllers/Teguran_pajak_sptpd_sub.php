<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Teguran_pajak_sptpd_sub extends Admin_Controller
{
    private $prefix         = 'pendataan/teguran_pajak_sptpd_sub';
    private $url            = 'pendataan/teguran_pajak_sptpd_sub';
    private $path           = 'pendataan/pajak/teguran_sptpd/';
    private $path_teguran   = 'pendataan/teguran_pajak_sptpd';
    private $table_db       = 'ta_teguran';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_teguran', 'mdb');
    }

    public function _remap($method, $args)
    {
        if (method_exists($this, $method)) {
            $this->$method($args);
        } else {
            $this->index($method, $args);
        }
    }

    public function index($kd_rek_4, $title)
    {
        $title = str_replace('%20', ' ', $title[0]);

        $data['pagetitle']  = "Teguran SPTPD $title";
        $data['subtitle']   = "manage Teguran SPTPD $title";

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Teguran' => null, 'Pajak' => null, 'SPTPD' => $this->path_teguran, "Teguran SPTPD $title" => $this->url.'/'.$kd_rek_4.'/'.$title ];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'wp', $data, $js, $css);
    }

    public function select($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_teguran'      => 'no_teguran',
            'masa2'           => 'masa2',
            'tgl_teguran'     => 'tgl_teguran',
            'tgl_jatuh_tempo' => 'tgl_jatuh_tempo',
            'keterangan'      => 'keterangan'
        ];

        $where    = null;
        $where_e  = "kd_pajak = $kd_rek_4";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "status = '$request' and kd_pajak = $kd_rek_4";
        } else {
            $where_e = "status = '1' and kd_pajak = $kd_rek_4";
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, null, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id, masa1, status, lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
              $i,
              '<span class="label label-m label-primary">'.strtoupper($rows->no_teguran).'</span>',
              tgl_format($rows->masa1).'<br> s/d <br>'.tgl_format($rows->masa2),
              tgl_format($rows->tgl_teguran),
              tgl_format($rows->tgl_jatuh_tempo),
              strtoupper($rows->keterangan),
              '<a href="'.base_url($this->url.'/show_teguran_wp/'.$kd_rek_4.'/'.$title.'/'.$rows->id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat Teguran Pajak"><i class="fa fa-folder-open"></i></a>'.
              '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$kd_rek_4.'/'.$title.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
              '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_teguran/'.
                                      ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                      ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                      ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                      ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
              '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_teguran/99'.
                                      ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add($ids)
    {
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);

        $data['pagetitle']  = 'Teguran Hotel';
        $data['subtitle']   = 'manage Teguran hotel';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['dokumen']    = $this->mdb->petugas();

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_teguran, "Teguran SPTPD $title" => $this->url.'/'.$kd_rek_4.'/'.$title, 'Form' => $this->url.'/show_add/'.$kd_rek_4.'/'.$title ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js);
    }

    public function show_edit($ids)
    {
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);
        $id       = $ids[2];

        $data['pagetitle']  = 'Teguran Hotel';
        $data['subtitle']   = 'manage Teguran hotel';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['dokumen']    = $this->mdb->petugas();
        $data['data']       = $this->mdb->show_teguran($id);

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_teguran, "Teguran SPTPD $title" => $this->url.'/'.$kd_rek_4.'/'.$title, 'Form' => $this->url.'/show_add/'.$kd_rek_4.'/'.$title.'/'.$id ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display($this->path.'form', $data, $js);
    }

    public function action_form($ids = null)
    {
        // echo "<pre>",print_r($this->input->post()),exit();
        $this->table_db = 'ta_teguran';

        $this->form_validation->set_rules('tgl_teguran', 'Tgl Teguran', 'trim|required');
        $this->form_validation->set_rules('tgl_jatuh_tempo', 'Tgl Jatuh Tempo', 'trim|required');
        $this->form_validation->set_rules('masa1', 'Masa Pajak', 'trim|required');
        $this->form_validation->set_rules('masa2', 'Masa Pajak', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'kd_pajak']        = $this->input->post('kd_pajak');
            $data[$this->table_prefix.'tgl_teguran']     = $this->m_global->setdateformat($this->input->post('tgl_teguran'));
            $data[$this->table_prefix.'tgl_jatuh_tempo'] = $this->m_global->setdateformat($this->input->post('tgl_jatuh_tempo'));
            $data[$this->table_prefix.'masa1']           = $this->m_global->setdateformat($this->input->post('masa1'));
            $data[$this->table_prefix.'masa2']           = $this->m_global->setdateformat($this->input->post('masa2'));
            $data[$this->table_prefix.'keterangan']      = $this->input->post('keterangan');
            $data[$this->table_prefix.'ttd_dok_id']      = $this->input->post('ttd_dok_id');

            if ($ids == null) {
                $date       = date("d/m/Y");
                $no_teguran = $this->mdb->no_teguran('T', "/Teguran/$date");

                $data[$this->table_prefix.'id']                = $this->mdb->auto_id();
                $data[$this->table_prefix.'tahun']             = date('Y');
                $data[$this->table_prefix.'no_teguran']        = $no_teguran;

                $result  = $this->m_global->insert($this->table_db, $data);
            } else {
                $id = $ids[0];
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function show_teguran_wp($ids)
    {
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);
        $id       = $ids[2];

        $data['pagetitle']  = 'Rincian Teguran SPTPD Hotel';
        $data['subtitle']   = 'manage rincian teguran SPTPD hotel';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_teguran, "Teguran SPTPD $title" => $this->url.'/'.$kd_rek_4.'/'.$title, "Rincian" => null ];
        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['id']         = $id;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'teguran_wp', $data, $js, $css);
    }

    public function select_teguran_wp($ids)
    {
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);
        $id       = $ids[2];

        $this->table_db = 'ta_teguran_rinc';

        $join = [
                    'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak', 'ta_teguran_rinc.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id', 'LEFT'],
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha', 'wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id', 'LEFT'],
                    'wp_wajib_pajak' => ['wp_wajib_pajak', 'wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id', 'LEFT'],
                    'wp_data_umum' => ['wp_data_umum', 'wp_wajib_pajak.data_umum_id = wp_data_umum.id', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
             'npwpd'          => 'wp_data_umum.npwpd',
             'nama_pendaftar' => 'wp_data_umum.nama_pendaftar',
             'nm_usaha'       => 'wp_wajib_pajak_usaha.nm_usaha',
             'alamat_usaha'   => 'wp_wajib_pajak_usaha.alamat_usaha'
         ];

        $where    = null;
        $where_e  = "wp_wajib_pajak_usaha_pajak.jns_pajak = $kd_rek_4";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }
        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ta_teguran_rinc.id, wp_wajib_pajak_usaha_pajak.id as id2, wp_wajib_pajak_usaha_pajak.status, wp_wajib_pajak_usaha_pajak.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query();exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                 // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
               $i,
               '<span class="label label-m label-primary">'.strtoupper($rows->npwpd).'</span>',
               strtoupper($rows->nama_pendaftar),
               strtoupper($rows->nm_usaha),
               strtoupper($rows->alamat_usaha),
                '<a data-original-title="Hapus" href="'.base_url().$this->url.'/delete_teguran_wp/'.$rows->id.'/'.$rows->id2.'" class=" btn red btn-icon-only tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'.
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>',
             );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function show_add_teguran_wp($ids)
    {
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);
        $id       = $ids[2];

        $data['pagetitle']  = 'SPTPD Hotel';
        $data['subtitle']   = 'manage SPTPD hotel';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'Pendataan' => null, 'Pajak' => null, 'SPTPD' => $this->path_teguran, "Teguran SPTPD $title" => $this->url.'/'.$kd_rek_4.'/'.$title, "Rincian" => $this->url.'/show_teguran_wp/'.$kd_rek_4.'/'.$title.'/'.$id, "Form" => null ];
        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];

        $this->template->display($this->path.'form_teguran_wp', $data, $js);
    }

    public function action_form_teguran_wp($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->table_db = 'ta_teguran_rinc';

        $this->form_validation->set_rules('wp_usaha_pajak_id', 'Wajib Pajak', 'trim|required');

        if ($this->form_validation->run($this)) {
            $data[$this->table_prefix.'teguran_id']        = $this->input->post('teguran_id');
            $data[$this->table_prefix.'wp_usaha_pajak_id'] = $this->input->post('wp_usaha_pajak_id');

            if ($ids == null) {
                $result  = $this->m_global->insert($this->table_db, $data);

                $id = $this->input->post('wp_usaha_pajak_id');
                $data2['status_teguran'] = 1;
                $result2 = $this->m_global->update('wp_wajib_pajak_usaha_pajak', $data2, ['id' => $id]);
            } else {
                $id     = $ids[0];
                $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    public function get_npwpd($ids)
    {
        $kd_rek_4 = $ids[0];

        $this->table_db = 'wp_wajib_pajak_usaha_pajak';

        $join = [
                    'wp_wajib_pajak_usaha' => ['wp_wajib_pajak_usaha', 'wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id', 'LEFT'],
                    'wp_wajib_pajak' => ['wp_wajib_pajak', 'wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id', 'LEFT'],
                    'wp_data_umum' => ['wp_data_umum', 'wp_wajib_pajak.data_umum_id = wp_data_umum.id', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
             'npwpd'          => 'wp_data_umum.npwpd',
             'nama_pendaftar' => 'wp_data_umum.nama_pendaftar',
             'nm_usaha'       => 'wp_wajib_pajak_usaha.nm_usaha',
             'alamat_usaha'   => 'wp_wajib_pajak_usaha.alamat_usaha'
         ];

        $where    = null;
        $where_e  = "wp_wajib_pajak_usaha_pajak.status_teguran = 0 and wp_wajib_pajak_usaha_pajak.jns_pajak = $kd_rek_4";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }
        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wp_wajib_pajak_usaha_pajak.id, wp_wajib_pajak_usaha_pajak.status, wp_wajib_pajak_usaha_pajak.lastupdate, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
               strtoupper($rows->npwpd),
               strtoupper($rows->nama_pendaftar),
               strtoupper($rows->nm_usaha),
               strtoupper($rows->alamat_usaha),
               '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.')" data-dismiss="modal" >'.
               '<i class="fa fa-check"></i>'.
               '</a>',
             );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

    public function get_npwpd_row()
    {
        $this->table_db  = 'wp_wajib_pajak_usaha_pajak';
        $id              = $this->input->post('id');

        $data['records'] = $this->mdb->get_npwpd($id);
        header("Content-Type:application/json");
        echo json_encode($data);
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($ids)
    {
        // echo "<pre>",print_r($id),exit();
        $id     = $ids[0];
        $table  = $ids[1];
        $status = $ids[2];
        $stat   = $ids[3];

        if ($stat == 'true') {
            // update sptpd
            $pungut               = $this->db->query("SELECT pungut_id from ta_sspd  where id = $id")->row();
            $id_pungut            = $pungut->pungut_id;
            $data2['status_sspd'] = 0;
            $result2              = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);

            $result               = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result               = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function delete_teguran_wp($ids)
    {
        $id  = $ids[0];
        $id2 = $ids[1];

        $data['status_teguran'] = 0;

        $result = $this->m_global->update('wp_wajib_pajak_usaha_pajak', $data, ['id' => $id2]);
        $result = $this->m_global->delete('ta_teguran_rinc', [$this->table_prefix.'id' => $id]);

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function print_pdf($ids){

        $kd_rek_4 = $ids[0];
        $id       = $ids[1];


        $this->load->library('Pdf');
        $pdf = $this->pdf->load();


        $data['data'] = $this->mdb->show_data($id);
        // echo "<pre>",print_r($data['data']),exit();

        $kota = $this->mdb->kota();


        $this->load->library('fpdf_gen');
        $pdf = new fpdf('P','mm','A4');

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 0);

        $w = 0;
        $image = base_url('./assets/img/'.$kota->logo);

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Image($image,10,10,25,25);

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(190,10,'PEMERINTAH KOTA PASURUAN', 'LTR', 1, 'C');
        $pdf->Cell(190,5,'BADAN PENDAPATAN DAERAH', 'LR', 1, 'C');
        $pdf->Cell(190,10,'malang', 'LBR', 1, 'C');

        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(100,7,'', 'LT', 0, 'R');
        $pdf->Cell(50,7,ucwords($kota->ibukota).', '.tgl_format(date("Y-m-d")), 'T', 0, 'L');
        $pdf->Cell(40,7,'', 'R', 1, 'C');
        $pdf->Cell(100,7,'', 'L', 0, 'R');
        $pdf->Cell(50,7,'Kepada :', 0, 0, 'L');
        $pdf->Cell(40,7,'', 'R', 1, 'R');
        $pdf->Cell(100,7,'', 'L', 0, 'R');
        $pdf->Cell(50,7,'Yth. Manajemen'.' '.ucwords($data['data']->nm_usaha), 0, 0, 'L');
        $pdf->Cell(40,7,'', 'R', 1, 'R');
        $pdf->Cell(100,7,'', 'L', 0, 'R');
        $pdf->Cell(50,7,'di tempat', '', 0, 'L');
        $pdf->Cell(40,7,'', 'R', 1, 'R');

        $pdf->Cell(190,7,'', 'LR', 1, 'C');

        $pdf->SetFont('Times', 'B', 10);

        $pdf->Cell(190,7,'SURAT TEGURAN', 'LR', 1, 'C');
        $pdf->Cell(190,7,'UNTUK PENYAMPAIKAN SPTPD', 'LR', 1, 'C');

        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(190,7,'Nomor :'.' '.ucwords($data['data']->no_teguran), 'LR', 1, 'C');

        $pdf->Cell(190,7,'', 'LR', 1, 'C');

        $pdf->Cell(40,7,'Nama Wajib Pajak', 'L', 0, 'c');
        $pdf->Cell(150,7,': '.$data['data']->nama_pendaftar, 'R', 1, 'c');
        $pdf->Cell(40,7,'NPWPD', 'L', 0, 'c');
        $pdf->Cell(150,7,': '.ucwords($data['data']->npwpd), 'R', 1, 'c');
        $pdf->Cell(40,7,'Alamat Wajib Pajak', 'L', 0, 'c');
        $pdf->Cell(150,7,': '.ucwords($data['data']->jalan).' '.ucwords($data['data']->rtrw).' '.ucwords($data['data']->kabupaten).' Kode Pos : '.ucwords($data['data']->kode_pos), 'R', 1, 'c');
        $pdf->Cell(40,7,'Nama Usaha', 'L', 0, 'c');
        $pdf->Cell(150,7,': '.ucwords($data['data']->nm_usaha), 'R', 1, 'c');
        $pdf->Cell(40,7,'Alamat Usaha', 'L', 0, 'c');
        $pdf->Cell(150,7,': '.ucwords($data['data']->alamat_usaha), 'R', 1, 'c');

        $pdf->Cell(190,7,'', 'LR', 1, 'C');

        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'Berdasarkan catatan kami ternyata sampai saat ini Saudara belum menyampaikan SPTPD dan lampiaran keterangan', 'R', 1, 'c');
        $pdf->Cell(190,5,'atau dokumen ke BADAN PENDAPATAN DAERAH, PEMERINTAH KOTA PASURUAN yaitu:', 'LR', 1, 'c');

        $pdf->Cell(190,5,'', 'LR', 1, 'C');

        $pdf->Cell(60,7,'- SPTPD dan data pendukung periode', 'L', 0, 'c');
        $pdf->Cell(130,7,': '.tgl_format($data['data']->masa1).' s.d '.tgl_format($data['data']->masa2), 'R', 1, 'c');
        $pdf->Cell(60,7,'- Jenis Pajak', 'L', 0, 'c');
        $pdf->Cell(130,7,': '.ucwords($data['data']->nm_rek_4), 'R', 1, 'c');
        $pdf->Cell(60,7,'- Keterangan', 'L', 0, 'c');
        $pdf->Cell(130,7,': '.ucwords($data['data']->keterangan), 'R', 1, 'c');

        $pdf->Cell(190,5,'', 'LR', 1, 'C');

        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'Maka dengan ini kami minta agar saudara segera menyampaikan SPTPD dan lampiran keterangan/dokumen', 'R', 1, 'c');
        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'pendukung paling lambat 7 hari setelah surat ini di terima.', 'R', 1, 'c');

        $pdf->Cell(190,5,'', 'LR', 1, 'C');

        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'Apabila surat Teguran ini tidak juga saudara indahkan, maka kami akan melakukan Penetapan Atas Objek Pajak yang', 'R', 1, 'c');
        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'saudara milki secara jabatan dan dikenakan sanksi Administrasi berupa Kenaikan Pajak dan bunga,', 'R', 1, 'c');
        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'yang akan merugikan Saudara Sendiri.', 'R', 1, 'c');

        $pdf->Cell(190,5,'', 'LR', 1, 'C');

        $pdf->Cell(5,5,'', 'L', 0, 'c');
        $pdf->Cell(185,5,'Untuk menjadikan perhatian Saudara, agar kewajiban Saudara dapat dipenuhi sebagaimana mestinya.', 'R', 1, 'c');

        $pdf->Cell(190,7,'', 'LR', 1, 'C');

        $pdf->Cell(190,3,'', 'LR', 1, 'c');
        $pdf->Cell(100,7,'', 'L', 0, 'c');
        $pdf->Cell(70,7,ucwords($kota->ibukota).', '.tgl_format(date("Y-m-d")), 0, 0, 'C');
        $pdf->Cell(20,7,'', 'R', 1, 'C');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(100,7,'', 'L', 0, 'c');
        $pdf->Cell(70,4,'pak rangga', 0, 0, 'C');
        $pdf->Cell(20,7,'', 'R', 1, 'C');

        $pdf->SetFont('Times', 'B', 11);
        $pdf->Cell(100,20,'', 'L', 0, 'c');
        $pdf->Cell(70,20,'', 'B', 0, 'C');
        $pdf->Cell(20,20,'', 'R', 1, 'C');

        $pdf->SetFont('Times', '', 11);
        $pdf->Cell(100,6,'', 'L', 0, 'c');
        $pdf->Cell(70,6,'NIP.123456765432', 0, 0, 'C');
        $pdf->Cell(20,6,'', 'R', 1, 'C');

        $pdf->Cell(190,5,'', 'LRB', 1, 'C');


        $pdf->Output("SPTPD.pdf","I");
    }

    // public function delete($ids)
    // {
    //     $id     = $ids[0];
    //     $result = $this->m_global->delete('ta_teguran', [$this->table_prefix.'id' => $id]);
    //
    //     if ($result) {
    //         $data['status'] = 1;
    //     } else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode($data);
    // }
    //
    // public function change_status($status, $where)
    // {
    //     // pecah array $where
    //     $out = "";
    //     foreach ($where as $k => $v) {
    //         $out .= "$k $v/";
    //     }
    //     $out = substr($out, 0, -1);
    //
    //     $result = $this->db->query("delete from $status where $out");
    // }

}
