<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends Admin_Controller
{
    private $prefix         = 'penagihan/penagihan_pajak_stpd';
    private $url            = 'penagihan/penagihan_pajak_stpd';
    private $path           = 'penagihan/pajak/stpd';
    private $table_db       = 'ref_rek_4';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }
	
	
	function randomcode(){
		echo sprintf("%06d", mt_rand(1, 999999));
		die();
	}
		
	function nomor()
    {
        $query = $this->db->query("SELECT CONCAT(prefix, LPAD(nomor, digit, 0), postfix) AS nomor FROM m_nomor WHERE type = 'produk'")->row();
		$nomor = $query->nomor;
		if(!strpos($nomor, "%YYYY%") === false){
			$nomor = str_replace('%YYYY%', date('Y'), $nomor);
		}
		echo $nomor;
		die();
    }
		
	function image($type){

		$file = str_replace('system/','',str_replace('\\','/',BASEPATH)).str_replace('./', '', 'upload/1/what.jpg');
		$imginfo = getimagesize($file);
		
		header("Content-type: $imginfo[mime]");	
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	  	header('Pragma: public');
		header('Content-Length: '.filesize($file));
		ob_clean();
		flush();
		readfile($file);
		exit;
	}

	public function index()
	{
		$config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'ssl://smtp.googlemail.com',
            'smtp_port' => 465,
            'smtp_user' => 'masraga.setiawan@gmail.com',
            'smtp_pass' => '123Adevi',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        $this->load->library('email', $config);
        $this->email->set_newline("\r\n");
        $this->email->from('laporan.pbf@gmail.com', 'Administrator E-REPORT PBF');
        $email = str_replace(';', ',', 'masraga.setiawan@gmail.com');
        $this->email->to($email);
        //$this->email->bcc('masraga.setiawan@gmail.com');
        $this->email->subject('test');
        $body = '<html><body style="background: #ffffff; color: #222; font-family: Arial; margin: 20px; color: #363636; font-size:11px;"><table style="font-family: Arial; border-collapse:collapse;"><tr><td style="width:90px;color: #009900;font-size: 20px;" valign="middle">SIMDA</td><td style="color: #222; padding-left:15px; font-size: 20px; border-left:1px solid;"><b>Verifikasi Akun</b><div style="color: #888; font-size: 10px;">CONFIDENTIAL</div></td></tr><tr><td colspan="2" height="5"></td></tr></table><div style="height:15px;"></div><table style="background: #efefef; font-size:11px; color: #444; font-family: Arial;"  cellpadding=3 cellspacing=2><tr><td>Untuk memverifikasi user anda mohon inputkan kode dibawah ini<br>Kode Verifikasi : <a href="'.site_url().'" target="_blank">VRS123</a></td></tr></table><div style="font-size: 10px; color: #888;"><br><b style="color:#222;">Copyright 2018 - PT. CENDANA IT 2000</div></body></html>';
        $this->email->message($body);
		$chk = $this->email->send();
		if($chk){
			echo "berhasul";
		}else{
			echo $this->email->print_debugger();
			echo "gagal";
		}
		die();
        return $this->email->send();
	}
	
	public function upload()
	{
		//mkdir("./upload/aaaazxc", 0777);
		$dir = './upload/1';
		if (file_exists($dir) && is_dir($dir)) {
			mkdir($dir."/2", 0777);
		}else {
            mkdir($dir, 0777);
			mkdir($dir."/2", 0777);
        }
		$config['upload_path'] = $dir. '/' . date('Ymd') . "/";
        //$config['allowed_types'] = $allowed; 
        $config['remove_spaces'] = TRUE;
        //$config['allowed_types'] = 'jpg|jpeg|pdf'; 
        $config['max_size'] = '2000';
        $config['overwrite'] = FALSE;
        $this->load->library('upload', $config);
        foreach ($_FILES as $a => $b) {
            $ar = explode(".", $_FILES[$a]['name']);
            $ext = $ar[count($ar) - 1];
            $ext = strtolower($ext);
            if ($ext == "pdf" || $ext == "jpg" || $ext == "jpeg") {
                $config['file_name'] = $a . '.' . $ext;
                $this->upload->initialize($config);
                $this->upload->display_errors('', '');
                if (!$this->upload->do_upload($a)) {
                    $error = array('error' => $this->upload->display_errors()); 
                    if ($error['error'] == "<p>The file you are attempting to upload is larger than the permitted size.</p>") {
                        //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal dikarenakan ukuran file melebihi ketentuan. Silahkan upload file dengan ukuran kurang dari 2MB.";
                        print '<script type="text/javascript">';
                        print 'alert("Maaf, proses upload Data Perusahaan Anda gagal dikarenakan ukuran file melebihi ketentuan.\nSilahkan upload file dengan ukuran kurang dari 2MB.");';
                        print 'location.href = "' . site_url() . 'front/register/second"';
                        print '</script>';
                        die();
                    }
                    else {
                        //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal.\nSilahkan ulangi kembali.";
                        print '<script type="text/javascript">';
                        print 'alert("Maaf, proses upload Data Perusahaan Anda gagal.\nSilahkan ulangi kembali.");';
                        print 'location.href = "' . site_url() . 'front/register/second"';
                        print '</script>';
                        die();
                    }
                }
                else {
                    $up = $this->upload->data();
                }
            }
            else {
                //$ret = "msg#error#Maaf, proses upload Data Perusahaan Anda gagal dikarenakan tipe data yang tidak sesuai. Silahkan upload file dengan  tipe data *.jpeg, *.jpg atau *.pdf.";
                print '<script type="text/javascript">';
                print 'alert("Maaf, proses upload Data Perusahaan Anda gagal dikarenakan tipe data yang tidak sesuai.\nSilahkan upload file dengan  tipe data *.jpeg, *.jpg atau *.pdf.");';
                print 'location.href = "' . site_url() . 'front/register/second"';
                print '</script>';
                die();
            }
        }
		die('xxxx');
        $data['pagetitle']  = 'STPD Pajak';
        $data['subtitle']   = 'manage STPD pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Penetapan' => null, 'Pajak' => null, 'STPD' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( $this->path.'/index', $data, $js, $css );
	}

    public function select()
    {
        $join = [
                    'ref_pemungutan' => ['ref_pemungutan', 'ref_rek_4.jns_pemungutan = ref_pemungutan.jn_pemungutan', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'ref_rek_4.nm_rek_4',
            'nm_jn_pemungutan' => 'ref_pemungutan.nm_jn_pemungutan'
        ];

        $where    = null;
        $where_e  = "ref_rek_4.id_rek_kegunaan = 2 and id_rek_4 != 72 and id_rek_4 != 15";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_rek_4.jns_pemungutan, ref_rek_4.nm_usaha_4, ref_rek_4.id_rek_4, ref_rek_4.link, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              ucwords($rows->nm_rek_4),
              ucwords ($rows->nm_jn_pemungutan),
              '<a href="'.base_url('penetapan/penetapan_pajak_np_sub/'.$rows->id_rek_4.'/'.$rows->nm_usaha_4).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="SPTPD Masa"><i class="fa fa-file-text"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

}

/* End of file Penetapan_pajak_np.php */
/* Location: ./application/modules/data_entry/controllers/Penetapan_pajak_np.php */
