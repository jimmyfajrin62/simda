<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan_pajak_sspd_sanksi_sub extends Admin_Controller
{
    private $prefix         = 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi_sub';
    private $url            = 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi_sub';
    private $path           = 'bendahara_penerimaan/pajak/sanksi/';
    private $path_sspd      = 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi';
    private $table_db       = 'ta_sspd';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_penerimaan_sspd_masa', 'mdb');

        if ($this->uri->segment(4) == 4) {
            $this->table_db = 'ta_kartu_pajak_hotel';
        } else if ($this->uri->segment(4) == 5) {
            $this->table_db = 'ta_kartu_pajak_restoran';
        } else if ($this->uri->segment(4) == 6) {
            $this->table_db = 'ta_kartu_pajak_hiburan';
        } else if ($this->uri->segment(4) == 7) {
            $this->table_db = 'ta_kartu_pajak_reklame';
        } else if ($this->uri->segment(4) == 8) {
            $this->table_db = 'ta_kartu_pajak_penerangan';
        } else if ($this->uri->segment(4) == 9) {
            $this->table_db = 'ta_kartu_pajak_mineral';
        } else if ($this->uri->segment(4) == 10) {
            $this->table_db = 'ta_kartu_pajak_parkir';
        } else if ($this->uri->segment(4) == 11) {
            $this->table_db = 'ta_kartu_pajak_air';
        } else if ($this->uri->segment(4) == 12) {
            $this->table_db = 'ta_kartu_pajak_walet';
        } else if ($this->uri->segment(4) == 15) {
            $this->table_db = 'ta_kartu_bphtb';
        }
    }

    public function _remap($method, $args)
    {
        if (method_exists($this, $method)) {
            $this->$method($args);
        } else {
            $this->index($method, $args);
        }
    }

    // Wajib Pajak
    public function index($ids, $title)
    {
        $title = str_replace('%20', ' ', $title[0]);
        $data ['kd_rek_4']  = $ids;
        $data['title']      = $title;

        $data['pagetitle']   = "SSPD Sanksi $title";
        $data['subtitle']    = "SSPD Sanksi $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => null];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'wp', $data, $js, $css);
    }

    // Wajib Pajak
    public function select($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4 = $ids[0];
        $title = str_replace('%20', ' ', $ids[1]);

        $this->table_db = 'ta_kartu_pajak_pungut';

        $join = [
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak', 'ta_kartu_pajak_pungut.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha', 'wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak', 'wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum', 'wp_wajib_pajak.data_umum_id = wp_data_umum.id', 'LEFT'],
                'ta_sspd'                    => ['ta_sspd', 'ta_kartu_pajak_pungut.id = ta_sspd.pungut_id', 'LEFT'],
                'ta_nota'                    => ['ta_nota', 'ta_sspd.id = ta_nota.sspd_id', 'LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'npwpd'          => 'wp_data_umum.npwpd',
            'nama_pendaftar' => 'wp_data_umum.nama_pendaftar',
            'nm_usaha'       => 'wp_wajib_pajak_usaha.nm_usaha',
            'alamat_usaha'   => 'wp_wajib_pajak_usaha.alamat_usaha',
            'tgl_aktif'      => 'wp_wajib_pajak.tgl_aktif',
        ];

        $where    = null;
        $where_e  = "wp_wajib_pajak_usaha_pajak.jns_pajak = $kd_rek_4 and ta_nota.jenis_nota = 'tambahan' and ta_nota.jenis_skpd = 'KBT' and ta_nota.sanksi_id != 0";
        $group    = "wp_data_umum.id";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e, $group);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'wp_data_umum.id as wp, ta_kartu_pajak_pungut.id, ta_nota.sanksi_id, ta_nota.jenis_nota, ta_nota.jenis_skpd, wp_wajib_pajak_usaha_pajak.jns_pajak, ta_kartu_pajak_pungut.status, ta_kartu_pajak_pungut.lastupdate,wp_data_umum.user_id, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength, $group);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                $i,
                '<span class="label label-m label-primary">'.strtoupper($rows->npwpd).'</span>',
                strtoupper($rows->nama_pendaftar),
                strtoupper($rows->nm_usaha),
                strtoupper($rows->alamat_usaha),
                strtoupper($rows->tgl_aktif),
                '<a href="'.base_url($this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$rows->wp.'/'.$rows->user_id).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="Lihat SSPD"><i class="fa fa-file-text"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // SSPD
    public function show_sspd($ids)
    {
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[3];

        $data ['kd_rek_4']  = $kd_rek_4;
        $data ['title']     = $title;
        $data ['wp']        = $wp;
        $data ['user_id']   = $user_id;
        $data['head']       = $this->mdb->header($wp);

        $data['pagetitle']  = "SSPD Sanksi $title";
        $data['subtitle']   = "SSPD Sanksi $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak' => null, 'SSPD Sanksi' => $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => null];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sspd', $data, $js, $css);
    }

    // SSPD
    public function select_sspd($ids)
    {
        $kd_rek_4  = $ids[0];
        $title     = str_replace('%20', ' ', $ids[1]);
        $wp        = $ids[2];
        $user_id   = $ids[3];

        $this->table_db = 'ta_sspd a';

        $join = [
                'ta_kartu_pajak_pungut'      => ['ta_kartu_pajak_pungut b', 'a.pungut_id = b.id', 'LEFT'],
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak c', 'b.wp_usaha_pajak_id = c.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha d', 'c.wp_usaha_id = d.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak e', 'd.wp_id = e.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum f', 'e.data_umum_id = f.id', 'LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_sspd'         => 'a.no_sspd',
            'total_pajak'     => 'a.total_pajak',
            'total_bayar'     => 'a.total_bayar',
            'pajak_terhutang' => 'a.pajak_terhutang',
            'keterangan'      => 'a.keterangan',
        ];

        $where    = null;
        $where_e  = "a.jns_pajak = $kd_rek_4 AND f.id = $wp AND a.jns_sspd = 3";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.status = '$request' and a.jns_pajak = $kd_rek_4 AND f.id = $wp  AND a.jns_sspd = 3";
        } else {
            $where_e = "a.status = '1' and a.jns_pajak = $kd_rek_4 AND f.id = $wp  AND a.jns_sspd = 3";
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.status_nota, a.id, a.pungut_id, a.status, a.lastupdate, f.npwpd, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                '<span class="label label-m label-primary">'.strtoupper($rows->no_sspd).'</span>',
                uang($rows->total_pajak),
                uang($rows->total_bayar),
                $rows->pajak_terhutang == 0 ? 'LUNAS' : uang($rows->pajak_terhutang),
                strtoupper($rows->keterangan),
                $rows->status_nota == 1 && $this->session->user_data->user_role != 1?
                '<a data-original-title="Lihat SSPD Rincian" href="'.base_url($this->url.'/show_sspd_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" class="ajaxify btn blue-steel btn-icon-only tooltips"><i class="fa fa-folder-open"></i></a>'.
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'.
                '<a class="btn btn-icon-only grey tooltips" disabled data-original-title="Tidak bisa dirubah karena SPTPD sudah didata"><i class="fa fa-lock"></i></a>'
                :
                $this->session->user_data->user_role != 1?
                '<a data-original-title="Lihat SSPD Rincian" href="'.base_url($this->url.'/show_sspd_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" class="ajaxify btn blue-steel btn-icon-only tooltips"><i class="fa fa-folder-open"></i></a>'.
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sspd/'.
                                        ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sspd/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>'
                :
                '<a data-original-title="Lihat SSPD Rincian" href="'.base_url($this->url.'/show_sspd_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" class="ajaxify btn blue-steel btn-icon-only tooltips"><i class="fa fa-folder-open"></i></a>'.
                '<a href="'.base_url($this->url.'/print_pdf/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$rows->id.'/'.$rows->pungut_id).'" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export PDF"><i class="fa fa-file-pdf-o"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_admin/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id.'/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sspd/'.
                                        ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sspd/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '/false" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // SSPD
    public function show_add($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[2];

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['wp']         = $wp;
        $data['user_id']    = $user_id;

        $data['bank']       = $this->mdb->show_bank();
        $data['ttd']        = $this->mdb->petugas();

        $data['pagetitle']   = "Pajak $title";
        $data['subtitle']    = "Pajak $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => $this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id, 'Form' => null];

        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'form_sspd', $data, $js, $css);
    }

    // SSPD
    public function show_edit($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[3];
        $id                 = $ids[4];

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['wp']         = $wp;
        $data['user_id']    = $user_id;
        $data['id']         = $id;
        $data['bank']       = $this->mdb->show_bank();
        $data['ttd']        = $this->mdb->petugas();
        $data['data']       = $this->mdb->show_sspd($id);

        $data['pagetitle']  = "Pajak $title";
        $data['subtitle']   = "Pajak $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => $this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id, 'Form' => null];

        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'form_sspd', $data, $js, $css);
    }

    // SSPD ADMIN
    public function show_edit_admin($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $title              = str_replace('%20', ' ', $ids[1]);
        $wp                 = $ids[2];
        $user_id            = $ids[3];
        $id                 = $ids[4];

        $data['kd_rek_4']   = $kd_rek_4;
        $data['title']      = $title;
        $data['wp']         = $wp;
        $data['user_id']    = $user_id;
        $data['id']         = $id;
        $data['bank']       = $this->mdb->show_bank();
        $data['ttd']        = $this->mdb->petugas();
        $data['data']       = $this->mdb->show_sspd($id);

        $data['pagetitle']  = "Pajak $title";
        $data['subtitle']   = "Pajak $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => $this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$user_id, 'Form' => null];

        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'form_sspd_admin', $data, $js, $css);
    }

    // SSPD
    public function action_form($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->form_validation->set_rules('tgl_sspd', 'Tgl SSPD', 'trim|required');
        $this->form_validation->set_rules('nm_penyetor', 'Nama', 'trim|required');
        $this->form_validation->set_rules('alamat_penyetor', 'Alammat', 'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan', 'trim');

        if ($this->form_validation->run($this)) {
            // perhitungan pajak
            $pajak      = $this->input->post('total_pajak');
            $bayar      = $this->input->post('total_bayar');
            $hutang     = ((int)$pajak) - ((int)$bayar);

            $data[$this->table_prefix.'pajak_terhutang'] = $hutang;
            $data[$this->table_prefix.'jns_sspd']        = 3;
            $data[$this->table_prefix.'skpd_no']         = $this->input->post('no_skpd');
            $data[$this->table_prefix.'jns_pajak']       = $this->input->post('jns_pajak');
            $data[$this->table_prefix.'tgl_sspd']        = $this->m_global->setdateformat($this->input->post('tgl_sspd'));
            $data[$this->table_prefix.'masa1']           = $this->m_global->setdateformat($this->input->post('masa1'));
            $data[$this->table_prefix.'masa2']           = $this->m_global->setdateformat($this->input->post('masa2'));
            $data[$this->table_prefix.'keterangan']      = $this->input->post('keterangan');
            $data[$this->table_prefix.'jns_pembayaran']  = $this->input->post('jns_pembayaran');
            $data[$this->table_prefix.'kd_bank']         = $this->input->post('kd_bank') == '' ? NULL : $this->input->post('kd_bank');
            $data[$this->table_prefix.'total_pajak']     = str_replace(['Rp', ',', ' '], '', $this->input->post('total_pajak'));
            $data[$this->table_prefix.'total_bayar']     = str_replace(['Rp', ',', ' '], '', $this->input->post('total_bayar'));
            $data[$this->table_prefix.'ttd_dok_id']      = $this->input->post('ttd_dok_id');
            $data[$this->table_prefix.'nm_penyetor']     = $this->input->post('nm_penyetor');
            $data[$this->table_prefix.'alamat_penyetor'] = $this->input->post('alamat_penyetor');
            $data[$this->table_prefix.'rek_penyetor']    = $this->input->post('rek_penyetor');

            if ($ids == null) {
                $date = date("d/m/Y");
                $sspd = $this->mdb->no_sspd('S', "/SSPD/$date");

                $data[$this->table_prefix.'id']        = $this->mdb->auto_id();
                $data[$this->table_prefix.'pungut_id'] = $this->input->post('pungut_id');
                $data[$this->table_prefix.'skpd_id']   = $this->input->post('skpd_id');
                $data[$this->table_prefix.'tahun']     = date('Y');
                $data[$this->table_prefix.'no_sspd']   = $sspd;

                // update sptpd
                $id_pungut = $this->input->post('pungut_id');
                $data2[$this->table_prefix.'status_sspd'] = 1;
                $result2 = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);
                //update SKPD
                $user_id = $this->input->post('user_id');
                $data3[$this->table_prefix.'sspd_process'] = 1;
                $result3 = $this->m_global->update('ta_skpd', $data3, ['user_id' => $user_id]);
                // instert sspd
                $result  = $this->m_global->insert($this->table_db, $data);

                //data log
                $log['id']      = $this->input->post('user_id');
                $log['action']  = 'Add Pajak SSPD Sanksi';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'No. SSPD : '.$this->input->post('no_sspd').' Jenis SSPD : Sanksi, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sspd_log', $log);

            } else {
                $id = $ids[0];

                //data log
                $log['id']      = $this->input->post('user_id');
                $log['action']  = 'Edit Pajak SSPD Sanksi ';
                $detail = '';
                foreach ($this->input->post() as $key => $value) {
                    $detail.= ' '.$key.' = '.$value.', ';
                }
                $log['detail']  = 'No. SSPD : '.$this->input->post('no_sspd').' Jenis SSPD : Sanksi, Input User = '.$detail;
                $log['status']  = '1';
                $log['user_id'] = $this->session->user_data->user_id;
                $log['ip']      = $_SERVER['REMOTE_ADDR'];
                $this->db->insert('sspd_log', $log);

                // update sspd
                $result = $this->m_global->update($this->table_db ='ta_sspd', $data, ['id' => $id]);
            }

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_penyetor').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // Rincian SSPD
    public function show_sspd_rinci($ids)
    {
        // echo $ids; exit();
        $kd_rek_4          = $ids[0];
        $title             = str_replace('%20', ' ', $ids[1]);
        $wp                = $ids[2];
        $id                = $ids[3];
        $sptpd_id          = $ids[4];

        $data ['kd_rek_4'] = $kd_rek_4;
        $data ['title']    = $title;
        $data ['wp']       = $wp;
        $data ['sspd_id']  = $id;
        $data['head']      = $this->mdb->header($wp);
        $data ['sptpd_id'] = $sptpd_id;

        $data['pagetitle']   = "SSPD Sanksi $title Rinci";
        $data['subtitle']    = "SSPD Sanksi $title Rinci";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => $this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$id, 'Rincian Sanksi' => null];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'sspd_rinci', $data, $js, $css);
    }

    // Rincian SSPD
    public function select_sspd_rinci($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4 = $ids[0];
        $title    = str_replace('%20', ' ', $ids[1]);
        $wp       = $ids[2];
        $id       = $ids[3];
        $sptpd_id = $ids[4];


        $this->table_db = "ta_nota a";

        $join = [
            'ta_kartu_pajak_pungut b' => ['ta_kartu_pajak_pungut b', 'a.sptpd_id = b.id','LEFT'],
            'ta_sspd c' => ['ta_sspd c', 'b.id = c.pungut_id','LEFT'],
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            // 'nm_rek_6'        => 'b.nm_rek_6',
            // 'tarif_pajak'     => 'a.tarif_pajak',
            // 'dasar_pengenaan' => 'a.dasar_pengenaan',
            // 'pajak_terhutang' => 'a.pajak_terhutang'
        ];

        $where    = null;
        $where_e = "a.sptpd_id = $sptpd_id and c.id = $id and c.jns_sspd = 3 and a.sanksi_id = 0";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $request = $_REQUEST['filterstatus'];
            $where_e = "a.sptpd_id = $sptpd_id and c.id = $id and c.jns_sspd = 3 and a.sanksi_id = 0";
        } else {
            $where_e = "a.sptpd_id = $sptpd_id and c.id = $id and c.jns_sspd = 3 and a.sanksi_id = 0";
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

         $select = 'a.keterangan,a.total_bayar,a.total_pajak_real,c.no_sspd '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->no_sspd),
                uang($rows->total_bayar),
                uang($rows->total_pajak_real),
                $rows->keterangan,
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // SSPD
    public function show_edit_sanksi($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4         = $ids[0];
        $title            = str_replace('%20', ' ', $ids[1]);
        $wp               = $ids[2];
        $sspd_id          = $ids[3];
        $id               = $ids[4];

        $data['kd_rek_4'] = $kd_rek_4;
        $data['title']    = $title;
        $data ['wp']      = $wp;
        $data['sspd_id']  = $sspd_id;
        $data['id']       = $id;

        $data['data']       = $this->mdb->show_nota_rinci($id);
        $data['get']       = $this->mdb->get_total_pajak($id);

        $data['pagetitle']  = "Pajak $title";
        $data['subtitle']   = "Pajak $title";

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Bendahara Penerimaan' => null, 'Pajak'=>null, 'SSPD Sanksi'=> $this->path_sspd, $title => $this->url.'/'.$kd_rek_4.'/'.$title, 'SSPD/WP' => $this->url.'/show_sspd/'.$kd_rek_4.'/'.$title.'/'.$wp,'Rincian Sanksi' => $this->url.'/show_sspd_rinci/'.$kd_rek_4.'/'.$title.'/'.$wp.'/'.$id, 'Form' => null];

        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display($this->path.'form_sspd_sanksi', $data, $js, $css);
    }

    // SSPD
    public function action_form_sanksi($ids = null)
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        $this->form_validation->set_rules('total_bayar', 'Total Bayar', 'trim|required');


        if ($this->form_validation->run($this)) {
            // perhitungan pajak
            $total      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_sanksi'));
            $bayar      = str_replace(['Rp', ',', ' '], '', $this->input->post('total_bayar'));
            $hutang     = $total - $bayar;

            $data[$this->table_prefix.'kurang_bayar'] = str_replace(['Rp', ',', ' '], '',$hutang);
            $data[$this->table_prefix.'total_bayar']  = str_replace(['Rp', ',', ' '], '', $this->input->post('total_bayar'));
            $data[$this->table_prefix.'nota_id']      = $this->input->post('nota_id');

            $id = $ids[0];
            $result = $this->m_global->update($this->table_db = 'ta_nota_rinc', $data, ['id' => $id]);
            // echo $this->db->last_query($result);exit();

            if ($result) {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nama').'</strong>';

                echo json_encode($data);
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nama').'</strong>';

                if (ENVIRONMENT == 'development') {
                    $data['error']  = $this->db->error();
                }

                echo json_encode($data);
            }
        } else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace($str, $str_replace, validation_errors());

            echo json_encode($data);
        }
    }

    // Modal
    public function select_skpd($ids)
    {
        // echo "<pre>",print_r($ids),exit();
        $kd_rek_4           = $ids[0];
        $wp                 = $ids[1];

        $this->table_db1 = 'ta_kartu_pajak_pungut a';

        $join = [
                'wp_wajib_pajak_usaha_pajak' => ['wp_wajib_pajak_usaha_pajak b', 'a.wp_usaha_pajak_id = b.id', 'LEFT'],
                'wp_wajib_pajak_usaha'       => ['wp_wajib_pajak_usaha c', 'b.wp_usaha_id = c.id', 'LEFT'],
                'wp_wajib_pajak'             => ['wp_wajib_pajak d', 'c.wp_id = d.id', 'LEFT'],
                'wp_data_umum'               => ['wp_data_umum e', 'd.data_umum_id = e.id', 'LEFT'],
                'ta_sspd'                    => ['ta_sspd f', 'a.id = f.pungut_id', 'RIGHT'],
                'ta_nota'                    => ['ta_nota g', 'f.id = g.sspd_id', 'RIGHT'],
                'ta_skpd'                    => ['ta_skpd h', 'g.id = h.nota_id', 'RIGHT']
        ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];
            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'no_skpd'    => 'h.no_skpd',
            'tgl_skpd'   => 'h.tgl_skpd',
            'keterangan' => 'g.keterangan',
        ];

        $where    = null;
        $where_e  = "e.id = $wp and b.jns_pajak = $kd_rek_4 and g.jenis_skpd = 'KBT'";

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
            $where['a.status']       = $_REQUEST['filterstatus'];
        } else {
            $where['a.status <>']    = '99';
        }

        $keys            = array_keys($aCari);
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count($this->table_db1, $join, $where, $where_e);
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'e.id, h.id as id2, h.status, h.lastupdate, h.no_skpd,h.tgl_skpd, h.masa1, h.masa2, g.total_pajak_real, g.total_bayar, (g.total_pajak_real - g.total_bayar) as hasil, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db1, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query();exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                strtoupper($rows->no_skpd),
                tgl_format($rows->tgl_skpd),
                strtoupper($rows->keterangan),
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id2.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        // echo "<pre>",print_r($result),exit();
        echo json_encode($records);
    }

    // Modal
    public function get_skpd()
    {
        // echo "<pre>",print_r($this->input->post()),exit();
        $id               = $this->input->post('id');
        $sptpd            = $this->m_global->get('ta_skpd ', null, ['id' => $id] )[0];
        $total_pajak      = $this->mdb->sum_sanksi($sptpd->nota_id);

        $data['records']  = $sptpd;
        $data['masa1'] = date("d-m-Y", strtotime($data['records']->masa1));
        $data['masa2'] = date("d-m-Y", strtotime($data['records']->masa2));
        $data['records2'] = $total_pajak;

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($ids)
    {
        // echo "<pre>",print_r($id),exit();
        $id     = $ids[0];
        $table  = $ids[1];
        $status = $ids[2];
        $stat   = $ids[3];

        if ($stat == 'true') {
            // update sptpd
            $pungut               = $this->db->query("SELECT pungut_id from ta_sspd  where id = $id")->row();
            $id_pungut            = $pungut->pungut_id;
            $data2['status_sspd'] = 0;
            $result2              = $this->m_global->update('ta_kartu_pajak_pungut', $data2, ['id' => $id_pungut]);

            $result               = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result               = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

     public function print_pdf($ids){

       $kd_rek_4  = $ids[0];
        $title     = str_replace('%20', ' ', $ids[1]);
        $wp        = $ids[2];
        $id        = $ids[3];
        $pungut_id = $ids[4];
    
        $data['records'] = $this->mdb->data($pungut_id);
        
        $data['records2'] = $this->mdb->datapdf($id, $pungut_id);
        //print_r($data['records2']);exit();
        $data['jumlah'] = $this->mdb->jumlah($pungut_id);

        $this->load->library('fpdf_gen');
        $pdf = new fpdf('P','mm','A4');

        $kota = $this->mdb->kota();

        $pdf->AddPage();
        $pdf->SetAutoPageBreak(true, 0);
        $pdf->SetFont('Times', 'B', 11);

        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,10,10,20,20);

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(25,7,'', 'LTR', 0, 'C');
        $pdf->Cell(105,7,$kota->nm_pemda, 'LTR', 0, 'C');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(60,7,'SSPD', 'LTR', 1, 'C');

        $pdf->SetFont('Times', 'B', 12);
        $pdf->Cell(25,7,'', 'LR', 0, 'C');
        $pdf->Cell(105,7,'BADAN PENDAPATAN DAERAH', 'LR', 0, 'C');

        $pdf->SetFont('Times', 'B', 9);
        $pdf->Cell(60,7,'(SURAT SETORAN PAJAK DAERAH)', 'R', 1, 'C');

        $pdf->SetFont('Times', '', 8);
        $pdf->Cell(25,7,'', 'LR', 0, 'C');
        $pdf->Cell(105,7,'', 'LR', 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(60,7,'Tahun '.$kota->tahun, 'R', 1, 'C');

        $pdf->SetFont('Times', '', 9);
        $pdf->Cell(25,7,'', 'LBR', 0, 'C');
        $pdf->Cell(105,7,$kota->alamat, 'LBR', 0, 'C');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(60,7,'', 'BR', 1, 'C');

        $pdf->SetFont('Times', 'B', 10);
        $pdf->Cell(110,7,'', 'L', 0, 'C');
        $pdf->Cell(80,7,'Nomor     : '.$data['records']->no_sspd, 'R', 1, 'L');

        $pdf->SetFont('Times', 'B', 9);
        $pdf->Cell(110,7,'', 'L', 0, 'C');
        $pdf->Cell(80,7,'Tanggal     : '.tgl_format(date("Y-m-d")), 'R', 1, 'L');

        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(40,6,'Nama', 'L', 0, 'c');
        $pdf->Cell(150,6,':  '.$data['records']->nama_pendaftar, 'R', 1, 'c');

        $pdf->Cell(40,6,'Alamat', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->jalan.',RT/RW '.$data['records']->rtrw.',Kel. '.$data['records']->nm_kel.',Kec.'.$data['records']->nm_kec.', Kab/Kota. '.$data['records']->kabupaten.', Kode Pos. '.$data['records']->kode_pos, 'R', 1, 'c');

        $pdf->Cell(40,6,'Nama Usaha', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->nm_usaha, 'R', 1, 'c');

        $pdf->Cell(40,6,'Alamat Usaha', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->alamat_usaha, 'R', 1, 'c');

        $pdf->Cell(40,6,'NPWPD', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->npwpd, 'R', 1, 'c');

        $pdf->Cell(40,7,'Menyetor berdasarkan', 'L', 0, 'c');
        $pdf->Cell(4,7,':', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S K P D', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S T P D', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->SetFont('ZapfDingbats', '', 10);
        $pdf->Cell(5, 5, 4, 1, 0, 'L');
        $pdf->SetFont('Times', '', 10);
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S K P D K B T', 0, 0, 'c');
        $pdf->Cell(43,7,'', 'R', 1, 'c');

        $pdf->Cell(40,7,'', 'L', 0, 'c');
        $pdf->Cell(4,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S K P D K B', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->Cell(5, 5, '', 1, 0, 'L');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S P T P D', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'SK Keberatan', 0, 0, 'c');
        $pdf->Cell(43,7,'', 'R', 1, 'c');

        $pdf->Cell(40,7,'', 'L', 0, 'c');
        $pdf->Cell(4,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'S K P D K B', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'SK Pembetulan', 0, 0, 'c');
        $pdf->Cell(26,7,'', 0, 0, 'c');
        $pdf->Cell(5,5, '',1, 0, 'c');
        $pdf->Cell(2,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,'Lain - lain', 0, 0, 'c');
        $pdf->Cell(43,7,'', 'R', 1, 'c');

        $pdf->Cell(40,6,'Dokumen Penetapan', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->no_sptpd, 'R', 1, 'c');

        $pdf->Cell(40,6,'Masa Pajak', 'L', 0, 'c');
        $pdf->Cell(150,6,': '.$data['records']->masa1.' S/D '.$data['records']->masa2, 'R', 1, 'c');

        if ($data['records']->jns_pembayaran == 1) {
            $pdf->Cell(40,7,'Menyetor berdasarkan', 'L', 0, 'c');
            $pdf->Cell(4,7,':', 0, 0, 'c');
            $pdf->SetFont('ZapfDingbats', '', 10);
            $pdf->Cell(5, 5, 4, 1, 0, 'L');
            $pdf->SetFont('Times', '', 10);
            $pdf->Cell(2,7,'', 0, 0, 'c');
            $pdf->Cell(10,7,'Tunai', 0, 0, 'c');
            $pdf->Cell(26,7,'', 0, 0, 'c');
            $pdf->Cell(5,5, '',1, 0, 'c');
            $pdf->Cell(2,7,'', 0, 0, 'c');
            $pdf->Cell(10,7,'Bank', 0, 0, 'c');
            $pdf->Cell(86,7,'', 'R', 1, 'c');

        }

        else {
            $pdf->Cell(40,7,'Menyetor berdasarkan', 'L', 0, 'c');
            $pdf->Cell(4,7,':', 0, 0, 'c');
            $pdf->Cell(5,5, '',1, 0, 'c');
            $pdf->Cell(2,7,'', 0, 0, 'c');
            $pdf->Cell(10,7,'Tunai', 0, 0, 'c');
            $pdf->Cell(26,7,'', 0, 0, 'c');
            $pdf->SetFont('ZapfDingbats', '', 10);
            $pdf->Cell(5, 5, 4, 1, 0, 'L');
            $pdf->SetFont('Times', '', 10);
            $pdf->Cell(2,7,'', 0, 0, 'c');
            $pdf->Cell(10,7,'Bank', 0, 0, 'c');
            $pdf->Cell(86,7,'', 'R', 1, 'c');

        }



        $pdf->Cell(40,7,'Bank Penerima Setoran', 'L', 0, 'c');
        $pdf->Cell(4,7,':', 0, 0, 'c');
        $pdf->Cell(10,7,$data['records']->nm_bank.' ,', 0, 0, 'c');
        $pdf->Cell(10,7,'', 0, 0, 'c');
        $pdf->Cell(1,7,'', 0, 0, 'c');
        $pdf->Cell(10,7,' No. Rek : 35836576568534534', 0, 0, 'c');
        $pdf->Cell(115,7,'', 'R', 1, 'c');

        $pdf->Cell(40,6,'Uraian', 'L', 0, 'c');
        $pdf->Cell(150,6,':   '.$data['records']->keterangan, 'R', 1, 'c');

        $pdf->Cell(190,10,'Jumlah Pembayaran dan pajak terhutang untuk masa pajak sekarang (lampiran foto copy dokumen)', 'LR', 1, 'L');

        $pdf->Cell(5,7,'', 'L', 0, 'c');
        $pdf->Cell(15,7,'No', 1, 0, 'C');
        $pdf->Cell(65,7,'Total bayar', 1, 0, 'C');
        $pdf->Cell(60,7,'Total pajak real', 1, 0, 'C');
        $pdf->Cell(40,7,'Nilai (Rp)', 1, 0, 'C');
        $pdf->Cell(5,7,'', 'R', 1, 'c');

        $no = 1;
        //print_r($data['records2']);exit();
     

        if($data['records2']){

            $pdf->Cell(5,7,'', 'L', 0, 'c');
            $pdf->Cell(15,7,'1', 1, 0, 'C');
            $pdf->Cell(65,7,uang($data['records2']->total_bayar), 1, 0, 'C');
            $pdf->Cell(60,7,uang($data['records2']->total_pajak_real), 1, 0, 'C');
            $pdf->Cell(40,7,uang($data['records2']->total_pajak_real-$data['records2']->total_bayar), 1, 0, 'C');
            $pdf->Cell(5,7,'', 'R', 1, 'c');

            $pdf->Cell(5,7,'', 'L', 0, 'c');
            $pdf->Cell(15,7,'', 'LB', 0, 'C');
            $pdf->Cell(65,7,'', 'B', 0, 'C');
            $pdf->Cell(60,7,'Jumlah', 'B', 0, 'C');
            $pdf->Cell(40,7,uang($data['records2']->total_pajak_real-$data['records2']->total_bayar), 'LBR', 0, 'C');
            $pdf->Cell(5,7,'', 'LR', 1, 'C');
            
        }else{
            $pdf->Cell(5,7,'', 'L', 0, 'c');
            $pdf->Cell(15,7,' ', 1, 0, 'C');
            $pdf->Cell(65,7,' ', 1, 0, 'C');
            $pdf->Cell(60,7,' ', 1, 0, 'C');
            $pdf->Cell(40,7,' ', 1, 0, 'C');
            $pdf->Cell(5,7,'', 'R', 1, 'c');

            $pdf->Cell(5,7,'', 'L', 0, 'c');
            $pdf->Cell(15,7,'', 'LB', 0, 'C');
            $pdf->Cell(65,7,'', 'B', 0, 'C');
            $pdf->Cell(60,7,'Jumlah', 'B', 0, 'C');
            $pdf->Cell(40,7,' ', 'LBR', 0, 'C');
            $pdf->Cell(5,7,'', 'LR', 1, 'C');
        }

        $pdf->Cell(190,6,'', 'LR', 1, 'L');
        $pdf->Cell(20,5,'Terbilang :', 'L', 0, 'L');
        $pdf->Cell(170,5,ucwords(terbilang($data['jumlah']->jumlah)), 'R', 1, 'L');
        $pdf->Cell(190,5,'', 'LRB', 1, 'L');

        $pdf->SetFont('Times', '', 9);

        $pdf->Cell(40,7,'Ruang untuk Teraan', 'LR', 0, 'C');
        $pdf->Cell(75,7,'Diterima Oleh :', 'LR', 0, 'C');
        $pdf->Cell(75,7,$kota->ibukota.', '.tgl_format(date("Y-m-d")), 'R', 1, 'C');

        $pdf->Cell(40,4,'Kas Register/Tanda Tangan', 'LR', 0, 'C');
        $pdf->Cell(75,4,'', 'LR', 0, 'C');
        $pdf->Cell(75,4,'Penyetor', 'R', 1, 'C');

        $pdf->Cell(40,4,'Petugas Penerima', 'LR', 0, 'C');
        $pdf->Cell(75,4,'', 'LR', 0, 'C');
        $pdf->Cell(75,4,'', 'R', 1, 'C');

        $pdf->Cell(40,30,'', 'LR', 0, 'C');
        $pdf->Cell(75,30,'', 'R', 0, 'C');
        $pdf->Cell(75,30,'', 'R', 1, 'C');

        $pdf->Cell(40,5,'', 'LR', 0, 'C');
        $pdf->Cell(6,5,'', 0, 0, 'C');
        $pdf->Cell(64,5,'', 'B', 0, 'C');
        $pdf->Cell(5,5,'', 'R', 0, 'C');
        $pdf->Cell(6,5,'', 0, 0, 'C');
        $pdf->Cell(64,5,$data['records']->nm_penyetor, 'B', 0, 'C');
        $pdf->Cell(5,5,'', 'R', 1, 'C');

        $pdf->Cell(40,5,'', 'LRB', 0, 'C');
        $pdf->Cell(6,5,'', 'B', 0, 'C');
        $pdf->Cell(64,5,'NIP.', 'B', 0, 'C');
        $pdf->Cell(5,5,'', 'B', 0, 'C');
        $pdf->Cell(6,5,'', 'LB', 0, 'C');
        $pdf->Cell(64,5,'', 'B', 0, 'C');
        $pdf->Cell(5,5,'', 'BR', 1, 'C');


        $pdf->Cell(190,40,'', 'LRB', 1, 'L');




        // lanjutkan

        $pdf->Output("SPTPD.pdf","I");
          }
        }
