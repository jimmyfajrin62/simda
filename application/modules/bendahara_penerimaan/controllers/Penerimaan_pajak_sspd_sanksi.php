<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class penerimaan_pajak_sspd_sanksi extends Admin_Controller
{
    private $prefix         = 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi';
    private $url            = 'bendahara_penerimaan/penerimaan_pajak_sspd_sanksi';
    private $path           = 'bendahara_penerimaan/pajak/sanksi';
    private $table_db       = 'ref_rek_4';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'Bendahara Penerimaan Pajak SSPD Sanksi';
        $data['subtitle']   = 'manage bendahara penerimaan pajak SSPD sanksi';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = ['Bendahara Penerimaan' => null, 'Pajak' => null, 'SSPD Sanksi' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( $this->path.'/index', $data, $js, $css );
	}

    public function select()
    {

        $user_id = $this->session->user_data->user_id;
        
        $this->table_db = 'ref_rek_4 a';

        $join = [
                    'ref_pemungutan' => ['ref_pemungutan b', 'a.jns_pemungutan = b.jn_pemungutan', 'LEFT']
                ];

        if (@$_REQUEST['customActionType'] == 'group_action') {
            $aChk = [0, 1, 99];

            if (in_array(@$_REQUEST['customActionName'], $aChk)) {
                $this->change_status($_REQUEST['customActionName'], ['id'.' IN ' => "('".implode("','", $_REQUEST['id'])."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_4'         => 'a.nm_rek_4',
            'nm_jn_pemungutan' => 'b.nm_jn_pemungutan'
        ];

        $where    = null;
        // query session petugas
        $query = $this->db->query("select * from user_rek_4 where user_id = $user_id")->row();

        if ($this->session->user_data->user_role == 1) {
        $where_e  = "a.id_rek_kegunaan = 2 and a.id_rek_4 != 72 and a.id_rek_4 != 15";            
        } else {
        $where_e  = "a.id_rek_kegunaan = 2 and a.id_rek_4 in ($query->id_rek_4)";   
        }

        if (@$_REQUEST['action'] == 'filter') {
            $where = [];
            foreach ($aCari as $key => $value) {
                if ($_REQUEST[$key] != '') {
                    if ($key == 'lastupdate') {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }


        if ($this->session->user_data->user_role == 1) {

            if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
                $request = $_REQUEST['filterstatus'];
                $where_e = "a.status = '$request' and a.id_rek_kegunaan = 2 and a.id_rek_4 != 72 and a.id_rek_4 != 15";
            } else {
                $where_e = "a.status = '1' and a.id_rek_kegunaan = 2 and a.id_rek_4 != 72 and a.id_rek_4 != 15";
            }
            
        } else {

            if (isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '') {
                $request = $_REQUEST['filterstatus'];
                $where_e = "a.status = '$request' and a.id_rek_kegunaan = 2 and a.id_rek_4 in ($query->id_rek_4)";
            } else {
                $where_e = "a.status = '1' and a.id_rek_kegunaan = 2 and a.id_rek_4 in ($query->id_rek_4)";
            }
            
        }

        $keys             = array_keys($aCari);
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count($this->table_db, $join, $where, $where_e);
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'a.jns_pemungutan, a.id_rek_4, a.nm_usaha_4, '.implode(',', $aCari);
        $result = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ($result as $rows) {
            $records["data"][] = array(
              $i,
              ucwords($rows->nm_rek_4),
              ucwords ($rows->nm_jn_pemungutan),
               '<a href="'.base_url('bendahara_penerimaan/penerimaan_pajak_sspd_sanksi_sub/'.$rows->id_rek_4.'/'.$rows->nm_usaha_4).'" class="btn btn-icon-only blue-steel tooltips" data-original-title="SPTPD Masa"><i class="fa fa-file-text"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);
    }

}

/* End of file Pendataan_pajak_sptpd.php */
/* Location: ./application/modules/data_entry/controllers/Pendataan_pajak_sptpd.php */
