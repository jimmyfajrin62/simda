<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?=$url.'/'.$kd_rek_4.'/'.$title?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th> No </th>
                                <th> NPWPD </th>
                                <th> Wajib Pajak </th>
                                <th> Nama Usaha </th>
                                <th> Alamat Usaha </th>
                                <th> Tgl Terdaftar </th>
                                <th width="100px"> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="npwpd" placeholder="NPWPD"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nama_pendaftar" placeholder="Wajib Pajak"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nm_usaha" placeholder="Nama Usaha"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="alamat_usaha" placeholder="Alamat Usaha"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="tgl_aktif" placeholder="Tgl Terdaftar"> </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // table data
        var select_url = '<?php echo $url ?>'+'/select/'+<?=$kd_rek_4?>+'/<?=$title?>';
        // console.log(select_url);
        var header     = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" }
        ];
        var order = [
            [5, "DESC"]
        ];
        var sort = [-1, 0, 1];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
