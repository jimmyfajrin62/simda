<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form_sanksi/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=$records->nm_usaha?>">
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Sanksi</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="total_sanksi" name="total_sanksi" value="<?=@$data->total_sanksi?>" placeholder="Total Sanksi" readonly>
                                <input style="text-transform:uppercase" type="hidden" class="form-control" name="nota_id" value="<?=@$get->id?>" placeholder="">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Bayar</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" value="<?=@$data->total_bayar?>" name="total_bayar" id="total_bayar" placeholder="Total Bayar">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Kurang Bayar</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" id="kurang_bayar" name="kurang_bayar" value="<?=@$data->kurang_bayar?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_sspd_rinci/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$sspd_id?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/show_sspd_rinci/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$sspd_id?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

      $("#total_bayar").on('keyup',function(){
          var total = $("#total_sanksi").val().replace(/[Rp \.\$,]/g, '');
          var bayar = $("#total_bayar").val().replace(/[Rp \.\$,]/g, '');
          var hasil = total - bayar;
          $("#kurang_bayar").val(hasil);
      });

      $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }
    });

    });
</script>
