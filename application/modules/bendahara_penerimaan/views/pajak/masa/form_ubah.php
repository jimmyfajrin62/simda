<div class="row">
	<div class="col-md-12">
		<!-- BEGIN VALIDATION STATES-->
		<div class="portlet blue box">

			<div class="portlet-title">
				<div class="caption">
					<span class="caption-subject sbold uppercase">Form Ubah Status</span>
				</div>
				<div class="actions">
				</div>
			</div>

			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?= @$url ?>/action_ubah/<?=$id?>" class="form-horizontal form-add" role="form" method="POST">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>

						<div class="row">
							
                            <input style="text-transform:uppercase" type="hidden" name="id" value="<?=$id?>">

							<?php foreach ($isi as $key=>$datax): ?>
								<div class="col-md-4 form-group">
									<label class="control-label"><strong><?=$datax[0]->status_label?></strong>
									<span class="required">*</span>
								</label>
								<select class="form-control" name="combo[<?= $key ?>]" style="text-transform:uppercase">
									<option value="">Pilih</option>
									<?php foreach ($datax as $nota): ?>
										<option value="<?=$nota->status_id?>"><?=$nota->status_detail?></option>
									<?php endforeach ?>
								</select>
								<span class="help-block"></span>
							</div>
						<?php endforeach ?>

						</div>
						<br>
						<div class="form-actions">
							<div class="row">
								<div class="col-md-4">
									<button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_sspd/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$user_id?>" class="btn grey ajaxify">Back</a>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/show_sspd/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$user_id?>" class="ajaxify reload"></a>


<script type="text/javascript">
jQuery(document).ready(function() {
	var rule = {};
	var message = {};
	var form = '.form-add';
	FormValidation.handleValidation( form, rule, message );

	$('.tgl').datepicker({
		format: 'dd-mm-yyyy'
	});


	});
</script>
