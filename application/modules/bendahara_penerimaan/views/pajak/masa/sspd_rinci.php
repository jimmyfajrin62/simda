<div class="row">
    <div class="col-md-12">

        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">
                    <table class="table table-striped table-bordered table-hover dt-responsive table-checkable" id="datatable_ajax" cellspacing="0" width="100%">
                        <thead>
                            <tr role="row" class="heading">
                                <th colspan="8" class="text-center"><h4><strong><?=strtoupper($head->nama_pendaftar)?> &nbsp (<?=$head->npwpd?>)</strong></h4></th>
                            </tr>
                            <tr role="row" class="heading">
                                <th> No </th>
                                <th> Uraian </th>
                                <th> Tarif Pajak </th>
                                <!-- <th> Dasar Pengenaan </th> -->
                                <th> Pajak Terhutang </th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var select_url = '<?php echo $url ?>/select_sspd_rinci/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$id_pungut?>';
        var header     = [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            // { "sClass": "text-left" },
            { "sClass": "text-left" }
        ];
        var order = [
            [2, "asc"]
        ];
        var sort = [-1, 0, 1];
        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        Helper.bsSelect();

        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });
</script>
