<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet blue box">

            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Form <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_form/<?=@$data->id?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <!-- <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>No.NPWPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="npwpd" readonly value="<?=$records->npwpd?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Wajib Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nama_pendaftar" readonly value="<?=$records->nama_pendaftar?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Usaha</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nm_usaha" readonly value="<?=$records->nm_usaha?>">
                                <span class="help-block"></span>
                            </div>
                        </div> -->
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>SKPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->skpd_no?>" name="no_skpd" id="no_skpd" readonly required>
                                    <span class="input-group-btn">
                                        <a type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening" id="add_rekening">
                                            <i class="fa fa-refresh"></i>
                                        </a>
                                    </span>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Masa Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group input-large">
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa1" id="masa1" value="<?=$this->m_global->getdateformat(@$data->masa1)?>" readonly required>
                                    <span class="input-group-addon"> s/d </span>
                                    <input style="text-transform:uppercase" type="text" class="form-control" name="masa2" id="masa2" value="<?=$this->m_global->getdateformat(@$data->masa2)?>" readonly required>
                                </div>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>SSPD</strong>
                                </label>
                                <input style="text-transform:uppercase" type="hidden" name="pungut_id" value="" id="pungut_id">
                                <input style="text-transform:uppercase" type="hidden" name="skpd_id" value="" id="skpd_id">
                                <input style="text-transform:uppercase" type="hidden" name="jns_pajak" value="<?=$kd_rek_4?>">
                                <input style="text-transform:uppercase" type="hidden" name="user_id" value="<?=$user_id?>">
                                <input style="text-transform:uppercase" type="hidden" name="tahun" value="<?=date('Y')?>">
                                <input style="text-transform:uppercase" type="text" class="form-control" name="no_sspd" readonly value="<?=@$data->no_sspd?>" placeholder="Nomer SSPD Generate">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Tanggal SSPD</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="input-group date" data-provide="datepicker">
                                    <input style="text-transform:uppercase" type="text" class="form-control" value="<?=$this->m_global->getdateformat(@$data->tgl_sspd)?>" name="tgl_sspd" required>
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="col-md-8 form-group">
                                <label class="control-label"><strong>Uraian</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="keterangan" value="<?=@$data->keterangan?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Bayar</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" name="total_bayar" id="total_bayar" value="<?=@$data->total_bayar?>" placeholder="Total Bayar" required>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Total Pajak</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text"  class="form-control uang" value="<?=@$data->total_pajak?>" name="total_pajak" id="total_pajak" readonly>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Pajak Terhutang</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control uang" name="pajak_terhutang" id="hutang" value="<?=@$data->pajak_terhutang?>" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Pembayaran Via</strong>
                                    <span class="required">*</span>
                                </label><br>
                                <label class="mt-radio">
                                    <input style="text-transform:uppercase" type="radio" name="jns_pembayaran" value="1" <?=@$data->jns_pembayaran == 1 ? 'checked' : '' ?> id="jns_pembayaran"> Diterima Tunai
                                    <span class="help-block"></span>
                                </label><br>
                                <label class="mt-radio">
                                    <input style="text-transform:uppercase" type="radio" name="jns_pembayaran" value="2" <?=@$data->jns_pembayaran == 2 ? 'checked' : '' ?> id="jns_pembayaran"> Di Setor WP ke Bank
                                    <span class="help-block"></span>
                                </label>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Bank</strong>
                                </label>
                                <select class="form-control" name="kd_bank" id="bank_id" style="text-transform:uppercase">
                                    <option value="">Pilih Bank</option>
                                    <?php foreach ($bank as $val): ?>
                                        <?php @$data->kd_bank == $val->kd_bank ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->kd_bank?>" data-1="<?=$val->no_rekening?>" <?=$selected?>><?=$val->nm_bank?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Rekening Bank</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" name="rek_bank" id="rek_bank" readonly value="<?=@$data->no_rekening?>">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <select class="form-control" name="ttd_dok_id" id="ttd_dok_id" style="text-transform:uppercase">
                                    <option value="">Pilih Petugas</option>
                                    <?php foreach ($ttd as $val): ?>
                                        <?php @$data->ttd_dok_id == $val->no_urut ? $selected = 'selected' : $selected = '' ?>
                                        <option value="<?=$val->no_urut?>" data-1="<?=$val->nip_penandatangan?>" data-2="<?=$val->nm_jab?>" <?=$selected?>><?=$val->nm_penandatangan?></option>
                                    <?php endforeach; ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>NIP Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="nip_penandatangan" id="nip_penandatangan" readonly value="<?=@$data->nip_penandatangan?>">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Jabatan Petugas</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required name="jbt_penandatangan" id="jbt_penandatangan" readonly value="<?=@$data->nm_jab?>">
                                <span class="help-block"></span>
                            </div>
                        </div><br>
                        <!-- <hr><h4><strong>Petugas Penerima</strong></h4> -->

                        <fieldset>
                            <legend>Penyetor</legend>
                        <div class="row">
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Nama</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required value="<?=@$data->nm_penyetor?>" name="nm_penyetor">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Alamat</strong>
                                    <span class="required">*</span>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" required value="<?=@$data->alamat_penyetor?>" name="alamat_penyetor">
                                <span class="help-block"></span>
                            </div>
                            <div class="col-md-4 form-group">
                                <label class="control-label"><strong>Rekening</strong>
                                </label>
                                <input style="text-transform:uppercase" type="text" class="form-control" value="<?=@$data->rek_penyetor?>" name="rek_penyetor">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        </fieldset>
                        <br>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-4">
                                    <button type="submit" class="btn blue">Submit</button>
                                    <a href="<?php echo $url ?>/show_sspd/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$user_id?>" class="btn grey ajaxify">Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>/show_sspd/<?=$kd_rek_4?>/<?=$title?>/<?=$wp?>/<?=$user_id?>" class="ajaxify reload"></a>

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <!-- <div class="action pull-right">
                            <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div> -->
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> No.SKPD </th>
                                    <th width="100px"> Tanggal </th>
                                    <th> Nama Usaha </th>
                                    <th width="50px"> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="no_skpd" placeholder="No.skpd">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="tanggal" placeholder="Tanggal">
                                    </td>
                                    <td>
                                        <input style="text-transform:uppercase" type="text" class="form-control form-filter input-sm" name="nama_usaha" placeholder="Nama Usaha">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->


<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_skpd/<?=$kd_rek_4?>/<?=$wp?>';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });

    $("#total_bayar").on('keyup',function(){
          var bayar  = $("#total_bayar").val().replace(/[Rp \.\$,]/g, '');
          var pajak  = $("#total_pajak").val().replace(/[Rp \.\$,]/g, '');
          var jumlah = bayar - pajak;
          $("#hutang").val(jumlah);
      });

    $('.uang').inputmask("numeric", {
        radixPoint: ".",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        prefix: 'Rp  ', //No Space, this will truncate the first character
        rightAlign: false,
        // oncleared: function () { self.Value(''); }
    });


    });

    function pilih(id){
        $.get({
            url:        '<?=$url?>/get_skpd/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {
                id: id,
            },
            success: function(res){
                console.log(res);
                $('#no_skpd').val(res.records.no_skpd);
                $('#masa1').val(res.masa1);
                $('#masa2').val(res.masa2);
                $('#pungut_id').val(res.records2.id3);
                $('#total_pajak').val(res.records2.hasil);
                $('#total_bayar').val(res.records2.hasil);
                $('#skpd_id').val(res.records.id);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>

<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('.date').datepicker({
            format: 'dd-mm-yyyy'
        });

        $('#ttd_dok_id').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            var data2  = $(this).find(':selected').attr('data-2');
            $('#nip_penandatangan').val(data1);
            $('#jbt_penandatangan').val(data2);
        });

        $('#bank_id').on('change', function(){
            var data1  = $(this).find(':selected').attr('data-1');
            $('#rek_bank').val(data1);
        });

        // $('#jns_pembayaran').click(function() {
        //     if($('input:radio[name=jns_pembayaran]:checked').val() == 1){
        //         $('#bank_id').attr('disabled','disabled');
        //     }
        //     else if($('input:radio[name=jns_pembayaran]:checked').val() == 2){
        //         $('#bank_id').attr('disabled','disabled');
        //     }
        // });

    });
</script>
