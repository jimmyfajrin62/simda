<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sptpd extends Admin_Controller
{
    private $prefix       = 'laporan/laporan_sptpd';
    private $url          = 'laporan/laporan_sptpd';
    private $table_db     = 'ta_kartu_pajak_pungut';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    	$query = "SELECT a.nm_rek_4, a.id_rek_4 FROM ref_rek_4 a WHERE
				  a. STATUS = '1'
				AND a.id_rek_kegunaan = 2
				AND a.id_rek_4 != 72
				AND a.id_rek_4 != 15";
		$data['jns_pajak'] = $this->db->query($query)->result();
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Laporan SSPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('laporan_sptpd/index', $data, $js, $css );

    }

    public function laporan()
    {
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Data Laporan SPTPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SPTPD' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;
        $this->template->display('laporan_sptpd/table_laporan', $data, $js, $css );
    }

    public function select($jns_pajak, $tgl1, $tgl2)
    {
        $this->table_db = 'ta_kartu_pajak_pungut';
        
        $aCari = [
            
        ];
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

       

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords :   $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end              = $iDisplayStart + $iDisplayLength;
        $end              = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select           = 'id, no_sspd, jns_pembayaran, masa1, masa2, total_bayar, '.implode(',' , $aCari);
      

       

        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
        }


        $query = "
        SELECT
            `ta_kartu_pajak_pungut`.`tgl_terima`,
            `wp_data_umum`.`npwpd`,
            `wp_wajib_pajak_usaha`.`nm_usaha`,
            `wp_wajib_pajak_usaha`.`alamat_usaha`,
            `ta_kartu_pajak_pungut`.`no_sptpd`,
            `ta_kartu_pajak_pungut`.`tgl_sptpd`,
            `ta_kartu_pajak_pungut`.`masa1`,
            `ta_kartu_pajak_pungut`.`masa2`,
            `ta_kartu_pajak_pungut`.`jns_sptpd`,
            ".$table_laporan.".`id`,
            ".$table_laporan.".`status`,
            ".$table_laporan.".`lastupdate`,
            ".$table_laporan.".`dasar_pengenaan`,
            ".$table_laporan.".`tarif_pajak`,
            ".$table_laporan.".`pajak_terhutang`
        FROM
            ".$table_laporan."
        LEFT JOIN `ref_rek_6` ON
            `".$table_laporan."`.`id_rek_6` = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_kartu_pajak_pungut` ON
            `".$table_laporan."`.`pungut_id` = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON
            ta_kartu_pajak_pungut.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id
        LEFT JOIN `wp_wajib_pajak_usaha` ON
            wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
        LEFT JOIN `wp_wajib_pajak` ON
            wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
        LEFT JOIN `wp_data_umum` ON
            wp_wajib_pajak.data_umum_id = wp_data_umum.id
        WHERE
            ".$table_laporan.".status = '1' AND masa1 BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY ".$table_laporan.".id DESC";
        
        $result = $this->db->query($query)->result();
       

        $i                = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            if ($rows->jns_sptpd == 1) {
                $jenis_pembayaran = 'sptpd masa';
            }else{
                $jenis_pembayaran = 'sptpd jabatan';
            }
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->tgl_terima,
                "",
                $rows->npwpd."<br>".$rows->nm_usaha,      
                $rows->nm_usaha." <br> ".$rows->alamat_usaha,
                $rows->no_sptpd."<br>".$rows->tgl_sptpd,
                $rows->masa1." - ".$rows->masa2,
                uang($rows->dasar_pengenaan)." <br> ".uang($rows->pajak_terhutang),
                $jenis_pembayaran,
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );exit();
    }

    public function export_excel($jns_pajak, $tgl1, $tgl2)
    {
        
        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
            $data['pajak'] = 'Pajak Hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
            $data['pajak'] = 'Pajak Restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
            $data['pajak'] = 'Pajak Hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
            $data['pajak'] = 'Pajak Reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
            $data['pajak'] = 'Pajak Penerangan Jalan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
            $data['pajak'] = 'Pajak Mineral Bukan Logam Dan Lainnya';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
            $data['pajak'] = 'Pajak Parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
            $data['pajak'] = 'Pajak Air Tanah';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
            $data['pajak'] = 'Pajak Sarang Burung Walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
            
        }


        $query = "
        SELECT
            `ta_kartu_pajak_pungut`.`tgl_terima`,
            `wp_data_umum`.`npwpd`,
            `wp_wajib_pajak_usaha`.`nm_usaha`,
            `wp_wajib_pajak_usaha`.`alamat_usaha`,
            `ta_kartu_pajak_pungut`.`no_sptpd`,
            `ta_kartu_pajak_pungut`.`tgl_sptpd`,
            `ta_kartu_pajak_pungut`.`masa1`,
            `ta_kartu_pajak_pungut`.`masa2`,
            `ta_kartu_pajak_pungut`.`jns_sptpd`,
            ".$table_laporan.".`id`,
            ".$table_laporan.".`status`,
            ".$table_laporan.".`lastupdate`,
            ".$table_laporan.".`dasar_pengenaan`,
            ".$table_laporan.".`tarif_pajak`,
            ".$table_laporan.".`pajak_terhutang`
        FROM
            ".$table_laporan."
        LEFT JOIN `ref_rek_6` ON
            `".$table_laporan."`.`id_rek_6` = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_kartu_pajak_pungut` ON
            `".$table_laporan."`.`pungut_id` = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON
            ta_kartu_pajak_pungut.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id
        LEFT JOIN `wp_wajib_pajak_usaha` ON
            wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
        LEFT JOIN `wp_wajib_pajak` ON
            wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
        LEFT JOIN `wp_data_umum` ON
            wp_wajib_pajak.data_umum_id = wp_data_umum.id
        WHERE
            ".$table_laporan.".status = '1' AND masa1 BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY ".$table_laporan.".id DESC";
       
        $data['laporan'] = $this->db->query($query)->result();
        $data['tgl1'] = $tgl1;
        $data['tgl2'] = $tgl2;
     

        $this->load->view('laporan_sptpd/table_excel', $data);
    }

    public function export_pdf($jns_pajak, $tgl1, $tgl2)
    {
        
        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
            $pajak = 'Pajak Hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
            $pajak = 'Pajak Restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
            $pajak = 'Pajak Hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
            $pajak = 'Pajak Reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
            $pajak = 'Pajak Penerangan Jalan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
            $pajak = 'Pajak Mineral Bukan Logam Dan Lainnya';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
            $pajak = 'Pajak Parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
            $pajak = 'Pajak Air Tanah';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
            $pajak = 'Pajak Sarang Burung Walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
            
        }

        $query = "
        SELECT
            `ta_kartu_pajak_pungut`.`tgl_terima`,
            `wp_data_umum`.`npwpd`,
            `wp_wajib_pajak_usaha`.`nm_usaha`,
            `wp_wajib_pajak_usaha`.`alamat_usaha`,
            `ta_kartu_pajak_pungut`.`no_sptpd`,
            `ta_kartu_pajak_pungut`.`tgl_sptpd`,
            `ta_kartu_pajak_pungut`.`masa1`,
            `ta_kartu_pajak_pungut`.`masa2`,
            `ta_kartu_pajak_pungut`.`jns_sptpd`,
            ".$table_laporan.".`id`,
            ".$table_laporan.".`status`,
            ".$table_laporan.".`lastupdate`,
            ".$table_laporan.".`dasar_pengenaan`,
            ".$table_laporan.".`tarif_pajak`,
            ".$table_laporan.".`pajak_terhutang`
        FROM
            ".$table_laporan."
        LEFT JOIN `ref_rek_6` ON
            `".$table_laporan."`.`id_rek_6` = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_kartu_pajak_pungut` ON
            `".$table_laporan."`.`pungut_id` = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON
            ta_kartu_pajak_pungut.wp_usaha_pajak_id = wp_wajib_pajak_usaha_pajak.id
        LEFT JOIN `wp_wajib_pajak_usaha` ON
            wp_wajib_pajak_usaha_pajak.wp_usaha_id = wp_wajib_pajak_usaha.id
        LEFT JOIN `wp_wajib_pajak` ON
            wp_wajib_pajak_usaha.wp_id = wp_wajib_pajak.id
        LEFT JOIN `wp_data_umum` ON
            wp_wajib_pajak.data_umum_id = wp_data_umum.id
        WHERE
            ".$table_laporan.".status = '1' AND masa1 BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY ".$table_laporan.".id DESC";
       
        $query = $this->db->query($query)->result();
        $kota = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())")->row(); 
        $this->load->library('fpdf_gen');
        $pdf = new fpdf('L','mm','A4');
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',14);
        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,12,10,20,20);
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'DAFTAR SURAT PEMBERITAHUAN PAJAK DAERAH WAJIB PAJAK (SPTPD WP)' ,0,1,'C');
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'PERIODE '.tgl_id_laporan($tgl1).' - '.tgl_id_laporan($tgl2) ,0,1,'C');

        $pdf->Ln();
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7, 6+6,'No.','LTR',0,'L');
        $pdf->Cell(25, 6,'TANGGAL','TR',0,'C');
        $pdf->Cell(35, 6,'NOMOR &','TR',0,'C');
        $pdf->Cell(40, 6,'NOMOR &','TR',0,'C');
        $pdf->Cell(40, 6,'NAMA &','TR',0,'C');
        $pdf->Cell(45, 6,'NOMOR &','TR',0,'C');
        $pdf->Cell(30, 6,'MASA','TR',0,'C');
        $pdf->Cell(30, 6,'OMSET','TR',0,'C');
        $pdf->Cell(30, 6,'KETERANGAN','TR',1,'C');
        $pdf->Cell(7, 6,'','LBR',0,'C');
        $pdf->Cell(25, 6,'TERIMA','BR',0,'C');
        $pdf->Cell(35, 6,'NAMA REKENING','BR',0,'C');
        $pdf->Cell(40, 6,'NAMA WP','BR',0,'C');
        $pdf->Cell(40, 6,'ALAMAT USAHA WP','BR',0,'C');
        $pdf->Cell(45, 6,'TANGGAL','BR',0,'C');
        $pdf->Cell(30, 6,'PAJAK','BR',0,'C');
        $pdf->Cell(30, 6,'NILAI PAJAK','BR',0,'C');
        $pdf->Cell(30, 6,'','BR',1,'L');
        $pdf->SetFont('Arial','',10);
        $no = 1;
        $pdf->Cell(282,6, $no++.". ".$pajak, 1);
        $no2 = 1;
        $total_pengenaan = 0;
        $total_terhutang = 0;
        foreach ($query as $data) {
            if ($data->jns_sptpd == 1) {
                $jenis_pembayaran = 'sptpd masa';
            }else{
                $jenis_pembayaran = 'sptpd jabatan';
            }
            $total_pengenaan += $data->dasar_pengenaan;
            $total_terhutang += $data->pajak_terhutang;
            $pdf->Ln();
            $pdf->Cell(7, 6+6, $no2,'LTR',0,'C');
            $pdf->Cell(25, 6+6, tgl_id_laporan($data->tgl_terima) ,'TR',0,'C');
            $pdf->Cell(35, 6, '','TR',0,'L');
            $pdf->Cell(40, 6, $data->npwpd,'TR',0,'C');
            $pdf->Cell(40, 6, $data->nm_usaha,'TR',0,'C');
            $pdf->Cell(45, 6, $data->no_sptpd,'TR',0,'C');
            $pdf->Cell(30, 6, tgl_id_laporan($data->masa1)." - ",'TR',0,'C');
            $pdf->Cell(30, 6, uang($data->dasar_pengenaan),'TR',0,'C');
            $pdf->Cell(30, 6, $jenis_pembayaran,'TR',0,'C');
            $pdf->Ln();
            $pdf->Cell(7, 6,'','LBR',0,'C');
            $pdf->Cell(25, 6,'','BR',0,'C');
            $pdf->Cell(35, 6,'','BR',0,'C');
            $pdf->Cell(40, 6, $data->nm_usaha,'BR',0,'C');
            $pdf->Cell(40, 6, $data->alamat_usaha,'BR',0,'C');
            $pdf->Cell(45, 6, tgl_id_laporan($data->tgl_sptpd),'BR',0,'C');
            $pdf->Cell(30, 6, tgl_id_laporan($data->masa2),'BR',0,'C');
            $pdf->Cell(30, 6, uang($data->pajak_terhutang),'BR',0,'C');
            $pdf->Cell(30, 6,'','BR',1,'C');
            
            $no2++;
        }
        $pdf->Cell(7,6,'','L',0);
        $pdf->Cell(25,6,'',0,0);
        $pdf->Cell(35,6,'',0,0);
        $pdf->Cell(40,6,'',0,0);
        $pdf->Cell(40,6,'',0,0);
        $pdf->Cell(45,6,'',0,0);
        $pdf->Cell(30,6+6,'JUMLAH','TLR',0,'C');
        $pdf->Cell(30,6,uang($total_pengenaan),'R',0);
        $pdf->Cell(30,6,'','R',0);
        $pdf->Ln();
        $pdf->Cell(7,6,'','LB',0);
        $pdf->Cell(25,6,'','B',0);
        $pdf->Cell(35,6,'','B',0);
        $pdf->Cell(40,6,'','B',0);
        $pdf->Cell(40,6,'','B',0);
        $pdf->Cell(45,6,'','B',0);

        $pdf->Cell(30,6,'','LRB',0);
        $pdf->Cell(30,6,uang($total_terhutang),'RB',0);
        $pdf->Cell(30,6,'','BR',0);

        $pdf->Output();
    }
}
