<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_jns_pajak extends Admin_Controller
{
    private $prefix       = 'laporan/laporan_jns_pajak';
    private $url          = 'laporan/laporan_jns_pajak';
    private $table_db     = 'ta_sspd';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $query = "SELECT
                          a.nm_rek_4,a.id_rek_4
                        FROM
                          ref_rek_4 a
                        WHERE
                          a. STATUS = '1'
                        AND a.id_rek_kegunaan = 2
                        AND a.id_rek_4 != 72
                        AND a.id_rek_4 != 15
                        ";
        $result = $this->db->query($query)->result();
        $data['jns_pajak']  = $result;
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Laporan  SSPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('laporan_jns_pajak/index', $data, $js, $css );

    }


    public function laporan()
    {
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Data Laporan Jenis Pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan Jenis Pajak' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;
        $this->template->display('laporan_jns_pajak/table_laporan', $data, $js, $css );
    }

    public function select($jns_pajak, $bulan, $tahun)
    {
      
        $this->table_db = 'ta_sspd';
        
        $aCari = [
            'no_sspd' => 'a.no_sspd',
            'jns_pajak' => 'b.nm_usaha_4',
            'masa' => 'a.masa1',
            'pajak_dibayar' => 'a.total_bayar'
        ];
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

       

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords :   $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end              = $iDisplayStart + $iDisplayLength;
        $end              = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select           = 'a.id,a.tgl_sspd,a.masa2,'.implode(',' , $aCari);
       
        
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }
        if ($jns_pajak != 'all') {
            $jns_pajak = $jns_pajak;
        }else{
            $jns_pajak = '';
        }

      

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun.' AND jns_pajak = '.$jns_pajak;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"'.' AND jns_pajak = "'.$jns_pajak.'"';
        }
         

        $query = "SELECT ".$select." FROM ta_sspd a LEFT JOIN ref_rek_4 b on a.jns_pajak = b.id_rek_4 WHERE ".$where." ORDER BY id DESC";
    
       
        $result = $this->db->query($query)->result();
       

   
     

        $i   = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
      
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->no_sspd,
                $rows->nm_usaha_4,
                // $jenis_pembayaran,
                tgl_id($rows->masa1)." - ".tgl_id($rows->masa2),
                uang($rows->total_bayar),
            
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );exit();
    }

    public function export_excel($jns_pajak, $bulan, $tahun)
    {
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }
        if ($jns_pajak != 'all') {
            $jns_pajak = $jns_pajak;
        }else{
            $jns_pajak = '';
        }

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun.'AND jns_pajak = '.$jns_pajak;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"'.'AND jns_pajak = "'.$jns_pajak.'"';
        }

        $query = "SELECT * FROM ta_sspd a LEFT JOIN ref_rek_4 b on a.jns_pajak = b.id_rek_4 WHERE ".$where." ORDER BY id DESC";
        
       
        $data['laporan_jns_pajak'] = $this->db->query($query)->result();
        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
     

        $this->load->view('laporan_jns_pajak/table_excel', $data);
    }

    public function export_pdf($jns_pajak, $bulan, $tahun)
    {
        
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }
        if ($jns_pajak != 'all') {
            $jns_pajak = $jns_pajak;
        }else{
            $jns_pajak = '';
        }

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun.'AND jns_pajak = '.$jns_pajak;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"'.'AND jns_pajak = "'.$jns_pajak.'"';
        }

        $query = "SELECT * FROM ta_sspd a LEFT JOIN ref_rek_4 b on a.jns_pajak = b.id_rek_4 WHERE ".$where." ORDER BY id DESC";
       
        $data = $this->db->query($query)->result();
      
        $kota = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())")->row(); 
        $this->load->library('fpdf_gen');
        $pdf = new fpdf('L','mm','A4');
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',14);
        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,12,10,20,20);
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'LAPORAN DAFTAR JENIS PAJAK' ,0,1,'C');
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'BULAN '.$bulan.' TAHUN '.$tahun ,0,1,'C');

        $pdf->Ln();
        
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7, 6,'No.',1,0,'C');
        $pdf->Cell(70, 6,'NOMOR SSPD',1,0,'C');
        $pdf->Cell(60, 6,'JENIS PAJAK',1,0,'C');
        $pdf->Cell(70, 6,'MASA PAJAK',1,0,'C');
        $pdf->Cell(70, 6,'PAJAK YANG DIBAYAR',1,0,'C');
        $pdf->SetFont('Arial','',10);

        $no=1;
        $total_pajak_keseluruhan = 0;
        foreach ($data as $row) {
            $total_pajak_keseluruhan+= $row->total_pajak;
            $pdf->Ln();
            $pdf->Cell(7, 6, $no,1,0,'C');
            $pdf->Cell(70, 6, $row->no_sspd,1,0,'C');
            $pdf->Cell(60, 6, $row->jns_pajak,1,0,'C');
            $pdf->Cell(70, 6, tgl_id($row->masa1)." - ".tgl_id($row->masa2),1,0,'C');
            $pdf->Cell(70, 6, uang($row->total_pajak),1,0,'C');
            $no++;
        }
        $pdf->Ln();
        $pdf->Cell(7, 6,'','LB',0);
        $pdf->Cell(70, 6,'','B',0);
        $pdf->Cell(60, 6,'','B',0);
        $pdf->Cell(70, 6,'TOTAL',1,0,'C');
        $pdf->Cell(70, 6,uang($total_pajak_keseluruhan),1,0,'C');

        $pdf->Output();
    }
}
