<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class laporan_pendapatan extends Admin_Controller
{
    private $prefix       = 'laporan/laporan_pendapatan';
    private $url          = 'laporan/laporan_pendapatan';
    private $table_db     = '';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
    
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Laporan Pendapatan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan Pendapatan' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('laporan_pendapatan/index', $data, $js, $css );

    }

    public function laporan()
    {
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Data Laporan Pendapatan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan Pendapatan' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;
        $this->template->display('laporan_sspd/table_laporan', $data, $js, $css );
    }

}
