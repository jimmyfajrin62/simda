<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sspd extends Admin_Controller
{
    private $prefix       = 'laporan/laporan_sspd';
    private $url          = 'laporan/laporan_sspd';
    private $table_db     = 'ta_sspd';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();

    }

    public function index()
    {
    
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Laporan SSPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('laporan_sspd/index', $data, $js, $css );

    }


    public function laporan()
    {
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Data Laporan SSPD';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;
        $this->template->display('laporan_sspd/table_laporan', $data, $js, $css );
    }

    public function select($bulan, $tahun)
    {
       
        $this->table_db = 'ta_sspd';
        
        $aCari = [
            'no_sspd' => 'no_sspd',
            'jenis_pembayaran' => 'jns_pembayaran',
            'masa' => 'masa1',
            'pajak_dibayar' => 'total_bayar'
        ];
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

       

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords :   $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end              = $iDisplayStart + $iDisplayLength;
        $end              = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select           = 'id, no_sspd, jns_pembayaran, masa1, masa2, total_bayar, '.implode(',' , $aCari);
        
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }
      

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"';
        }
        $query = "SELECT * FROM ta_sspd WHERE ".$where." ORDER BY id DESC";
       
        $result = $this->db->query($query)->result();
     

        $i                = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            if ($rows->jns_pembayaran == 1) {
                $jenis_pembayaran = 'Cash';
            }else{
                $jenis_pembayaran = 'Bank';
            }
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->no_sspd,
                $jenis_pembayaran,
                tgl_id($rows->masa1)." - ".tgl_id($rows->masa2),
                uang($rows->total_bayar),
            
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );exit();
    }

    public function export_excel($bulan, $tahun)
    {
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"';
        }

        $query = "SELECT * FROM ta_sspd WHERE ".$where." ORDER BY id DESC";
        
       
        $data['laporan_sspd'] = $this->db->query($query)->result();
        $data['tahun'] = $tahun;
        $data['bulan'] = $bulan;
     

        $this->load->view('laporan_sspd/table_excel', $data);
    }

    public function export_pdf($bulan, $tahun)
    {
        if ($bulan != 'all') {
            $bulan = $bulan;
        }else{
            $bulan = '';
        }

        if ($bulan == '') {
            $where = 'DATE_FORMAT(masa1, "%Y") = '.$tahun;
        }else{

            $where = 'DATE_FORMAT(masa1, "%Y %m") = "'.$tahun.' '.$bulan.'"';
        }

        $query = "SELECT * FROM ta_sspd WHERE ".$where." ORDER BY id DESC";
        
       
        $data = $this->db->query($query)->result();
        

        
        $this->load->library('fpdf_gen');
        $pdf = new fpdf('P','mm','A4');

       
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',14);
        // mencetak string 
        if ($bulan != '') {
            $pdf->Cell(190,7,'LAPORAN SSPD TAHUN '.$tahun.' BULAN '.$bulan ,0,1,'C');
            
        }else{
            $pdf->Cell(190,7,'LAPORAN SSPD TAHUN '.$tahun,0,1,'C');
        }
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7,6,'No',1,0);
        $pdf->Cell(45,6,'No SSPD',1,0);
        $pdf->Cell(35,6,'Jenis Pembayaran',1,0);
        $pdf->Cell(60,6,'Masa Pajak',1,0);
        $pdf->Cell(52,6,'Pajak Yang Dibayarkan',1,1);
        $pdf->SetFont('Arial','',10);
        $no=1;
        $total_pembayaran =0;
        foreach ($data as $row){
            if ($row->jns_pembayaran == 1) {
                $jenis_pembayaran = 'Cash';
            }else{
                $jenis_pembayaran = 'Bank';
            }
            $total_pembayaran += $row->total_bayar;
            $pdf->Cell(7,6, $no, 1,0);
            $pdf->Cell(45,6, $row->no_sspd, 1,0);
            $pdf->Cell(35,6, $jenis_pembayaran, 1,0);
            
            $pdf->Cell(60,6, tgl_id($row->masa1)." - ".tgl_id($row->masa2), 1,0);
            $pdf->Cell(52,6, uang($row->total_bayar), 1,1);
            $no++;
        }
        $pdf->Cell(7,6, "", 1,0);
        $pdf->Cell(45,6, "", 1,0);
        $pdf->Cell(35,6, "", 1,0);
        $pdf->Cell(60,6, "Total : ", 1,0);
        $pdf->Cell(52,6, uang($total_pembayaran), 1,0);

        $pdf->Output();
    }
}
