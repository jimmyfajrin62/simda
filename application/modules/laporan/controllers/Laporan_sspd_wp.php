<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_sspd_wp extends CI_Controller
{
    private $prefix       = 'laporan/laporan_sspd_wp';
    private $url          = 'laporan/laporan_sspd_wp';
    private $table_db     = 'ta_sspd';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $query = "SELECT a.nm_rek_4, a.id_rek_4 FROM ref_rek_4 a WHERE
                  a. STATUS = '1'
                AND a.id_rek_kegunaan = 2
                AND a.id_rek_4 != 72
                AND a.id_rek_4 != 15";
        $data['jns_pajak'] = $this->db->query($query)->result();
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Laporan SSPD WP';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD WP' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display('laporan_sspd_wp/index', $data, $js, $css );

    }

    public function laporan()
    {
        $data['pagetitle']  = 'Laporan';
        $data['subtitle']   = 'Data Laporan SSPD WP';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Laporan' => null,'Laporan SSPD WP' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;
        $this->template->display('laporan_sspd_wp/table_laporan', $data, $js, $css );
    }

    public function select($jns_pajak, $tgl1, $tgl2)
    {
        $this->table_db = 'ta_sspd';
        
        $aCari = [
            'no_sspd'       => 'ta_sspd.no_sspd',
            'tgl_sspd'      => 'jns_pembayaran',
            'npwpd'         => 'wp_data_umum.npwpd',
            'nm_usaha'      => 'wp_wajib_pajak_usaha.nm_usaha',
            'total_pajak'   => 'ta_sspd.total_pajak'
        ];
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords :   $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end              = $iDisplayStart + $iDisplayLength;
        $end              = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select           = 'id, '.implode(',' , $aCari);
      

        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
        }

        $query = "SELECT `ta_sspd`.`no_sspd`, `ta_sspd`.`tgl_sspd`, `wp_data_umum`.`npwpd`, `wp_wajib_pajak_usaha`.`nm_usaha`, `ta_sspd`.`total_pajak`, `ta_sspd`.`jns_sspd`, `ta_kartu_pajak_pungut`.`masa1`, `ta_kartu_pajak_pungut`.`masa2`, `".$table_laporan."`.`id`, `".$table_laporan."`.`status` FROM `".$table_laporan."`
        LEFT JOIN `ref_rek_6`                  ON `".$table_laporan."`.`id_rek_6`             = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_sspd`                    ON `".$table_laporan."`.`pungut_id`            = `ta_sspd`.`pungut_id`
        LEFT JOIN `ta_kartu_pajak_pungut`      ON `ta_sspd`.`pungut_id`                       = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON `ta_kartu_pajak_pungut`.`wp_usaha_pajak_id` = `wp_wajib_pajak_usaha_pajak`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha`       ON `wp_wajib_pajak_usaha_pajak`.`wp_usaha_id`  = `wp_wajib_pajak_usaha`.`id`
        LEFT JOIN `wp_wajib_pajak`             ON `wp_wajib_pajak_usaha`.`wp_id`              = `wp_wajib_pajak`.`id`
        LEFT JOIN `wp_data_umum`               ON `wp_wajib_pajak`.`data_umum_id`             = `wp_data_umum`.`id`

        WHERE `".$table_laporan."`.`status` = '1' AND `ta_kartu_pajak_pungut`.`masa1` BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY `".$table_laporan."`.`id` DESC";
        
        $result = $this->db->query($query)->result();
       

        $i                = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            if ($rows->jns_sspd == 1) {
                $jenis_pembayaran = 'SSPD Masa';
            }elseif ($rows->jns_sspd == 2) {
                $jenis_pembayaran = 'SSPD Kurang Bayar';
            }else {
                $jenis_pembayaran = 'SSPD Sanksi';
            }
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->no_sspd,
                $rows->tgl_sspd,
                $rows->npwpd,      
                $rows->nm_usaha,
                $rows->masa1." - ".$rows->masa2,
                $jenis_pembayaran,
                uang($rows->total_pajak)
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );exit();
    }

    public function export_excel($jns_pajak, $tgl1, $tgl2)
    {
        
        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
            $data['pajak'] = 'Pajak Hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
            $data['pajak'] = 'Pajak Restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
            $data['pajak'] = 'Pajak Hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
            $data['pajak'] = 'Pajak Reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
            $data['pajak'] = 'Pajak Penerangan Jalan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
            $data['pajak'] = 'Pajak Mineral Bukan Logam Dan Lainnya';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
            $data['pajak'] = 'Pajak Parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
            $data['pajak'] = 'Pajak Air Tanah';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
            $data['pajak'] = 'Pajak Sarang Burung Walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
            
        }


        $query = "SELECT `ta_sspd`.`no_sspd`, `ta_sspd`.`tgl_sspd`, `wp_data_umum`.`npwpd`, `wp_wajib_pajak_usaha`.`nm_usaha`, `ta_sspd`.`total_pajak`, `ta_sspd`.`jns_sspd`, `ta_kartu_pajak_pungut`.`masa1`, `ta_kartu_pajak_pungut`.`masa2`, `".$table_laporan."`.`id`, `".$table_laporan."`.`status` FROM `".$table_laporan."`
        LEFT JOIN `ref_rek_6`                  ON `".$table_laporan."`.`id_rek_6`             = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_sspd`                    ON `".$table_laporan."`.`pungut_id`            = `ta_sspd`.`pungut_id`
        LEFT JOIN `ta_kartu_pajak_pungut`      ON `ta_sspd`.`pungut_id`                       = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON `ta_kartu_pajak_pungut`.`wp_usaha_pajak_id` = `wp_wajib_pajak_usaha_pajak`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha`       ON `wp_wajib_pajak_usaha_pajak`.`wp_usaha_id`  = `wp_wajib_pajak_usaha`.`id`
        LEFT JOIN `wp_wajib_pajak`             ON `wp_wajib_pajak_usaha`.`wp_id`              = `wp_wajib_pajak`.`id`
        LEFT JOIN `wp_data_umum`               ON `wp_wajib_pajak`.`data_umum_id`             = `wp_data_umum`.`id`

        WHERE `".$table_laporan."`.`status` = '1' AND `ta_kartu_pajak_pungut`.`masa1` BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY `".$table_laporan."`.`id` DESC";
       
        $data['laporan'] = $this->db->query($query)->result();
        $data['tgl1'] = $tgl1;
        $data['tgl2'] = $tgl2;
     

        $this->load->view('laporan_sspd_wp/table_excel', $data);
    }

    public function export_pdf($jns_pajak, $tgl1, $tgl2)
    {
        
        if ($jns_pajak == 4) {
            $table_laporan = 'ta_kartu_pajak_hotel';
            $pajak = 'Pajak Hotel';
        } else if ($jns_pajak == 5) {
            $table_laporan = 'ta_kartu_pajak_restoran';
            $pajak = 'Pajak Restoran';
        } else if ($jns_pajak == 6) {
            $table_laporan = 'ta_kartu_pajak_hiburan';
            $pajak = 'Pajak Hiburan';
        } else if ($jns_pajak == 7) {
            $table_laporan = 'ta_kartu_pajak_reklame';
            $pajak = 'Pajak Reklame';
        } else if ($jns_pajak == 8) {
            $table_laporan = 'ta_kartu_pajak_penerangan';
            $pajak = 'Pajak Penerangan Jalan';
        } else if ($jns_pajak == 9) {
            $table_laporan = 'ta_kartu_pajak_mineral';
            $pajak = 'Pajak Mineral Bukan Logam Dan Lainnya';
        } else if ($jns_pajak == 10) {
            $table_laporan = 'ta_kartu_pajak_parkir';
            $pajak = 'Pajak Parkir';
        } else if ($jns_pajak == 11) {
            $table_laporan = 'ta_kartu_pajak_air';
            $pajak = 'Pajak Air Tanah';
        } else if ($jns_pajak == 12) {
            $table_laporan = 'ta_kartu_pajak_walet';
            $pajak = 'Pajak Sarang Burung Walet';
        } else if ($jns_pajak == 15) {
            $table_laporan = 'ta_kartu_bphtb';
            
        }



        $query = "SELECT `ta_sspd`.`no_sspd`, `ta_sspd`.`tgl_sspd`, `wp_data_umum`.`npwpd`, `wp_wajib_pajak_usaha`.`nm_usaha`, `ta_sspd`.`total_pajak`, `ta_sspd`.`jns_sspd`, `ta_kartu_pajak_pungut`.`masa1`, `ta_kartu_pajak_pungut`.`masa2`, `".$table_laporan."`.`id`, `".$table_laporan."`.`status` FROM `".$table_laporan."`
        LEFT JOIN `ref_rek_6`                  ON `".$table_laporan."`.`id_rek_6`             = `ref_rek_6`.`id_rek_6`
        LEFT JOIN `ta_sspd`                    ON `".$table_laporan."`.`pungut_id`            = `ta_sspd`.`pungut_id`
        LEFT JOIN `ta_kartu_pajak_pungut`      ON `ta_sspd`.`pungut_id`                       = `ta_kartu_pajak_pungut`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha_pajak` ON `ta_kartu_pajak_pungut`.`wp_usaha_pajak_id` = `wp_wajib_pajak_usaha_pajak`.`id`
        LEFT JOIN `wp_wajib_pajak_usaha`       ON `wp_wajib_pajak_usaha_pajak`.`wp_usaha_id`  = `wp_wajib_pajak_usaha`.`id`
        LEFT JOIN `wp_wajib_pajak`             ON `wp_wajib_pajak_usaha`.`wp_id`              = `wp_wajib_pajak`.`id`
        LEFT JOIN `wp_data_umum`               ON `wp_wajib_pajak`.`data_umum_id`             = `wp_data_umum`.`id`

        WHERE `".$table_laporan."`.`status` = '1' AND `ta_kartu_pajak_pungut`.`masa1` BETWEEN '".$tgl1."' AND '".$tgl2."' ORDER BY `".$table_laporan."`.`id` DESC";
       
        $query = $this->db->query($query)->result();
        $kota = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())")->row(); 
        $this->load->library('fpdf_gen');
        $pdf = new fpdf('L','mm','A4');
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',14);
        $image = base_url('./assets/img/'.$kota->logo);
        $pdf->Image($image,12,10,20,20);
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'DAFTAR SURAT SETORAN PAJAK DAERAH WAJIB PAJAK (SSPD WP)' ,0,1,'C');
        $pdf->Cell(50,7, "", 0,0,'C');
        $pdf->Cell(190,7,'PERIODE '.tgl_id_laporan($tgl1).' - '.tgl_id_laporan($tgl2) ,0,1,'C');

        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(7,6,'No',1,0);
        $pdf->Cell(45,6,'No SSPD',1,0);
        $pdf->Cell(20,6,'TGL',1,0);
        $pdf->Cell(40,6,'NPWP',1,0);
        $pdf->Cell(50,6,'NAMA USAHA',1,0);
        $pdf->Cell(40,6,'MASA',1,0);
        $pdf->Cell(45,6,'KETERANGAN',1,0);
        $pdf->Cell(35,6,'NILAI',1,1);
        $pdf->SetFont('Arial','',10);
        $pdf->Cell(282,6,$pajak, 1,1);
        $no = 1;
        $total_pajak_keseluruhan = 0;
        foreach ($query as $data) {
            if ($data->jns_sspd == 1) {
                $jenis_pembayaran = 'SSPD Masa';
            }elseif ($data->jns_sspd == 2) {
                $jenis_pembayaran = 'SSPD Kurang Bayar';
            }else {
                $jenis_pembayaran = 'SSPD Sanksi';
            }
            $total_pajak_keseluruhan+= $data->total_pajak;
            $pdf->Cell(7,6, $no ,1,0);
            $pdf->Cell(45,6, $data->no_sspd ,1,0);
            $pdf->Cell(20,6, tgl_id_laporan($data->tgl_sspd) ,1,0);
            $pdf->Cell(40,6, $data->npwpd ,1,0);
            $pdf->Cell(50,6, $data->nm_usaha ,1,0);
            $pdf->Cell(40,6, tgl_id_laporan($data->masa1).' - '.tgl_id_laporan($data->masa2) ,1,0);
            $pdf->Cell(45,6, $jenis_pembayaran ,1,0);
            $pdf->Cell(35,6, uang($data->total_pajak) ,1,1);
            $no++;
        }
        $pdf->Cell(7,6,'',1,0);
        $pdf->Cell(45,6,'',1,0);
        $pdf->Cell(20,6,'',1,0);
        $pdf->Cell(40,6,'',1,0);
        $pdf->Cell(50,6,'',1,0);

        $pdf->Cell(85,6,'JUMLAH',1,0);
        $pdf->Cell(35,6,uang($total_pajak_keseluruhan),1,1);

        $pdf->Cell(7,6,'',1,0);
        $pdf->Cell(45,6,'',1,0);
        $pdf->Cell(20,6,'',1,0);
        $pdf->Cell(40,6,'',1,0);
        $pdf->Cell(50,6,'',1,0);

        $pdf->Cell(85,6,'JUMLAH PERIODE INI',1,0);
        $pdf->Cell(35,6,uang($total_pajak_keseluruhan),1,1);

        $pdf->Cell(7,6,'',1,0);
        $pdf->Cell(45,6,'',1,0);
        $pdf->Cell(20,6,'',1,0);
        $pdf->Cell(40,6,'',1,0);
        $pdf->Cell(50,6,'',1,0);

        $pdf->Cell(85,6,'JUMLAH PERIODE SEBELUMNYA',1,0);
        $pdf->Cell(35,6,uang(0),1,1);

        $pdf->Cell(7,6,'',1,0);
        $pdf->Cell(45,6,'',1,0);
        $pdf->Cell(20,6,'',1,0);
        $pdf->Cell(40,6,'',1,0);
        $pdf->Cell(50,6,'',1,0);

        $pdf->Cell(85,6,'JUMLAH SELURUHNYA',1,0);
        $pdf->Cell(35,6,uang($total_pajak_keseluruhan),1,1);

        $pdf->Output();
    }
}
