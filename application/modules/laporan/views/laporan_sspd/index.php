<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Data Umum</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <form method="GET" action="<?= @$url ?>/laporan">
                    <div class="form-body">
                        <fieldset>
                        
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Bulan</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="bulan">
                                        <option value="all">Pilih Bulan</option>
                                        <option value="01">Januari</option>
                                        <option value="02">Februari</option>
                                        <option value="03">Maret</option>
                                        <option value="04">April</option>
                                        <option value="05">Mei</option>
                                        <option value="06">Juni</option>
                                        <option value="07">Juli</option>
                                        <option value="08">Agustus</option>
                                        <option value="09">September</option>
                                        <option value="10">Oktober</option>
                                        <option value="11">November</option>
                                        <option value="12">Desember</option>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Tahun</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <select name="tahun" class="form-control">
                                    
                                    <?php 
                                    $now = date('Y'); 
                                    for ($i = $now-2; $i<=$now+2; $i++){
                                        if ($i == $now) {
                                            $select = "selected='selected'";
                                        }else{
                                            $select = "";
                                        }
                                    ?>

                                        <option value="<?=$i; ?>" <?=$select  ?>><?=$i; ?></option>
                                    <?php } ?>
                                    </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            
                        </fieldset>
                        <button class="btn btn-primary" type="submit">Cek Laporan</button>
                    </div>
                </form>
            </div>
        </div>