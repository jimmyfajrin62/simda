<?php  
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporan-sptpd-wp-self-assesment".date('d-M-Y').".xls");
?>
<table border="1">
	
	<thead>
		<tr>
			<th colspan="3"></th>
			<th colspan="2">
				<center><h3>
					PEMERINTAH KOTA PASURUAN<br>
					DAFTAR SPTPD SELF ASSESMENT</h1>
					PERIODE <?php echo tgl_id_laporan($tgl1); ?> s.d. <?php echo tgl_id_laporan($tgl2); ?>
				</h3></center>
			</th>
		</tr>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">TGL TERIMA</th>
			<th rowspan="2">NO REK.<br> NAMA REKENING</th>
			<th rowspan="2">NOMOR DAN.<br> NAMA WP</th>
			<th rowspan="2">NAMA DAN.<br> ALAMAT USAHA WP</th>
			<th colspan="2">SPTPD</th>
			<th rowspan="2">OMSET <br> NILAI PAJAK</th>
			<th rowspan="2">KETERANGAN</th>

		</tr>
		<tr>
			<th>NOMOR DAN <br> TANGGAL</th>
			<th>MASA PAJAK</th>
		</tr>
		<tr>
			<th colspan="9" style="text-align: left;">
				<i>Jenis Pajak : <?php echo $pajak; ?></i>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		if($laporan) {
		$no = 1; 
		$total_pengenaan = 0;
		$total_terhutang = 0;
			foreach ($laporan as $data) {
			if ($data->jns_sptpd == 1) {
                $jenis_pembayaran = 'sptpd masa';
            }else{
                $jenis_pembayaran = 'sptpd jabatan';
            }
            $total_pengenaan += $data->dasar_pengenaan;
            $total_terhutang += $data->pajak_terhutang;
		?>
			<tr>
				<td><?=$no; ?></td>
				<td><?=tgl_id_laporan($data->tgl_terima); ?></td>
				<td></td>
				<td><?php echo $data->npwpd."<br>".$data->nm_usaha; ?></td>
				<td><?php echo $data->nm_usaha."<br>".$data->alamat_usaha; ?></td>
				<td><?php echo $data->no_sptpd."<br>".tgl_id_laporan($data->tgl_sptpd); ?></td>
				<td><?php echo tgl_id_laporan($data->masa1)." - ".tgl_id_laporan($data->masa2) ?></td>
				<td><?php echo uang($data->dasar_pengenaan)."<br>".uang($data->pajak_terhutang); ?></td>
				<td><?=$jenis_pembayaran ?></td>
			</tr>
		<?php $no++;} } ?>
		<tr>
			<td colspan="6"></td>
			<td>Total</td>
			<td><?php echo uang($total_pengenaan); ?><br><?php echo uang($total_terhutang) ?></td>
		</tr>
	</tbody>
</table>