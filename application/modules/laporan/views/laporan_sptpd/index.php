<div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Data Umum</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <form method="GET" action="<?= @$url ?>/laporan">
                    <div class="form-body">
                        <fieldset>
                        
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Jenis Pajak</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <select class="form-control" name="jns_pajak" required="">
                                       <option>Pilih Jenis Pajak</option>
                                        <?php foreach ($jns_pajak as $key => $value) { ?>
                                            <option value="<?=$value->id_rek_4?>"><?=$value->nm_rek_4?></option>
                                      <?php } ?>
                                     </select>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Tanggal Awal</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="date" name="tgl1" class="form-control" required="">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1"><strong>Tanggal Akhir</strong>
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="date" name="tgl2" class="form-control" required="">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            
                        </fieldset>
                        <button class="btn btn-primary" type="submit">Cek Laporan</button>
                    </div>
                </form>
            </div>
        </div>