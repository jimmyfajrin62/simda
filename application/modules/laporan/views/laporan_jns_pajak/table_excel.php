<?php  
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporan-jenis-pajak-".date('d-M-Y').".xls");
?>
<table border="1">
	<tr>
		<td></td>
		<td colspan="5"> <center>Laporan Jenis Pajak Tahun <?=$tahun ?> <?php if($bulan != 'all') echo " Bulan " .$bulan ?></center></td>
	</tr>
	<tr>
		<td colspan="7"></td>
	</tr>
	<tr>
		<th></th>
		<th>No</th>
		<th>No. SSPD</th>
		<th>Jenis Pajak</th>
		<th>Masa Pajak</th>
		<th>Pajak Yang Dibayar</th>
	</tr>
	<?php 
	$no=1;  
	$total_pembayaran =0;
	foreach ($laporan_jns_pajak as $data) {
	$total_pembayaran += $data->total_bayar;
	?>
		
	<tr>
		<td></td>
		<td><?=$no; ?></td>
		<td><?=$data->no_sspd; ?></td>
		<td><?=$data->nm_usaha_4; ?></td>
		<td><?php echo tgl_id($data->masa1); ?>-<?php echo tgl_id($data->masa2); ?></td>
		<td><?php echo uang($data->total_bayar); ?></td>
	</tr>
	<?php $no++; } ?>
	<tr>
		<td colspan="4"></td>
		<td>Total</td>
		<td><?=uang($total_pembayaran) ?></td>
	</tr>
</table>