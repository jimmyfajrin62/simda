<?php  
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporan-sptpd-wp-self-assesment".date('d-M-Y').".xls");
?>
<table border="1">
	
	<thead>
		<tr>
			<th colspan="3"></th>
			<th colspan="2">
				<center><h3>
					PEMERINTAH KOTA PASURUAN<br>
					DAFTAR SSPD WP</h1>
					PERIODE <?php echo tgl_id_laporan($tgl1); ?> s.d. <?php echo tgl_id_laporan($tgl2); ?>
				</h3></center>
			</th>
		</tr>
		<tr>
			<th rowspan="2">No</th>
			<th rowspan="2">NO. SSPD</th>
			<th rowspan="2">TANGGAL</th>
			<th rowspan="2">NPWP</th>
			<th rowspan="2">NAMA USAHA WP</th>
			<th colspan="2">NILAI</th>
			<th colspan="2">PERIODE</th>

		</tr>
		<tr>
			<th>NOMOR DAN <br> TANGGAL</th>
			<th>MASA PAJAK</th>
		</tr>
		<tr>
			<th colspan="9" style="text-align: left;">
				<i>Jenis Pajak : <?php echo $pajak; ?></i>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		if($laporan) {
		$no = 1;
		?>
			<tr>
				<td><?=$no; ?></td>
				<td><?php echo $data->no_sspd; ?></td>
				<td><?php echo tgl_id_laporan($data->tgl_sspd); ?></td>
				<td><?php echo $data->npwpd; ?></td>
				<td><?php echo $data->nm_usaha; ?></td>
				<td><?php echo uang($data->total_pajak); ?></td>
				<td><?php echo tgl_id_laporan($data->masa1)." - ".tgl_id_laporan($data->masa2) ?></td>
			</tr>
		<?php $no++;} } ?>
		<tr>
			<td colspan="6"></td>
			<td>Total</td>
			<td><?php echo uang($total_pajak); ?></td>
		</tr>
	</tbody>
</table>