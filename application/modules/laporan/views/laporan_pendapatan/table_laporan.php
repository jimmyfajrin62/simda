<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                  
                        <div class="btn-group btn-group-devided">
                            <div class="clearfix">
                                     <span style='display: inline;'></span>
                                <a  href="<?php echo $url ?>/export_excel/<?php echo @$_GET['bulan'] ?>/<?php echo @$_GET['tahun'] ?>" target="_blank" class="btn btn-icon-only red-thunderbird tooltips" data-original-title="Export Excel">
                                    <i class="fa fa-file-excel-o"></i>

                    </a>

                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php 
$actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
echo $actual_link;
                                ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

<!--                         <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                   
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue-steel">
                            <option value="">Select...</option>
                            <option value="99">Delete</option>
                            <option value="0">InActive</option>
                            <option value="1">Active</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue-steel table-group-action-submit">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>
                    
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="30px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="20px">No. </th>
                                <th width="20px">No SSPD </th>
                                <th width="30px">Jenis Pembayaran </th>
                                <th width="200px">Masa Pajak </th>
                                <th width="50px">Pajak Yang Dibayar </th>
                            </tr>

                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="no_sspd" placeholder="No SSPD">
                                </td>
                                <td>
                                    <div class="clearfix">
                                        <input type="text" class="form-control form-filter input-sm" name="jenis_pembayaran" placeholder="Jenis Pembayaran">
                                        
                                        
                                    </div>
                                </td>
                                 <td class="text-center">
                                    <div class="input-group margin-bottom-5" id="defaultrange">
                                        <input type="text" class="form-control form-filter input-sm" name="masa" placeholder="Masa Pajak">
                                    </div>
                                </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <input type="text" class="form-control form-filter input-sm" name="pajak_dibayar" placeholder="Masa Pajak">
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<script type="text/javascript">
    jQuery(document).ready(function() {

        // table data
        var select_url = '<?php echo $url ?>' + '/select/<?php echo @$_GET['bulan'] ?>/<?php echo @$_GET['tahun'] ?>';

        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-right" }
        ];
        var order = [
            [2, "asc"]
        ];

        var sort = [-1,0,1];

        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        // bs select setelah datatable, bug
        Helper.bsSelect();

    });
</script>
