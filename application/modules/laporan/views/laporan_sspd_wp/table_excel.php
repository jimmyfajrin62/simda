<?php  
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=laporan-sspd-wp-self-assesment".date('d-M-Y').".xls");
?>
<table border="1">
	
	<thead>
		<tr>
			<th colspan="2"><center>LOGO</center></th>
			<th colspan="7">
				<center><h3>
					DAFTAR SURAT SETORAN PAJAK DAERAH WAJIB PAJAK (SSPD WP)<br>
					PERIODE <?php echo tgl_id_laporan($tgl1); ?> s.d. <?php echo tgl_id_laporan($tgl2); ?>
				</h3></center>
			</th>
		</tr>
		<tr>
			<th colspan="1">No.</th>
			<th colspan="1">NO. SSPD</th>
			<th colspan="1">TANGGAL</th>
			<th colspan="1">NPWP</th>
			<th colspan="1">NAMA USAHA</th>
			<th colspan="1">MASA</th>
			<th colspan="1">KETERANGAN</th>
			<th colspan="2">NILAI</th>
		</tr>
		<tr>
			<th colspan="9" style="text-align: left;">
				<i><?php
				$no = 1;
				echo $no++.". ".$pajak; ?></i>
			</th>
		</tr>
	</thead>
	<tbody>
		<?php 
		if($laporan) {
		$no = 1;
		$total_pajak_keseluruhan = 0;
			foreach ($laporan as $data) {
			if ($data->jns_sspd == 1) {
                $jenis_pembayaran = 'SSPD Masa';
            }elseif ($data->jns_sspd == 2) {
                $jenis_pembayaran = 'SSPD Kurang Bayar';
            }else {
                $jenis_pembayaran = 'SSPD Sanksi';
            }
            $total_pajak_keseluruhan+= $data->total_pajak;
		?>
			<tr>
				<td><?=$no; ?></td>
				<td><?php echo $data->no_sspd; ?></td>
				<td><?php echo tgl_id_laporan($data->tgl_sspd); ?></td>
				<td><?php echo $data->npwpd; ?></td>
				<td><?php echo $data->nm_usaha; ?></td>
				<td><?php echo tgl_id_laporan($data->masa1)." - ".tgl_id_laporan($data->masa2) ?></td>
				<td><center><?=$jenis_pembayaran ?></center></td>
				<td colspan="2"><center><?php echo uang($data->total_pajak); ?></center></td>
			</tr>
		<?php $no++;} } ?>
		<tr>
			<td colspan="7" align="right"><b>JUMLAH</b></td>
			<td colspan="2" align="center"><?php echo uang($total_pajak_keseluruhan); ?></td>
		</tr>
		<tr>
			<td colspan="7" align="right"><b>JUMLAH PERIODE INI</b></td>
			<td colspan="2" align="center" rules="cols" ><?php echo uang($total_pajak_keseluruhan); ?></td>
		</tr>
		<tr>
			<td colspan="7" align="right"><b>JUMLAH PERIODE SEBELUMNYA</b></td>
			<td colspan="2" align="center"><?php echo uang(0); ?></td>
		</tr>
		<tr>
			<td colspan="7" align="right"><b>JUMLAH SELURUHNYA</b></td>
			<td colspan="2" align="center"><?php echo uang($total_pajak_keseluruhan); ?></td>
		</tr>
	</tbody>
</table>