<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fetch extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    /* Start Fetch Core Controller */
    public function get_fetch( $table, $prefix, $name, $nostatus = null )
    {
        $q                                       = $_GET['q'];
        $where[$prefix.'_'.$name .' LIKE']       = '%'.$q.'%';
        if(!$nostatus) $where[$prefix.'_status'] = '1';

        $result = $this->m_global->get( $table, null, $where, '*', null, null, 0, 5, null, 1 );

        $data = [];
        for ($i=0; $i < count( $result ); $i++) {
            $data[$i] = ['id' => $result[$i][$prefix.'_id'], 'text' => $result[$i][$prefix.'_'.$name] ];
        }

        echo json_encode( ['item' => $data] );
    }

    public function get_user( $table, $prefix, $name, $nostatus = null, $e_name = null, $e_val = null )
    {
        $q                                       = $_GET['q'];
        $where[$prefix.'_'.$name .' LIKE']       = '%'.$q.'%';
        $where_e[$prefix.'_'.$e_name]            = "'".$e_val."'";
        if(!$nostatus) $where[$prefix.'_status'] = '1';

        $result = $this->m_global->get( $table, null, $where, '*', $where_e, null, 0, 5, null, 1 );

        $data   = [];
        for ($i=0; $i < count( $result ); $i++) {
            $data[$i] = ['id' => $result[$i][$prefix.'_id'], 'text' => $result[$i][$prefix.'_'.$name] ];
        }

        echo json_encode( ['item' => $data, 'sql' => $this->db->last_query()] );
    }

    public function quick( $prefix )
    {
        $q                           = $_GET['q'];
        $where[$prefix.'_nama LIKE'] = '%'.$q.'%';
        $where[$prefix.'_status']    = '1';

        $result = $this->m_global->get( $prefix, null, $where, '*', null, null, 0, 5, null, 1 );

        $data   = [];
        for ($i=0; $i < count($result); $i++) {
            $data[$i] = ['id' => $result[$i][$prefix.'_id'], 'text' => $result[$i][$prefix.'_nama']];
        }

        echo json_encode( ['item' => $data] );
    }

    /*
        Contoh Fetch untuk select2
    */
    public function unit()
    {
        $q                       = $_GET['q'];
        $where['unit_nama LIKE'] = '%'.$q.'%';
        $where['unit_status']    = '1';

        $result = $this->m_global->get( 'unit', null, $where, '*', null, null, 0, 5 );

        $data   = [];
        for ($i=0; $i < count($result); $i++) {
            $data[$i] = ['id' => $result[$i]->unit_id, 'text' => $result[$i]->unit_nama];
        }

        echo json_encode( ['item' => $data] );
    }

}

/* End of file config.php */
/* Location: ./application/modules/config/controllers/config.php */
