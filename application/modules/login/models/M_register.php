<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_register extends CI_Model
{
	private $table_db       = 'user';
	
    public function __construct()
    {
        parent::__construct();
    }
	
	public function verifikasi_email($user, $code)
    {
		$arrquery = $this->db->query("SELECT * FROM user WHERE user_name = '$user' AND user_verification_code = '$code'")->row_array();
        if($arrquery['user_status'] != "0"){
            $result = '2';
        }else if($arrquery['user_id'] != ""){
            $this->db->where('user_name', $user);
            $this->db->where('user_verification_code', $code);
            $this->db->update('user', array('user_status' => '1'));
			$result = '1';
		}else{
			$result = '0';
		}
		return $result;
	}
	
    public function show_sspd($id)
    {
        $query = $this->db->query(" SELECT b.no_sptpd, c.nm_bank, c.no_rekening, d.nip_penandatangan, e.nm_jab, a.*
                                    FROM ta_sspd a
                                    left join ta_kartu_pajak_pungut b on a.pungut_id = b.id
                                    LEFT JOIN ref_bank c ON a.kd_bank = c.id
                                    left join ref_penandatangan d on a.ttd_dok_id = d.no_urut
                                    left join ref_jabatan e on d.jbt_penandatangan = e.kd_jab
                                    WHERE a.id = $id");
        return $query->row();
    }

    public function sum_sspd($id, $table)
    {
        $query = $this->db->query("SELECT sum(pajak_terhutang) as total_pajak
                                    from $table
                                    where pungut_id = $id");
        return $query->row();
    }

    public function header($id)
    {
        $query = $this->db->query("SELECT d.npwpd, d.nama_pendaftar, b.nm_usaha, b.alamat_usaha
                                    from wp_wajib_pajak_usaha b
                                    left join wp_wajib_pajak c on b.wp_id = c.id
                                    left join wp_data_umum d on c.data_umum_id = d.id
                                    where d.id = $id");
        return $query->row();
    }

    public function select_sptpd($id)
    {
        $query = $this->db->query("SELECT a.id, a.no_sptpd, a.tgl_sptpd, c.nm_usaha
                                    FROM ta_kartu_pajak_pungut a
                                    LEFT JOIN wp_wajib_pajak_usaha_pajak b ON a.wp_usaha_pajak_id = b.id
                                    LEFT JOIN wp_wajib_pajak_usaha c ON b.wp_usaha_id = c.id
                                    LEFT JOIN wp_wajib_pajak d ON c.wp_id = d.id
                                    LEFT JOIN wp_data_umum e ON d.data_umum_id = e.id
                                    where a.id = $id");
        return $query->row();
    }

    public function show_bank()
    {
        $query = $this->db->query("SELECT kd_bank, nm_bank, no_rekening from ref_bank");
        return $query->result();
    }

    public function petugas()
    {
        $query = $this->db->query("SELECT a.no_urut, a.nm_penandatangan, a.nip_penandatangan, b.nm_jab
                                    from ref_penandatangan a
                                    left join ref_jabatan b on a.no_urut = b.kd_jab
                                    where no_dok = 12");
        return $query->result();
    }

    public function auto_id()
    {
        $query = $this->db->query("SELECT id from ta_sspd order by id desc");
        if ($query->num_rows() == 0) {
            return 1;
        } else {
            $data = $query->row();
            return $val  = $data->id + 1;
        }
    }

    public function no_sspd($prefix=null, $sufix=null)
    {
        $query = $this->db->query("SELECT id from ta_sspd order by id desc");
        if ($query->num_rows() == 0) {
            return $this->generate_id(1, $prefix, $sufix);
        } else {
            $data = $query->row();
            $val  = $data->id + 1;
            return $this->generate_id($val, $prefix, $sufix);
        }
    }

    public function generate_id($num, $prefix, $sufix)
    {
        $start_dig = 6;
        $num_dig   = strlen($num);
        $id        = $num;

        if ($num_dig <= $start_dig) {
            $num_zero = $start_dig - $num_dig;

            for ($i=0;$i< $num_zero; $i++) {
                $id = '0' . $id;
            }
        }

        $id = $prefix . $id . $sufix;
        return $id;
    }

    public function kota()
    {
        $query = $this->db->query("select * from ta_data_umum_pemda where tahun = year(CURDATE())");
        return $query->row();

    }

    public function data($pungut_id)
    {
        $query = $this->db->query("select 
                                     f.nama_pendaftar, f.npwpd, f.jalan, f.rtrw, h.nm_kec, g.nm_kel, f.kabupaten, f.kode_pos, 
                                     d.nm_usaha, d.alamat_usaha,
                                     b.no_sptpd, b.masa1, b.masa2, a.jns_pembayaran, 
                                     a.kd_bank, a.rek_penyetor, a.keterangan, a.no_sspd, i.nm_bank, a.nm_penyetor
                                    from ta_sspd a
                                    left join ref_bank i on a.kd_bank = i.kd_bank
                                    join ta_kartu_pajak_pungut b on a.pungut_id = b.id
                                    join wp_wajib_pajak_usaha_pajak c on b.wp_usaha_pajak_id = c.id
                                    join wp_wajib_pajak_usaha d on c.wp_usaha_id = d.id
                                    join wp_wajib_pajak e on d.wp_id = e.id
                                    join wp_data_umum f on e.data_umum_id = f.id
                                    join ref_kelurahan g on f.kd_kel = g.kel_id
                                    join ref_kecamatan h on f.kd_kec = h.kd_kec

                                    WHERE a.pungut_id = $pungut_id");
        return $query->row();
        
    }

    public function jumlah($pungut_id)
    {

        $query = $this->db->query("select sum(pajak_terhutang ) as jumlah from ta_sspd where pungut_id = $pungut_id");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

    public function data2($pungut_id)
    {
        $query = $this->db->query("SELECT `a`.`status_nota`, `a`.`id`,
                                 `a`.`pungut_id`, `a`.`status`,
                                 `a`.`lastupdate`, `f`.`npwpd`,
                                 `a`.`no_sspd`, `a`.`total_pajak`,
                                 `a`.`total_bayar`, `a`.`pajak_terhutang`,
                                 `a`.`keterangan`,g.nm_rek_6, g.kd_rek_1, g.kd_rek_2, g.kd_rek_3, g.kd_rek_4 ,g.kd_rek_5, g.kd_rek_6
                                FROM `ta_sspd` `a`
                                LEFT JOIN `ta_kartu_pajak_pungut` `b` ON `a`.`pungut_id` = `b`.`id`
                                LEFT JOIN `wp_wajib_pajak_usaha_pajak` `c` ON `b`.`wp_usaha_pajak_id` = `c`.`id`
                                LEFT JOIN `wp_wajib_pajak_usaha` `d` ON `c`.`wp_usaha_id` = `d`.`id`
                                LEFT JOIN `wp_wajib_pajak` `e` ON `d`.`wp_id` = `e`.`id`
                                LEFT JOIN `wp_data_umum` `f` ON `e`.`data_umum_id` = `f`.`id`
                                left join ref_rek_6 g on c.jns_pajak = g.id_rek_6
                                WHERE a.status = '1' and a.pungut_id = $pungut_id");

        return $query->result();
        
    }

    public function get_total_pajak($id)
    {

        $query = $this->db->query("select id from ta_nota where id = $id");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }

    public function show_nota_rinci($id)
    {

        $query = $this->db->query("select total_sanksi, id, total_bayar, kurang_bayar from ta_nota_rinc where nota_id = $id");
        // echo $this->db->last_query($query);exit();
        return $query->row();
    }
        


}
