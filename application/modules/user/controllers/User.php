<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends Admin_Controller
{
    private $prefix         = 'user';
    private $url            = 'user';
    private $table_db       = 'user';
    private $path           = ''; //optional
    private $table_prefix   = 'user_'; //optional
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
    {
        parent::__construct();
    }

	public function index()
	{
        // echo "<pre>",print_r($this->session->all_userdata()),exit();
        $data['pagetitle']  = 'User';
        $data['subtitle']   = 'manage user';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'User' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'index', $data, $js, $css );
	}

    public function show_add()
    {
        $data['pagetitle']  = 'User';
        $data['subtitle']   = 'add user';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'User' => $this->url, 'Add' => $this->url.'/show_add' ];

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'add', $data, $js, $css );
    }

    public function show_edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_db, null, ['user_id' => $id] )[0];

        $data['pagetitle']  = 'User';
        $data['subtitle']   = 'manage user';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'User' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'edit', $data, $js, $css );
    }


    public function action_add()
    {
        $this->form_validation->set_rules('name', 'Nama', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            //jika melakukak upload file
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload( 'image' ) ){
                    $data['status']      = 0;
                    $data['message']     = $this->upload->display_errors();
                    echo json_encode( $data );
                    die();
                } else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image'] = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'name']     = $this->input->post('name');
            $data[$this->table_prefix.'password'] = md5_mod($this->input->post('password'), $this->input->post('email'));
            $data[$this->table_prefix.'email']    = $this->input->post('email');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] ){
                $data['status']  = 1;
                $data['message'] = 'Successfully add User with Name <strong>'.$this->input->post('name').'</strong>';
                echo json_encode( $data );
            } else {
                $data['status']  = 0;
                $data['message'] = 'Failed add User with Name <strong>'.$this->input->post('name').'</strong>';
                if(ENVIRONMENT == 'development'){
                    $data['error']  = $this->db->error();
                }
                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']  = 3;
            $str             = ['<p>', '</p>'];
            $str_replace     = ['<li>', '</li>'];
            $data['message'] = str_replace( $str, $str_replace, validation_errors() );
            echo json_encode( $data );
        }
    }

    public function action_edit( $id )
    {
        $this->form_validation->set_rules('name', 'Nama', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'name'] = $this->input->post('name');

            if ($this->input->post('password')){
                $data[$this->table_prefix.'password']   = md5_mod($this->input->post('password'), $this->input->post('email'));
            }

            $data[$this->table_prefix.'email']          = $this->input->post('email');

            $result = $this->m_global->update($this->table_db, $data, ['user_id' => $id]);

            if ( $result ){
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('name').'</strong>';
                echo json_encode( $data );
            } else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('name').'</strong>';
                if(ENVIRONMENT == 'development'){
                    $data['error']  = $this->db->error();
                }
                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );
            echo json_encode( $data );
        }
    }

    public function select()
    {
        // join
        $join = null;

        // if action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) ){
                $this->change_status($_REQUEST['customActionName'], ['user_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        // searching
        $aCari = [
            'name'       => 'user_name',
            'email'      => 'user_email',
            'lastupdate' => 'user_lastupdate'
        ];

        // where, order
        $where      = NULL;
        $where_e    = NULL;

        // if lastupdate show in table
        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value ){
                if ( $_REQUEST[$key] != '' ){
                    if ( $key == 'lastupdate' ){
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(user_lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    } else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        // filter show active, inactive, delete
        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = "user_status = '$request'";
        }
        else {
            $where_e = "user_status = '1'";
        }

        // pagination
        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords   = $this->m_global->count( $this->table_db, $join, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);
        $records         = array();
        $records["data"] = array();
        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        // select and query
        $select          = 'user_id, user_status,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_db, $join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        // throw in view table
        $i               = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->user_id.'"/><span></span></label>',
                $i,
                $rows->user_name,
                $rows->user_email,
                $rows->user_lastupdate,
                '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit/'.$rows->user_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->user_id.'/'.
                            ($rows->user_status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"' ) ).' class="btn btn-icon-only tooltips '.
                            ($rows->user_status == 0 ? 'grey-cascade' : 'green-seagreen' ). '" onClick="return f_status(1, this, event)"><i title="'.
                            ($rows->user_status == 0 ? 'InActive' : ($rows->user_status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.
                            ($rows->user_status == 0 ? '-eye-slash' : ($rows->user_status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->user_id.'/99/'.
                            ($rows->user_status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode( $records );
    }

    // change status batch
    public function change_status( $status, $where )
    {
        $data[ $this->table_prefix.'status' ]   = $status;

        $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    }

    // change status
    public function change_status_by( $id, $status, $stat = FALSE )
    {
        if ( $stat ){
            $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );
        } else{
            $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id' => $id]);
        }

        if ( $result ){
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function print_pdf()
    {
        // header('Content-Type: application/pdf');
        // header('Content-Disposition: inline; filename="VivesPromocion.pdf"');
        define('K_PATH_IMAGES', 'assets/img/');
        $this->load->library('Tpdf');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);

        // document information
        $pdf->SetCreator('SIMDA');
        $pdf->SetTitle('Invoice SPTPD masa');
        $pdf->SetSubject('SPTPD MASA');

        // header & footer
        // $pdf->SetHeaderData('logo.png', 15, 'PEMERINTAH KOTA PASURUAN', 'BADAN PENDAPATAN DAERAH', array(48,89,112), array(48,89,112) );
        // $pdf->SetFooterData(array(0,0,0), array(0,0,0));
        // $pdf->SetHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // $pdf->SetFooterFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

        // set margin
        // $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        // $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        // $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(6, 9, 7);

        // set typography
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set page break
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set scaling image
        $pdf->SetImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        $pdf->setFontSubsetting(TRUE);
        $pdf->SetFont('helvetica', '', 14, '', TRUE);

        // page
        $pdf->AddPage();
        $data['user'] = $this->m_global->get( $this->table_db );
        $html = $this->load->view('pdf', $data, true);

        // render & output
        $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, TRUE, '', TRUE);
        $pdf->Output('contoh.pdf', 'I');
        // exit($pdf->Output('contoh.pdf', 'I'));
    }


    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'User Report';

        // query
        $data['report']  = $this->db->query("SELECT user_full_name, user_name, user_email, user_lastupdate from user")->result();

        $data['namaFile']   ='User_Report_'.date('Y-m-d');
        $data['title']      ='User Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
                                ['No', '8'], ['Fullname', '30'], ['Name', '30'], ['Email', '30'], ['Lastupdate', '30']
        ];

        $this->load->view('export',$data);
    }


    public function import_excel()
    {
        $config['upload_path']   = './assets/upload/import/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['overwrite']     = TRUE;
        $config['file_name']     = date('Ymdhis').'-area';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('file')){
            $report['status']   = '0';
            $report['message']  = 'Invalid file uploaded!';
            $report['err']      = $this->upload->display_errors().display($_FILES);
        }
        else{
            $data          = array('upload_data' => $this->upload->data());
            $inputFileName = $data['upload_data']['full_path'];

            $this->load->library('excel');

            try {
                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                $objReader     = PHPExcel_IOFactory::createReader($inputFileType);
                $objPHPExcel   = $objReader->load($inputFileName);
            }
            catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }

            // Read Sheet by Name
            // $sheetName = 'Area';
            // $sheet = $objPHPExcel->getSheetByName($sheetName);

            // Read Sheet By Index
            $sheet = $objPHPExcel->getSheet(0);

            if(!empty($sheet)){
                $highestRow    = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                $startColumn   = 'B';
                $endColumn     = '5';

                $jml           = 0;
                $jmlA          = 0;

                for ($row = $endColumn; $row <= $highestRow; $row++){
                    $dataSheet = $sheet->rangeToArray($startColumn . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $param     = [];
                    $param[$this->table_prefix.'name']        = $dataSheet[0][1];
                    $param[$this->table_prefix.'email']       = $dataSheet[0][2];
                    $param[$this->table_prefix.'salt']        = generate_salt();
                    $param[$this->table_prefix.'pass']        = md5_mod($dataSheet[0][3], $param[$this->table_prefix.'salt'] );
                    $param[$this->table_prefix.'jk']          = $dataSheet[0][4] == 'L' ? '0': '1';
                    $param[$this->table_prefix.'birth_date']  = date('Y-m-d', strtotime($dataSheet[0][5]));
                    $param[$this->table_prefix.'birth_place'] = $dataSheet[0][6];
                    $param[$this->table_prefix.'religion']    = @religion_to_index($dataSheet[0][7]);
                    $param[$this->table_prefix.'biaya']       = str_replace(',', '', $dataSheet[0][8]);
                    $param[$this->table_prefix.'level']       = $dataSheet[0][9] == 'Admin' ? '0' : '1';

                    $validate = $this->m_global->validation($this->table_db,  [$this->table_prefix.'email' => $param[$this->table_prefix.'email']] );

                    if($validate){
                        $this->m_global->insert($this->table_db, $param);
                        $jml++;
                    }
                    else {
                        $jmlA++;
                    }
                }

                $report['status']   = '1';
                $report['message']  = 'Successfully imported file!</br>Record add : <strong>'.$jml.'</strong></br>Another Record Exist : <strong>'.$jmlA.'</strong>';
            }
            else {
                $report['status']   = '0';
                $report['message']  = 'Sheet was not found!';
            }

            unlink($data['upload_data']['full_path']);
        }

        echo json_encode($report);
    }

}
