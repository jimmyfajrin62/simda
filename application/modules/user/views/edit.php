<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>

            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?php echo $url; ?>/action_edit/<?php echo $id ?>" class="form-horizontal form-add" method="POST" data-confirm="1" role="form">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Name
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php echo $records->user_name ?>" placeholder="Nama" required name="name">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Email
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="email" class="form-control" value="<?php echo $records->user_email ?>" required placeholder="Email" name="email">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Password
                                <span class="required"></span>
                            </label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" placeholder="Password" name="password">
                                <span class="help-block">Keep empty if no changes</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="submit btn green-seagreen">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form><!-- END FORM-->
            </div>
        </div><!-- END VALIDATION STATES-->
    </div>
</div><!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url.'/show_edit/'.$id?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var title   = 'Confirmation';
        var text    = 'Are you sure to continue this action?';
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message, title, text );
    });
</script>
