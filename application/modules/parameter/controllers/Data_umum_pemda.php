<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_umum_pemda extends Admin_Controller
{
	private $prefix         = '';
    private $url            = 'parameter/data_umum_pemda';
    private $table_db       = 'ta_data_umum_pemda';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
        $data['records']    = $this->m_global->get( $this->table_db, null)[3];

        $data['url']        = base_url().$this->url;

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Data Umum Pemda';

		$data['breadcrumb'] = [ 'Parameter' => '', 'Data Umum Pemda' => $this->url ];
        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $js['js']           = [ 'form-validation' ];

		$data['records']    = $this->m_global->get( $this->table_db, null, ['tahun' => date('Y')])[0];
        $this->template->display('data_umum_pemda/index', $data, $js);
	}

	public function show_edit()
    {
        $data['records']    = $this->m_global->get( $this->table_db, null)[3];

        $data['url']        = base_url().$this->url;

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Data Umum Pemda';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => '', 'Data Umum Pemda' => $this->url, 'Edit' => $this->url.'/show_edit/'];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'data_umum_pemda/edit', $data, $js );
    }

    public function action_edit()
    {
        // echo $this->input->post();exit();
        $this->form_validation->set_rules('tahun', 'Tahun', 'trim|required');
        $this->form_validation->set_rules('nm_pemda', 'Nama Pemda', 'trim|required');
        $this->form_validation->set_rules('ibukota', 'Ibukota', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('nm_pimpdaerah', 'Nama Pimpinan Daerah', 'trim|required');
        $this->form_validation->set_rules('jab_pimpdaerah', 'Jabatan Pimpinan Daerah', 'trim|required');
        $this->form_validation->set_rules('nm_sekda', 'Nama Sekda', 'trim|required');
        $this->form_validation->set_rules('nip_sekda', 'NIP Sekda', 'trim|required');
        $this->form_validation->set_rules('jbt_sekda', 'Jabatan Sekda', 'trim|required');
        $this->form_validation->set_rules('nm_kepala_kantor', 'Nama Kepala Kantor', 'trim|required');
        $this->form_validation->set_rules('nip_kepala_kantor', 'NIP Kepala Kantor', 'trim|required');
        $this->form_validation->set_rules('jbt_kepala_kantor', 'Jabatan Kepala Kantor', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'tahun']     			= $this->input->post('tahun');
            $data[$this->table_prefix.'nm_pemda']           = $this->input->post('nm_pemda');
            $data[$this->table_prefix.'ibukota']            = $this->input->post('ibukota');
            $data[$this->table_prefix.'alamat']          	= $this->input->post('alamat');
            $data[$this->table_prefix.'nm_pimpdaerah']      = $this->input->post('nm_pimpdaerah');
            $data[$this->table_prefix.'jab_pimpdaerah']     = $this->input->post('jab_pimpdaerah');
            $data[$this->table_prefix.'nm_sekda']          	= $this->input->post('nm_sekda');
            $data[$this->table_prefix.'nip_sekda']          = $this->input->post('nip_sekda');
            $data[$this->table_prefix.'jbt_sekda']          = $this->input->post('jbt_sekda');
            $data[$this->table_prefix.'nm_kepala_kantor']   = $this->input->post('nm_kepala_kantor');
            $data[$this->table_prefix.'nip_kepala_kantor']  = $this->input->post('nip_kepala_kantor');
            $data[$this->table_prefix.'jbt_kepala_kantor']  = $this->input->post('jbt_kepala_kantor');

            $result = $this->m_global->update($this->table_db, $data, ["tahun" => date("Y")]);

            if ( $result ){
                $data['status']     = 1;
                $data['message']    = '<strong>Successfully edit Data Umum Pemda</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = '<strong>Failed edit User Data Umum Pemda </strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function proses_update()
    {
        // echo '<pre>', print_r($this->input->post()), exit();
        // $tes = base_url(). 'assets/img/'.$logo; echo $tes; exit();
        $config['upload_path'] = './assets/img/';
        $config['allowed_types'] = 'gif|jpg|png';
      // $config['max_size']  = '100';
      // $config['max_width']  = '1024';
      // $config['max_height']  = '768';


      $this->load->library('upload', $config);
      $this->load->helper("file");

      if ( ! $this->upload->do_upload('logo')){
          $data['logo'] = $this->input->post('logo');
          $file = $_FILES['logo'];

      }
      else{
        $data = array('upload_data' => $this->upload->data());
        $param['logo'] = $data ['upload_data']['file_name'];
        // $param['logo'] = $this->input->post('logo');

        $result = $this->m_global->update($this->table_db, $param, ["tahun" => date("Y")]);

        // unlink(base_url(). 'assets/img/'.$logo);
        delete_files(base_url(). 'assets/img/'.$logo);
        redirect('parameter/data_umum_pemda');
      }

    }

}

/* End of file Data_umum.php */
/* Location: ./application/modules/parameter/data_umum/controllers/Data_umum_pemda.php */
