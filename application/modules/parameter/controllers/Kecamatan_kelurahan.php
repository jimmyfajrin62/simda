<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kecamatan_kelurahan extends Admin_Controller
{
	private $prefix         = 'parameter/kecamatan_kelurahan';
    private $url            = 'parameter/kecamatan_kelurahan';
    private $table_kecamatan= 'ref_kecamatan';
    private $table_kelurahan= 'ref_kelurahan';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
		$data['pagetitle']  = 'Kecamatan ';
        $data['subtitle']   = '';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Kecamatan' => $this->url  ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'kecamatan_kelurahan/kecamatan', $data, $js, $css );

	}

	public function show_kelurahan($id)
	{
		$data['records']    = $this->m_global->get( $this->table_kecamatan, null, ['kd_kec' => $id] )[0];
		$data['pagetitle']  = ' Kelurahan';
        $data['subtitle']   = '';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']			= $id;
        $data['breadcrumb'] = [ 'Kecamatan' => $this->url, 'Kelurahan' => $this->url."/show_kelurahan/".$id  ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'kecamatan_kelurahan/kelurahan', $data, $js, $css );

	}

    public function select_kecamatan()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_kec'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_kecamatan'   => 'kd_kec',
            'nama_kecamatan' => 'nm_kec',
            'lastupdate'     => 'lastupdate',
            ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";

        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_kecamatan, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_kecamatan, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_kec.'"/><span></span></label>',
                $i,
                $rows->kd_kec,
                $rows->nm_kec,
                $rows->lastupdate,
                '<a href="'.base_url().$this->url.'/show_kelurahan/'.$rows->kd_kec.'" class="ajaxify btn blue-steel btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_kecamatan/'.$rows->kd_kec.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_kec.'/ref_kecamatan/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_kec.'/ref_kecamatan/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_kecamatan()
    {
        $data['pagetitle']  = 'kecamatan';
        $data['subtitle']   = 'Tambah Kecamatan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'kecamatan' => $this->url, 'Add' => $this->url.'/show_add_kecamatan' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'kecamatan_kelurahan/add_kecamatan', $data, $js );
    }

    public function show_edit_kecamatan( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_kecamatan, null, ['kd_kec' => $id] )[0];

        $data['pagetitle']  = 'User';
        $data['subtitle']   = 'manage user';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'User' => $this->url, 'Edit' => $this->url.'/show_edit_kecamatan/'.$id ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'kecamatan_kelurahan/edit_kecamatan', $data, $js );
    }

    public function action_add_kecamatan()
    {
        $this->form_validation->set_rules('kecamatan', 'kecamatan', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else
                {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'nm_kec']     = $this->input->post('kecamatan');
            $data[$this->table_prefix.'lastupdate'] = date( 'Y-m-d H:i:s');

            $result  = $this->m_global->insert( $this->table_kecamatan, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add User with Name <strong>'.$this->input->post('kecamatan').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add User with Name <strong>'.$this->input->post('kecamatan').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }

        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
            redirect('kecamatan_kelurahan/index');
        }

    }

    public function action_edit_kecamatan($id)
    {
        $this->form_validation->set_rules('nm_kec', 'nm_kec', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'nm_kec'] = $this->input->post('nm_kec');

            $result = $this->m_global->update($this->table_kecamatan, $data, ['kd_kec' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_kec').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_kec').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id[$field.' IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['kd_kec' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['kd_kec' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }


    public function select_kelurahan($id)
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kel_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_kecamatan' => 'kd_kec',
            'kd_kelurahan' => 'kel_id',
            'nm_kelurahan' => 'nm_kel',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e = 'kd_kec='.$id;
        $where   = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }


        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_kelurahan, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kel_id, status,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_kelurahan, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kel_id.'"/><span></span></label>',
                $i,
                $rows->kd_kec,
                $rows->kel_id,
                $rows->nm_kel,
                $rows->lastupdate,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_kel.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_kelurahan/'.$rows->kel_id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by_kelurahan/'.$rows->kel_id.'/ref_kelurahan/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by_kelurahan/'.$rows->kel_id.'/ref_kelurahan/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_kelurahan($id)
    {
        $data['pagetitle']  = 'Kelurahan';
        $data['subtitle']   = 'Tambah Kelurahan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;
        $data['breadcrumb'] = [ 'kecamatan' => $this->url, 'Kelurahan' => $this->url."/show_kelurahan/".$id , 'add' => '' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'kecamatan_kelurahan/add_kelurahan', $data, $js );
    }

    public function action_add_kelurahan()
    {
        $this->form_validation->set_rules('nm_kel', 'nm_kel', 'trim|required');
        $this->form_validation->set_rules('kd_kec', 'kd_kec', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else
                {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_kec']     = $this->input->post('kd_kec');
            $data[$this->table_prefix.'nm_kel']     = $this->input->post('nm_kel');
            $data[$this->table_prefix.'lastupdate'] = date( 'Y-m-d H:i:s');

            $result  = $this->m_global->insert( $this->table_kelurahan, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add User with Name <strong>'.$this->input->post('nm_kel;').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add User with Name <strong>'.$this->input->post('nm_kel').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }

    }

    public function change_status_by_kelurahan($id, $table, $status, $stat = false)
    {
            if ($stat) {
            $result = $this->m_global->delete($table, ['kel_id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['kel_id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function show_edit_kelurahan( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_kelurahan, null, ['kel_id' => $id] )[0];

        $data['pagetitle']  = 'Kelurahan';
        $data['subtitle']   = 'Edit Kelurahan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;
        // $data['kecamatan']  = $data['record'];

        $data['breadcrumb'] = [ 'User' => $this->url, 'Edit' => $this->url.'/show_edit_kelurahan/'.$id ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'kecamatan_kelurahan/edit_kelurahan', $data, $js );
    }

    public function action_edit_kelurahan($id)
    {
        $this->form_validation->set_rules('nm_kel', 'nm_kel', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'nm_kel']           = $this->input->post('nm_kel');
            $result = $this->m_global->update($this->table_kelurahan, $data, ['kel_id' => $id]);

            if ( $result ){
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_kel').'</strong>';
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_kel').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

}

/* End of file kelurahan_kecamatan.php */
/* Location: ./application/modules/parameter/controllers/kelurahan_kecamatan.php */
