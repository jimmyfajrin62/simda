<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perhitungan_sanksi extends Admin_Controller
{
    private $prefix       = 'perhitungan_sanksi';
    private $url          = 'parameter/perhitungan_sanksi';
    private $table_db     = 'ref_sanksi';
    private $table_sat    = 'ref_satuan';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
        $data['pagetitle']  = 'Perhitungan Sanksi Pajak';
        $data['subtitle']   = 'manage sanksi pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Perhitungan Sanksi Pajak' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'perhitungan_sanksi_pajak/index', $data, $js, $css );
	}

	public function show_add()
	{
		$data['pagetitle']  = 'Perhitungan Sanksi Pajak';
        $data['subtitle']   = 'manage sanksi pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['satuan']     = $this->m_global->get($this->table_sat);

        $data['breadcrumb'] = [ 'Perhitungan Sanksi Pajak' => $this->url, 'Add' => $this->url."/show_add" ];

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'perhitungan_sanksi_pajak/add', $data, $js, $css );
	}

	public function action_add()
	{
		// set table db
        $this->table_db     = 'ref_sanksi';

        $this->form_validation->set_rules('kd_sanksi',  'Kode Sanksi',  'trim|required');
        $this->form_validation->set_rules('no_urut',  	'Nomer Urut',  	'trim|required');
        $this->form_validation->set_rules('uraian',    	'Uraian',    	'trim|required');
        $this->form_validation->set_rules('nilai',    	'Nilai',    	'trim|required');
        $this->form_validation->set_rules('satuan',    	'Satuan',    	'trim|required');
        $this->form_validation->set_rules('keterangan',	'Keterangan',   'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
				else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_sanksi']      = $this->input->post('kd_sanksi');
            $data[$this->table_prefix.'no_urut']     	= $this->input->post('no_urut');
            $data[$this->table_prefix.'uraian']        	= $this->input->post('uraian');
            $data[$this->table_prefix.'nilai']        	= $this->input->post('nilai');
            $data[$this->table_prefix.'satuan']        	= $this->input->post('satuan');
            $data[$this->table_prefix.'keterangan']     = $this->input->post('keterangan');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add sanksi with Uraian <strong>'.$this->input->post('uraian').'</strong>';

                echo json_encode( $data );
            }
			else {
                $data['status']     = 0;
                $data['message']    = 'Failed add sanksi with Uraian <strong>'.$this->input->post('uraian').'</strong>';

				if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
		else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
	}

	public function select()
    {
        // echo "test"; exit();
    	$this->table_db = 'ref_sanksi';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_sanksi'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_sanksi'     => $this->table_prefix.'kd_sanksi',
            'uraian'     	=> $this->table_prefix.'uraian',
            'no_urut'     	=> $this->table_prefix.'no_urut',
            'nilai'     	=> $this->table_prefix.'nilai',
            'keterangan'    => $this->table_prefix.'keterangan',
            'lastupdate'	=> $this->table_prefix.'lastupdate'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
					else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request'";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id_sanksi, kd_sanksi, uraian, no_urut, keterangan, satuan, status, lastupdate,'.implode(',' , $aCari);

		$result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
				'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_sanksi.'"/><span></span></label>',
				$rows->kd_sanksi,
				$rows->uraian,
				$rows->no_urut,
				$rows->nilai,
				$rows->keterangan,
				$rows->lastupdate,
				'<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->id_sanksi.'/'.$rows->id_sanksi.'/'.$rows->no_urut.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->id_sanksi.'/ref_sanksi/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->id_sanksi.'/ref_sanksi/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
			);
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_edit($id_sanksi,$kd_sanksi, $no_urut)
    {
        $this->table_db     = 'ref_sanksi';

        $data['records']    = $this->m_global->get($this->table_db, null, ['id_sanksi'=>$id_sanksi,'no_urut'=>$no_urut])[0];

        $data['pagetitle']  = 'Perhitungan Sanksi Pajak';
        $data['subtitle']   = 'manage sanksi pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['satuan']     = $this->m_global->get($this->table_sat);

        $data['breadcrumb'] = [ 'Perhitungan Sanksi Pajak' => $this->url, 'Edit' => $this->url."/show_edit" ];

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'perhitungan_sanksi_pajak/edit', $data, $js, $css );
    }

    public function action_edit($id_sanksi, $kd_sanksi, $no_urut)
    {
        // set table db
        $this->table_db     = 'ref_sanksi';

        $this->form_validation->set_rules('kd_sanksi',  'Kode Sanksi',  'trim|required');
        $this->form_validation->set_rules('no_urut',    'Nomer Urut',   'trim|required');
        $this->form_validation->set_rules('uraian',     'Uraian',       'trim|required');
        $this->form_validation->set_rules('nilai',      'Nilai',        'trim|required');
        $this->form_validation->set_rules('satuan',     'Satuan',       'trim|required');
        $this->form_validation->set_rules('keterangan', 'Keterangan',   'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_sanksi']      = $this->input->post('kd_sanksi');
            $data[$this->table_prefix.'no_urut']        = $this->input->post('no_urut');
            $data[$this->table_prefix.'uraian']         = $this->input->post('uraian');
            $data[$this->table_prefix.'nilai']          = $this->input->post('nilai');
            $data[$this->table_prefix.'satuan']         = $this->input->post('satuan');
            $data[$this->table_prefix.'keterangan']     = $this->input->post('keterangan');

            $result  = $this->m_global->update( $this->table_db, $data,['id_sanksi'=>$id_sanksi, 'kd_sanksi'=>$kd_sanksi, 'no_urut'=>$no_urut] );

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit sanksi with Uraian <strong>'.$this->input->post('uraian').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit sanksi with Uraian <strong>'.$this->input->post('uraian').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

     public function change_status($status, $id)
{
    $status = explode("/", $status);
    $value  = $status[0];
    $field  = $status[1];
    $table  = $status[2];
    $id     = $id['id_sanksi IN '];

    $result = $this->db->query("SELECT status from $table where $field in $id")->row();

    if ($result->status == '99' and $value == '99') {
        $query = $this->db->query("DELETE from $table where $field in $id");
    } else {
        $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
    }
}

    public function change_status_by($id, $table, $status, $stat = false)
{
    if ($stat) {
        $result = $this->m_global->delete($table, ['id_sanksi' => $id]);
        //echo $this->db->last_query(); exit();
    } else {
        $result = $this->m_global->update($table, ['status' => $status], ['id_sanksi' => $id]);
        //echo $this->db->last_query(); exit();
    }

    if ($result) {
        $data['status'] = 1;
    } else {
        $data['status'] = 0;
    }

    echo json_encode($data);
}
}

/* End of file Perhitungan_sanksi_pajak.php */
/* Location: ./application/modules/parameter/controllers/Perhitungan_sanksi_pajak.php */
