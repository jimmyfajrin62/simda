<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peraturan_terkait extends Admin_Controller
{
    private $prefix         = 'parameter/peraturan_terkait';
    private $url            = 'parameter/peraturan_terkait';
    private $table_db       = 'ref_peraturan';
    private $table_db2      = 'ref_jenis_pajak';
    private $table_db3      = 'ref_jenis_retribusi';
    private $table_db4      = 'ref_jenis_peraturan';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Peraturan Terkait';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Peraturan Terkait' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'peraturan_terkait/index', $data, $js, $css );
    }

    public function show_add()
    {
        $data['pagetitle']  = 'Peraturan Terkait';
        $data['subtitle']   = 'add';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['pajak']      = $this->m_global->get($this->table_db2);
        $data['retribusi']  = $this->m_global->get($this->table_db3);

        $data['peraturan']  = $this->m_global->get($this->table_db4);

        $data['breadcrumb'] = [ 'Parameter' => '', 'Peraturan Terkait' => $this->url, 'Add' => $this->url.'/show_add' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'peraturan_terkait/add', $data, $js );
    }

    public function show_edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_db, null, ['id' => $id] )[0];

        $data['pagetitle']  = 'Peraturan Terkait';
        $data['subtitle']   = 'Edit';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['pajak']      = $this->m_global->get($this->table_db2);
        $data['retribusi']  = $this->m_global->get($this->table_db3);
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'Parameter' => '', 'Peraturan Terkait' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'peraturan_terkait/edit', $data, $js );
    }

    public function action_add()
    {
        // echo print_r($this->input->post());exit();
        $this->form_validation->set_rules('kd_peraturan', 'Jenis Peraturan', 'trim|required');
        // $this->form_validation->set_rules('id', 'No.urut', 'trim|required');
        $this->form_validation->set_rules('no_peraturan', 'No.Peraturan', 'trim|required');
        $this->form_validation->set_rules('tgl_peraturan', 'Tanggal Peraturan', 'trim|required');
        $this->form_validation->set_rules('tgl_berlaku', 'Tanggal Berlaku', 'trim|required');
        $this->form_validation->set_rules('uraian_peraturan', 'Uraian', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            // $data[$this->table_prefix.'kd_peraturan']     = $this->input->post('kd_peraturan');
            $data[$this->table_prefix.'kd_peraturan']     = $this->input->post('kd_peraturan');
            $data[$this->table_prefix.'no_peraturan']     = $this->input->post('no_peraturan');
            $data[$this->table_prefix.'tgl_peraturan']    = $this->input->post('tgl_peraturan');
            $data[$this->table_prefix.'tgl_berlaku']      = $this->input->post('tgl_berlaku');
            $data[$this->table_prefix.'uraian_peraturan'] = $this->input->post('uraian_peraturan');
            $data[$this->table_prefix.'uraian_peraturan'] = $this->input->post('uraian_peraturan');
            // $data[$this->table_prefix.'jn_pajak']      = $this->input->post('jn_pajak');
            // $data[$this->table_prefix.'jn_retribusi']  = $this->input->post('jn_retribusi');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Peraturan Terkait';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Peraturan Terkait';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit( $id )
    {
        $this->form_validation->set_rules('kd_peraturan', 'Jenis Peraturan', 'trim|required');
        $this->form_validation->set_rules('no_peraturan', 'No.Peraturan', 'trim|required');
        $this->form_validation->set_rules('tgl_peraturan', 'Tanggal Peraturan', 'trim|required');
        $this->form_validation->set_rules('tgl_berlaku', 'Tanggal Berlaku', 'trim|required');
        $this->form_validation->set_rules('uraian_peraturan', 'Uraian', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_peraturan']           = $this->input->post('kd_peraturan');
            $data[$this->table_prefix.'no_peraturan']           = $this->input->post('no_peraturan');
            $data[$this->table_prefix.'tgl_peraturan']          = $this->input->post('tgl_peraturan');
            $data[$this->table_prefix.'tgl_berlaku']            = $this->input->post('tgl_berlaku');
            $data[$this->table_prefix.'uraian_peraturan']       = $this->input->post('uraian_peraturan');
            // $data[$this->table_prefix.'jn_pajak']               = $this->input->post('jn_pajak');
            // $data[$this->table_prefix.'jn_retribusi']           = $this->input->post('jn_retribusi');

            $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Peraturan Terkait';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Peraturan Terkait';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_peraturan'          => 'kd_peraturan',
            'no_peraturan'          => 'no_peraturan',
            'tgl_peraturan'         => 'tgl_peraturan',
            'tgl_berlaku'           => 'tgl_berlaku',
            'uraian_peraturan'      => 'uraian_peraturan',
            'lastupdate'            => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1'";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->kd_peraturan,
                $rows->no_peraturan,
                $rows->tgl_peraturan,
                $rows->tgl_berlaku,
                $rows->uraian_peraturan,
                $rows->lastupdate,
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ref_peraturan/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ref_peraturan/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

// global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }
}
