<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rekening extends Admin_Controller
{
    private $prefix         = 'rekening';
    private $url            = 'parameter/rekening';
    private $table_db       = '';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    // =============== START REKENING-1 AKUN ===============
    public function index()
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null,'Rekening' => null, 'Akun' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'rekening/index', $data, $js, $css );
    }

    public function select()
    {
        // select table urusan
        $this->table_db = 'ref_rek_1';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_rek_1'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'         => 'kd_rek_1',
            'nama'         => 'nm_rek_1',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

       if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
        $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_rek_1,nm_rek_1,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->nm_rek_1,
                '<a data-original-title="Select" href="'.base_url().$this->url.'/show_rek_2/'.$rows->kd_rek_1.'" class="ajaxify btn blue-steel tooltips">Lihat Kelompok</a>'



            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }
    // =============== END REKENING-1 AKUN ===============


    // =============== START REKENING-2 KELOMPOK ===============
    public function show_rek_2($ref_rek_1 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                                'Parameter' => null,
                                'Rekening'  => null,
                                'Akun'      => $this->url,
                                'Kelompok'  => $this->url."/show_rek_2/".$ref_rek_1,
                            ];

        $data['ref_rek_1']  = $ref_rek_1;

        $js['js']           = [ 'table-datatables-ajax' ];

        $this->template->display( 'rekening/kelompok', $data, $js );
    }

    public function select_kelompok($ref_rek_1 = '')
    {
        // select table urusan
        $this->table_db = 'ref_rek_2';

        $tb_join = [
                    'ref_rek_1' => ['ref_rek_1','kd_rek_1']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_rek_1'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'  => 'kd_rek_1',
            'kode2' => 'kd_rek_2',
            'nama'  => 'nm_rek_2',
        ];

        if (!empty($ref_rek_1)) {
            $where = ['kd_rek_1' => $ref_rek_1];
        }
        else{
            $where = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_rek_1,kd_rek_2,nm_rek_2,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->nm_rek_2,
                '<a data-original-title="Select" href="'.base_url().$this->url.'/show_rek_3/'.$rows->kd_rek_1.'/'.$rows->kd_rek_2.'" class="ajaxify btn blue-steel tooltips">Lihat Jenis</a>'
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }
    // =============== END REKENING-2 KELOMPOK ===============


    // =============== START REKENING-3 JENIS ===============
    public function show_rek_3($ref_rek_1 = '',$ref_rek_2 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                                'Parameter' => null,
                                'Rekening'  => $this->url,
                                'Akun'      => $this->url,
                                'Kelompok'  => $this->url."/show_rek_2/".$ref_rek_1,
                                'Jenis'     => $this->url."/show_rek_3/".$ref_rek_1.'/'.$ref_rek_2,
                            ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;

        $js['js']             = [ 'table-datatables-ajax' ];

        $this->template->display( 'rekening/jenis', $data, $js );
    }

    public function select_jenis($id_1 = '', $id_2 = '')
    {
        // select table urusan
        // echo "test"; exit();
        $this->table_db = 'ref_rek_3';

        $tb_join = [
                    'ref_rek_3' => ['ref_rek_2','kd_rek_2']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_3'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'          => $this->table_db.'.kd_rek_1',
            'kode2'         => 'kd_rek_2',
            'kode3'         => 'kd_rek_3',
            'nama'          => 'nm_rek_3',
            'saldo'         => 'saldonorm',
        ];

        if (!empty($id_1 && $id_2)) {
            $where = [$this->table_db.'.kd_rek_1' => $id_1, 'kd_rek_2' => $id_2];
        }
        else{
            $where = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_rek_3.status = '$request' ";
        }
        else {
            $where_e = " ref_rek_3.status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = $this->table_db.'.kd_rek_1,kd_rek_2,kd_rek_3,nm_rek_3,saldonorm,'.$this->table_db.'.status,id_rek_3,ref_rek_3.id_rek_3,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_rek_3.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->nm_rek_3,
                $rows->saldonorm,
                '<a data-original-title="Lihat Obyek" href="'.base_url().$this->url.'/show_rek_4/'.$rows->kd_rek_1.'/'.$rows->kd_rek_2.'/'.$rows->kd_rek_3.'" class="ajaxify btn btn-icon-only tooltips blue-steel tooltips"><i class="fa fa-check"></i></a>'.
                // '<a href="'.base_url($this->url.'/change_status_by_jenis/'.$rows->id_rek_3.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jenis/'.$rows->id_rek_3.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_jenis/'.$rows->id_rek_3.'/ref_rek_3/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_jenis/'.$rows->id_rek_3.'/ref_rek_3/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_jenis($ref_rek_1 = '', $ref_rek_2 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening jenis';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter' => null,
                    'Rekening'  => null,
                    'Akun'      => null,
                    'Kelompok'  => null,
                    'Jenis'     => null,
                    'Add Jenis' => null
                    ];

        $data['ref_rek_1'] = $ref_rek_1;
        $data['ref_rek_2'] = $ref_rek_2;

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/add_jenis', $data, $js );
    }

    public function show_edit_jenis($id_rek_3)
    {
        //tabel
        $this->table_db = 'ref_rek_3';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['id_rek_3' => $id_rek_3] )[0];
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening jenis';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        // $data['id']         = $id;
        $data['breadcrumb'] = [
                    'Parameter'  => null,
                    'Rekening'   => null,
                    'Akun'       => null,
                    'Kelompok'   => null,
                    'Jenis'      => null,
                    'Edit Jenis' => null
                    ];

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/edit_jenis', $data, $js );
    }

    public function action_add_jenis()
    {
        // set table db
        $this->table_db     = 'ref_rek_3';

        $this->form_validation->set_rules('kd_rek_1',  'Kode Rekening 1',  'trim|required');
        $this->form_validation->set_rules('kd_rek_2',  'Kode Rekening 2',  'trim|required');
        $this->form_validation->set_rules('kd_rek_3',  'Kode Rekening 3',  'trim|required');
        $this->form_validation->set_rules('saldonorm', 'Saldo Normal',     'trim|required');
        $this->form_validation->set_rules('nm_rek_3',  'Nama Rekening 3',  'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']        = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']        = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']        = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'saldonorm']       = $this->input->post('saldonorm');
            $data[$this->table_prefix.'nm_rek_3']        = $this->input->post('nm_rek_3');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Unit with Name <strong>'.$this->input->post('nm_rek_3').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Unit with Name <strong>'.$this->input->post('nm_rek_3').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_jenis($id_rek_3)
    {
        $this->table_db     = 'ref_rek_3';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',      'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',  'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',     'trim|required');
        $this->form_validation->set_rules('saldonorm',     'Saldo Normal',   'trim|required');
        $this->form_validation->set_rules('nm_rek_3',      'Nama Obyek',     'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']      = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']      = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']      = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'saldonorm']     = $this->input->post('saldonorm');
            $data[$this->table_prefix.'nm_rek_3']      = $this->input->post('nm_rek_3');

            $result = $this->m_global->update($this->table_db, $data, ['id_rek_3' => $id_rek_3]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_3').'</strong>';
                // echo $this->db->last_query();
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_3').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END REKENING-3 JENIS ===============


    // =============== START REKENING-4 OBYEK ===============
    public function show_rek_4($ref_rek_1 = '', $ref_rek_2 = '', $ref_rek_3 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter' => null,
                    'Rekening'  => $this->url,
                    'Akun'      => $this->url,
                    'Kelompok'  => $this->url."/show_rek_2/".$ref_rek_1,
                    'Jenis'     => $this->url."/show_rek_3/".$ref_rek_1.'/'.$ref_rek_2,
                    'Obyek'     => $this->url."/show_rek_4/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3,
                    ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;

        $js['js']           = [ 'table-datatables-ajax' ];

        $this->template->display( 'rekening/obyek', $data, $js );
    }

    public function select_obyek($id_1 = '', $id_2 = '', $id_3 = '')
    {
        // select table urusan
        // echo "test"; exit();
        $this->table_db = 'ref_rek_4';

        $tb_join = [
                        ['ref_rek_3','ref_rek_3.kd_rek_3 = ref_rek_4.kd_rek_3 AND ref_rek_3.kd_rek_1 = ref_rek_4.kd_rek_1 AND ref_rek_3.kd_rek_2 = ref_rek_4.kd_rek_2']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_4'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'          => $this->table_db.'.kd_rek_1',
            'kode2'         => $this->table_db.'.kd_rek_2',
            'kode3'         => $this->table_db.'.kd_rek_3',
            'kode4'         => 'kd_rek_4',
            'nama'          => 'nm_rek_4',
        ];

        if (!empty($id_1 && $id_2 && $id_3)) {
            $where = [$this->table_db.'.kd_rek_1' => $id_1, $this->table_db.'.kd_rek_2' => $id_2, $this->table_db.'.kd_rek_3' => $id_3];
        }
        else{
            $where = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_rek_4.status = '$request' and ref_rek_4.kd_rek_1 = $id_1 and ref_rek_4.kd_rek_2 = $id_2 and ref_rek_4.kd_rek_3 = $id_3";
        }
        else {
            $where_e = " ref_rek_4.status = '1' and ref_rek_4.kd_rek_1 = $id_1 and ref_rek_4.kd_rek_2 = $id_2 and ref_rek_4.kd_rek_3 = $id_3 ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = $this->table_db.'.kd_rek_1,'.$this->table_db.'.kd_rek_2,'.$this->table_db.'.kd_rek_3,kd_rek_4,nm_rek_4,'.$this->table_db.'.status,id_rek_4,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $tb_join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);
        // echo $this->db->last_query(); exit();

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_rek_4.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->nm_rek_4,
                '<a data-original-title="Lihat Rincian Obyek" href="'.base_url().$this->url.'/show_rek_5/'.$rows->kd_rek_1.'/'.$rows->kd_rek_2.'/'.$rows->kd_rek_3.'/'.$rows->kd_rek_4.'" class="ajaxify btn btn-icon-only tooltips blue-steel tooltips"><i class="fa fa-check"></i></a>'.
                // '<a href="'.base_url($this->url.'/change_status_by_obyek/'.$rows->id_rek_4.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_obyek/'.$rows->id_rek_4.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_obyek/'.$rows->id_rek_4.'/ref_rek_4/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_obyek/'.$rows->id_rek_4.'/ref_rek_4/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_obyek($ref_rek_1 = '', $ref_rek_2 = '', $ref_rek_3 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening obyek';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter' => null,
                    'Rekening'  => null,
                    'Akun'      => null,
                    'Kelompok'  => null,
                    'Jenis'     => null,
                    'Obyek'     => null,
                    'Add Obyek' => null
                    ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/add_obyek', $data, $js );
    }

    public function show_edit_obyek($id_rek_4)
    {
        //tabel
        $this->table_db     = 'ref_rek_4';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['id_rek_4' => $id_rek_4] )[0];

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening obyek';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        // $data['id']         = $id;

        $data['breadcrumb'] = [
                    'Parameter'  => null,
                    'Rekening'   => null,
                    'Akun'       => null,
                    'Kelompok'   => null,
                    'Jenis'      => null,
                    'Obyek'      => null,
                    'Edit Obyek' => null
                    ];

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/edit_obyek', $data, $js );
    }

    public function action_add_obyek()
    {
        // set table db
        $this->table_db     = 'ref_rek_4';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',         'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',     'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',        'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',        'trim|required');
        $this->form_validation->set_rules('nm_rek_4',      'Nama Obyek',        'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']          = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']          = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']          = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']          = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'nm_rek_4']          = $this->input->post('nm_rek_4');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Obyek with Name <strong>'.$this->input->post('nm_rek_4').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Sub Obyek Name <strong>'.$this->input->post('nm_rek_4').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_obyek($id_rek_4)
    {
        $this->table_db     = 'ref_rek_4';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',      'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',  'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',     'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',     'trim|required');
        $this->form_validation->set_rules('nm_rek_4',      'Nama Obyek',     'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']      = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']      = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']      = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']      = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'nm_rek_4']      = $this->input->post('nm_rek_4');

            $result = $this->m_global->update($this->table_db, $data, ['id_rek_4' => $id_rek_4]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_4').'</strong>';
                // echo $this->db->last_query();
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_4').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END REKENING-4 OBYEK ===============


    // =============== END REKENING-5 RINCIAN OBYEK ===============
    public function show_rek_5($ref_rek_1 = '',$ref_rek_2 = '',$ref_rek_3 = '',$ref_rek_4 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter' => null,
                    'Rekening'  => $this->url,
                    'Akun'      => $this->url,
                    'Kelompok'  => $this->url."/show_rek_2/".$ref_rek_1,
                    'Jenis'     => $this->url."/show_rek_3/".$ref_rek_1.'/'.$ref_rek_2,
                    'Obyek'     => $this->url."/show_rek_4/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3,
                    'Rincian'   => $this->url."/show_rek_5/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3.'/'.$ref_rek_4,
                    ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;
        $data['ref_rek_4']    = $ref_rek_4;

        $js['js']           = [ 'table-datatables-ajax' ];

        $this->template->display( 'rekening/rincian', $data, $js );
    }

    public function select_rincian($id_1 = '',$id_2 = '',$id_3 = '', $id_4 = '')
    {
        // select table urusan
        $this->table_db = 'ref_rek_5';

        $tb_join = [
                    'ref_rek_4' => ['ref_rek_4','kd_rek_4']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_5'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'          => 'kd_rek_1',
            'kode2'         => 'kd_rek_2',
            'kode3'         => 'kd_rek_3',
            'kode4'         => 'kd_rek_4',
            'kode5'         => 'kd_rek_5',
            'nama'          => 'nm_rek_5',
            'peraturan'     => 'peraturan',
        ];

        if (!empty($id_1 && $id_2 && $id_3 && $id_4)) {
            $where = [$this->table_db.'.kd_rek_1' => $id_1, $this->table_db.'.kd_rek_2' => $id_2, $this->table_db.'.kd_rek_3' => $id_3, $this->table_db.'.kd_rek_4' => $id_4];
        }
        else{
            $where = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_rek_5.status = '$request' ";
        }
        else {
            $where_e = " ref_rek_5.status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,kd_rek_5,nm_rek_5,status,id_rek_5,peraturan,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_rek_5.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->nm_rek_5,
                $rows->peraturan,
                '<a data-original-title="Lihat Sub Rincian Obyek" href="'.base_url().$this->url.'/show_rek_6/'.$rows->kd_rek_1.'/'.$rows->kd_rek_2.'/'.$rows->kd_rek_3.'/'.$rows->kd_rek_4.'/'.$rows->kd_rek_5.'" class="ajaxify btn btn-icon-only tooltips blue-steel tooltips"><i class="fa fa-check"></i></a>'.
                // '<a href="'.base_url($this->url.'/change_status_by_rincian/'.$rows->id_rek_5.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_rincian/'.$rows->id_rek_5.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_rincian/'.$rows->id_rek_5.'/ref_rek_5/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_rincian/'.$rows->id_rek_5.'/ref_rek_5/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_rincian($ref_rek_1 = '', $ref_rek_2 = '', $ref_rek_3 = '', $ref_rek_4 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening rincian';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter'   => null,
                    'Rekening'    => null,
                    'Akun'        => null,
                    'Kelompok'    => null,
                    'Jenis'       => null,
                    'Obyek'       => null,
                    'Rincian'     => null,
                    'Add Rincian' => null
                ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;
        $data['ref_rek_4']    = $ref_rek_4;

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/add_rincian', $data, $js );
    }

    public function show_edit_rincian( $id_rek_5 )
    {
        //tabel
        $this->table_db = 'ref_rek_5';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['id_rek_5' => $id_rek_5] )[0];
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening rincian';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        // $data['id']         = $id;

        $data['breadcrumb'] = [
                    'Parameter'    => null,
                    'Rekening'     => null,
                    'Akun'         => null,
                    'Kelompok'     => null,
                    'Jenis'        => null,
                    'Obyek'        => null,
                    'Rincian'      => null,
                    'Edit Rincian' => null
                ];

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'rekening/edit_rincian', $data, $js );
    }

    public function action_add_rincian()
    {
        // set table db
        $this->table_db     = 'ref_rek_5';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',         'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',     'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',        'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',        'trim|required');
        $this->form_validation->set_rules('kd_rek_5',      'Kode Rincian',      'trim|required');
        $this->form_validation->set_rules('nm_rek_5',      'Nama Obyek',        'trim|required');
        $this->form_validation->set_rules('peraturan',     'Peraturan',         'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']  = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']  = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']  = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']  = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']  = $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'nm_rek_5']  = $this->input->post('nm_rek_5');
            $data[$this->table_prefix.'peraturan'] = $this->input->post('peraturan');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Rincian with Name <strong>'.$this->input->post('nm_rek_5').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Sub Rincian Name <strong>'.$this->input->post('nm_rek_5').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_rincian($id_rek_5)
    {
        $this->table_db     = 'ref_rek_5';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',      'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',  'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',     'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',     'trim|required');
        $this->form_validation->set_rules('kd_rek_5',      'Kode Rincian',   'trim|required');
        $this->form_validation->set_rules('nm_rek_5',      'Nama Obyek',     'trim|required');
        $this->form_validation->set_rules('peraturan',     'Peraturan',     'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']      = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']      = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']      = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']      = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']      = $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'nm_rek_5']      = $this->input->post('nm_rek_5');
            $data[$this->table_prefix.'peraturan']     = $this->input->post('peraturan');

            $result = $this->m_global->update($this->table_db, $data, ['id_rek_5' => $id_rek_5]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_5').'</strong>';
                // echo $this->db->last_query();
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_5').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END REKENING-5 RINCIAN OBYEK ===============


    // =============== END REKENING-6 SUB-RINCIAN OBYEK ===============
    public function show_rek_6($ref_rek_1 = '', $ref_rek_2 = '', $ref_rek_3 = '', $ref_rek_4 = '', $ref_rek_5 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter'   => null,
                    'Rekening'    => $this->url,
                    'Akun'        => $this->url,
                    'Kelompok'    => $this->url."/show_rek_2/".$ref_rek_1,
                    'Jenis'       => $this->url."/show_rek_3/".$ref_rek_1.'/'.$ref_rek_2,
                    'Obyek'       => $this->url."/show_rek_4/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3,
                    'Rincian'     => $this->url."/show_rek_5/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3.'/'.$ref_rek_4,
                    'Sub Rincian' => $this->url."/show_rek_6/".$ref_rek_1.'/'.$ref_rek_2.'/'.$ref_rek_3.'/'.$ref_rek_4.'/'.$ref_rek_5
                ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;
        $data['ref_rek_4']    = $ref_rek_4;
        $data['ref_rek_5']    = $ref_rek_5;

        $js['js']             = [ 'table-datatables-ajax' ];

        $this->template->display( 'rekening/subrincian', $data, $js );
    }

    public function select_subrincian($id_1 = '',$id_2 = '',$id_3 = '', $id_4 = '', $id_5 = '')
    {
        // select table urusan
        $this->table_db = 'ref_rek_6';

        $tb_join = [
                    'ref_rek_5' => ['ref_rek_5','kd_rek_5']
                ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'          => 'kd_rek_1',
            'kode2'         => 'kd_rek_2',
            'kode3'         => 'kd_rek_3',
            'kode4'         => 'kd_rek_4',
            'kode5'         => 'kd_rek_5',
            'kode6'         => 'kd_rek_6',
            'nama'          => 'nm_rek_6',
        ];

        if (!empty($id_1 && $id_2 && $id_3 && $id_4 && $id_5)) {
            $where = [$this->table_db.'.kd_rek_1' => $id_1, $this->table_db.'.kd_rek_2' => $id_2, $this->table_db.'.kd_rek_3' => $id_3, $this->table_db.'.kd_rek_4' => $id_4, $this->table_db.'.kd_rek_5' => $id_5];
        }
        else{
            $where = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_rek_6.status = '$request' ";
        }
        else {
            $where_e = " ref_rek_6.status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,kd_rek_5,kd_rek_6,nm_rek_6,status,id_rek_6,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_rek_6.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->url.'/change_status_by_subrincian/'.$rows->id_rek_6.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_subrincian/'.$rows->id_rek_6.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_subrincian/'.$rows->id_rek_6.'/ref_rek_6/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_subrincian/'.$rows->id_rek_6.'/ref_rek_6/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_subrincian($ref_rek_1 = '', $ref_rek_2 = '', $ref_rek_3 = '', $ref_rek_4 = '', $ref_rek_5 = '')
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening sub rincian';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [
                    'Parameter'       => null,
                    'Rekening'        => null,
                    'Akun'            => null,
                    'Kelompok'        => null,
                    'Jenis'           => null,
                    'Obyek'           => null,
                    'Rincian'         => null,
                    'Sub Rincian'     => null,
                    'Add Sub Rincian' => null
                ];

        $js['js']             = [ 'form-validation' ];

        $data['ref_rek_1']    = $ref_rek_1;
        $data['ref_rek_2']    = $ref_rek_2;
        $data['ref_rek_3']    = $ref_rek_3;
        $data['ref_rek_4']    = $ref_rek_4;
        $data['ref_rek_5']    = $ref_rek_5;

        $data['peraturan']    = $this->m_global->get('ref_peraturan', null, null );

        $this->template->display( 'rekening/add_subrincian', $data, $js );
    }

    public function show_edit_subrincian( $id_rek_6 )
    {
        //tabel
        $this->table_db     = 'ref_rek_6';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['id_rek_6' => $id_rek_6] )[0];

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage rekening sub rincian';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        // $data['id']         = $id;

        $data['breadcrumb'] = [
                    'Parameter'        => null,
                    'Rekening'         => null,
                    'Akun'             => null,
                    'Kelompok'         => null,
                    'Jenis'            => null,
                    'Obyek'            => null,
                    'Rincian'          => null,
                    'Sub Rincian'      => null,
                    'Edit Sub Rincian' => null
                ];

        $js['js']             = [ 'form-validation' ];
        $data['peraturan']    = $this->m_global->get('ref_peraturan', null, null );

        $this->template->display( 'rekening/edit_subrincian', $data, $js );
    }

    public function action_add_subrincian()
    {
        // set table db
        $this->table_db     = 'ref_rek_6';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',         'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',     'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',        'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',        'trim|required');
        $this->form_validation->set_rules('kd_rek_5',      'Kode Rincian',      'trim|required');
        $this->form_validation->set_rules('kd_rek_6',      'Kode Rincian',      'trim|required');
        $this->form_validation->set_rules('nm_rek_6',      'Nama Obyek',        'trim|required');
        $this->form_validation->set_rules('no_peraturan',  'No Peraturan',      'trim|required');
        $this->form_validation->set_rules('tgl_peraturan', 'Tanggal Peraturan', 'trim|required');
        $this->form_validation->set_rules('nm_pendapatan', 'Nama Pendapatan',   'trim|required');
        $this->form_validation->set_rules('tarif',         'Tarif',   'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']          = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']          = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']          = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']          = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']          = $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'kd_rek_6']          = $this->input->post('kd_rek_6');
            $data[$this->table_prefix.'nm_rek_6']          = $this->input->post('nm_rek_6');
            $data[$this->table_prefix.'no_peraturan']      = $this->input->post('no_peraturan');
            $data[$this->table_prefix.'tgl_peraturan']     = $this->input->post('tgl_peraturan');
            $data[$this->table_prefix.'nm_pendapatan']     = $this->input->post('nm_pendapatan');
            $data[$this->table_prefix.'tarif']             = $this->input->post('tarif');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Sub Rincian with Name <strong>'.$this->input->post('nm_rek_6').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Sub Rincian Name <strong>'.$this->input->post('nm_rek_6').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_subrincian($id_rek_6)
    {
        $this->table_db     = 'ref_rek_6';

        $this->form_validation->set_rules('kd_rek_1',      'Kode Akun',         'trim|required');
        $this->form_validation->set_rules('kd_rek_2',      'Kode Kelompok',     'trim|required');
        $this->form_validation->set_rules('kd_rek_3',      'Kode Jenis',        'trim|required');
        $this->form_validation->set_rules('kd_rek_4',      'Kode Obyek',        'trim|required');
        $this->form_validation->set_rules('kd_rek_5',      'Kode Rincian',      'trim|required');
        $this->form_validation->set_rules('kd_rek_6',      'Kode Sub Rincian',  'trim|required');
        $this->form_validation->set_rules('nm_rek_6',      'Nama Sub Rincian',  'trim|required');
        $this->form_validation->set_rules('no_peraturan',  'No Peraturan',      'trim|required');
        $this->form_validation->set_rules('tgl_peraturan', 'Tanggal Peraturan', 'trim|required');
        $this->form_validation->set_rules('nm_pendapatan', 'Nama Pendapatan',   'trim|required');
        $this->form_validation->set_rules('tarif',         'Tarif',             'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']      = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']      = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']      = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']      = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']      = $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'kd_rek_6']      = $this->input->post('kd_rek_6');
            $data[$this->table_prefix.'nm_rek_6']      = $this->input->post('nm_rek_6');
            $data[$this->table_prefix.'no_peraturan']  = $this->input->post('no_peraturan');
            $data[$this->table_prefix.'tgl_peraturan'] = $this->input->post('tgl_peraturan');
            $data[$this->table_prefix.'nm_pendapatan'] = $this->input->post('nm_pendapatan');
            $data[$this->table_prefix.'tarif']         = $this->input->post('tarif');

            $result = $this->m_global->update($this->table_db, $data, ['id_rek_6' => $id_rek_6]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_6').'</strong>';
                // echo $this->db->last_query();
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Sub Unit with Name <strong>'.$this->input->post('nm_rek_6').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END REKENING-6 SUB-RINCIAN OBYEK ===============


    // =============== BELUM FIX ===============
    // global actions
    public function change_status($status, $id)
    {
        // echo "<pre>";
        // print_r ($status);
        // echo "</pre>";
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id[$field.' IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by_jenis($id, $table, $status, $stat = false)
    {
        // echo "test"; exit();
        if ($stat) {
            $result = $this->m_global->delete($table, ['id_rek_3' => $id]);
            // echo $this->db->last_query(); exit();
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id_rek_3' => $id]);
            // echo $this->db->last_query(); exit();
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function change_status_by_obyek($id, $table, $status, $stat = false)
    {
        // echo "test"; exit();
        if ($stat) {
            $result = $this->m_global->delete($table, ['id_rek_4' => $id]);
            // echo $this->db->last_query(); exit();
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id_rek_4' => $id]);
            // echo $this->db->last_query(); exit();
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function change_status_by_rincian($id, $table, $status, $stat = false)
    {
        // echo "test"; exit();
        if ($stat) {
            $result = $this->m_global->delete($table, ['id_rek_5' => $id]);
            // echo $this->db->last_query(); exit();
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id_rek_5' => $id]);
            // echo $this->db->last_query(); exit();
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function change_status_by_subrincian($id, $table, $status, $stat = false)
    {
        // echo "test"; exit();
        if ($stat) {
            $result = $this->m_global->delete($table, ['id_rek_6' => $id]);
            // echo $this->db->last_query(); exit();
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id_rek_6' => $id]);
            // echo $this->db->last_query(); exit();
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    // public function change_status_jenis( $status, $where )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_3';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_status_by_jenis( $id, $status, $stat = FALSE )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_3';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id_rek_3' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id_rek_3' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }
    //
    // public function change_status_obyek( $status, $where )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_4';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_status_by_obyek( $id, $status, $stat = FALSE )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_4';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id_rek_4' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id_rek_4' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }
    //
    // public function change_status_rincian( $status, $where )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_5';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_status_by_rincian( $id, $status, $stat = FALSE )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_5';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id_rek_5' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id_rek_5' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }
    //
    // public function change_status_subrincian( $status, $where )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_6';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_status_by_subrincian( $id, $status, $stat = FALSE )
    // {
    //     //tabel
    //     $this->table_db = 'ref_rek_6';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id_rek_6' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id_rek_6' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }
}

/* End of file Rekening.php */
/* Location: ./application/modules/parameter/controllers/Rekening.php */
