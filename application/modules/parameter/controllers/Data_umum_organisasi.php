<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class data_umum_organisasi extends Admin_Controller
{
    private $prefix         = 'parameter/data_umum_organisasi';
    private $url            = 'parameter/data_umum_organisasi';
    private $table_db       = 'ref_unit';
    private $table_sub_unit = 'ref_sub_unit';
    private $table_ta_sub   = 'ta_sub_unit';
    private $table_ta_jab   = 'ta_sub_unit_jab';
    private $table_jab      = 'ref_jabatan';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';
    // private $uri1        = [];

	function __construct()
	{
		parent::__construct();
	}

    // =============== START UNIT ===============
	public function index()
	{
		$data['pagetitle']  = 'Data Umum' ;
		$data['subtitle']   = 'Organisasi';

		$data['url']        = base_url().$this->url;
		$data['prefix']     = $this->prefix;
		$data['breadcrumb'] = [ 'Unit' => $this->url  ];

		$js['js']           = [ 'table-datatables-ajax' ];
		$css['css']         = null;

		$this->template->display( 'data_umum_organisasi/index', $data, $js, $css );
	}

    public function select()
    {
        $this->table_db     = 'ref_unit';
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'         => 'kd_urusan',
            'bidang'         => 'kd_bidang',
            'unit'           => 'kd_unit',
            'uraian_unit'    => 'nm_unit',
            'lastupdate'     => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                '<span class="label  blue-steel label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.'</span>',
                $rows->nm_unit,
                tgl_format($rows->lastupdate),
                '<a data-original-title="pilih" href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_urusan.'/'.$rows->nm_unit.'" class="ajaxify btn blue-steel tooltips">Lihat Sub Unit</a>',
                // '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }
    // =============== END UNIT ===============


    // =============== START SUB-UNIT ===============
    public function show_sub_unit($kd_urusan='',$kd_bidang='',$kd_unit='',$head='')
    {
        $data['pagetitle']  = 'Data Umum Organisasi';
        $data['subtitle']   = 'Sub Unit';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['head']       = str_replace('%20', ' ', $head);

        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit ];

        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/sub_unit', $data, $js, $css  );
    }

    public function select_sub ($kd_urusan='',$kd_bidang='',$kd_unit='')
    {
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status_sub($_REQUEST['customActionName'], [$this->table_prefix.'id_organisasi'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'         => 'kd_urusan',
            'bidang'         => 'kd_bidang',
            'unit'           => 'kd_unit',
            'sub_unit'       => 'kd_sub',
            'uraian_unit'    => 'nm_sub_unit',
            'lastupdate'     => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = 'kd_urusan='.$kd_urusan.' and kd_bidang='.$kd_bidang.' and kd_unit='.$kd_unit;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_sub_unit, null, $where, $where_e );
        $iDisplayLength   = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,kd_unit,kd_bidang,kd_urusan,kd_sub,nm_sub_unit,lastupdate,id_organisasi,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_sub_unit, null,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_organisasi.'"/><span></span></label>',
                $i,
                // $rows->kd_urusan,
                // $rows->kd_bidang,
                // $rows->kd_unit,
                // $rows->kd_sub,
                '<span class="label  blue-steel label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.' .  '.$rows->kd_sub.'</span>',
                $rows->nm_sub_unit,
                $rows->lastupdate,
              '<a data-original-title="Select" href="'.base_url().$this->url.'/show_datum_organisasi/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->kd_sub.'/'.$rows->id_organisasi.'/'.$rows->nm_sub_unit.'" class="ajaxify btn blue-steel tooltips">Data Umum</a>',
                // '<a href="'.base_url( ''.$this->prefix.'/change_status_sub_unit_by/'.$rows->id_organisasi.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }
    // =============== END SUB-UNIT ===============


    // =============== START DATUM ORGANISASI ===============
    public function show_datum_organisasi($kd_urusan='',$kd_bidang='',$kd_unit='',$kd_sub='',$id_organisasi='',$head='')
    {
        $data['pagetitle']  = 'Data Umum Organisasi';

        $data['add']        = base_url()."parameter/data_umum_organisasi/show_add_datum/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi ;
        $data['reload']     = $this->url."/show_datum_organisasi/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi.'/'.$head;
        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,
        'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit ,
        'Data Umum' => $this->url."/show_datum_organisasi/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi.'/'.$head];

        $data['prefix']     = $this->prefix;

        $data['kd_urusan']     = $kd_urusan;
        $data['kd_bidang']     = $kd_bidang;
        $data['kd_unit']       = $kd_unit;
        $data['kd_sub']        = $kd_sub;
        $data['id_organisasi'] = $id_organisasi;
        $data['head']          = str_replace('%20', ' ', $head);

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/datum', $data, $js, $css  );
    }

    public function select_datum($kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id_organisasi)
    {
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_pimpinan'  => 'nm_pimpinan',
            'alamat'       => 'alamat',
            'nip_pimpinan' => 'nip_pimpinan',
            'jbt_pimpinan' => 'jbt_pimpinan',
            'visi'         => 'visi',
        ];

        $where      = NULL;
        $where_e    = "id_organisasi=$id_organisasi";

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
                $request = $_REQUEST['filterstatus'];
                $where_e = " status = '$request' and id_organisasi = $id_organisasi ";
            }
            else {
                $where_e = " status = '1' and id_organisasi = $id_organisasi ";
            }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_ta_sub, null, $where, $where_e );
        $iDisplayLength   = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,nm_pimpinan,alamat,nip_pimpinan,jbt_pimpinan,visi,lastupdate,id,id_organisasi,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_ta_sub, null,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->nm_pimpinan,
                $rows->alamat,
                $rows->nip_pimpinan,
                $rows->jbt_pimpinan,
                $rows->visi,
                // $rows->lastupdate,
                '<a data-original-title="Select" href="'.base_url().$this->url.'/show_jabatan/'.$rows->id.'/'.$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi.'" class="ajaxify btn blue-steel tooltips">Jabatan</a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sub_unit/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_datum/'.$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$rows->id_organisasi.'/'.$rows->id.'"  class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_datum($kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id_organisasi)
    {
        $data['pagetitle']  = 'Data Umum Organisasi';

        $data['reload']     = $this->url."/show_datum_organisasi/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi;
        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit ,'Data Umum' => $this->url."/show_datum_organisasi/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi];
        $data['prefix']     = $this->prefix;

        $data['kd_urusan']     = $kd_urusan;
        $data['kd_bidang']     = $kd_bidang;
        $data['kd_unit']       = $kd_unit;
        $data['kd_sub']        = $kd_sub;
        $data['id_organisasi'] = $id_organisasi;

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/form_datum', $data, $js, $css  );
    }

    public function show_edit_datum($kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id_organisasi,$id)
    {
        $data['pagetitle']  = 'Data Umum Organisasi';

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' =>null,'Data Umum' =>null];
        $data['prefix']     = $this->prefix;

        $data['record']     = $this->m_global->get( $this->table_ta_sub = 'ta_sub_unit', null, ['id' => $id] )[0];

        $data['kd_urusan']     = $kd_urusan;
        $data['kd_bidang']     = $kd_bidang;
        $data['kd_unit']       = $kd_unit;
        $data['kd_sub']        = $kd_sub;
        $data['id_organisasi'] = $id_organisasi;
        $data['id']            = $id;

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/edit_datum', $data, $js, $css  );
    }

    // ACTION ADD
    public function action_form()
    {
        // print_r ($this->input->post());exit();
        $this->form_validation->set_rules('visi', 'visi', 'trim|required');
        $this->form_validation->set_rules('nip_pimpinan', 'nip_pimpinan', 'trim|required');
        $this->form_validation->set_rules('jbt_pimpinan', 'jbt_pimpinan', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'visi']          = $this->input->post('visi');
            $data[$this->table_prefix.'id_organisasi'] = $this->input->post('id_organisasi');
            $data[$this->table_prefix.'nm_pimpinan']   = $this->input->post('nm_pimpinan');
            $data[$this->table_prefix.'nip_pimpinan']  = $this->input->post('nip_pimpinan');
            $data[$this->table_prefix.'jbt_pimpinan']  = $this->input->post('jbt_pimpinan');
            $data[$this->table_prefix.'alamat']        = $this->input->post('alamat');
            $data[$this->table_prefix.'tahun']         = date('Y');
            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";exit();

            $result  = $this->m_global->insert( $this->table_ta_sub,$data );

            if ( $result ['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add <strong>'.$this->input->post('nm_pimpinan').'</strong>';

                echo json_encode( $data );

            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add <strong>'.$this->input->post('nm_pimpinan').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit($id)
    {
        // print_r ($this->input->post());exit();
        $this->form_validation->set_rules('visi', 'visi', 'trim|required');
        $this->form_validation->set_rules('nip_pimpinan', 'nip_pimpinan', 'trim|required');
        $this->form_validation->set_rules('jbt_pimpinan', 'jbt_pimpinan', 'trim|required');
        $this->form_validation->set_rules('alamat', 'alamat', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'visi']          = $this->input->post('visi');
            $data[$this->table_prefix.'id_organisasi'] = $this->input->post('id_organisasi');
            $data[$this->table_prefix.'nm_pimpinan']   = $this->input->post('nm_pimpinan');
            $data[$this->table_prefix.'nip_pimpinan']  = $this->input->post('nip_pimpinan');
            $data[$this->table_prefix.'jbt_pimpinan']  = $this->input->post('jbt_pimpinan');
            $data[$this->table_prefix.'alamat']        = $this->input->post('alamat');
            $data[$this->table_prefix.'tahun']         = date('Y');
            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";exit();
            // echo $id;exit();

            $result = $this->m_global->update($this->table_ta_sub, $data, ['id' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit<strong>'.$this->input->post('nm_pimpinan').'</strong>';
                echo json_encode( $data );

            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit <strong>'.$this->input->post('nm_pimpinan').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }

        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END DATUM ORGANISASI ===============


    // =============== END DATUM JABATAN ===============
    public function show_jabatan($id_sub_unit, $kd_urusan,$kd_bidang,$kd_unit,$kd_sub,$id_organisasi)
    {
        $data['pagetitle']  = 'Data Umum Organisasi';
        $data['subtitle']   = 'Sub Unit';

        $data['add']        = base_url()."parameter/data_umum_organisasi/show_add_jab/".$id_sub_unit ;

        $data['breadcrumb'] = [
                    'Unit'      => $this->url,
                    'Sub Unit'  => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit ,
                    'Data Umum' => $this->url."/show_datum_organisasi/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi,
                    'Jabatan'   => null
                ];
        $data['url']         = base_url().$this->url;
        $data['prefix']      = $this->prefix;
        $data['id_sub_unit'] = $id_sub_unit;


        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/jabatan', $data, $js, $css  );
    }

    public function select_jab($id_sub_unit)
    {
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nama'    => 'nama',
            'nip'     => 'nip',
            'jabatan' => 'jabatan',

        ];

        $where      = NULL;
        $where_e    = "id_sub_unit=$id_sub_unit";

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' and id_sub_unit = $id_sub_unit ";
        }
        else {
            $where_e = " status = '1' and id_sub_unit=$id_sub_unit ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_ta_jab, null, $where, $where_e );
        $iDisplayLength   = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,nama,nip,jabatan,lastupdate,id,id_sub_unit,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_ta_jab, null,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->nama,
                $rows->nip,
                $rows->jabatan,
              // '<a data-original-title="Select" href="'.base_url().$this->url.'/show_datum_organisasi/'.$rows->id.'" class="ajaxify btn green tooltips">Data Umum</a>',
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_jab/'.$rows->id.'"  class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sub_unit_jab/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ta_sub_unit_jab/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_jab($id_sub_unit)
    {
        $data['pagetitle']  = 'Data Umum Organisasi';

        $data['url']        = base_url().$this->url;


        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => null,'Data Umum' => null,'Jabatan' => null,'Add' => null];
        $data['prefix']     = $this->prefix;

        $data['id_sub_unit'] = $id_sub_unit;

        $data['jab']     = $this->m_global->get( $this->table_jab);

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/form_jabatan', $data, $js, $css  );
    }

    public function show_edit_jab($id)
    {
        $data['pagetitle']  = 'Data Umum Organisasi';

        $data['url']        = base_url().$this->url;


        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => null,'Data Umum' => null,'Jabatan' => null,'Edit' => null];
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;

        $data['jab']        = $this->m_global->get( $this->table_jab);

        $data['record']     = $this->m_global->get( $this->table_ta_jab = 'ta_sub_unit_jab', null, ['id' => $id] )[0];

        $js['js']           = [ 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'data_umum_organisasi/edit_jabatan', $data, $js, $css  );
    }

    // ACTION ADD
    public function action_form_jab()
    {
        // print_r ($this->input->post());exit();
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'id_sub_unit'] = $this->input->post('id_sub_unit');
            $data[$this->table_prefix.'nama']        = $this->input->post('nama');
            $data[$this->table_prefix.'nip']         = $this->input->post('nip');
            $data[$this->table_prefix.'jabatan']     = $this->input->post('jabatan');
            $data[$this->table_prefix.'kd_jab']      = $this->input->post('kd_jab');
            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";exit();

            $result  = $this->m_global->insert( $this->table_ta_jab,$data );

            if ( $result ['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add <strong>'.$this->input->post('nama').'</strong>';

                echo json_encode( $data );

            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add <strong>'.$this->input->post('nama').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_jab($id)
    {
        // print_r ($this->input->post());exit();
        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('nip', 'nip', 'trim|required');
        $this->form_validation->set_rules('jabatan', 'jabatan', 'trim|required');
        $this->form_validation->set_rules('kd_jab', 'kd_jab', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'id_sub_unit'] = $this->input->post('id_sub_unit');
            $data[$this->table_prefix.'nama']        = $this->input->post('nama');
            $data[$this->table_prefix.'nip']         = $this->input->post('nip');
            $data[$this->table_prefix.'jabatan']     = $this->input->post('jabatan');
            $data[$this->table_prefix.'kd_jab']      = $this->input->post('kd_jab');
            // echo "<pre>";
            // print_r ($data);
            // echo "</pre>";exit();
            // echo $id;exit();

            $result = $this->m_global->update($this->table_ta_jab, $data, ['id' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit<strong>'.$this->input->post('nama').'</strong>';
                echo json_encode( $data );

            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit <strong>'.$this->input->post('nama').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }

        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END DATUM JABATAN ===============


    // =============== BELUM FIX ===============
    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

// global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    // public function change_status( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_status_sub( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_sub_unit, $data, NULL, $where );
    // }
    //
    // public function change_status_datum( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_ta_sub, $data, NULL, $where );
    // }
    //
    // public function change_status_jab( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_ta_jab, $data, NULL, $where );
    // }

}

/* End of file Data_umum.php */
/* Location: ./application/modules/parameter/data_umum/controllers/Data_umum.php */
