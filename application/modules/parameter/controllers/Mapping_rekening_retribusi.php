<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapping_rekening_retribusi extends Admin_Controller
{
    private $prefix         = 'parameter/mapping_rekening_retribusi';
    private $url            = 'parameter/mapping_rekening_retribusi';
    private $table_db       = 'ref_jenis_retribusi';
    private $table_rek      = 'ref_rek_6';
    private $table_unit     = 'ref_unit';
    private $table_sub_unit = 'ref_sub_unit';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

    public function index()
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Mapping Rekening Retribusi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Parameter' => '', 'Mapping Rekening Retribusi' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'mapping_rekening_retribusi/index', $data, $js, $css );
    }

    public function show_add()
    {
        $urusan = $this->input->post('kd_urusan');
        $bidang = $this->input->post('kd_bidang');
        $unit = $this->input->post('kd_unit');

    	$data['pagetitle']  = 'Mapping Rekening Retribusi';
        $data['subtitle']   = 'add';

        $where = ['kd_urusan'=>1, 'kd_bidang'=>1, 'kd_unit'=>2];

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['unit']       = $this->m_global->get($this->table_unit);
        $data['sub_unit']   = $this->m_global->get($this->table_sub_unit, null, $where);

        $data['breadcrumb'] = [ 'Parameter' => '', 'Mapping Rekening Retribusi' => $this->url, 'Add' => $this->url.'/show_add' ];
        $js['js']           = [ 'form-validation' , 'table-datatables-ajax'];

        $this->template->display( 'mapping_rekening_retribusi/add', $data, $js );
    }

    public function show_edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_db, null, ['kd_jenis' => $id] )[0];

        $data['pagetitle']  = 'Mapping Rekening Retribusi';
        $data['subtitle']   = '';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'Parameter' => '', 'Mapping Rekening Retribusi' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];

        $this->template->display( 'mapping_rekening_retribusi/edit', $data, $js );
    }

    public function action_add()
    {
        $this->form_validation->set_rules('kd_rek_1', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_2', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_3', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_4', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_5', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('nm_retribusi', 'Rekening', 'trim|required');
        $this->form_validation->set_rules('kd_urusan', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_bidang', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_unit', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_sub', 'Unit Organisasi', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
				else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']           	= $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']           	= $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']           	= $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']           	= $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']           	= $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'nm_retribusi']           = $this->input->post('nm_retribusi');
            $data[$this->table_prefix.'kd_urusan']           	= $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']           	= $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']           		= $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']           		= $this->input->post('kd_sub');
            $data[$this->table_prefix.'kd_rek_1_denda']         = $this->input->post('kd_rek_1_denda') == '' ? NULL : $this->input->post('kd_rek_1_denda');
            $data[$this->table_prefix.'kd_rek_2_denda']         = $this->input->post('kd_rek_2_denda') == '' ? NULL : $this->input->post('kd_rek_2_denda');
            $data[$this->table_prefix.'kd_rek_3_denda']         = $this->input->post('kd_rek_3_denda') == '' ? NULL : $this->input->post('kd_rek_3_denda');
            $data[$this->table_prefix.'kd_rek_4_denda']         = $this->input->post('kd_rek_4_denda') == '' ? NULL : $this->input->post('kd_rek_4_denda');
            $data[$this->table_prefix.'kd_rek_5_denda']         = $this->input->post('kd_rek_5_denda') == '' ? NULL : $this->input->post('kd_rek_5_denda');
            $data[$this->table_prefix.'kd_rek_6_denda']         = $this->input->post('kd_rek_6_denda') == '' ? NULL : $this->input->post('kd_rek_6_denda');
            $data[$this->table_prefix.'kd_rek_1_bunga']         = $this->input->post('kd_rek_1_bunga') == '' ? NULL : $this->input->post('kd_rek_1_bunga');
            $data[$this->table_prefix.'kd_rek_2_bunga']         = $this->input->post('kd_rek_2_bunga') == '' ? NULL : $this->input->post('kd_rek_2_bunga');
            $data[$this->table_prefix.'kd_rek_3_bunga']         = $this->input->post('kd_rek_3_bunga') == '' ? NULL : $this->input->post('kd_rek_3_bunga');
            $data[$this->table_prefix.'kd_rek_4_bunga']         = $this->input->post('kd_rek_4_bunga') == '' ? NULL : $this->input->post('kd_rek_4_bunga');
            $data[$this->table_prefix.'kd_rek_5_bunga']         = $this->input->post('kd_rek_5_bunga') == '' ? NULL : $this->input->post('kd_rek_5_bunga');
            $data[$this->table_prefix.'kd_rek_6_bunga']         = $this->input->post('kd_rek_6_bunga') == '' ? NULL : $this->input->post('kd_rek_6_bunga');
            $data[$this->table_prefix.'kd_rek_1_kenaikan']      = $this->input->post('kd_rek_1_kenaikan') == '' ? NULL : $this->input->post('kd_rek_1_kenaikan');
            $data[$this->table_prefix.'kd_rek_2_kenaikan']      = $this->input->post('kd_rek_2_kenaikan') == '' ? NULL : $this->input->post('kd_rek_2_kenaikan');
            $data[$this->table_prefix.'kd_rek_3_kenaikan']      = $this->input->post('kd_rek_3_kenaikan') == '' ? NULL : $this->input->post('kd_rek_3_kenaikan');
            $data[$this->table_prefix.'kd_rek_4_kenaikan']      = $this->input->post('kd_rek_4_kenaikan') == '' ? NULL : $this->input->post('kd_rek_4_kenaikan');
            $data[$this->table_prefix.'kd_rek_5_kenaikan']      = $this->input->post('kd_rek_5_kenaikan') == '' ? NULL : $this->input->post('kd_rek_5_kenaikan');
            $data[$this->table_prefix.'kd_rek_6_kenaikan']      = $this->input->post('kd_rek_6_kenaikan') == '' ? NULL : $this->input->post('kd_rek_6_kenaikan');
            $data[$this->table_prefix.'jn_pemungutan']		    = 2;

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Mapping Rekening Retribusi';

                echo json_encode( $data );
            }
			else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Mapping Rekening Retribusi';

				if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
		else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit( $id )
    {
        $this->form_validation->set_rules('kd_rek_1', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_2', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_3', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_4', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('kd_rek_5', 'Uraian', 'trim|required');
        $this->form_validation->set_rules('nm_retribusi', 'Rekening', 'trim|required');
        $this->form_validation->set_rules('kd_urusan', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_bidang', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_unit', 'Unit Organisasi', 'trim|required');
        $this->form_validation->set_rules('kd_sub', 'Unit Organisasi', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']           	= $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']           	= $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']           	= $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']           	= $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']           	= $this->input->post('kd_rek_5');
            $data[$this->table_prefix.'nm_retribusi']           = $this->input->post('nm_retribusi');
            $data[$this->table_prefix.'kd_urusan']           	= $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']           	= $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']           		= $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']           		= $this->input->post('kd_sub');
            $data[$this->table_prefix.'kd_rek_1_denda']         = $this->input->post('kd_rek_1_denda') == '' ? NULL : $this->input->post('kd_rek_1_denda');
            $data[$this->table_prefix.'kd_rek_2_denda']         = $this->input->post('kd_rek_2_denda') == '' ? NULL : $this->input->post('kd_rek_2_denda');
            $data[$this->table_prefix.'kd_rek_3_denda']         = $this->input->post('kd_rek_3_denda') == '' ? NULL : $this->input->post('kd_rek_3_denda');
            $data[$this->table_prefix.'kd_rek_4_denda']         = $this->input->post('kd_rek_4_denda') == '' ? NULL : $this->input->post('kd_rek_4_denda');
            $data[$this->table_prefix.'kd_rek_5_denda']         = $this->input->post('kd_rek_5_denda') == '' ? NULL : $this->input->post('kd_rek_5_denda');
            $data[$this->table_prefix.'kd_rek_6_denda']         = $this->input->post('kd_rek_6_denda') == '' ? NULL : $this->input->post('kd_rek_6_denda');
            $data[$this->table_prefix.'kd_rek_1_bunga']         = $this->input->post('kd_rek_1_bunga') == '' ? NULL : $this->input->post('kd_rek_1_bunga');
            $data[$this->table_prefix.'kd_rek_2_bunga']         = $this->input->post('kd_rek_2_bunga') == '' ? NULL : $this->input->post('kd_rek_2_bunga');
            $data[$this->table_prefix.'kd_rek_3_bunga']         = $this->input->post('kd_rek_3_bunga') == '' ? NULL : $this->input->post('kd_rek_3_bunga');
            $data[$this->table_prefix.'kd_rek_4_bunga']         = $this->input->post('kd_rek_4_bunga') == '' ? NULL : $this->input->post('kd_rek_4_bunga');
            $data[$this->table_prefix.'kd_rek_5_bunga']         = $this->input->post('kd_rek_5_bunga') == '' ? NULL : $this->input->post('kd_rek_5_bunga');
            $data[$this->table_prefix.'kd_rek_6_bunga']         = $this->input->post('kd_rek_6_bunga') == '' ? NULL : $this->input->post('kd_rek_6_bunga');
            $data[$this->table_prefix.'kd_rek_1_kenaikan']      = $this->input->post('kd_rek_1_kenaikan') == '' ? NULL : $this->input->post('kd_rek_1_kenaikan');
            $data[$this->table_prefix.'kd_rek_2_kenaikan']      = $this->input->post('kd_rek_2_kenaikan') == '' ? NULL : $this->input->post('kd_rek_2_kenaikan');
            $data[$this->table_prefix.'kd_rek_3_kenaikan']      = $this->input->post('kd_rek_3_kenaikan') == '' ? NULL : $this->input->post('kd_rek_3_kenaikan');
            $data[$this->table_prefix.'kd_rek_4_kenaikan']      = $this->input->post('kd_rek_4_kenaikan') == '' ? NULL : $this->input->post('kd_rek_4_kenaikan');
            $data[$this->table_prefix.'kd_rek_5_kenaikan']      = $this->input->post('kd_rek_5_kenaikan') == '' ? NULL : $this->input->post('kd_rek_5_kenaikan');
            $data[$this->table_prefix.'kd_rek_6_kenaikan']      = $this->input->post('kd_rek_6_kenaikan') == '' ? NULL : $this->input->post('kd_rek_6_kenaikan');
            $data[$this->table_prefix.'jn_pemungutan']		    = 2;

            $result = $this->m_global->update($this->table_db, $data, ['kd_jenis' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Mapping Rekening Pajak';

                echo json_encode( $data );
            }
			else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Mapping Rekening Pajak';

				if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
		else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_jenis'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_retribusi' => 'nm_retribusi'
            // 'lastupdate'   => 'lastupdate'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
					else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,kd_jenis, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_1_bunga, kd_rek_2_bunga, kd_rek_3_bunga, kd_rek_4_bunga, kd_rek_5_bunga, kd_rek_6_bunga, kd_rek_1_denda, kd_rek_2_denda, kd_rek_3_denda, kd_rek_4_denda, kd_rek_5_denda, kd_rek_6_denda, kd_rek_1_kenaikan, kd_rek_2_kenaikan, kd_rek_3_kenaikan, kd_rek_4_kenaikan, kd_rek_5_kenaikan, kd_rek_6_kenaikan,'.implode(',' , $aCari);

		$result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
				'<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_jenis.'"/><span></span></label>',
				$i,
				$rows->nm_retribusi,
				$rows->kd_rek_1 . '.' . $rows->kd_rek_2 . '.' . $rows->kd_rek_3 . '.' . $rows->kd_rek_4 . '.' . $rows->kd_rek_5,
				$rows->kd_rek_1_denda . '.' . $rows->kd_rek_2_denda . '.' . $rows->kd_rek_3_denda . '.' . $rows->kd_rek_4_denda . '.' . $rows->kd_rek_5_denda . '.' . $rows->kd_rek_6_denda,
				$rows->kd_rek_1_bunga . '.' . $rows->kd_rek_2_bunga . '.' . $rows->kd_rek_3_bunga . '.' . $rows->kd_rek_4_bunga . '.' . $rows->kd_rek_5_bunga . '.' . $rows->kd_rek_6_bunga,
				$rows->kd_rek_1_kenaikan . '.' . $rows->kd_rek_2_kenaikan . '.' . $rows->kd_rek_3_kenaikan . '.' . $rows->kd_rek_4_kenaikan . '.' . $rows->kd_rek_5_kenaikan . '.' . $rows->kd_rek_6_kenaikan,
				// $rows->lastupdate,

				'<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
				'<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_jenis.'/ref_jenis_retribusi/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_jenis.'/ref_jenis_retribusi/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_rek()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'unit_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_retribusi' => 'nm_retribusi',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = null;
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_jenis,status,kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,jn_pemungutan,kd_rek_1_denda,kd_rek_2_denda,
        kd_rek_3_denda,kd_rek_4_denda,kd_rek_1_kenaikan,kd_rek_2_kenaikan,kd_rek_3_kenaikan,kd_rek_4_kenaikan,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3.'.'.$rows->kd_rek_4,
                $rows->nm_retribusi,
                $rows->lastupdate,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->kd_jenis.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->kd_jenis.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_unit()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_unit'    => 'nm_unit',
            'lastupdate' => 'lastupdate',
        ];

        $where_e = null;
        $where   = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_unit, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_unit,kd_urusan,kd_bidang,id,status,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_unit, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_urusan,
                $rows->kd_bidang,
                $rows->kd_unit,
                $rows->nm_unit,
                $rows->lastupdate,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" href="#subunit" data-target="#subunit" data-toggle="modal" onClick="show_subunit('.$rows->kd_urusan.','.$rows->kd_bidang.','.$rows->kd_unit.')" >'.
                // '<button data-toggle="modal" href="#subunit">Launch modal</button>'.
                '<i class="fa fa-check"></i></a>',
                // <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#unit" id="add_unit"><i class="fa fa-refresh"> </i></button>
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_subunit($urusan,$bidang,$unit)
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_organisasi'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_sub_unit' => $this->table_prefix.'nm_sub_unit',
            'lastupdate'  => $this->table_prefix.'lastupdate'
        ];

        $where      = NULL;
        $where_e    = "kd_urusan = $urusan and kd_bidang = $bidang and kd_unit = $unit";

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status =']    = '1';
        }

        $keys            = array_keys( $aCari );
        @$order          = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords   = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength  = intval($_REQUEST['length']);
        $iDisplayLength  = $iDisplayLength < 0 ? $iTotalRecords:   $iDisplayLength;
        $iDisplayStart   = intval($_REQUEST['start']);
        $sEcho           = intval($_REQUEST['draw']);

        $records         = array();
        $records["data"] = array();

        $end             = $iDisplayStart + $iDisplayLength;
        $end             = $end > $iTotalRecords ? $iTotalRecords: $end;

        $select          = 'id_organisasi, kd_urusan, kd_bidang, kd_unit, kd_sub, status,'.implode(',' , $aCari);
        $result          = $this->m_global->get($this->table_sub_unit, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i               = 1 + $iDisplayStart;

        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_organisasi.'"/><span></span></label>',
                $i,
                $rows->kd_urusan,
                $rows->kd_bidang,
                $rows->kd_unit,
                $rows->kd_sub,
                $rows->nm_sub_unit,
                tgl_format($rows->lastupdate),
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->no_daftar.'/'.
                //     ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"' ) ).
                //     ' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green-seagreen' ).
                //     '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).
                //     '" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a data-original-title="Ubah Data" href="'.base_url().$this->url.'/show_edit/'.$rows->no_daftar.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                // '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->no_daftar.'/99/'.($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-times"></i></a>',
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_subunit('.$rows->id_organisasi.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
            );

            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_denda()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=8';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_denda('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_bunga()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=8';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_bunga('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_kenaikan()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=8';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->kd_jenis.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_kenaikan('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function change_status( $status, $where )
    {
        $data[ $this->table_prefix.'status' ]   = $status;

        $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    }

    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['kd_jenis' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['kd_jenis' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function get_rekening()
    {
        $kd_jenis        = sprintf("%05d", $this->input->post('kd_jenis') ) ;
        $data['records'] = $this->m_global->get($this->table_db, null, ['kd_jenis' => $kd_jenis] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_unit()
    {
        $kd_jenis        = sprintf("%05d", $this->input->post('unit_id') ) ;
        $data['records'] = $this->m_global->get($this->table_sub_unit, null, ['unit_id' => $kd_jenis] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_subunit()
    {
        $id_organisasi     = sprintf("%05d", $this->input->post('id_organisasi') ) ;
        $data['records'] = $this->m_global->get($this->table_sub_unit, null, ['id_organisasi' => $id_organisasi] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_denda()
    {
        $id_rek_6        = sprintf("%05d", $this->input->post('id_rek_6') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id_rek_6] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_bunga()
    {
        $id_rek_6        = sprintf("%05d", $this->input->post('id_rek_6') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id_rek_6] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_kenaikan()
    {
        $id_rek_6        = sprintf("%05d", $this->input->post('id_rek_6') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id_rek_6] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Rekening Retribusi Report';

        // query
        $data['report']  = $this->db->query("SELECT nm_retribusi, concat(kd_rek_1, '.', kd_rek_2, '.', kd_rek_3, '.',
                            kd_rek_4, '.', kd_rek_5) as gab_rek,
                            concat(kd_rek_1_denda, '.', kd_rek_2_denda, '.', kd_rek_3_denda, '.', kd_rek_4_denda, '.',
                            kd_rek_5_denda, '.', kd_rek_6_denda) as gab_denda,
                            concat(kd_rek_1_bunga, '.', kd_rek_2_bunga, '.', kd_rek_3_bunga, '.', kd_rek_4_bunga, '.',
                            kd_rek_5_bunga, '.', kd_rek_6_bunga) as gab_bunga,
                            concat(kd_rek_1_kenaikan, '.', kd_rek_2_kenaikan, '.', kd_rek_3_kenaikan, '.',
                            kd_rek_4_kenaikan, '.', kd_rek_5_kenaikan, '.', kd_rek_6_kenaikan) as gab_kenaikan
                            from ref_jenis_retribusi WHERE status = '1'")->result();

        $data['namaFile']   ='Rekening_Retribusi_Report_'.date('Y-m-d');
        $data['title']      ='Rekening Retribusi Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['Jenis Retribusi', '90'], ['Rekening', '15'], ['Denda', '15'], ['Bunga', '15'], ['Kenaikan', '15']
        ];

        $this->load->view('mapping_rekening_retribusi/export',$data);
    }

}


/* End of file Mapping_rekening_retribusi.php */
/* Location: ./application/modules/parameter/controllers/Mapping_rekening_retribusi.php */
