<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends Admin_Controller
{
    private $prefix       = 'parameter/Bank';
    private $url          = 'parameter/Bank';
    private $table_db     = 'ref_bank';
    private $table_rek    = 'ref_rek_6';
    private $table_prefix = '';
    private $rule_valid   = 'xss_clean|encode_php_tags';

    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Bank';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Parameter' => null,'Bank' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'bank/index', $data, $js, $css );
    }

    public function show_add()
    {
        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'add bank';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $where = ['kd_urusan'=>1];

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['unit']       = $this->m_global->get($this->table_db);
        $data['breadcrumb'] = [ 'parameter' =>null,'Bank' => $this->url.'/index', 'Add' => null ];

        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'bank/add', $data, $js );
    }

    public function show_edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_db = 'ref_bank', null, ['kd_bank' => $id] )[0];

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'manage bank';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['id']         = $id;
        $where = ['kd_urusan'=>1];

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['unit']       = $this->m_global->get($this->table_db);

        $data['breadcrumb'] = [ 'parameter' => null,'Bank' => $this->url.'/index', 'Edit' => null ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'bank/edit', $data, $js );
    }

    public function action_add()
    {
        $this->form_validation->set_rules('kode', 'Kode Bank');
        $this->form_validation->set_rules('nm_bank', 'Nama Bank', 'trim|required');
        $this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required');
        $this->form_validation->set_rules('kd_rek_1', 'Kode Rekening 1', 'trim|required');
        $this->form_validation->set_rules('kd_rek_2', 'Kode Rekening 2', 'trim|required');
        $this->form_validation->set_rules('kd_rek_3', 'Kode Rekening 3', 'trim|required');
        $this->form_validation->set_rules('kd_rek_4', 'Kode Rekening 4', 'trim|required');
        $this->form_validation->set_rules('kd_rek_5', 'Kode Rekening 5', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_bank']           = $this->input->post('kode');
            $data[$this->table_prefix.'nm_bank']           = $this->input->post('nm_bank');
            $data[$this->table_prefix.'no_rekening']    = $this->input->post('no_rekening');
            $data[$this->table_prefix.'kd_rek_1']       = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']       = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']       = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']       = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']       = $this->input->post('kd_rek_5');

            $result  = $this->m_global->insert( $this->table_db,$data );

            if ( $result ['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add <strong>'.$this->input->post('nm_bank').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add <strong>'.$this->input->post('nm_bank').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit( $id )
    {
        $this->form_validation->set_rules('kd_bank', 'Kode Bank');
        $this->form_validation->set_rules('nm_bank', 'Nama', 'trim|required');
        $this->form_validation->set_rules('no_rekening', 'No Rekening', 'trim|required');
        $this->form_validation->set_rules('kd_rek_1', 'Kode Rekening 1', 'trim|required');
        $this->form_validation->set_rules('kd_rek_2', 'Kode Rekening 2', 'trim|required');
        $this->form_validation->set_rules('kd_rek_3', 'Kode Rekening 3', 'trim|required');
        $this->form_validation->set_rules('kd_rek_4', 'Kode Rekening 4', 'trim|required');
        $this->form_validation->set_rules('kd_rek_5', 'Kode Rekening 5', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_bank']     = $this->input->post('kd_bank');
            $data[$this->table_prefix.'nm_bank']     = $this->input->post('nm_bank');
            $data[$this->table_prefix.'no_rekening'] = $this->input->post('no_rekening');
            $data[$this->table_prefix.'kd_rek_1']    = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']    = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']    = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']    = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_5']    = $this->input->post('kd_rek_5');

            $result = $this->m_global->update($this->table_db, $data, ['kd_bank' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit<strong>'.$this->input->post('nm_bank').'</strong>';
                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit <strong>'.$this->input->post('nm_bank').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }

        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select()
    {
        // select table
        $this->table_db = 'ref_bank';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_bank'       => 'kd_bank',
            'nm_bank'       => 'nm_bank',
            'no_rekening'   => 'no_rekening',
            'lastupdate'    => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];
        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords :   $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);
        $records          = array();
        $records["data"]  = array();
        $end              = $iDisplayStart + $iDisplayLength;
        $end              = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select           = 'id, kd_bank, nm_bank ,no_rekening, lastupdate, status, '.implode(',' , $aCari);
        $result           = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i                = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->kd_bank,
                $rows->nm_bank,
                $rows->no_rekening,
                $rows->lastupdate,
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->kd_bank.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ref_bank/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->id.'/ref_bank/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

    // global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    /*========================
    // MODAL
    ==========================*/
    public function select_rek_pajak()
    {
        // select table
        $this->table_rek = 'ref_rek_6';
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_6'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6'         => 'nm_rek_6',
            'lastupdate'        => 'lastupdate',
        ];

        $where_e      = 'id_rek_6 = 1';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id_rek_6,status,kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,kd_rek_5,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_rek_6.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3.'.'.$rows->kd_rek_4.'.'.$rows->kd_rek_5,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id_rek_6.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by_Kelurahan/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function get_rekening_pajak()
    {

        $id_rek_6         = sprintf("%05d", $this->input->post('id_rek_6') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id_rek_6] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

}

/* End of file Bank */
/* Location: ./application/modules/parameter/controllers/Bank */
