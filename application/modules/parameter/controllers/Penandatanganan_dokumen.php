<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class penandatanganan_dokumen extends Admin_Controller
{
    private $prefix              = 'parameter/penandatanganan_dokumen';
    private $url                 = 'parameter/penandatanganan_dokumen';
    private $table_db            = 'ref_unit';
    private $table_sub_unit      = 'ref_sub_unit';
    private $table_penandatangan = 'ref_penandatangan';
    private $table_prefix        = '';
    private $rule_valid          = 'xss_clean|encode_php_tags';
    // private $uri1             = [];

	function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $data['pagetitle']  = 'Penandatangan';
        $data['subtitle']   = 'Dokumen';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['breadcrumb'] = [ 'Unit' => $this->url  ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'penandatanganan_dokumen/index', $data, $js, $css );

    }

    public function select()
    {
        // select table urusan

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'         => 'kd_urusan',
            'bidang'         => 'kd_bidang',
            'unit'           => 'kd_unit',
            'uraian_unit'    => 'nm_unit',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys   = array_keys( $aCari );
        @$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords  = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $records        = array();
        $records["data"]= array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              $i,

              // $rows->kd_urusan,
              // $rows->kd_bidang,
              // $rows->kd_unit,
              '<span class="label label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.'</span>',
              $rows->nm_unit,
              '<a data-original-title="Select" href="'.base_url().$this->url.'/show_subunit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->nm_unit.'" class="ajaxify btn blue-steel tooltips">Lihat Sub Unit</a>'

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_subunit($kd_urusan = '', $kd_bidang = '', $kd_unit = '', $head = '')
    {
        $data['pagetitle']  = 'Penandatangan Dokumen';
        $data['subtitle']   = 'Sub Unit';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['head']  = str_replace('%20', ' ', $head);
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_subunit/"];
        $data['kd_urusan']    = $kd_urusan;
        $data['kd_bidang']    = $kd_bidang;
        $data['kd_unit']      = $kd_unit;

        $js['js']           = [ 'table-datatables-ajax' ];

        $this->template->display( 'penandatanganan_dokumen/sub_unit', $data, $js );
    }

    public function select_subunit($kd_urusan = '', $kd_bidang = '', $kd_unit = '',$head = '')
    {
        // select table urusan
        $this->table_db = 'ref_sub_unit';
        $tb_join    = [
                        'ref_unit' => ['ref_unit','kd_unit']
                    ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);

                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'urusan'      => $this->table_db.'.kd_urusan',
            'bidang'     => $this->table_db.'.kd_bidang',
            'unit'     => $this->table_prefix.'kd_unit',
            'sub_unit'     => $this->table_prefix.'kd_sub',
            'uraian_unit'     => $this->table_prefix.'nm_sub_unit'
        ];

        if (!empty($kd_urusan && $kd_bidang && $kd_unit)) {
            $where      = [$this->table_db.'.kd_urusan'=> $kd_urusan, $this->table_db.'.kd_bidang'=> $kd_bidang, $this->table_db.'.kd_unit'=> $kd_unit] ;
        }else{
            $where      = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";

                    } else {

                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';

                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys   = array_keys( $aCari );
        @$order = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords  = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart  = intval($_REQUEST['start']);
        $sEcho          = intval($_REQUEST['draw']);

        $records        = array();
        $records["data"]= array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = $this->table_db.'.id_organisasi, '.$this->table_db.'.kd_urusan, '.$this->table_db.'.kd_bidang, kd_unit, kd_sub, nm_sub_unit, '.$this->table_db.'.status, '.$this->table_db.'.lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
              $i,
              // $rows->kd_urusan,
              // $rows->kd_bidang,
              // $rows->kd_unit,
              // $rows->kd_sub,
              '<span class="label label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.' . '.$rows->kd_sub.'</span>',
              $rows->nm_sub_unit,
              '<a data-original-title="Select" href="'.base_url().$this->url.'/show_penandatanganan/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->kd_sub.'/'.$rows->id_organisasi.'/'.$rows->nm_sub_unit.'" class="ajaxify btn blue-steel tooltips">Penandatangan</a>'
              // '<a style="visibility: hidden">test</a>'
              /*'<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_urusan.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
              '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->kd_urusan.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
              '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->kd_urusan.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',*/
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_penandatanganan($kd_urusan = '', $kd_bidang = '', $kd_unit = '', $kd_sub = '' ,$id_organisasi='', $head = '')
    {
        $data['pagetitle']  = 'Penandatangan Dokumen';
        $data['subtitle']   = 'Penandatangan';

        $data['add']        = base_url()."parameter/penandatanganan_dokumen/show_add_penandatanganan/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi;
        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,'Sub Unit' => $this->url."/show_subunit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$head,'Penandatanganan' => $this->url."/show_penandatanganan/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi.'/'.$head];
        $data['prefix']     = $this->prefix;
        $data['head']  = str_replace('%20', ' ', $head);

        $data['kd_urusan']     = $kd_urusan;
        $data['kd_bidang']     = $kd_bidang;
        $data['kd_sub']        = $kd_sub;
        $data['kd_unit']       = $kd_unit;
        $data['id_organisasi'] = $id_organisasi;

        $js['js']           = [ 'table-datatables-ajax' ];

        $this->template->display( 'penandatanganan_dokumen/penandatanganan', $data, $js  );
    }

    public function select_penandatangan($kd_urusan='',$kd_bidang='',$kd_unit='',$kd_sub='',$id_organisasi)
    {
        // echo $id_organisasi;exit();
        $this->table_penandatangan = 'ref_penandatangan';

        // $tb_join    = [
        //                 'ref_unit' => ['ref_unit','kd_unit'],
        //                 'ref_jabatan' => ['ref_jabatan','kd_jab']

        //             ];

        $tb_join = [
                    'ref_jabatan_join' => ['ref_jabatan', 'ref_penandatangan.jbt_penandatangan = ref_jabatan.kd_jab', 'LEFT'],
                    'ref_jenis_dokumen_join' => ['ref_jenis_dokumen', 'ref_penandatangan.no_dok = ref_jenis_dokumen.jn_dokumen', 'LEFT'],
                ];

        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

            $aCari               = [
            'nama_penandatangan' => 'ref_penandatangan.nm_penandatangan',
            'nip_penandatangan'  => 'ref_penandatangan.nip_penandatangan',
            'id'                 => 'ref_penandatangan.jbt_penandatangan',
            'nm_dokumen'         => 'ref_jenis_dokumen.nm_dokumen',
        ];

        $where      = NULL;
        $where_e    = "ref_penandatangan.id_organisasi=$id_organisasi";


        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_penandatangan.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_penandatangan.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_penandatangan, $tb_join, $where, $where_e );
        $iDisplayLength   = intval(@$_REQUEST['length']);
        // echo "test";exit;
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        // echo 'string';exit();

        $select = 'ref_penandatangan.status,ref_penandatangan.nm_penandatangan,ref_penandatangan.nip_penandatangan,ref_jabatan.nm_jab,ref_jenis_dokumen.nm_dokumen,ref_penandatangan.jbt_penandatangan,ref_penandatangan.no_urut,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_penandatangan, $tb_join,$where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->no_urut.'"/><span></span></label>',
                $i,
                $rows->nm_penandatangan,
                $rows->nip_penandatangan,
                $rows->nm_jab,
                $rows->nm_dokumen,
                // $rows->lastupdate,
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->no_urut.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->no_urut.'/ref_penandatangan/'.
                        ($rows->status == 1 ? '0/false" data-original-title="Set ke Tidak Aktif"' : '1/false" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_penandatanganan/'.$rows->no_urut.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_penandatanganan($kd_urusan='',$kd_bidang='',$kd_unit='',$kd_sub='',$id_organisasi='')
    {
        $data['pagetitle']  = 'Penandatangan Dokumen';
        $data['subtitle']   = 'Add Penandatangan';

        $data['kembali']    = $this->url."/show_penandatanganan/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi ;
        $data['refresh']    = base_url()."parameter/penandatanganan_dokumen/show_add_penandatanganan/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi;
        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,
        'Sub Unit' => $this->url."/show_subunit/".$kd_urusan.'/'.$kd_bidang. '/'.$kd_unit.'/'.$kd_sub,
        'Penandatanganan' => $this->url."/show_penandatanganan/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit.'/'.$kd_sub.'/'.$id_organisasi,
        'Add' => null ];

        $data['kd_urusan']     = $kd_urusan;
        $data['kd_bidang']     = $kd_bidang;
        $data['kd_unit']       = $kd_unit;
        $data['kd_sub']        = $kd_sub;
        $data['id_organisasi'] = $id_organisasi;

        $data['jabatan']         = $this->db->query("select * from ref_jabatan")->result();

        $js['js']           = ['form-validation'];
        $css['css']         = null;

        $this->template->display( 'penandatanganan_dokumen/add_penandatanganan', $data, $js, $css );
    }

    public function show_edit_penandatanganan($no_urut='')
    {
        $data['pagetitle']  = 'Penandatangan Dokumen';
        $data['subtitle']   = 'Add Penandatangan';

        $data['records']    = $this->m_global->get( $this->table_penandatangan, null, ['no_urut' => $no_urut] )[0];

        $data['url']        = base_url().$this->url;
        $data['breadcrumb'] = [ 'Unit' => $this->url,
        'Sub Unit'          => null,
        'Penandatanganan'   => null,
        'Edit'              => null ];

        $data['no_urut']    = $no_urut;

        $data['jabatan']    = $this->db->query("select * from ref_jabatan")->result();

        $js['js']           = ['form-validation'];
        $css['css']         = null;

        $this->template->display( 'penandatanganan_dokumen/edit_penandatangan', $data, $js, $css  );
    }

    public function action_add()
    {
        // echo '<pre>';print_r($this->input->post()); exit();
        $data2[$this->table_prefix.'no_dok2']          = $this->input->post('no_dok');
        $data[$this->table_prefix.'kd_urusan']         = $this->input->post('kd_urusan');
        $data[$this->table_prefix.'kd_bidang']         = $this->input->post('kd_bidang');
        $data[$this->table_prefix.'kd_unit']           = $this->input->post('kd_unit');
        $data[$this->table_prefix.'kd_sub']            = $this->input->post('kd_sub');
        $data[$this->table_prefix.'tahun']             = $this->input->post('tahun');
        $data[$this->table_prefix.'nm_penandatangan']  = $this->input->post('nm_penandatangan');
        $data[$this->table_prefix.'nip_penandatangan'] = $this->input->post('nip_penandatangan');

        foreach ($data2[$this->table_prefix.'no_dok2'] as $data[$this->table_prefix.'no_dok']) {
            $result  = $this->m_global->insert( $this->table_penandatangan, $data );
        }

        redirect('parameter/penandatanganan_dokumen/index','refresh');

    }

    public function change_status_by( $id, $status, $stat = FALSE )
    {
        $this->table_penandatangan = 'ref_penandatangan';

        if ( $stat ){
            $result = $this->m_global->delete( $this->table_penandatangan, [$this->table_prefix.'no_urut' => $id] );
        }
        else{
            $result = $this->m_global->update( $this->table_penandatangan, [$this->table_prefix.'status' => $status], [$this->table_prefix.'no_urut' => $id]);
        }

        if ( $result ){
            $data['status'] = 1;
        }
        else{
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }



}
