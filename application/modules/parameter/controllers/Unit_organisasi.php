<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_organisasi extends Admin_Controller
{
	private $prefix         = 'unit_organisasi';
    private $url            = 'parameter/unit_organisasi';
    private $table_db       = '';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

    // =============== START URUSAN ===============
	public function index()
	{
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Urusan' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'unit_organisasi/index', $data, $js, $css );

	}

    public function select()
    {
        // select table urusan
        $this->table_db = 'ref_urusan';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'kd_urusan'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_urusan'  => $this->table_prefix.'kd_urusan',
            'nm_urusan'  => $this->table_prefix.'nm_urusan',
            'lastupdate' => $this->table_prefix.'lastupdate'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_urusan, nm_urusan, status, lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_urusan.'"/><span></span></label>',
                $i,
                '<span class="label label-info">'.$rows->kd_urusan.'</span>',
                $rows->nm_urusan,
                tgl_format($rows->lastupdate),
                '<a href="'.base_url($this->url.'/show_bidang/'.$rows->kd_urusan.'/'.$rows->nm_urusan).'" class="btn blue-steel tooltips ajaxify"> Lihat Bidang </a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_urusan/'.$rows->kd_urusan.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_edit_urusan($kd_urusan = '')
    {
        $this->table_db     = 'ref_urusan';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['kd_urusan'=>$kd_urusan] )[0];

        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage Urusan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;


        $data['breadcrumb'] = ['Urusan' => $this->url, 'Edit' => $this->url."/show_edit_urusan/".$kd_urusan ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/edit_urusan', $data, $js );

    }

    public function action_edit_urusan($id)
    {
        $this->table_db     = 'ref_urusan';

        $this->form_validation->set_rules('kd_urusan',  'Kode Urusan',  'trim|required');
        $this->form_validation->set_rules('nm_urusan',  'nama urusan',  'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_urusan']      = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'nm_urusan']      = $this->input->post('nm_urusan');


            $result = $this->m_global->update($this->table_db, $data, ['kd_urusan' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Unit with Name <strong>'.$this->input->post('nm_urusan').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Unit with Name <strong>'.$this->input->post('nm_urusan').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // ===============END URUSAN ===============


    // =============== START BIDANG ===============
    public function show_bidang($kd_urusan='',$nm_urusan='')
    {
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['kd_urusan']  = $kd_urusan;
        $data['nm_urusan']  = str_replace('%20', ' ', $nm_urusan);
        $data['breadcrumb'] = [ 'Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang/".$kd_urusan.'/'.$nm_urusan ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'unit_organisasi/show_bidang', $data, $js, $css );
    }

    public function select_bidang($kd_urusan = '')
    {
        // select table urusan
        $this->table_db = 'ref_bidang';

        $tb_join    = [
                        'ref_urusan' => ['ref_urusan','kd_urusan','nm_urusan'],
                        'ref_fungsi' => ['ref_fungsi','kd_fungsi']
                    ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status_bidang($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_bidang'  => $this->table_prefix.'kd_bidang',
            'nm_bidang'  => $this->table_prefix.'nm_bidang',
            'kd_fungsi'  => $this->table_prefix.'kd_fungsi'
        ];

        if (!empty($kd_urusan)) {
            $where      = ['kd_urusan'=> $kd_urusan] ;
        }
        else{
            $where      = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == '' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix.") BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, $tb_join, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_bidang.kd_urusan, ref_bidang.kd_bidang, ref_bidang.nm_bidang, ref_bidang.kd_fungsi, ref_bidang.id, ref_bidang.status,ref_bidang.lastupdate, kd_bidang, nm_bidang, kd_fungsi, ref_bidang.lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $tb_join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_urusan.'"/><span></span></label>',
                $i,
                '<span class="label label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.'</span>',
				$rows->nm_bidang,
                $rows->kd_fungsi,
                // $rows->lastupdate,
                '<a href="'.base_url($this->url.'/show_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->nm_bidang).'" class="btn blue-steel tooltips ajaxify"> Lihat Unit </a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_bidang/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_edit_bidang($id ='')
    {
        $this->table_db     = 'ref_bidang';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['id'=>$id] )[0];

        $data['pagetitle']  = 'Bidang';
        $data['subtitle']   = 'manage BIdang';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;


        $data['breadcrumb'] = ['Urusan' => $this->url, 'Edit' => $this->url."/show_edit_bidang/"];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/edit_bidang', $data, $js );

    }

    public function action_edit_bidang($id)
    {
        $this->table_db     = 'ref_bidang';

        $this->form_validation->set_rules('kd_urusan',  'Kode Urusan',  'trim|required');
        $this->form_validation->set_rules('kd_bidang',  'kode bidang',  'trim|required');
        $this->form_validation->set_rules('nm_bidang',  'nama bidang',  'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_urusan']      = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']      = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'nm_bidang']      = $this->input->post('nm_bidang');


            $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Unit with Name <strong>'.$this->input->post('nm_bidang').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Unit with Name <strong>'.$this->input->post('nm_bidang').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END BIDANG ===============


    // =============== START UNIT ===============
    public function show_unit($kd_urusan='', $kd_bidang='', $nm_unit='')
    {
        // echo '<PRE>', print_r($this->input->post()), exit();
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['nm_unit']  = str_replace('%20', ' ', $nm_unit);

        // kode parameter untuk filter data dengan kd_urusan dan kd_bidang
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang/".$kd_urusan.'/'.$nm_unit, 'Unit' => $this->url."/show_unit/$kd_urusan/$kd_bidang/$nm_unit" ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'unit_organisasi/show_unit', $data, $js, $css );
    }

    public function select_unit($kd_urusan='', $kd_bidang = '')
    {
        // select table urusan
        $this->table_db = 'ref_unit';

        $tb_join        = [
                                'ref_bidang' => ['ref_urusan','kd_urusan'],
                          ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            // 'id'    => $this->table_prefix.'id',
            'kd_urusan'  => $this->table_prefix.'kd_urusan',
            // 'kd_bidang'  => $this->table_prefix.'kd_bidang',
            // 'kd_unit'    => $this->table_prefix.'kd_unit',
            'nm_unit'    => $this->table_prefix.'nm_unit'
        ];

        if (!empty($kd_bidang)) {
            $where      = [ 'kd_urusan' => $kd_urusan, 'kd_bidang'=> $kd_bidang] ;
        }
        else{
            $where      = NULL;
        }

        $where_e    = 'kd_urusan = 1';

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == '' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_db.") BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

       if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_unit.status = '$request' and ref_unit.kd_urusan = $kd_urusan and ref_unit.kd_bidang = $kd_bidang ";
        }
        else {
            $where_e = " ref_unit.status = '1' and ref_unit.kd_urusan = $kd_urusan and ref_unit.kd_bidang = $kd_bidang ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, $tb_join, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_unit.id, ref_unit.kd_urusan, ref_unit.kd_bidang, ref_unit.kd_unit, ref_unit.nm_unit,ref_unit.status,ref_unit.lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $tb_join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                '<span class="label label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.'</span>',
                $rows->nm_unit,
                // $rows->lastupdate,
                '<a href="'.base_url($this->url.'/show_sub_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->nm_unit).'" class="btn blue-steel tooltips" data-original-title="Lihat Sub Unit">Lihat Sub Unit</a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_unit/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->id.'/ref_unit/'.
                                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->id.'/ref_unit/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_unit($kd_urusan='', $kd_bidang='')
    {
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang/".$kd_urusan, 'Unit' => $this->url."/show_unit/".$kd_urusan.'/'.$kd_bidang, 'Add Unit' => "" ];
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;

        $css['css']         = null;
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/add_unit', $data, $js, $css );
    }

    public function show_edit_unit($kd_urusan, $kd_bidang, $kd_unit)
    {
        $this->table_db     = 'ref_unit';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['kd_urusan'=>$kd_urusan, 'kd_bidang'=>$kd_bidang, 'kd_unit'=>$kd_unit] )[0];

        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;


        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang/".$kd_urusan, 'Unit' => $this->url."/show_unit/".$kd_urusan."/".$kd_bidang, 'Edit Unit' => "" ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/edit_unit', $data, $js );
    }

    public function action_add_unit()
    {
        // set table db
        $this->table_db     = 'ref_unit';

        $this->form_validation->set_rules('kd_urusan',  'Kode Urusan',  'trim|required');
        $this->form_validation->set_rules('kd_bidang',  'Kode Bidang',  'trim|required');
        // $this->form_validation->set_rules('kd_unit',    'Kode Unit',    'trim|required');
        $this->form_validation->set_rules('nm_unit',    'Nama Unit',    'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
				else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_urusan'] = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang'] = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']   = $this->input->post('kd_unit');
            $data[$this->table_prefix.'nm_unit']   = $this->input->post('nm_unit');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Unit with Name <strong>'.$this->input->post('nm_unit').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Unit with Name <strong>'.$this->input->post('nm_unit').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_unit($id)
    {
        $this->table_db     = 'ref_unit';

        $this->form_validation->set_rules('kd_urusan',  'Kode Urusan',  'trim|required');
        $this->form_validation->set_rules('kd_bidang',  'Kode Bidang',  'trim|required');
        $this->form_validation->set_rules('kd_unit',    'Kode Unit',    'trim|required');
        $this->form_validation->set_rules('nm_unit',    'Nama Unit',    'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_urusan']      = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']      = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']        = $this->input->post('kd_unit');
            $data[$this->table_prefix.'nm_unit']        = $this->input->post('nm_unit');


            $result = $this->m_global->update($this->table_db, $data, ['id' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Unit with Name <strong>'.$this->input->post('nm_unit').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Unit with Name <strong>'.$this->input->post('nm_unit').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END UNIT ===============


    // =============== START SUB-UNIT ===============
    public function show_sub_unit($kd_urusan='', $kd_bidang='', $kd_unit='', $nm_sub_unit='')
    {
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['nm_sub_unit']  = str_replace('%20', ' ', $nm_sub_unit);

        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang", 'Unit' => $this->url."//show_unit/$kd_urusan/$kd_bidang/$nm_sub_unit", 'Sub Unit' => null];

        // kode parameter untuk filter data dengan kd_urusan dan kd_bidang
        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'unit_organisasi/show_subunit', $data, $js, $css );
    }

    public function select_sub_unit($kd_urusan='', $kd_bidang = '', $kd_unit='')
    {
        // select table urusan
        $this->table_db = 'ref_sub_unit';

        $tb_join        = [
                                ['ref_unit','ref_unit.kd_unit = ref_sub_unit.kd_unit AND ref_unit.kd_urusan = ref_sub_unit.kd_urusan AND ref_unit.kd_bidang = ref_sub_unit.kd_bidang']
                          ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kd_sub'      => $this->table_prefix.'kd_sub',
            'nm_sub_unit' => $this->table_prefix.'nm_sub_unit',
            'lastupdate'  => $this->table_db.'.lastupdate'
        ];

        if (!empty($kd_unit)) {
            $where      = [ $this->table_db.'.kd_urusan' => $kd_urusan, $this->table_db.'.kd_bidang'=> $kd_bidang, $this->table_db.'.kd_unit'=>$kd_unit] ;
        }
        else{
            $where      = NULL;
        }

        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_sub_unit.status = '$request' and ref_sub_unit.kd_urusan = $kd_urusan and ref_sub_unit.kd_bidang = $kd_bidang and ref_sub_unit.kd_unit = $kd_unit";
        }
        else {
            $where_e = " ref_sub_unit.status = '1' and ref_sub_unit.kd_urusan = $kd_urusan and ref_sub_unit.kd_bidang = $kd_bidang and ref_sub_unit.kd_unit = $kd_unit";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, $tb_join, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'ref_sub_unit.id_organisasi,'.$this->table_db.'.kd_urusan,'.$this->table_db.'.kd_bidang, ref_unit.kd_unit,kd_sub, nm_sub_unit, '.$this->table_db.'.status, '.$this->table_db.'.lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, $tb_join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id_organisasi.'"/><span></span></label>',
                $i,
                '<span class="label label-info">'.$rows->kd_urusan.' . '.$rows->kd_bidang.' . '.$rows->kd_unit.' . '.$rows->kd_sub.'</span>',
                $rows->nm_sub_unit,
                tgl_format($rows->lastupdate),
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_sub/'.$rows->kd_urusan.'/'.$rows->kd_bidang.'/'.$rows->kd_unit.'/'.$rows->kd_sub.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_subunit/'.$rows->id_organisasi.'/ref_sub_unit/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by_subunit/'.$rows->id_organisasi.'/ref_sub_unit/99'.
                                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_sub($kd_urusan='', $kd_bidang='', $kd_unit='')
    {
        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang/".$kd_urusan, 'Unit' => $this->url."/show_unit/".$kd_urusan.'/'.$kd_bidang , 'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan.'/'.$kd_bidang.'/'.$kd_unit, "Add Sub Unit" => '' ];

        $data['kd_urusan']  = $kd_urusan;
        $data['kd_bidang']  = $kd_bidang;
        $data['kd_unit']    = $kd_unit;

        $css['css']         = null;
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/add_sub', $data, $js, $css );
    }

    public function show_edit_sub($kd_urusan, $kd_bidang, $kd_unit, $kd_sub)
    {
        $this->table_db     = 'ref_sub_unit';

        $data['records']    = $this->m_global->get( $this->table_db, null, ['kd_urusan'=>$kd_urusan, 'kd_bidang'=>$kd_bidang, 'kd_unit'=>$kd_unit, 'kd_sub'=>$kd_sub] )[0];

        $data['pagetitle']  = 'Unit Organisasi';
        $data['subtitle']   = 'manage unit organisasi';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;


        $data['breadcrumb'] = ['Urusan' => $this->url, 'Bidang' => $this->url."/show_bidang", 'Unit' => $this->url."/show_unit/".$kd_urusan."/".$kd_bidang, 'Sub Unit' => $this->url."/show_sub_unit/".$kd_urusan."/".$kd_bidang."/".$kd_unit, 'Edit Sub Unit'=> '' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'unit_organisasi/edit_sub', $data, $js );
    }

    public function action_add_sub()
    {
        // set table db
        $this->table_db     = 'ref_sub_unit';

        $this->form_validation->set_rules('kd_urusan',      'Kode Urusan',          'trim|required');
        $this->form_validation->set_rules('kd_bidang',      'Kode Bidang',          'trim|required');
        $this->form_validation->set_rules('kd_unit',        'Kode Unit',            'trim|required');
        $this->form_validation->set_rules('kd_sub',         'Kode Sub Unit',        'trim|required');
        $this->form_validation->set_rules('nm_sub_unit',    'Nama Sub Unit',        'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_urusan']          = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']          = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']            = $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']             = $this->input->post('kd_sub');
            $data[$this->table_prefix.'nm_sub_unit']        = $this->input->post('nm_sub_unit');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Sub Unit with Name <strong>'.$this->input->post('nm_sub_unit').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add Sub Unit with Name <strong>'.$this->input->post('nm_sub_unit').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit_sub($id)
    {
        // echo '<pre>', print_r($this->input->post()), exit();

        $this->table_db     = 'ref_sub_unit';

        $this->form_validation->set_rules('kd_urusan',  'Kode Urusan',  'trim|required');
        $this->form_validation->set_rules('kd_bidang',  'kode bidang',  'trim|required');
        $this->form_validation->set_rules('nm_sub_unit',  'nama Sub Unit',  'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_urusan']   = $this->input->post('kd_urusan');
            $data[$this->table_prefix.'kd_bidang']   = $this->input->post('kd_bidang');
            $data[$this->table_prefix.'kd_unit']     = $this->input->post('kd_unit');
            $data[$this->table_prefix.'kd_sub']      = $this->input->post('kd_sub');
            $data[$this->table_prefix.'nm_sub_unit'] = $this->input->post('nm_sub_unit');


            $result = $this->m_global->update($this->table_db, $data, ['id_organisasi' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Unit with Name <strong>'.$this->input->post('nm_sub_unit').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Unit with Name <strong>'.$this->input->post('nm_sub_unit').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }
    // =============== END SUB-UNIT ===============


    // =============== BELUM FIX ===============
    // global actions
    public function change_status($status, $id)
    {
        $status = explode("/", $status);
        $value  = $status[0];
        $field  = $status[1];
        $table  = $status[2];
        $id     = $id['id IN '];

        $result = $this->db->query("SELECT status from $table where $field in $id")->row();

        if ($result->status == '99' and $value == '99') {
            $query = $this->db->query("DELETE from $table where $field in $id");
        } else {
            $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
        }
    }

// global actions
    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function change_status_by_subunit($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['id_organisasi' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['id_organisasi' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    // public function change_status( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_db ='ref_urusan', $data, NULL, $where );
    // }
    //
    // public function change_status_bidang( $status, $where )
    // {
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //     $result = $this->m_global->update( $this->table_db ='ref_bidang', $data, NULL, $where );
    // }
    //
    // public function change_unit_status( $status, $where )
    // {
    //     $this->table_db     = 'ref_unit';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_unit_status_by( $id, $status, $stat = FALSE )
    // {
    //     $this->table_db     = 'ref_unit';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }
    //
    // public function change_sub_status( $status, $where )
    // {
    //     $this->table_db     = 'ref_sub_unit';
    //
    //     $data[ $this->table_prefix.'status' ]   = $status;
    //
    //     $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    // }
    //
    // public function change_sub_status_by( $id, $status, $stat = FALSE )
    // {
    //     $this->table_db     = 'ref_sub_unit';
    //
    //     if ( $stat ){
    //         $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'id_organisasi' => $id] );
    //     }
    //     else {
    //         $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id_organisasi' => $id]);
    //     }
    //
    //     if ( $result ){
    //         $data['status'] = 1;
    //     }
    //     else {
    //         $data['status'] = 0;
    //     }
    //
    //     echo json_encode( $data );
    // }

}

/* End of file Unit_organisasi.php */
/* Location: ./application/modules/parameter/controllers/Unit_organisasi.php */
