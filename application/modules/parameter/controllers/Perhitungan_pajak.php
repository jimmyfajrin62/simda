<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perhitungan_pajak extends Admin_Controller
{
	private $prefix         = 'rekening';
    private $url            = 'parameter/Perhitungan_Pajak';
    private $table_db       = '';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

    public function __construct()
    {
    	parent::__construct();
    }

	public function index()
	{
		$data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'Perhitungan Pajak';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null,'Perhitungan Pajak' => $this->url];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'perhitungan_pajak/index', $data, $js, $css );
	}

}

/* End of file Perhitungan_pajak.php */
/* Location: ./application/modules/parameter/controllers/Perhitungan_pajak.php */
