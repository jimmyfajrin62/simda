<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bphtb extends Admin_Controller
{
	private $prefix         = 'parameter/bphtb';
    private $url            = 'parameter/bphtb';
    private $table_db       = 'ref_bphtb';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
		$data['records']    = $this->m_global->get( $this->table_db, null)[0];

        $data['pagetitle']  = 'Parameter';
        $data['subtitle']   = 'BPHTB';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $data['breadcrumb'] = [ 'Parameter' => '', 'BPHTB' => $this->url ];
        $this->template->display('bphtb/index', $data, $js, $css);
	}

	public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'jn_perolehan'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_perolehan'          => 'nm_perolehan',
            'lastupdate'          => 'lastupdate',
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else
                    {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        }
        else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,jn_perolehan,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->jn_perolehan.'"/><span></span></label>',
                $i,
                $rows->nm_perolehan,
                $rows->lastupdate,
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->jn_perolehan.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit/'.$rows->jn_perolehan.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->jn_perolehan.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add()
    {
        $data['pagetitle']  = 'BPHTB';
        $data['subtitle']   = 'add';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => '', 'BPHTB' => $this->url, 'Add' => $this->url.'/show_add' ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'bphtb/add', $data, $js );
    }

    public function show_edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_db, null, ['jn_perolehan' => $id] )[0];

        $data['pagetitle']  = 'BPHTB';
        $data['subtitle']   = 'Edit';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;

        $data['breadcrumb'] = [ 'Parameter' => '', 'BPHTB' => $this->url, 'Edit' => $this->url.'/show_edit/'.$id ];
        $js['js']           = [ 'form-validation' ];

        $this->template->display( 'bphtb/edit', $data, $js );
    }

    public function action_add()
    {
        $this->form_validation->set_rules('nm_perolehan', 'Keterangan', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else
                {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'nm_perolehan'] = $this->input->post('nm_perolehan');

            $result = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add BPHTB';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add BPHTB';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function action_edit( $id )
    {
        $this->form_validation->set_rules('nm_perolehan', 'Keterangan', 'trim');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'nm_perolehan']  = $this->input->post('nm_perolehan');

            $result = $this->m_global->update($this->table_db, $data, ['jn_perolehan' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit BPHTB';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed edit BPHTB';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }

        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function change_status( $status, $where )
    {
        $data[ $this->table_prefix.'status' ]   = $status;

        $result = $this->m_global->update( $this->table_db, $data, NULL, $where );
    }

    public function change_status_by( $id, $status, $stat = FALSE )
    {
        if ( $stat ){
            $result = $this->m_global->delete( $this->table_db, [$this->table_prefix.'jn_perolehan' => $id] );
        }
        else {
            $result = $this->m_global->update( $this->table_db, [$this->table_prefix.'status' => $status], [$this->table_prefix.'jn_perolehan' => $id]);
        }

        if ( $result ){
            $data['status'] = 1;
        }
        else {
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

}

/* End of file Bphtb.php */
/* Location: ./application/modules/parameter/controllers/Bphtb.php */
?>
