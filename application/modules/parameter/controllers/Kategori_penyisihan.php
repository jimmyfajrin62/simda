<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori_penyisihan extends Admin_Controller
{
	private $prefix         = 'parameter/kategori_penyisihan';
    private $url            = 'parameter/kategori_penyisihan';
    private $table_db       = 'ref_kategori';
    private $table_prefix   = '';
    private $rule_valid     = 'xss_clean|encode_php_tags';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'Kategori Penyisihan';
        $data['subtitle']   = 'manage kategori penyisihan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null,'Kategori penyisihan' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'kategori_penyisihan/index', $data, $js, $css );
	}

	public function select()
    {
        // select table urusan
        $this->table_db = 'ref_kategori';
        $action_db      = 'edit_data';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'         => 'kd_kategori',
            'kategori'     => 'uraian'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
					else
					{
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request'";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_kategori, uraian, status, lastupdate,'.implode(',' , $aCari);
        $result = $this->m_global->get($this->table_db, NULL, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_kategori.'"/><span></span></label>',
                $i,
                $rows->kd_kategori,
                $rows->uraian,
                $rows->lastupdate,
                '<a data-original-title="Edit" data-toggle="modal" href="#modal_form" id="modal_edit" class="btn blue btn-icon-only tooltips" onclick="action_db('.$rows->kd_kategori.')" data-action="edit_data"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kategori.'/ref_kategori/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kategori.'/ref_kategori/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function change_status($status, $id)
{
    $status = explode("/", $status);
    $value  = $status[0];
    $field  = $status[1];
    $table  = $status[2];
    $id     = $id['id IN '];

    $result = $this->db->query("SELECT status from $table where $field in $id")->row();

    if ($result->status == '99' and $value == '99') {
        $query = $this->db->query("DELETE from $table where $field in $id");
    } else {
        $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
    }
}

// global actions
public function change_status_by($id, $table, $status, $stat = false)
{
    if ($stat) {
        $result = $this->m_global->delete($table, ['kd_kategori' => $id]);
    } else {
        $result = $this->m_global->update($table, ['status' => $status], ['kd_kategori' => $id]);
    }

    if ($result) {
        $data['status'] = 1;
    } else {
        $data['status'] = 0;
    }

    echo json_encode($data);
}

    public function show_add()
    {
        $data['pagetitle']  = 'Kategori Penyisihan';
        $data['subtitle']   = 'Add kategori penyisihan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null,'Kategori penyisihan' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax', 'form-validation' ];
        $css['css']         = null;

        $this->template->display( 'kategori_penyisihan/add', $data, $js, $css );

    }

    public function action_add()
    {
        // echo print_r($this->input->post()); exit()
        $this->table_db = 'ref_kategori';

        $this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );

                    die();
                }
                else
                {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'uraian']     = $this->input->post('kategori');
            $data[$this->table_prefix.'lastupdate'] = date( 'Y-m-d H:i:s');

            $result  = $this->m_global->insert( $this->table_db, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add Kategori with Name <strong>'.$this->input->post('kategori').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed add Kategori with Name <strong>'.$this->input->post('kategori').'</strong>';
                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
            redirect('index');
        }

    }

    public function action_edit($id)
    {
        $this->table_db = 'ref_kategori';
        $this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'uraian']     = $this->input->post('kategori');
            $data[$this->table_prefix.'lastupdate'] = date( 'Y-m-d H:i:s');
            // $data[$this->table_prefix.'']           = $this->input->post('');
            $result = $this->m_global->update($this->table_db, $data, ['kd_kategori' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully Edit Kategori with Name <strong>'.$this->input->post('kategori').'</strong>';

                echo json_encode( $data );
            }
            else
            {
                $data['status']     = 0;
                $data['message']    = 'Failed Edit Kategori with Name <strong>'.$this->input->post('kategori').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else
        {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function get_kategori()
    {
        $id              = $this->input->post('id');
        $this->table_db  = 'ref_kategori';
        $data['records'] = $this->m_global->get($this->table_db, null, ['kd_kategori' => $id] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

}

/* End of file Kategori_penyisihan.php */
/* Location: ./application/modules/parameter/controllers/Kategori_penyisihan.php */
