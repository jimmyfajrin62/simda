<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mapping_rekening_pajak extends Admin_Controller
{
    private $prefix          = 'parameter/mapping_rekening_pajak';
    private $url             = 'parameter/mapping_rekening_pajak';
    private $table_pajak     = 'ref_jenis_pajak';
    private $table_rek       = 'ref_rek_6';
    private $table_kelurahan = 'ref_kelurahan';
    private $table_prefix    = '';
    private $rule_valid      = 'xss_clean|encode_php_tags';

    function __construct()
	{
        parent::__construct();
    }

	public function index()
	{
		$data['pagetitle']  = 'Mapping Rekening Pajak ';
        $data['subtitle']   = '';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Rekening Pajak' => $this->url  ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'mapping_rek_pajak/mapping_rek_pajak', $data, $js, $css );

	}

	public function select()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'id'         => 'id',
            'nm_pajak'   => 'nm_pajak',

            // 'lastupdate' => 'lastupdate',
        ];

        $where_e      = null;
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        };

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' )
        {
            $request = $_REQUEST['filterstatus'];
            $where_e = " status = '$request' ";
        }
        else {
            $where_e = " status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_pajak, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id,status,kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,jn_pemungutan,kd_rek_1_denda,kd_rek_2_denda,
        kd_rek_3_denda,kd_rek_4_denda,kd_rek_5_denda,kd_rek_1_bunga,kd_rek_2_bunga,
        kd_rek_3_bunga,kd_rek_4_bunga,kd_rek_5_bunga,kd_rek_1_kenaikan,kd_rek_2_kenaikan,kd_rek_3_kenaikan,kd_rek_4_kenaikan,kd_rek_5_kenaikan,kd_rek_6_kenaikan,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_pajak, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                // '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->id,
                $rows->nm_pajak,
                $rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3.'.'.$rows->kd_rek_4,
                $rows->kd_rek_1_denda.'.'.$rows->kd_rek_2_denda.'.'.$rows->kd_rek_3_denda.'.'.$rows->kd_rek_4_denda.'.'.$rows->kd_rek_5_denda,
                $rows->kd_rek_1_bunga.'.'.$rows->kd_rek_2_bunga.'.'.$rows->kd_rek_3_bunga.'.'.$rows->kd_rek_4_bunga.'.'.$rows->kd_rek_5_bunga,
                $rows->kd_rek_1_kenaikan.'.'.$rows->kd_rek_2_kenaikan.'.'.$rows->kd_rek_3_kenaikan.'.'.$rows->kd_rek_4_kenaikan.'.'.$rows->kd_rek_5_kenaikan.'.'.$rows->kd_rek_6_kenaikan,
                // $rows->lastupdate,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/show_edit_mapping_pajak/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->id.'/ref_jenis_pajak/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_rek_pajak()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'unit_id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_pajak'   => 'nm_pajak',
            'lastupdate' => 'lastupdate',
        ];

        $where_e      = null;
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_pajak, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'id,status,kd_rek_1,kd_rek_2,kd_rek_3,kd_rek_4,jn_pemungutan,kd_rek_1_denda,kd_rek_2_denda,
        kd_rek_3_denda,kd_rek_4_denda,kd_rek_1_kenaikan,kd_rek_2_kenaikan,kd_rek_3_kenaikan,kd_rek_4_kenaikan,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_pajak, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->id.'"/><span></span></label>',
                $i,
                $rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3.'.'.$rows->kd_rek_4,
                $rows->nm_pajak,
                $rows->lastupdate,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih('.$rows->id.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>'.
                '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function show_add_rekening_pajak()
    {
        $data['pagetitle']  = 'Mapping Rekening Pajak';
        $data['subtitle']   = 'Tambah Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['rekening']   = $this->m_global->get($this->table_pajak, null,null);

        $data['breadcrumb'] = [ 'Rekening Pajak' => $this->url, 'Add' => null ];
        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];

        $this->template->display( 'mapping_rek_pajak/add_rekening_pajak', $data, $js );
    }

    public function show_edit_mapping_pajak( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_pajak, null, ['id' => $id] )[0];

        $data['pagetitle']  = 'Mapping Rekening Pajak';
        $data['subtitle']   = 'Edit Rekening';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;
        // echo $id;exit();

        $data['breadcrumb'] = [ 'Rekening Pajak' => $this->url, 'Edit' => $this->url.'/show_edit_mapping_pajak/'.$id ];
        $js['js']           = [ 'form-validation' , 'table-datatables-ajax'];

        $this->template->display( 'mapping_rek_pajak/edit_rekening_pajak', $data, $js );
    }

    public function action_add_rekening_pajak()
    {
       	$this->form_validation->set_rules('nm_pajak', 'Nama Pajak', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'nm_pajak']          = $this->input->post('nm_pajak');
            $data[$this->table_prefix.'kd_rek_1']          = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']          = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']          = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']          = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_1_denda']    = $this->input->post('kd_rek_1_denda');
            $data[$this->table_prefix.'kd_rek_2_denda']    = $this->input->post('kd_rek_2_denda');
            $data[$this->table_prefix.'kd_rek_3_denda']    = $this->input->post('kd_rek_3_denda');
            $data[$this->table_prefix.'kd_rek_4_denda']    = $this->input->post('kd_rek_4_denda');
            $data[$this->table_prefix.'kd_rek_5_denda']    = $this->input->post('kd_rek_5_denda');
            $data[$this->table_prefix.'kd_rek_1_bunga']    = $this->input->post('kd_rek_1_bunga');
            $data[$this->table_prefix.'kd_rek_2_bunga']    = $this->input->post('kd_rek_2_bunga');
            $data[$this->table_prefix.'kd_rek_2_bunga']    = $this->input->post('kd_rek_3_bunga');
            $data[$this->table_prefix.'kd_rek_3_bunga']    = $this->input->post('kd_rek_4_bunga');
            $data[$this->table_prefix.'kd_rek_4_bunga']    = $this->input->post('kd_rek_5_bunga');
            $data[$this->table_prefix.'kd_rek_5_bunga']    = $this->input->post('kd_rek_5_bunga');
            $data[$this->table_prefix.'kd_rek_6_bunga']    = $this->input->post('kd_rek_6_bunga');
            $data[$this->table_prefix.'kd_rek_1_kenaikan'] = $this->input->post('kd_rek_1_kenaikan');
            $data[$this->table_prefix.'kd_rek_2_kenaikan'] = $this->input->post('kd_rek_2_kenaikan');
            $data[$this->table_prefix.'kd_rek_2_kenaikan'] = $this->input->post('kd_rek_3_kenaikan');
            $data[$this->table_prefix.'kd_rek_3_kenaikan'] = $this->input->post('kd_rek_4_kenaikan');
            $data[$this->table_prefix.'kd_rek_4_kenaikan'] = $this->input->post('kd_rek_5_kenaikan');
            $data[$this->table_prefix.'kd_rek_5_kenaikan'] = $this->input->post('kd_rek_5_kenaikan');
            $data[$this->table_prefix.'kd_rek_6_kenaikan'] = $this->input->post('kd_rek_6_kenaikan');
            $data[$this->table_prefix.'lastupdate']        = date( 'Y-m-d H:i:s');

            $result  = $this->m_global->insert( $this->table_pajak, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
            redirect('kecamatan_kelurahan/index');
        }

    }

    public function action_edit_rekening_pajak($id)
    {
        $this->form_validation->set_rules('nm_pajak', 'nm_pajak', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'nm_pajak']           = $this->input->post('nm_pajak');
            $data[$this->table_prefix.'kd_rek_1']           = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']           = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']           = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_rek_4']           = $this->input->post('kd_rek_4');
            $data[$this->table_prefix.'kd_rek_1_denda']     = $this->input->post('kd_rek_1_denda');
            $data[$this->table_prefix.'kd_rek_2_denda']     = $this->input->post('kd_rek_2_denda');
            $data[$this->table_prefix.'kd_rek_3_denda']     = $this->input->post('kd_rek_3_denda');
            $data[$this->table_prefix.'kd_rek_4_denda']     = $this->input->post('kd_rek_4_denda');
            $data[$this->table_prefix.'kd_rek_5_denda']     = $this->input->post('kd_rek_5_denda');
            $data[$this->table_prefix.'kd_rek_6_denda']     = $this->input->post('kd_rek_6_denda');
            $data[$this->table_prefix.'kd_rek_1_bunga']     = $this->input->post('kd_rek_1_bunga');
            $data[$this->table_prefix.'kd_rek_2_bunga']     = $this->input->post('kd_rek_2_bunga');
            $data[$this->table_prefix.'kd_rek_3_bunga']     = $this->input->post('kd_rek_3_bunga');
            $data[$this->table_prefix.'kd_rek_4_bunga']     = $this->input->post('kd_rek_4_bunga');
            $data[$this->table_prefix.'kd_rek_5_bunga']     = $this->input->post('kd_rek_5_bunga');
            $data[$this->table_prefix.'kd_rek_6_bunga']     = $this->input->post('kd_rek_6_bunga');
            $data[$this->table_prefix.'kd_rek_1_kenaikan']  = $this->input->post('kd_rek_1_kenaikan');
            $data[$this->table_prefix.'kd_rek_2_kenaikan']  = $this->input->post('kd_rek_2_kenaikan');
            $data[$this->table_prefix.'kd_rek_3_kenaikan']  = $this->input->post('kd_rek_3_kenaikan');
            $data[$this->table_prefix.'kd_rek_4_kenaikan']  = $this->input->post('kd_rek_4_kenaikan');
            $data[$this->table_prefix.'kd_rek_5_kenaikan']  = $this->input->post('kd_rek_5_kenaikan');
            $data[$this->table_prefix.'kd_rek_6_kenaikan']  = $this->input->post('kd_rek_6_kenaikan');

            // $data[$this->table_prefix.'']           = $this->input->post('');
            $result = $this->m_global->update($this->table_pajak, $data, ['id' => $id]);
           // echo $this->db->last_query();exit();

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';
                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select_denda()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=7';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_denda('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
                // '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_bunga()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=7';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_bunga('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
                // '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function select_kenaikan()
    {
        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'nm_rek_6' => 'nm_rek_6',
            'lastupdate'   => 'lastupdate',
        ];

        $where_e      = 'kd_rek_1=4 and kd_rek_2=1 and kd_rek_3=4 and kd_rek_4=7';
        $where        = null;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_prefix.'status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_prefix.'status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_rek, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'status,id_rek_6, kd_rek_1, kd_rek_2, kd_rek_3, kd_rek_4, kd_rek_5, kd_rek_6,nm_rek_6,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_rek, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $i,
                $rows->kd_rek_1,
                $rows->kd_rek_2,
                $rows->kd_rek_3,
                $rows->kd_rek_4,
                $rows->kd_rek_5,
                $rows->kd_rek_6,
                $rows->nm_rek_6,
                // '<a href="'.base_url($this->prefix.'/change_status_by/'.$rows->kd_kec.'/'.($rows->status == 1 ? '0" data-original-title="Set to InActive"' : '1" data-original-title="Set to Active"' ) ).' class="btn btn-icon-only tooltips '.($rows->status == 0 ? 'grey-cascade' : 'green' ). '" onClick="return f_status(1, this, event)"><i title="'.($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active') ).'" class="fa fa'.($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-trash-o' : '-eye') ).'"></i></a>'.
                // '<a href="'.base_url().$this->url.'/show_sub_unit/'.$rows->id.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-check"></i></a>'.
                '<a class="btn blue btn-icon-only tooltips" data-original-title="Select" onClick="pilih_kenaikan('.$rows->id_rek_6.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i></a>',
                // '<a href="'.base_url( ''.$this->prefix.'/change_status_by/'.$rows->id_rek_6.'/99/'.($rows->status == 99 ? '/true" data-original-title="Delete Permanently"' : '" data-original-title="Delete"' )).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash"></i></a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode( $records );
    }

    public function get_rekening_pajak()
    {
        $id        = sprintf("%05d", $this->input->post('id') ) ;
        $data['records'] = $this->m_global->get($this->table_pajak, null, ['id' => $id] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_denda()
    {
        $id        = sprintf("%05d", $this->input->post('id') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_bunga()
    {
        $id        = sprintf("%05d", $this->input->post('id') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function get_kenaikan()
    {
        $id        = sprintf("%05d", $this->input->post('id') ) ;
        $data['records'] = $this->m_global->get($this->table_rek, null, ['id_rek_6' => $id] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }

    public function change_status( $status, $where )
    {
        $data[ $this->table_prefix.'status' ]   = $status;
        $result = $this->m_global->update( $this->table_pajak, $data, NULL, $where );
    }

    public function change_status_by( $id, $status, $stat = FALSE )
    {

        $this->table_pajak = 'ref_jenis_pajak';

        if ( $stat ){
            $result = $this->m_global->delete( $this->table_pajak, [$this->table_prefix.'id' => $id] );
        }
        else{
            $result = $this->m_global->update( $this->table_pajak, [$this->table_prefix.'status' => $status], [$this->table_prefix.'id' => $id]);
        }

        if ( $result ){
            $data['status'] = 1;
        }
        else{
            $data['status'] = 0;
        }

        echo json_encode( $data );
    }

    public function export_data()
    {
        $this->load->library('excel');
        $data['pagetitle'] = 'Mapping_rekening_pajak Report';

        // query
        $data['report']  = $this->db->query("SELECT id, nm_pajak, concat(kd_rek_1, '.', kd_rek_2, '.', kd_rek_3, '.', kd_rek_4) as gab_rek, concat(kd_rek_1_denda, '.', kd_rek_2_denda, '.', kd_rek_3_denda, '.', kd_rek_4_denda, '.', kd_rek_5_denda,'.', kd_rek_6_denda) as gab_denda, concat(kd_rek_1_bunga,'.', kd_rek_2_bunga,'.', kd_rek_3_bunga, '.', kd_rek_4_bunga, '.', kd_rek_5_bunga, '.', kd_rek_6_bunga) as gab_bunga, concat(kd_rek_1_kenaikan, '.', kd_rek_2_kenaikan, '.', kd_rek_3_kenaikan, '.', kd_rek_4_kenaikan, '.', kd_rek_5_kenaikan, '.', kd_rek_6_kenaikan) as gab_kenaikan from ref_jenis_pajak where status = '1'")->result();

        $data['namaFile']   ='Mapping_rekening_pajak_Report_'.date('Y-m-d');
        $data['title']      ='Mapping Rekening Pajak Report';
        $data['title_2']    ='Periode '.tgl_format(date('d-m-Y'));
        $data['header']     = [
            ['No', '8'], ['Kode Jenis', '15'], ['Jenis Pajak', '45'], ['Rekening Pajak', '20'], ['Denda', '15'], ['Bunga', '15'], ['Kenaikan', '15']
        ];

        $this->load->view('mapping_rek_pajak/export',$data);
    }
}

/* End of file mapping_rekening_pajak.php */
/* Location: ./application/modules/parameter/controllers/mapping_rekening_pajak.php */
