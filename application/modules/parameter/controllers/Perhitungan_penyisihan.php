<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perhitungan_penyisihan extends Admin_Controller
{
	private $prefix            = 'parameter/perhitungan_penyisihan';
    private $url               = 'parameter/perhitungan_penyisihan';
    private $table_penyisihan  = 'ref_penyisihan';
    private $table_kategori    = 'ref_kategori';
    private $table_prefix      = '';
    private $rule_valid        = 'xss_clean|encode_php_tags';

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$data['pagetitle']  = 'Perhitungan Penyisihan';
        $data['subtitle']   = 'manage perhitungan penyisihan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null, 'Perhitungan penyisihan' => $this->url ];

        $js['js']           = [ 'table-datatables-ajax' ];
        $css['css']         = null;

        $this->template->display( 'perhitungan_penyisihan/index', $data, $js, $css );
	}

	public function select()
    {
        // select table urusan
        $this->table_db = 'ref_penyisihan';

        $tb_join        = [
                                ['ref_kategori','ref_penyisihan.kd_kategori = ref_kategori.kd_kategori']
                          ];

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            'kode'     => 'kd_penyisihan',
            'kategori' => 'ref_penyisihan.kd_kategori',
            'rekening' => 'kd_rek_1',
            'u_awal'   => 'umur_tahun_1',
            'u_akhir'  => 'umur_tahun_2'
        ];

        $where      = NULL;
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $request = $_REQUEST['filterstatus'];
            $where_e = " ref_penyisihan.status = '$request' ";
        }
        else {
            $where_e = " ref_penyisihan.status = '1' ";
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column']-2)]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = 'kd_penyisihan, kd_rek_1, kd_rek_2, kd_rek_3, '.$this->table_db.'.kd_kategori, ref_kategori.uraian, umur_tahun_1, umur_bulan_1,umur_hari_1,umur_tahun_2,umur_bulan_2,umur_hari_2,tarif_penyisihan,'.$this->table_db.'.status,'.$this->table_db.'.lastupdate,'.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, $tb_join, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                '<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input name="id[]" type="checkbox" class="checkboxes" value="'.$rows->kd_penyisihan.'"/><span></span></label>',
                $i,
                $rows->kd_penyisihan,
                $rows->uraian,
                '<span class="label  blue-steel label-info">'.$rows->kd_rek_1.' . '.$rows->kd_rek_2.' . '.$rows->kd_rek_3,
                $rows->umur_tahun_1.' Tahun '.$rows->umur_bulan_1.' Bulan '.$rows->umur_hari_1.' Hari',
                $rows->umur_tahun_2.' Tahun '.$rows->umur_bulan_2.' Bulan '.$rows->umur_hari_2.' Hari',
                $rows->lastupdate,
                '<a data-original-title="Edit" href="'.base_url().$this->url.'/edit/'.$rows->kd_penyisihan.'" class="ajaxify btn blue btn-icon-only tooltips"><i class="fa fa-edit"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_penyisihan.'/ref_penyisihan/'.
                        ($rows->status == 1 ? '0" data-original-title="Set ke Tidak Aktif"' : '1" data-original-title="Set ke Aktif"')).' class="btn btn-icon-only tooltips '.
                        ($rows->status == 0 ? 'grey-cascade' : 'green-seagreen'). '" onClick="return f_status(1, this, event)"><i title="'.
                        ($rows->status == 0 ? 'InActive' : ($rows->status == 99 ? 'Deleted' : 'Active')).'" class="fa fa'.
                        ($rows->status == 0 ? '-eye-slash' : ($rows->status == 99 ? '-refresh' : '-eye')).'"></i></a>'.
                '<a href="'.base_url($this->url.'/change_status_by/'.$rows->kd_penyisihan.'/ref_penyisihan/99'.
                        ($rows->status == 99 ? '/true" data-original-title="Hapus Permanen"' : '" data-original-title="Hapus Data"')).' class="btn btn-icon-only red tooltips" onClick="return f_status(2, this, event)"><i class="fa fa-trash-o"></i></a>',

            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        echo json_encode( $records );
    }

    public function change_status($status, $id)
{
    $status = explode("/", $status);
    $value  = $status[0];
    $field  = $status[1];
    $table  = $status[2];
    $id     = $id['id IN '];

    $result = $this->db->query("SELECT status from $table where $field in $id")->row();

    if ($result->status == '99' and $value == '99') {
        $query = $this->db->query("DELETE from $table where $field in $id");
    } else {
        $query = $this->db->query("UPDATE $table set status = '$value' where $field in $id");
    }
}

    public function change_status_by($id, $table, $status, $stat = false)
    {
        if ($stat) {
            $result = $this->m_global->delete($table, ['kd_penyisihan' => $id]);
        } else {
            $result = $this->m_global->update($table, ['status' => $status], ['kd_penyisihan' => $id]);
        }

        if ($result) {
            $data['status'] = 1;
        } else {
            $data['status'] = 0;
        }

        echo json_encode($data);
    }

    public function add()
    {
    	$data['kategori']   = $this->m_global->get( $this->table_kategori, null, ['status' => '1'] );
        $data['pagetitle']  = 'Perhitungan Penyisihan';
        $data['subtitle']   = 'Tambah Perhitungan Penyisihan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;

        $data['breadcrumb'] = [ 'Parameter' => null, 'Perhitungan penyisihan' => $this->url, 'Add' => $this->url.'/add' ];
        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];

        $this->template->display( 'perhitungan_penyisihan/add_penyisihan', $data, $js );
    }

    public function action_add_perhitungan_penyisihan()
    {
       	$this->form_validation->set_rules('kd_rek_1', 'Rekening 1', 'trim|required');
       	$this->form_validation->set_rules('kd_rek_2', 'Rekening 2', 'trim|required');
       	$this->form_validation->set_rules('kd_rek_3', 'Rekening 3', 'trim|required');
       	$this->form_validation->set_rules('tarif', 'Tarif', 'trim|required');
       	$this->form_validation->set_rules('tahun_aw', 'Tahun Awal', 'trim|required');
       	$this->form_validation->set_rules('bulan_aw', 'Bulan Awal', 'trim|required');
       	$this->form_validation->set_rules('hari_aw', 'Hari Awal', 'trim|required');
       	$this->form_validation->set_rules('tahun_ak', 'Tahun Akhir', 'trim|required');
       	$this->form_validation->set_rules('bulan_ak', 'Bulan Akhir', 'trim|required');
       	$this->form_validation->set_rules('hari_ak', 'Hari Akhir', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            if ( ! empty( $_FILES ) )
            {
                $config['upload_path']   = './assets/upload/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg';
                $config['max_size']      = '8024';
                $config['file_name']     = time().'_'.$_FILES["image"]['name'];

                $this->load->library('upload', $config);
                if ( ! $this->upload->do_upload( 'image' ) )
                {
                    $data['status']     = 0;
                    $data['message']    = $this->upload->display_errors();

                    echo json_encode( $data );
                    die();
                }
                else {
                    $upload = $this->upload->data();
                    $data[$this->table_prefix.'image']    = $upload['file_name'];
                }
            }

            $data[$this->table_prefix.'kd_rek_1']         = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']         = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']         = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_kategori']      = $this->input->post('kategori');
            $data[$this->table_prefix.'umur_tahun_1']     = $this->input->post('tahun_aw');
            $data[$this->table_prefix.'umur_bulan_1']     = $this->input->post('bulan_aw');
            $data[$this->table_prefix.'umur_hari_1']      = $this->input->post('hari_aw');
            $data[$this->table_prefix.'umur_tahun_2']     = $this->input->post('tahun_ak');
            $data[$this->table_prefix.'umur_bulan_2']     = $this->input->post('bulan_ak');
            $data[$this->table_prefix.'umur_hari_2']      = $this->input->post('hari_ak');
            $data[$this->table_prefix.'tarif_penyisihan'] = $this->input->post('tarif');
            $data[$this->table_prefix.'lastupdate']       = date( 'Y-m-d H:i:s');

            $result  = $this->m_global->insert( $this->table_penyisihan, $data );

            if ( $result['status'] )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully add User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed add User with Name <strong>'.$this->input->post('nm_pajak').'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
            redirect('Perhitungan_penyisihan');
        }

    }

    public function edit( $id )
    {
        $data['records']    = $this->m_global->get( $this->table_penyisihan, null, ['kd_penyisihan' => $id] )[0];

        $data['kategori']   = $this->m_global->get( $this->table_kategori, null, ['status' => '1'] );

        $data['pagetitle']  = 'Penyisihan';
        $data['subtitle']   = 'Edit Penyisihan';

        $data['url']        = base_url().$this->url;
        $data['prefix']     = $this->prefix;
        $data['id']         = $id;
        // $data['kecamatan']  = $data['record'];

        $data['breadcrumb'] = [ 'Parameter' => null, 'Penyisihan' => $this->url, 'Edit' => $this->url.'/edit/'.$id ];
        $js['js']           = [ 'form-validation', 'table-datatables-ajax' ];

        $this->template->display( 'perhitungan_penyisihan/edit_penyisihan', $data, $js );
    }

    public function action_edit($id)
    {
        $this->form_validation->set_rules('tarif', 'Tarif', 'trim|required');

        if ( $this->form_validation->run( $this ) )
        {
            $data[$this->table_prefix.'kd_rek_1']         = $this->input->post('kd_rek_1');
            $data[$this->table_prefix.'kd_rek_2']         = $this->input->post('kd_rek_2');
            $data[$this->table_prefix.'kd_rek_3']         = $this->input->post('kd_rek_3');
            $data[$this->table_prefix.'kd_kategori']      = $this->input->post('kategori');
            $data[$this->table_prefix.'umur_tahun_1']     = $this->input->post('tahun_aw');
            $data[$this->table_prefix.'umur_bulan_1']     = $this->input->post('bulan_aw');
            $data[$this->table_prefix.'umur_hari_1']      = $this->input->post('hari_aw');
            $data[$this->table_prefix.'umur_tahun_2']     = $this->input->post('tahun_ak');
            $data[$this->table_prefix.'umur_bulan_2']     = $this->input->post('bulan_ak');
            $data[$this->table_prefix.'umur_hari_2']      = $this->input->post('hari_ak');
            $data[$this->table_prefix.'tarif_penyisihan'] = $this->input->post('tarif');
            $data[$this->table_prefix.'lastupdate']       = date( 'Y-m-d H:i:s');
            // $data[$this->table_prefix.'']                 = $this->input->post('');
            $result = $this->m_global->update($this->table_penyisihan, $data, ['kd_penyisihan' => $id]);

            if ( $result )
            {
                $data['status']     = 1;
                $data['message']    = 'Successfully edit Penyisihan with Kode <strong>'.$id.'</strong>';

                echo json_encode( $data );
            }
            else {
                $data['status']     = 0;
                $data['message']    = 'Failed edit Penyisihan with Kode <strong>'.$id.'</strong>';

                if(ENVIRONMENT == 'development')
                    $data['error']  = $this->db->error();

                echo json_encode( $data );
            }
        }
        else {
            $data['status']     = 3;
            $str                = ['<p>', '</p>'];
            $str_replace        = ['<li>', '</li>'];
            $data['message']    = str_replace( $str, $str_replace, validation_errors() );

            echo json_encode( $data );
        }
    }

    public function select_kode_penyisihan()
    {
        $this->table_db = 'ref_rek_3';

        // jika action checkbox
        if ( @$_REQUEST['customActionType'] == 'group_action' )
        {
            $aChk = [0, 1, 99];

            if ( in_array( @$_REQUEST['customActionName'], $aChk) )
            {
                $this->change_status($_REQUEST['customActionName'], [$this->table_prefix.'id_rek_3'.' IN ' => "('".implode("','", $_REQUEST['id'] )."')"]);
                $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
                $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
            }
        }

        $aCari = [
            // 'filter_no_daftar'      => $this->table_db.'.no_daftar',
            'filter_nama_pendaftar' => $this->table_prefix.'nm_rek_3'
        ];

        $where      = ['kd_rek_1' => 4];
        $where_e    = NULL;

        if ( @$_REQUEST['action'] == 'filter')
        {
            $where = [];
            foreach ( $aCari as $key => $value )
            {
                if ( $_REQUEST[$key] != '' )
                {
                    if ( $key == 'lastupdate' )
                    {
                        $tmp = explode(' ', $_REQUEST[$key]);
                        $where_e = "DATE(".$this->table_prefix."lastupdate) BETWEEN '".$this->db->escape_str($tmp[0])."' AND '".$this->db->escape_str($tmp[1])."'";
                    }
                    else {
                        $where[$value.' LIKE '] = '%'.$_REQUEST[$key].'%';
                    }
                }
            }
        }

        if ( isset($_REQUEST['filterstatus']) && $_REQUEST['filterstatus'] != '' ){
            $where[$this->table_db.'.status']       = $_REQUEST['filterstatus'];
        } else {
            $where[$this->table_db.'.status <>']    = '99';
        }

        $keys             = array_keys( $aCari );
        @$order           = [$aCari[$keys[($_REQUEST['order'][0]['column'])]], $_REQUEST['order'][0]['dir']];

        $iTotalRecords    = $this->m_global->count( $this->table_db, null, $where, $where_e );
        $iDisplayLength   = intval($_REQUEST['length']);
        $iDisplayLength   = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart    = intval($_REQUEST['start']);
        $sEcho            = intval($_REQUEST['draw']);

        $records          = array();
        $records["data"]  = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $select = $this->table_db.'.id_rek_3, kd_rek_1, kd_rek_2, kd_rek_3, nm_rek_3,'.$this->table_db.'.status, '.$this->table_db.'.lastupdate, '.implode(',' , $aCari);

        $result = $this->m_global->get($this->table_db, null, $where, $select, $where_e, $order, $iDisplayStart, $iDisplayLength);

        $i = 1 + $iDisplayStart;
        foreach ( $result as $rows )
        {
            $records["data"][] = array(
                $rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3 ,
                $rows->nm_rek_3,
                '<a class="btn blue btn-icon-only tooltips" data-original-title="'.$rows->kd_rek_1.'.'.$rows->kd_rek_2.'.'.$rows->kd_rek_3.'" onClick="pilih('.$rows->kd_rek_1.','.$rows->kd_rek_2.','.$rows->kd_rek_3.')" data-dismiss="modal" >'.
                '<i class="fa fa-check"></i>'.
                '</a>',
            );
            $i++;
        }

        $records["draw"]            = $sEcho;
        $records["recordsTotal"]    = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        // echo pre($_REQUEST['order'][0]['column']);
        // echo $this->db->last_query();
        echo json_encode( $records );
    }

    public function get_kode_penyisihan()
    {
        $this->table_db    = 'ref_rek_3';
        $kd_rek_1          = $this->input->post('kd_rek_1') ;
        $kd_rek_2          = $this->input->post('kd_rek_2') ;
        $kd_rek_3          = $this->input->post('kd_rek_3') ;

        $data['records']   = $this->m_global->get($this->table_db, null, ['kd_rek_1' => $kd_rek_1, 'kd_rek_2' => $kd_rek_2, 'kd_rek_3' => $kd_rek_3] )[0];

        header("Content-Type:application/json");
        echo json_encode($data);
    }
}

/* End of file Perhitungan_penyisihan.php */
/* Location: ./application/modules/parameter/controllers/Perhitungan_penyisihan.php */
