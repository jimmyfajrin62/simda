<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?php echo $url; ?>/action_edit/<?php echo $id ?>" class="form-horizontal form-add" method="POST" data-confirm="1" role="form">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Jenis Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <select name="kd_peraturan" class="form-control">
                                    <option>-- Jenis Peraturan --</option>
                                    <option <?php if ($records->kd_peraturan==1){echo "selected";}?> value="1">Undang-undang</option>
                                    <option <?php if ($records->kd_peraturan==2){echo "selected";}?> value="2">Peraturan Pemerintah</option>
                                    <option <?php if ($records->kd_peraturan==3){echo "selected";}?> value="3">Keputusan Menteri Dalam Negeri</option>
                                    <option <?php if ($records->kd_peraturan==4){echo "selected";}?> value="4">Peraturan Menteri Dalam Negeri</option>
                                    <option <?php if ($records->kd_peraturan==5){echo "selected";}?> value="5">Peraturan Daerah</option>
                                    <option <?php if ($records->kd_peraturan==6){echo "selected";}?> value="6">Peraturan Kepala Daerah</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No. Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php echo $records->no_peraturan ?>" placeholder="Nama" required name="no_peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tanggal Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="tgl_peraturan" value="<?=$records->tgl_peraturan?>">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tanggal Berlaku
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="tgl_berlaku" value="<?=$records->tgl_berlaku?>">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php echo $records->uraian_peraturan ?>" placeholder="Nama" required name="uraian_peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Peraturan berlaku untuk:
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_all">All
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_pajak">Pajak
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_retribusi">Retribusi
                                </label>
                                <span class="help-block"></span>
                                <br>

                                <!-- tampilan setelah memilih radio button -->
                                <div id="jn_all" style="display: none"></div>
                                <div id="jn_pajak" style="display: none">
                                    <select name="jn_pajak" class="form-control">
                                        <option>-- Pilih Jenis Pajak</option>
                                        <?php foreach ($pajak as $key => $value): ?>
                                            <?php
                                                $select = '';
                                                if ($records->jn_pajak == $value->id) {
                                                    $select = 'selected';
                                                }else{
                                                    $select = '';
                                                }
                                            ?>
                                            <option value="<?=$value->id?>" <?=$select?>>
                                                <?=$value->nm_pajak?>
                                            </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div id="jn_retribusi" style="display: none">
                                    <select name="jn_retribusi" class="form-control">
                                        <option>-- Pilih Jenis Retribusi</option>
                                        <?php foreach ($retribusi as $key => $value): ?>
                                        <?php
                                            $select = '';
                                            if ($records->jn_retribusi == $value->kd_jenis) {
                                                $select = 'selected';
                                            }else{
                                                $select = '';
                                            }
                                        ?>
                                        <option value="<?=$value->kd_jenis?>" <?=$select?>>
                                            <?=$value->nm_retribusi?>
                                        </option>
                                    <?php endforeach ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="submit btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url ?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var title   = 'Confirmation';
        var text    = 'Are you sure to continue this action?';
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message, title, text );
    });
</script>
<script type="text/javascript">
   $(document).ready(function(){
        $("#pil_all").click(function(){
            $("#jn_all:hidden").show('slow');
            $("#jn_pajak").hide();
            $("#jn_retribusi").hide();
        });
        $("#pil_all").click(function(){
            if ($('#pil_all').prop('checked')===false) {
                $('jn_all').hide();
            }
        });

        $("#pil_pajak").click(function(){
            $("#jn_pajak:hidden").show('slow');
            $("#jn_all").hide();
            $("#jn_retribusi").hide();
        });
        $("#pil_pajak").click(function(){
            if ($('#pil_pajak').prop('checked')===false) {
                $('jn_pajak').hide();
            }
        });

        $("#pil_retribusi").click(function(){
            $("#jn_retribusi:hidden").show('slow');
            $("#jn_pajak").hide();
            $("#jn_all").hide();
        });
        $("#pil_retribusi").click(function(){
            if ($('#pil_retribusi').prop('checked')===false) {
                $('jn_retribusi').hide();
            }
        });

   });
</script>
