<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Jenis Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <select name="kd_peraturan" class="form-control">
                                    <option>-- Jenis Peraturan --</option>
                                        <?php foreach ($peraturan as $key): ?>
                                        <option value="<?=$key->id?>"><?=$key->jn_peraturan?></option>
                                        <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No. Urut
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" placeholder="No. Urut" name="id" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No. Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required placeholder="No. Peraturan" name="no_peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="date" class="form-control" required name="tgl_peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tgl. Berlaku
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="date" class="form-control" required name="tgl_berlaku">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <textarea name="uraian_peraturan" class="form-control" placeholder="Uraian Peraturan"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Peraturan berlaku untuk:
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_all">All
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_pajak">Pajak
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" id="pil_retribusi">Retribusi
                                </label>
                                <span class="help-block"></span>
                                <br>

                                <!-- tampilan setelah memilih radio button -->
                                <div id="jn_all" style="display: none"></div>
                                <div id="jn_pajak" style="display: none">
                                    <select name="jn_pajak" class="form-control">
                                        <option value="0">-- Jenis Pajak --</option>
                                        <?php foreach ($pajak as $key => $jn_pajak): ?>
                                        <option value="<?=$jn_pajak->id?>"><?=$jn_pajak->nm_pajak?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div id="jn_retribusi" style="display: none">
                                    <select name="jn_retribusi" class="form-control">
                                        <option value="0">-- Jenis Retribusi --</option>
                                        <?php foreach ($retribusi as $key => $jn_retribusi): ?>
                                        <option value="<?=$jn_retribusi->kd_jenis?>"><?=$jn_retribusi->nm_retribusi?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>
<script type="text/javascript">
   $(document).ready(function(){
        $("#pil_all").click(function(){
            $("#jn_all:hidden").show('slow');
            $("#jn_pajak").hide();
            $("#jn_retribusi").hide();
        });
        $("#pil_all").click(function(){
            if ($('#pil_all').prop('checked')===false) {
                $('jn_all').hide();
            }
        });

        $("#pil_pajak").click(function(){
            $("#jn_pajak:hidden").show('slow');
            $("#jn_all").hide();
            $("#jn_retribusi").hide();
        });
        $("#pil_pajak").click(function(){
            if ($('#pil_pajak').prop('checked')===false) {
                $('jn_pajak').hide();
            }
        });

        $("#pil_retribusi").click(function(){
            $("#jn_retribusi:hidden").show('slow');
            $("#jn_pajak").hide();
            $("#jn_all").hide();
        });
        $("#pil_retribusi").click(function(){
            if ($('#pil_retribusi').prop('checked')===false) {
                $('jn_retribusi').hide();
            }
        });

   });
</script>
