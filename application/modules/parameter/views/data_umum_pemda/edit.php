<div class="row">
    <div class="col-md-6">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Gambar Pemda</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_edit" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <center>
                            <img src="<?=base_url('./assets/img/'.$records->logo)?>" alt="Data Umum Pemda Kota Pasuruan" class="img-rounded" width="150px">
                        </center>
                    </div><br>
                    <div class="form-actions">
                        <div class="row">
                            <div class="profile-userbuttons">
                                <center>
                                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening" id="add_rekening">Upload
                                    </button>
                                </center>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
    <div class="col-md-6">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject sbold uppercase">Data Umum</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                    <div class="form-body">
                        <fieldset>
                            <!-- <legend>Data Umum</legend> -->
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Tahun
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Tahun" name="tahun" value="<?=$records->tahun?>" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Pemda
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nama pemda" value="<?=$records->nm_pemda?>" name="nm_pemda" readonly>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Ibukota
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Ibukota" value="<?=$records->ibukota?>" name="ibukota" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Alamat
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <textarea class="form-control" placeholder="Alamat" name="alamat"  rows="5"><?=$records->alamat?></textarea>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                    </div>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-eye"></i>
                    <span class="caption-subject sbold uppercase">Show <?php echo $subtitle ?></span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                    <div class="form-body">
                        <fieldset>
                            <legend>Kepala Daerah</legend>
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Nama
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nama" value="<?=$records->nm_pimpdaerah?>" name="nm_pimpdaerah" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Jabatan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Jabatan" value="<?=$records->jab_pimpdaerah?>" name="jab_pimpdaerah" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Sekretaris Daerah</legend>
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Nama
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nama" value="<?=$records->nm_sekda?>" name="nm_sekda" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">NIP
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="NIP" value="<?=$records->nip_sekda?>" name="nip_sekda" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Jabatan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Jabatan" value="<?=$records->jbt_sekda?>" name="jbt_sekda" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Kepala Dinas / Kantor</legend>
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Nama
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Nama" value="<?=$records->nm_kepala_kantor?>" name="nm_kepala_kantor" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">NIP
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="NIP" value="<?=$records->nip_kepala_kantor?>" name="nip_kepala_kantor" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Jabatan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" placeholder="Jabatan" value="<?=$records->jbt_kepala_kantor?>" name="jbt_kepala_kantor" >
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Pengelola Pajak</legend>
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">SKPD
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Pengelola Retribusi</legend>
                            <div class="alert alert-warning display-hide">
                                <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                                <span> </span>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">SKPD
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">

                                    <div class="row">
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                        <div class="col-md-3">
                                            <input type="text" class="form-control" placeholder="SKPD" required name="skpd" >
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                    </div><br>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="submit btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

    <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="rekening" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                </div>
                <form id="myForm" class="form-horizontal" action="<?= $url."/proses_update/".@$records->logo?>" method="post" enctype="multipart/form-data" role="form">
                  <div class="modal-body">
                    <center>
                      <h3><i class="fa fa-info"></i> Edit Logo : <?=$records->logo?></h3>
                    </center><br>
                    <div class="form-group">
                        <div class="col-lg-12">
                          <center>
                          <img class="rounded-circle" alt="Gambar" width="40%" id="gambar-preview" src="<?=base_url('./assets/img/'.$records->logo)?>" name="gambar-preview">
                          <br>
                          <br>
                                <label>
                                    <input class="btn btn-primary" type="file" name="logo" id="image-source" accept = "image/*" onchange="previewImage()">
                                </label>
                        </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                      <button class="btn btn-success" type="submit"></i> Edit</i></button>
                      <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</i></button>
                  </div>
                </form>
                </div>
            </div>
        </div>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var title   = 'Confirmation';
        var text    = 'Are you sure to continue this action?';
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message, title, text );
    });
</script>

<script>
    function previewImage() {
        document.getElementById("gambar-preview").style.display = "block";
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("image-source").files[0]);
            oFReader.onload = function(oFREvent) {
            document.getElementById("gambar-preview").src = oFREvent.target.result;
          }
        }
</script>
