<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add_perhitungan_penyisihan" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>

                        <fieldset>
                            <legend>Penyisihan</legend>

                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Obyek Pajak
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" id="kd_rek_1" class="form-control" required placeholder="Rek 1" name="kd_rek_1" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" id="kd_rek_2" class="form-control" required placeholder="Rek 2" name="kd_rek_2" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" id="kd_rek_3" class="form-control" required placeholder="Rek 3" name="kd_rek_3" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">

                                         <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show"><i class="fa fa-refresh "></i></a>
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Kategori
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-2">
                                    <select class="form-control" name="kategori">
                                        <?php foreach ($kategori as $key => $value): ?>
                                            <option value="<?=$value->kd_kategori ?>" >
                                                <?=$value->uraian ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <div class="input-group">
                                        <input type="text" class="form-control" required placeholder=" Tarif " name="tarif" >
                                        <div class="input-group-addon">
                                            <!-- <span class="fa fa-percent"></span> -->
                                            %
                                        </div>
                                    </div>
                                </div>
                                <!-- <label class="col-md-1 control-label" for="form_control_1" style="text-align: left">
                                    %
                                </label> -->
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Umur</legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Umur Awal
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Tahun" name="tahun_aw" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Bulan" name="bulan_aw" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Hari" name="hari_aw" >
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Umur Akhir
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Tahun" name="tahun_ak" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Bulan" name="bulan_ak" >
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" required placeholder="Hari" name="hari_ak" >
                                            <span class="help-block"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL -->
<div id="large" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <div class="action pull-right">
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> Kode </th>
                                    <th> Uraian </th>
                                    <th> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_no_daftar" placeholder="Nomer Pendaftaran">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#modal_show').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_kode_penyisihan';
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [0, "asc"]
            ];

            var sort = [-1, 0];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });
    });

    function pilih(id1, id2, id3){

        $.get({
            url:        '<?=$url?>/get_kode_penyisihan/',
            type:       'POST',
            dataType:   'JSON',
            data:       {kd_rek_1: id1, kd_rek_2: id2, kd_rek_3: id3},
            success: function(res){
                console.log(res.records);
                // $('#npwr_gab').val('R . '+res.records.jn_wr+' . '+res.records.no_daftar+' . '+res.records.kd_kec+' . '+res.records.kd_kel);
                $('#kd_rek_1').val(res.records.kd_rek_1);
                $('#kd_rek_2').val(res.records.kd_rek_2);
                $('#kd_rek_3').val(res.records.kd_rek_3);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
