<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_edit_sub/<?=$records->id_organisasi?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Urusan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_urusan" value="<?=$records->kd_urusan?>" readonly="readonly">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Bidang
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_bidang" value="<?=$records->kd_bidang?>" readonly="readonly">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Unit
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" required name="kd_unit" value="<?=$records->kd_unit?>" readonly="readonly">
                                <span class="help-block"></span>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Sub Unit
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-2">
                                <input type="text" class="form-control" placeholder="Kode Unit" required name="kd_sub" value="<?=$records->kd_sub?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Sub Unit
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" required placeholder="Uraian Nama Unit" name="nm_sub_unit" value="<?=$records->nm_sub_unit?>">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url.'/show_sub_unit/'.$records->kd_urusan.'/'.$records->kd_bidang.'/'.$records->kd_unit?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url.'/show_sub_unit/'.$records->kd_urusan.'/'.$records->kd_bidang.'/'.$records->kd_unit?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>
