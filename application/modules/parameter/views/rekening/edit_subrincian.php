<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit Rincisn Obyek Rekening</span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= $url."/action_edit_subrincian/".$records->id_rek_6?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Akun
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_1?>" required name="kd_rek_1" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Kelompok
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_2?>" required name="kd_rek_2" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Jenis
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_3?>" required name="kd_rek_3" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Obyek
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_4?>" required name="kd_rek_4" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Rincian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_5?>" required name="kd_rek_5" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_6?>" required name="kd_rek_6">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required value="<?=$records->nm_rek_6?>" name="nm_rek_6">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <select id="no_peraturan" class="form-control" required name="no_peraturan">
                                    <option disabled selected>Pilih No Peraturan</option>
                                <?php foreach ($peraturan as $key):?>
                                        <option <?php if ($records->no_peraturan == $key->no_peraturan) {
                                            echo "selected='selected'";
                                        } ?> data-tgl='<?=$key->tgl_peraturan?>' data-uraian='<?=$key->uraian_peraturan?>' value="<?php echo $key->no_peraturan; ?>"><?php echo $key->no_peraturan; ?></option>

                                <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tanggal Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" value="<?=$records->tgl_peraturan?>" name="tgl_peraturan" id="tgl_peraturan">
                                    <div class="input-group-addon">
                                        <span class="fa fa-calendar"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required value="<?=$records->nm_pendapatan?>" name="nm_pendapatan" id="nm_peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Tarif Pendapatan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required value="<?=$records->tarif?>" name="tarif">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url.'/show_rek_6/'.$records->kd_rek_1.'/'.$records->kd_rek_2.'/'.$records->kd_rek_3.'/'.$records->kd_rek_4.'/'.$records->kd_rek_5?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url.'/show_rek_6/'.$records->kd_rek_1.'/'.$records->kd_rek_2.'/'.$records->kd_rek_3.'/'.$records->kd_rek_4.'/'.$records->kd_rek_5?>" class="ajaxify reload"></a>

<script type="text/javascript">
$(document).ready(function() {
    $('#no_peraturan').on('change', function(){
        var id_peraturan1 = $(this).find(":selected").data("tgl");
        var id_peraturan2 = $(this).find(":selected").data("uraian");
        // console.log(id_peraturan1);
        // console.log(id_peraturan2);
         $('#tgl_peraturan').val(id_peraturan1);
         $('#nm_peraturan').val(id_peraturan2);
    });
});
</script>
<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>
