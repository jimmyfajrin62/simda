<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit Jenis Obyek</span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= $url."/action_edit_rincian/".$records->id_rek_5?>" class="form-horizontal form-add" method="POST" data-confirm="1" role="form">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Akun
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_1?>" required name="kd_rek_1" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Kelompok
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_2?>" required name="kd_rek_2" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Jenis
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$records->kd_rek_3?>" required name="kd_rek_3" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Obyek
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?= $records->kd_rek_4 ?>" required name="kd_rek_4" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php echo $records->kd_rek_5 ?>" required name="kd_rek_5">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required"></span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nm_rek_5" value="<?php echo $records->nm_rek_5 ?>">
                                <span class="help-block">Keep empty if no changes</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Peraturan
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?php echo $records->peraturan ?>" required name="peraturan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="submit btn blue">Submit</button>
                                <a href="<?php echo $url.'/show_rek_5/'.$records->kd_rek_1.'/'.$records->kd_rek_2.'/'.$records->kd_rek_3.'/'.$records->kd_rek_4?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->
<a href="<?php echo $url.'/show_rek_5/'.$records->kd_rek_1.'/'.$records->kd_rek_2.'/'.$records->kd_rek_3.'/'.$records->kd_rek_4?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule    = {};
        var message = {};
        var title   = 'Confirmation';
        var text    = 'Are you sure to continue this action?';
        var form    = '.form-add';
        FormValidation.handleValidation( form, rule, message, title, text );
    });
</script>
