<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add Obyek Rekening</span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add_obyek" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Akun
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$ref_rek_1?>" required name="kd_rek_1" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Kelompok
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$ref_rek_2?>" required name="kd_rek_2" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Jenis
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" value="<?=$ref_rek_3?>" required name="kd_rek_3" readonly>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Kode" required name="kd_rek_4">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Uraian
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required placeholder="Uraian" name="nm_rek_4">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url?>/show_rek_4/<?=$ref_rek_1?>/<?=$ref_rek_2?>/<?=$ref_rek_3?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<a href="<?php echo $url?>/show_rek_4/<?=$ref_rek_1?>/<?=$ref_rek_2?>/<?=$ref_rek_3?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );
    });
</script>
