<!-- BEGIN PAGE BASE CONTENT -->
<div class="row">
    <div class="col-md-12">
        <!-- Begin: life time stats -->
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    <span class="caption-subject font-blue sbold uppercase"><?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                    
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <div class="btn-group">
                        <a class="btn btn-icon-only red fullscreen" href="javascript:;"> </a>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <center>
                <a href="<?php echo $url ?>/#" class="btn blue tooltips ajaxify" data-original-title="Unit Organisasi" >
                        <span>Pajak Reklame</span>
                </a>
                    <br>
                    <br>    
                <a href="<?php echo $url ?>" class="btn blue tooltips ajaxify" style="pointer-events: none; cursor: unavaliable;" data-original-title="Unit Organisasi" >
                        <span>Pajak Penerangan Jalan</span>
                </a>
                </center>
                <div class="table-container">

                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->