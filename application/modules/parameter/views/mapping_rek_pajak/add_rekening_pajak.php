<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?= @$url ?>/action_add_rekening_pajak" class="form-horizontal form-add" role="form" method="POST">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>

						<fieldset>
							<legend>Rekening Pajak</legend>

							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Rekening
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_1" id="kd_rek_1">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_2" id="kd_rek_2">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_3" id="kd_rek_3">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_4" id="kd_rek_4">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">

             							<button type="button" class="btn btn-primary" data-toggle="modal" href="#rekening" id="add_rekening"><i class="fa fa-refresh"> </i></button>
             							 <!-- <a class="btn blue btn-outline sbold" data-toggle="modal" href="#large" id="modal_show">
                                            <i class="fa fa-refresh"></i> -->

										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Uraian
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="nm_pajak" id="nm_pajak">
											<span class="help-block"></span>

								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Rekening Sanksi</legend>
							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Rekening Denda 
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_1_denda" id="kd_rek_1_denda">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_2_denda" id="kd_rek_2_denda">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_3_denda" id="kd_rek_3_denda">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_4_denda" id="kd_rek_4_denda">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_5_denda" id="kd_rek_5_denda">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">

             							 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening_denda"><i class="fa fa-refresh"> </i></button>

											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Rekening Bunga
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_1_bunga"  id="kd_rek_1_bunga">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_2_bunga"  id="kd_rek_2_bunga">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_3_bunga"  id="kd_rek_3_bunga">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_4_bunga"  id="kd_rek_4_bunga">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_5_bunga"  id="kd_rek_5_bunga">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">

             							 <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening_bunga"><i class="fa fa-refresh"> </i></button>

											<span class="help-block"></span>
										</div>
									</div>

								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Rekening Kenaikan
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_1_kenaikan" id="kd_rek_1_kenaikan" >
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_2_kenaikan"  id="kd_rek_2_kenaikan">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_3_kenaikan" id="kd_rek_3_kenaikan" >
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_4_kenaikan" id="kd_rek_4_kenaikan" >
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_5_kenaikan" id="kd_rek_5_kenaikan" >
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">

             							<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening_kenaikan"><i class="fa fa-refresh"> </i></button>

											<span class="help-block"></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						</fieldset>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-8">
								<button type="submit" class="btn blue">Submit</button>
								<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 500px;"><div class="scroller" style="height: 500px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <div class="action pull-right">
                            <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> Rekening </th>
                                    <th> Uraian </th>
                                    <th> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                	<td></td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_no_daftar" placeholder="Nomer Pendaftaran">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_rek_pajak';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });

    function pilih(id){

        $.get({
            url:        '<?=$url?>/get_rekening_pajak/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {kd_jenis: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1').val(res.records.kd_rek_1);
                $('#kd_rek_2').val(res.records.kd_rek_2);
                $('#kd_rek_3').val(res.records.kd_rek_3);
                $('#kd_rek_4').val(res.records.kd_rek_4);
                $('#kd_rek_5').val(res.records.kd_rek_5);
                $('#nm_pajak').val(res.records.nm_pajak);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
