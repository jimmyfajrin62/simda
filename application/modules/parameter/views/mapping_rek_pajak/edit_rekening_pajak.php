<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit <?php echo $pagetitle ?></span>
				</div>
				<div class="actions">
				</div>
			</div>
			<div class="portlet-body">
				<!-- BEGIN FORM-->
				<form action="<?=$url.'/action_edit_rekening_pajak/'.$id?>" class="form-horizontal form-add" role="form" method="POST">
					<div class="form-body">
						<div class="alert alert-warning display-hide">
							<button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
							<span> </span>
						</div>

						<fieldset>
							<legend>Rekening Pajak</legend>

						<div class="form-group">
							<label class="col-md-3 control-label" for="form_control_1">Kode Jenis
								<span class="required">*</span>
							</label>
							<div class="col-md-8">
								<div class="row">
									<div class="col-sm-2">
										<input type="text" class="form-control" required placeholder="" name="kd_rek_1" value="<?=$records->id?>" id="kd_rek_1" disabled>
										<span class="help-block"></span>
									</div>
								</div>
							</div>
						</div>

							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Rekening
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<div class="row">
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_1" value="<?=$records->kd_rek_1?>" id="kd_rek_1">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_2" value="<?=$records->kd_rek_2?>" id="kd_rek_2">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_3" value="<?=$records->kd_rek_3?>" id="kd_rek_3">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">
											<input type="text" class="form-control" required placeholder="" name="kd_rek_4" value="<?=$records->kd_rek_4?>" id="kd_rek_4">
											<span class="help-block"></span>
										</div>
										<div class="col-sm-2">

             							 <button type="button" class="btn btn-primary" data-toggle="modal" href="#rekening" id="add_rekening"><i class="fa fa-refresh"> </i></button>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-md-3 control-label" for="form_control_1">Uraian
									<span class="required">*</span>
								</label>
								<div class="col-md-8">
									<input type="text" class="form-control" name="nm_pajak" value="<?= $records->nm_pajak?>" id="nm_pajak">
											<span class="help-block"></span>

								</div>
							</div>
						</fieldset>
						<fieldset>
							<legend>Rekening Sanksi</legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Denda
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_1_denda" id="kd_rek_1_denda" value="<?=$records->kd_rek_1_denda?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_2_denda" id="kd_rek_2_denda" value="<?=$records->kd_rek_2_denda?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_3_denda" id="kd_rek_3_denda" value="<?=$records->kd_rek_3_denda?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_4_denda" id="kd_rek_4_denda" value="<?=$records->kd_rek_4_denda?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_5_denda" id="kd_rek_5_denda" value="<?=$records->kd_rek_5_denda?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_6_denda" id="kd_rek_6_denda" value="<?=$records->kd_rek_6_denda?>">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                   <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#denda" id="add_denda"><i class="fa fa-refresh"> </i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Bunga
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_1_bunga" id="kd_rek_1_bunga" value="<?=$records->kd_rek_1_bunga?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_2_bunga" id="kd_rek_2_bunga" value="<?=$records->kd_rek_2_bunga?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_3_bunga" id="kd_rek_3_bunga" value="<?=$records->kd_rek_3_bunga?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_4_bunga" id="kd_rek_4_bunga" value="<?=$records->kd_rek_4_bunga?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_5_bunga" id="kd_rek_5_bunga" value="<?=$records->kd_rek_5_bunga?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_6_bunga" id="kd_rek_6_bunga" value="<?=$records->kd_rek_6_bunga?>">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#bunga" id="add_bunga"><i class="fa fa-refresh"> </i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Kenaikan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_1_kenaikan" id="kd_rek_1_kenaikan" value="<?=$records->kd_rek_1_kenaikan?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_2_kenaikan" id="kd_rek_2_kenaikan" value="<?=$records->kd_rek_2_kenaikan?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_3_kenaikan" id="kd_rek_3_kenaikan" value="<?=$records->kd_rek_3_kenaikan?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_4_kenaikan" id="kd_rek_4_kenaikan" value="<?=$records->kd_rek_4_kenaikan?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_5_kenaikan" id="kd_rek_5_kenaikan" value="<?=$records->kd_rek_5_kenaikan?>">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_6_kenaikan" id="kd_rek_6_kenaikan" value="<?=$records->kd_rek_6_kenaikan?>">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#kenaikan" id="add_kenaikan"><i class="fa fa-refresh"> </i></button>
                                </div>
						</div>
						</fieldset>
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-8">
								<button type="submit" class="btn blue">Submit</button>
								<a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END VALIDATION STATES-->
	</div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- MODAL POPUP UNTUK REKENING DENDA -->
<div class="modal fade bs-modal-lg in" id="denda" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_denda">
                    <thead>
                        <tr role="row" class="heading">
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                            <!-- <td></td> -->
                        </tr>
                        <tr role="row" class="filter">
                            <!-- <td></td> -->
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK REKENING BUNGA -->
<div class="modal fade bs-modal-lg in" id="bunga" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_bunga">
                    <thead>
                        <tr role="row" class="heading">
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                            <!-- <td></td> -->
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK REKENING KENAIKAN -->
<div class="modal fade bs-modal-lg in" id="kenaikan" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_kenaikan">
                    <thead>
                        <tr role="row" class="heading">
                            <!-- <td></td> -->
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 500px;"><div class="scroller" style="height: 500px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <div class="action pull-right">
                            <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> Rekening </th>
                                    <th> Uraian </th>
                                    <th> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                	<td></td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_no_daftar" placeholder="Nomer Pendaftaran">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_rek_pajak';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add denda
        $('#add_denda').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_denda').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_denda';
           // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_denda');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add bunga
        $('#add_bunga').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_bunga').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_bunga';
            // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_bunga');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add kenaikan
        $('#add_kenaikan').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_kenaikan').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_kenaikan';
            // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_kenaikan');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });

    function pilih(id){

        $.get({
            url:        '<?=$url?>/get_rekening_pajak/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1').val(res.records.kd_rek_1);
                $('#kd_rek_2').val(res.records.kd_rek_2);
                $('#kd_rek_3').val(res.records.kd_rek_3);
                $('#kd_rek_4').val(res.records.kd_rek_4);
                $('#kd_rek_5').val(res.records.kd_rek_5);
                $('#nm_pajak').val(res.records.nm_pajak);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function pilih_denda(id){
        $.get({
            url:        '<?=$url?>/get_denda/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_denda').val(res.records.kd_rek_1);
                $('#kd_rek_2_denda').val(res.records.kd_rek_2);
                $('#kd_rek_3_denda').val(res.records.kd_rek_3);
                $('#kd_rek_4_denda').val(res.records.kd_rek_4);
                $('#kd_rek_5_denda').val(res.records.kd_rek_5);
                $('#kd_rek_6_denda').val(res.records.kd_rek_6);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function pilih_bunga(id){
        $.get({
            url:        '<?=$url?>/get_bunga/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_bunga').val(res.records.kd_rek_1);
                $('#kd_rek_2_bunga').val(res.records.kd_rek_2);
                $('#kd_rek_3_bunga').val(res.records.kd_rek_3);
                $('#kd_rek_4_bunga').val(res.records.kd_rek_4);
                $('#kd_rek_5_bunga').val(res.records.kd_rek_5);
                $('#kd_rek_6_bunga').val(res.records.kd_rek_6);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    function pilih_kenaikan(id){
        $.get({
            url:        '<?=$url?>/get_kenaikan/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_kenaikan').val(res.records.kd_rek_1);
                $('#kd_rek_2_kenaikan').val(res.records.kd_rek_2);
                $('#kd_rek_3_kenaikan').val(res.records.kd_rek_3);
                $('#kd_rek_4_kenaikan').val(res.records.kd_rek_4);
                $('#kd_rek_5_kenaikan').val(res.records.kd_rek_5);
                $('#kd_rek_6_kenaikan').val(res.records.kd_rek_6);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

</script>
