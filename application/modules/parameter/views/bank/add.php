<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light note note-info">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kode Bank
                                <!-- <span class="required">*</span> -->
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Kode Bank" name="kode">
                                <!-- <span class="help-block"></span> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama Bank
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" placeholder="Nama Bank" name="nm_bank" required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No Rekening
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" required placeholder="No Rekening" name="no_rekening">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                        <fieldset>
                            <legend></legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Kode Rekening
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_1" id="kd_rek_1" required>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_2" id="kd_rek_2" required>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_3" id="kd_rek_3" required>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_4" id="kd_rek_4" required>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_5" id="kd_rek_5" required>
                                            <span class="help-block"></span>
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#rekening" id="add_rekening"><i class="fa fa-refresh"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->

<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <div class="action pull-right">
                            <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                                <i class="fa fa-refresh"></i>
                            </a>
                            <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                                <i class="fa fa-search"></i>
                            </a>
                        </div>
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> No </th>
                                    <th> Rekening </th>
                                    <th> Uraian </th>
                                    <th> Action </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td></td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_no_rek" placeholder="Nomer Rekening">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="filter_uraian" placeholder="Uraian">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<!-- START MODAL -->
<div id="large" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                            <thead>
                                <tr role="row" class="heading">
                                    <th width="200px"> No.Registrasi </th>
                                    <th> Nama Wajib Pajak </th>
                                    <th> Tgl Terdaftar </th>
                                    <th width="100px"> Action </th>
                                </tr>
                                <tr role="row">
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="no_daftar" placeholder="No.Registrasi">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="nama_pendaftar" placeholder="Nama Pendaftar">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="tgl_terdaftar" placeholder="Tgl Terdaftar">
                                    </td>
                                    <td class="text-center">
                                        <div class="clearfix">
                                            <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_rek_pajak';
           console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


    });

    function pilih(id){

        $.get({
            url:        '<?=$url?>/get_rekening_pajak/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id_rek_6: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1').val(res.records.kd_rek_1);
                $('#kd_rek_2').val(res.records.kd_rek_2);
                $('#kd_rek_3').val(res.records.kd_rek_3);
                $('#kd_rek_4').val(res.records.kd_rek_4);
                $('#kd_rek_5').val(res.records.kd_rek_5);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }
</script>
