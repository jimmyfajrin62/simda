<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $add  ?>" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

<!--                         <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                    <div class="table-actions-wrapper">
                        <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
                            <option>Select...</option>
                            <!-- id / field-table / table -->
                            <option value="99/id/ta_sub_unit">Delete</option>
                            <option value="0/id/ta_sub_unit">InActive</option>
                            <option value="1/id/ta_sub_unit">Active</option>
                        </select>
                        <button class="btn btn-sm btn-icon-only blue table-group-action-submit" onClick="">
                            <i class="fa fa-check"></i>
                        </button>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr>
                                <th colspan="8">
                                     <h2 align="center"><?php echo $head?></h2>
                                </th>
                            </tr>
                            <tr role="row" class="heading">
                                <th width="30px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="10px"> No </th>
                                <th width="20px"> Nama </th>
                                <th width="20px"> Alamat </th>
                                <th width="40px"> Nip </th>
                                <th width="30px"> Jabatan </th>
                                <th width="30px"> Visi </th>
                                <th width="40px"> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nm_pimpinan" placeholder="Nama"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="alamat" placeholder="Alamat"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="nip_pimpinan" placeholder="NIP"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="jbt_pimpinan" placeholder="Jabatan"> </td>
                                <td><input type="text" class="form-control form-filter input-sm" name="visi" placeholder="Visi"> </td>
                                 <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i></button>
                                        <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i></button>
                                        <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>

                </div>
            </div><!-- End: life time stats -->
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

        // table data
        var select_url = '<?php echo $url ?>/select_datum/<?=$kd_urusan?>/<?=$kd_bidang?>/<?=$kd_unit?>/<?=$kd_sub?>/<?=$id_organisasi?>';

        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            // { "sClass": "text-center" },
            // { "sClass": "text-center" },
            { "sClass": "text-center" }
        ];
        var order = [
            [2, "asc"]
        ];

        var sort = [-1,0,1];

        TableDatatablesAjax.handleRecords( select_url, header, order, sort);
        // bs select setelah datatable, bug
        Helper.bsSelect();

    });
</script>
