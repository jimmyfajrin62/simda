<div class="row">
    <div class="col-md-12">

          <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet box blue">
            <div class="portlet-title">

                <div class="caption">
                    <i class="fa fa-table font-white"></i>Table <?php echo $pagetitle ?>
                </div>

                <div class="tools">
                    <div class="actions">
                        <div class="btn-group btn-group-devided" data-toggle="buttons">
                            <div class="clearfix">
                                <a data-original-title="Add" href="<?php echo $url ?>/show_add" class="ajaxify btn btn-transparent default btn-icon-only btn-sm tooltips">
                                    <i class="fa fa-plus"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <a data-original-title="Reload" href="<?php echo $url ?>" class="tooltips ajaxify btn default btn-transparent btn-icon-only btn-sm">
                                    <i class="fa fa-refresh"></i>
                                </a>
                                <span class='help-block' style='display: inline;'></span>
                                <span data-original-title="Search" class="tooltips btn btn-transparent default btn-icon-only btn-sm " id="find">
                                    <i class="fa fa-search"></i>
                                </span>
                                <span class='help-block' style='display: inline;'></span>
                            </div>
                        </div>

<!--                         <div class="btn-group">
                            <a class="btn default" href="javascript:;" data-toggle="dropdown">
                                <i class="fa fa-cogs"></i>
                                <span class="hidden-xs"></span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu pull-right">
                                <li>
                                    <a href="javascript:window.location.assign(base_url + '/user/export_data')"> Export Excel </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="javascript:;"> Print PDF </a>
                                </li>
                            </ul>
                        </div> -->
                    </div>
                </div>
            </div>

            <div class="portlet-body">
                <div class="table-container">

                   <div class="table-actions-wrapper">
                    <select class="bs-select table-group-action-input form-control input-small" data-style="blue">
                        <option>Select...</option>
                        <option value="99/kd_kategori/ref_kategori">Delete</option>
                        <option value="0/kd_kategori/ref_kategori">InActive</option>
                        <option value="1/kd_kategori/ref_kategori">Active</option>
                    </select>
                    <button class="btn btn-sm btn-icon-only blue table-group-action-submit" onClick="">
                        <i class="fa fa-check"></i>
                    </button>
                    </div>

                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <th width="30px">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_2 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th width="20px"> No. </th>
                                <th width="50px"> Kode </th>
                                <th> Kategori Penyisihan </th>
                                <th width="150px"> Last Update </th>
                                <th width="140px"> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td> </td>
                                <td> </td>
                                <td> <input type="text" class="form-control form-filter input-sm" name="kode" placeholder="Kode"> </td>
                                <td> <input type="text" class="form-control form-filter input-sm" name="kategori" placeholder="Kategori Penyisihan"> </td>
                                <td>
                                    <div class="input-group margin-bottom-5" id="defaultrange">
                                        <input type="text" name="lastupdate_show" class="form-control form-filter">
                                        <input type="hidden" name="lastupdate" class="form-filter">
                                        <span class="input-group-btn">
                                            <button class="btn default date-range-toggle" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                </td>
                                <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green-seagreen btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button>
                                    <button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                    <button data-original-title="Show InActive Only" data-status="0" class="tooltips btn btn-icon-only btn-sm blue-madison filter-status"><i class="fa fa-tasks"></i></button>
                                </div>
                            </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- End: life time stats -->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
</div>
<!-- END CONTENT BODY -->

<!-- START MODAL ADD -->
<div id="modal_form" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="display: none; padding-right: 15px;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Form Kategori Penyisihan</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 300px;"><div class="scroller" style="height: 300px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                        <form id="form-modal" action="" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Kategori
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required placeholder="Kategori penyisihan" name="kategori" id="uraian">
                                <span class="help-block"></span>
                            </div>
                        </div>

                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue ajaxadd">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
</div>
<!-- END MODAL ADD -->


<a href="<?php echo $url ?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {

        var select_url = '<?php echo $url ?>' + '/select';

        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-left" },
            { "sClass": "text-center" }
        ];
        var order = [
            [2, "asc"]
        ];

        var sort = [-1, 0, 1];

        TableDatatablesAjax.handleRecords( select_url, header, order, sort );
        // bs select setelah datatable, bug
        Helper.bsSelect();

        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

    });

    function get_kategori(id){
        $.ajax({
            url: '<?=$url?>/get_kategori/',
            type: 'POST',
            dataType: 'JSON',
            data: {id: id},
            success: function(res){
                console.log(res.records.uraian);
                $('#uraian').val(res.records.uraian);
            },
            error: function(jqXHR, textStatus, errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        });

    }

    function action_db(action){
        if (action == null) {
            $('#uraian').attr('placeholder','Kategori Penyisihan');
            $('#uraian').val('');
            $('#form-modal').attr('action', '<?=$url?>/action_add');
        }else{
            $('#form-modal').attr('action', '<?=$url?>/action_edit/'+action);
            get_kategori(action);
        }
    }
</script>
