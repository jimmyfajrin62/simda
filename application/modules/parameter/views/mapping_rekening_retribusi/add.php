<div class="row">
    <div class="col-md-12">
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-plus"></i>
                    <span class="caption-subject sbold uppercase">Add <?php echo $pagetitle ?></span>
                </div>
                <div class="actions">
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_add" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <fieldset>
                            <legend>Rekening Retribusi</legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Uraian
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_1" required id="kd_rek_1">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_2" required id="kd_rek_2">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_3" required id="kd_rek_3">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_4" required id="kd_rek_4">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_rek_5" required id="kd_rek_5">
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" id="add_rekening"  href="#rekening"><i class="fa fa-refresh"></i>
                                            </button>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="nm_retribusi" placeholder="Nama Rekening" id="nm_retribusi" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Unit Organisasi
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_urusan" required id="kd_urusan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_bidang" required id="kd_bidang">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_unit" required id="kd_unit">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" class="form-control" name="kd_sub" required id="kd_sub">
                                        </div>
                                        <div class="col-md-2">
                                            <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#unit" id="add_unit"><i class="fa fa-refresh"> </i></button>
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Rekening Sanksi</legend>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Denda
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_1_denda" id="kd_rek_1_denda">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_2_denda" id="kd_rek_2_denda">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_3_denda" id="kd_rek_3_denda">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_4_denda" id="kd_rek_4_denda">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_5_denda" id="kd_rek_5_denda">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_6_denda" id="kd_rek_6_denda">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                   <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#denda" id="add_denda"><i class="fa fa-refresh"> </i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Bunga
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_1_bunga" id="kd_rek_1_bunga">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_2_bunga" id="kd_rek_2_bunga">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_3_bunga" id="kd_rek_3_bunga">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_4_bunga" id="kd_rek_4_bunga">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_5_bunga" id="kd_rek_5_bunga">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_6_bunga" id="kd_rek_6_bunga">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#bunga" id="add_bunga"><i class="fa fa-refresh"> </i></button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Rekening Kenaikan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-7">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_1_kenaikan" id="kd_rek_1_kenaikan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_2_kenaikan" id="kd_rek_2_kenaikan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_3_kenaikan" id="kd_rek_3_kenaikan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_4_kenaikan" id="kd_rek_4_kenaikan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_5_kenaikan" id="kd_rek_5_kenaikan">
                                        </div>
                                        <div class="col-md-2">
                                            <input type="text" class="form-control" name="kd_rek_6_kenaikan" id="kd_rek_6_kenaikan">
                                        </div>
                                    </div>
                                    <span class="help-block"></span>
                                </div>
                                <div class="col-md-2">
                                   <button type="button" class="btn btn-primary" data-toggle="modal" href="" data-target="#kenaikan" id="add_kenaikan"><i class="fa fa-refresh"> </i></button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url?>" class="btn grey ajaxify">Back</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>
<!-- END PAGE BASE CONTENT -->


<!-- MODAL POPUP UNTUK URAIAN UNIT-->
<div class="modal fade bs-modal-lg in" id="unit" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body"  >
        <div class="portlet-body"  >
            <div class="table-container">
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="finds" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>

                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_unit">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="5px"> No </th>
                            <th width="5px"> Urusan  </th>
                            <th width="5px"> Bidang </th>
                            <th width="5px"> Unit </th>
                            <th width="40px"> Uraian Unit </th>
                            <th width="10px"> Lastupdate </th>
                            <th width="15px"> Action </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="urusan" placeholder="Urusan">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="bidang" placeholder="Bidang">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="unit" placeholder="Unit">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="uraian_unit" placeholder="Uraian Unit">
                            </td>
                            <td></td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>

                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK URAIAN SUBUNIT-->
<div class="modal fade bs-modal-lg in" id="subunit" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body"  >
        <div class="portlet-body"  >
            <div class="table-container">
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="finds" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_unit1">
                    <thead>
                        <tr role="row" class="heading">
                            <th width="5px"> No </th>
                            <th width="5px"> Urusan  </th>
                            <th width="5px"> Bidang </th>
                            <th width="5px"> Unit </th>
                            <th width="5px"> Sub </th>
                            <th width="40px"> Uraian Unit </th>
                            <th width="10px"> Lastupdate </th>
                            <th width="15px"> Action </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="urusan" placeholder="Urusan">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="bidang" placeholder="Bidang">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="unit" placeholder="Unit">
                            </td>
                             <td>
                                <input type="text" class="form-control form-filter input-sm" name="subunit" placeholder="Sub Unit">
                            </td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="uraian_unit" placeholder="Uraian Unit">
                            </td>
                            <td></td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK REKENING DENDA -->
<div class="modal fade bs-modal-lg in" id="denda" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_denda">
                    <thead>
                        <tr role="row" class="heading">
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                            <!-- <td></td> -->
                        </tr>
                        <tr role="row" class="filter">
                            <!-- <td></td> -->
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK REKENING BUNGA -->
<div class="modal fade bs-modal-lg in" id="bunga" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_bunga">
                    <thead>
                        <tr role="row" class="heading">
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                            <!-- <td></td> -->
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- MODAL POPUP UNTUK REKENING KENAIKAN -->
<div class="modal fade bs-modal-lg in" id="kenaikan" tabindex="-1" role="dialog" aria-labelledby="uraianLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="uraianLabel">
            <center>PEMERINTAH KOTA PASURUAN</center>
        </h4>
      </div>
      <div class="modal-body">
        <div class="portlet-body">
            <div class="table-container">
                 <!-- MODAL CONTENT AREA -->
                <div class="action pull-right">
                    <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                        <i class="fa fa-refresh"></i>
                    </a>
                    <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                        <i class="fa fa-search"></i>
                    </a>
                </div>
                <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax_kenaikan">
                    <thead>
                        <tr role="row" class="heading">
                            <!-- <td></td> -->
                            <td> No </td>
                            <th> R1 </th>
                            <th> R2 </th>
                            <th> R3 </th>
                            <th> R4 </th>
                            <th> R5 </th>
                            <th> R6 </th>
                            <th> Uraian </th>
                            <th> Action </th>
                        </tr>
                        <tr role="row" class="filter">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                            </td>
                            <td class="text-center">
                                <div class="clearfix">
                                    <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                        <i class="fa fa-search"></i>
                                    </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </div>
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <!-- End Modal Content -->
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!-- START MODAL REKENING-->
<div id="rekening" class="modal fade bs-modal-lg in" tabindex="-1" aria-hidden="true" style="">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Wajib Pajak</h4>
            </div>
            <div class="modal-body">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 500px;"><div class="scroller" style="height: 500px; overflow-y: auto; width: auto;" data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                    <div class="row" style="padding: 25px">
                    <!-- MODAL CONTENT AREA -->
                    <div class="action pull-right">
                        <a href="<?php echo $url ?>" class="btn btn-icon-only blue tooltips ajaxify" data-original-title="Reload" >
                            <i class="fa fa-refresh"></i>
                        </a>
                        <a href="javascript:;" id="find" class="btn btn-icon-only blue tooltips" data-original-title="Search" >
                            <i class="fa fa-search"></i>
                        </a>
                    </div>
                    <table class="table table-striped table-bordered table-hover table-checkable" id="datatable_ajax">
                        <thead>
                            <tr role="row" class="heading">
                                <td></td>
                                <th> No </th>
                                <th width="300px"> Rekening </th>
                                <th> Uraian </th>
                                <th> Action </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td></td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="filter_no_daftar" placeholder="Nomer Pendaftaran">
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="filter_nama_pendaftar" placeholder="Nama Pendaftar">
                                </td>
                                <td class="text-center">
                                    <div class="clearfix">
                                        <button data-original-title="Search" class="tooltips btn btn-sm green btn-icon-only filter-submit margin-bottom">
                                            <i class="fa fa-search"></i>
                                        </button><button data-original-title="Reset" class="tooltips btn btn-sm btn-icon-only red filter-cancel">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                    <!-- End Modal Content -->
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" data-dismiss="modal" class="btn dark btn-outline">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- END MODAL -->


<a href="<?php echo $url?>" class="ajaxify reload"></a>


<script type="text/javascript">
    jQuery(document).ready(function() {
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );


        // add rekening
        $('#add_rekening').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_rek';
           // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort );
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add unit
        $('#add_unit').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_unit').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_unit';
            // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_unit');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add denda
        $('#add_denda').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_denda').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_denda';
           // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_denda');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add bunga
        $('#add_bunga').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_bunga').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_bunga';
            // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_bunga');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });


        // add kenaikan
        $('#add_kenaikan').on('click', function(e){
            e.preventDefault();
            $('#datatable_ajax_kenaikan').DataTable().clear().destroy();
            // table data
            var select_url = '<?php echo $url ?>/select_kenaikan';
            // console.log(select_url);
            var header = [
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" },
                { "sClass": "text-center" }
            ];
            var order = [
                [1, "asc"]
            ];

            var sort = [-1];

            TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_kenaikan');
            // bs select setelah datatable, bug
            Helper.bsSelect();
        });

    });


    function pilih(id){
        $.get({
            url:        '<?=$url?>/get_rekening/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {kd_jenis: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1').val(res.records.kd_rek_1);
                $('#kd_rek_2').val(res.records.kd_rek_2);
                $('#kd_rek_3').val(res.records.kd_rek_3);
                $('#kd_rek_4').val(res.records.kd_rek_4);
                $('#kd_rek_5').val(res.records.kd_rek_5);
                $('#nm_retribusi').val(res.records.nm_retribusi);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function pilih_denda(id){
        $.get({
            url:        '<?=$url?>/get_denda/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id_rek_6: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_denda').val(res.records.kd_rek_1);
                $('#kd_rek_2_denda').val(res.records.kd_rek_2);
                $('#kd_rek_3_denda').val(res.records.kd_rek_3);
                $('#kd_rek_4_denda').val(res.records.kd_rek_4);
                $('#kd_rek_5_denda').val(res.records.kd_rek_5);
                $('#kd_rek_6_denda').val(res.records.kd_rek_6);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function pilih_bunga(id){
        $.get({
            url:        '<?=$url?>/get_bunga/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id_rek_6: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_bunga').val(res.records.kd_rek_1);
                $('#kd_rek_2_bunga').val(res.records.kd_rek_2);
                $('#kd_rek_3_bunga').val(res.records.kd_rek_3);
                $('#kd_rek_4_bunga').val(res.records.kd_rek_4);
                $('#kd_rek_5_bunga').val(res.records.kd_rek_5);
                $('#kd_rek_6_bunga').val(res.records.kd_rek_6);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }

    function pilih_kenaikan(id){
        $.get({
            url:        '<?=$url?>/get_kenaikan/'+id,
            type:       'POST',
            dataType:   'JSON',
            data:       {id_rek_6: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_rek_1_kenaikan').val(res.records.kd_rek_1);
                $('#kd_rek_2_kenaikan').val(res.records.kd_rek_2);
                $('#kd_rek_3_kenaikan').val(res.records.kd_rek_3);
                $('#kd_rek_4_kenaikan').val(res.records.kd_rek_4);
                $('#kd_rek_5_kenaikan').val(res.records.kd_rek_5);
                $('#kd_rek_6_kenaikan').val(res.records.kd_rek_6);
            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function pilih_unit(kd_urusan,kd_bidang,kd_unit,kd_sub){

        $.get({
            url:        '<?=$url?>/get_unit/'+$kd_urusan+'/'+$kd_bidang+'/'+$kd_unit+'/'+$kd_sub,
            type:       'POST',
            dataType:   'JSON',
            data:       {kd_jenis: id},
            success: function(res){
                console.log(res.records.kd_rek_1);
                $('#kd_urusan').val(res.records.kd_urusan);
                $('#kd_bidang').val(res.records.kd_bidang);
                $('#kd_unit').val(res.records.kd_unit);
                $('#kd_sub').val(res.records.kd_sub);

            },
            errors: function(jqXHR, textStatus,errorThrown){
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
            }
        })
    }


    function show_subunit(kd_urusan,kd_bidang,kd_unit){
        console.log('tes');
        $('#datatable_ajax_unit1').DataTable().clear().destroy();
        // table data
        var select_url = '<?php echo $url ?>/select_subunit/'+kd_urusan+'/'+kd_bidang+'/'+kd_unit;
        // console.log(select_url);
        var header = [
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" },
            { "sClass": "text-center" }
        ];
        var order = [
            [1, "asc"]
        ];

        var sort = [-1];

        TableDatatablesAjax.handleRecords( select_url, header, order, sort, '#datatable_ajax_unit1');
        // bs select setelah datatable, bug
        Helper.bsSelect();
  }

  function pilih_subunit(id_organisasi){

      $.get({
          url:        '<?=$url?>/get_subunit/'+id_organisasi,
          type:       'POST',
          dataType:   'JSON',
          data:       {id_organisasi: id_organisasi},
          // console.log(res.records.id_organisasi);
          success: function(res){
              console.log(res.records.id_organisasi);
              $('#kd_urusan').val(res.records.kd_urusan);
              $('#kd_bidang').val(res.records.kd_bidang);
              $('#kd_unit').val(res.records.kd_unit);
              $('#kd_sub').val(res.records.kd_sub);
          },
          errors: function(jqXHR, textStatus,errorThrown){
              console.log(jqXHR);
              console.log(textStatus);
              console.log(errorThrown);
          }
      })
  }
</script>
