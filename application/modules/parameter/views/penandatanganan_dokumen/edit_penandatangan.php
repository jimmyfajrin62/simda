<div class="row">
    <div class="col-md-12">

        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class=" fa fa-edit"></i>
                    <span class="caption-subject sbold uppercase">Edit <?php echo $pagetitle ?></span>
                </div>
                <div class="actions"></div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="<?= @$url ?>/action_edit/<?=$no_urut?>" class="form-horizontal form-add" role="form" method="POST">
                    <div class="form-body">
                        <div class="alert alert-warning display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. <br/>
                            <span> </span>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">No. urut
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="hidden" name="tahun" value="<?=date('Y')?>">
                                <input type="text" class="form-control" placeholder="No. urut" value="<?=$records->no_urut?>" name="no_urut" disabled>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nama
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required placeholder="Nama" value="<?=$records->nm_penandatangan?>" name="nm_penandatangan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Nip
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" required placeholder="NIP" value="<?=$records->nip_penandatangan?>" name="nip_penandatangan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                                <label class="col-md-3 control-label" for="form_control_1">Jabatan
                                    <span class="required">*</span>
                                </label>
                                <div class="col-md-8">
                                <select name="jabatan_id" class="form-control" id="jabatan_id" required>
                                    <option id="select_jabatan"> -- Pilih Jabatan -- </option>
                                    <?php foreach ($jabatan as $key => $value) :?>
                                        <option value="<?=$value->kd_jab?>"> <?=$value->nm_jab?> </option>
                                    <?php endforeach ?>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="form_control_1">Jenis Dokumen
                                <span class="required">*</span>
                            </label>
                            <div class="col-md-8">
                                <div class="row">
                                    <div class="col-md-4">
                                        <input type="checkbox" name="no_dok[]" value="1"> SPTPD <br>
                                        <input type="checkbox" name="no_dok[]" value="2"> SKPD/SKRD <br>
                                        <input type="checkbox" name="no_dok[]" value="3"> STPD/TPRD <br>
                                        <input type="checkbox" name="no_dok[]" value="4"> SPTRD <br>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" name="no_dok[]" value="5"> Nota-Penetapan <br>
                                        <input type="checkbox" name="no_dok[]" value="6"> Nota-Perhitunga <br>
                                        <input type="checkbox" name="no_dok[]" value="7"> SSPD-Penerima <br>
                                    </div>
                                    <div class="col-md-4">
                                        <input type="checkbox" name="no_dok[]" value="8"> SSPD-Verifikasi <br>
                                        <input type="checkbox" name="no_dok[]" value="9"> STS <br>
                                        <input type="checkbox" name="no_dok[]" value="11"> Bukti-Penerimaan <br>
                                        <input type="checkbox" name="no_dok[]" value="12"> SSRD <br>

                                    </div>
                                </div>
                            </div>
                        </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-8">
                                <button type="submit" class="btn blue">Submit</button>
                                <a href="<?php echo $url ?>/show_edit_penandatanganan/<?=$no_urut?>" class="btn grey ajaxify">Back</a>

                            </div>
                        </div>
                    </div>
                </div>
                </div>
                    <br>
                </form>
                <!-- END FORM-->
            </div>
        </div>
        <!-- END VALIDATION STATES-->
    </div>
</div>

<a href="<?php echo $url?>" class="ajaxify reload"></a>

<script type="text/javascript">
    jQuery(document).ready(function() {
        // Fungsi Form Validasi
        var rule = {};
        var message = {};
        var form = '.form-add';
        FormValidation.handleValidation( form, rule, message );

    });

</script>
